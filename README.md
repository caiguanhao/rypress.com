### RyPress, Quality Software Tutorials
© 2012-2013 RyPress.com All Rights Reserved

This is an archive of RyPress.com on Mon April 1, 2013.

Contains Git, MathML and Objective-C tutorials.

Uses [SiteSucker](http://www.sitesucker.us/mac/mac.html) to recursively download whole website. You may want to execute the following command to fix the SVG files naming error:

    find . -name '*.svg.html' -exec bash -c 'mv "$1" "${1/%.svg.html/.svg}"' -- {} \;
