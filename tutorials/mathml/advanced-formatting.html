<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's MathML Tutorial | Advanced Formatting | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="This module shows you how to format MathML elements and
integrate them with the surrounding HTML content." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />
  <link rel="stylesheet" href="../../media/stix.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s MathML
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Advanced Formatting</h1>

<p>By now, we&rsquo;ve covered pretty much all of the MathML tags for marking
up equations. This module shows you how to format these elements and integrate
them with the surrounding HTML content. We&rsquo;ll talk about colors, font
sizes, inline expressions, whitespace editing, and phantom elements.</p>

<p>Browser support for some of these formatting options is spotty, so you are
strongly encouraged to use the MathJax template discussed at the <a
href='introduction.html#setting-up'>beginning of this tutorial</a>.</p>


<h2>Colors</h2>

<p>While they&rsquo;re not exactly &ldquo;advanced&rdquo; formatting options,
this is a fitting place to introduce the <code>mathcolor</code> and
<code>mathbackground</code> attributes. These let you set the text color and
background color of MathML elements, which is useful for highlighting important
aspects of an equation. Both attributes take a hexadecimal color as a
value:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&quot;http://www.w3.org/1998/Math/MathML&quot;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;msup&gt;</span>
    <span class="nt">&lt;mi&gt;</span>e<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mrow</span> <span class="na">mathcolor=</span><span class="s">&#39;#cc0000&#39;</span><span class="nt">&gt;</span>
      <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
      <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;/msup&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>This tells the renderer to draw the entire exponent in red:</p>

<figure>
	<img style='max-width: 114px'
		 src='media/advanced-formatting/colors.png'
		 alt='e^(2x+1) with exponent in red' />
</figure>

<p>These two attributes can be applied to <em>any</em> Presentation
Markup&mdash;they aren&rsquo;t limited to the token elements like
<code>mathvariant</code> is. This makes it possible to cascade colors to an
element&rsquo;s children, as we saw with <code>&lt;mrow&gt;</code> above.


<h2>Font Sizes</h2>

<p>The <code>mathsize</code> works in a similar fashion, except it takes a
measurement specified in <code>em</code>, <code>px</code>, <code>pt</code>,
etc. For example, we can increase the size of the <code>e</code> just a little
bit using the following snippet.</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&quot;http://www.w3.org/1998/Math/MathML&quot;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;msup&gt;</span>
    <span class="nt">&lt;mi</span> <span class="na">mathsize=</span><span class="s">&#39;1.2em&#39;</span><span class="nt">&gt;</span>e<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mrow</span> <span class="na">mathcolor=</span><span class="s">&#39;#00cc00&#39;</span><span class="nt">&gt;</span>
      <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
      <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;/msup&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>This makes the base <code>1.2</code> times the current font height,
resulting in the rendering shown below.</p>

<figure>
	<img style='max-width: 114px' src='media/advanced-formatting/font-sizes.png' />
</figure>

<p>Font sizes can only be applies to token elements (<code>&lt;mi&gt;</code>,
<code>&lt;mn&gt;</code>, and <code>&lt;mo&gt;</code>), which means you cannot
apply it to an <code>&lt;mrow&gt;</code> like <code>mathcolor</code>. Instead,
you need to use a style group.</p>


<h2>Style Groups</h2>

<p>The <code>&lt;mstyle&gt;</code> element represents a change in the default
style for all child elements. If we want an entire group of elements to use a
smaller font, you simply wrap them in an <code>&lt;mstyle&gt;</code> element and set
the <code>mathsize</code> attribute on that, like so:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&quot;http://www.w3.org/1998/Math/MathML&quot;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;msup&gt;</span>
    <span class="nt">&lt;mi&gt;</span>e<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mstyle</span> <span class="na">mathcolor=</span><span class="s">&#39;#0000cc&#39;</span> <span class="na">mathsize=</span><span class="s">&#39;.7em&#39;</span><span class="nt">&gt;</span>
      <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
      <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mstyle&gt;</span>
  <span class="nt">&lt;/msup&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>This tells the contained <code>&lt;mn&gt;</code>, <code>&lt;mi&gt;</code>,
and <code>&lt;mo&gt;</code> elements to change their size to <code>.7</code>
times the current font height. Also notice that <code>&lt;mstyle&gt;</code>
creates an inferred row, so we can safely omit the <code>&lt;mrow&gt;</code>
tag.</p>

<figure>
	<img style='max-width: 106px' src='media/advanced-formatting/style-groups.png' />
</figure>

<p>You can override an <code>&lt;mstyle&gt;</code>&rsquo;s default value by
explicitly setting the same attribute on the target child element.</p>


<h2 id='inline-expressions'>Inline Expressions</h2>

<p>For the entirety of this tutorial, we&rsquo;ve been rendering expressions as
<em>block</em> formulas, but MathML also provides <em>inline</em> formulas.
Whereas block formulas are rendered in their own formatting block, inline
expressions appear alongside prose. For this reason, a more compact form is
typically chosen for inline expressions:</p>

<figure>
	<img style='max-width: 303px'
		 src='media/advanced-formatting/inline-expressions-1.png'
		 alt='Block summation with over/underscripts vs inline summation with super/subscripts' />
</figure>

<p>As mentioned at the <a href='basic-algebra.html#variables'>beginning</a> of
this tutorial, the rendering mode is determined by the <code>display</code>
attribute of the top-level <code>&lt;math&gt;</code> element. To change a block
expression into an inline one, set <code>display</code> to <code>inline</code>,
then include the expression inside of the surrounding text:</p>

<div class="highlight"><pre><span class="nt">&lt;p&gt;</span>The series
<span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&quot;http://www.w3.org/1998/Math/MathML&quot;</span> <span class="na">display=</span><span class="s">&#39;inline&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;munderover&gt;</span>
    <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;sum;</span><span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mi&gt;</span>n<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
    <span class="nt">&lt;mi&gt;</span><span class="ni">&amp;infin;</span><span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/munderover&gt;</span>
  <span class="nt">&lt;mfrac&gt;</span>
    <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;mi&gt;</span>n<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/mfrac&gt;</span>
<span class="nt">&lt;/math&gt;</span>
is divergent.<span class="nt">&lt;/p&gt;</span>
</pre></div>


<p>To fit the expression onto a single line, the renderer should display the
over/underscripts as super/subscripts and shrink the summation symbol and
fraction. The result is a high-quality layout with the expression seamlessly
integrating into the surrounding text.</p>

<figure>
	<img style='max-width: 463px'
		 src='media/advanced-formatting/inline-expressions-2.png'
		 alt='The series &sum; 1/n from n=1 to infinity is divergent.' />
</figure>

<p>Different expressions will require different changes (e.g., shorter integral
signs), and some simply <em>can&rsquo;t</em> be displayed inline. Most of these
decisions are left to the rendering engine, but there is one common exception,
discussed below.</p>


<h2>Movable Limits</h2>

<p>Next, let&rsquo;s try to create an inline limit using the above technique.
Our goal is to mark up the following:</p>

<figure>
	<img style='max-width: 378px'
		 src='media/advanced-formatting/movable-limits.png'
		 alt='lim 1/x as x approaches infinity with bounds rendered as subscript' />
</figure>

<p>A logical first attempt might look something like:</p>

<div class="highlight"><pre><span class="nt">&lt;p&gt;&lt;math</span> <span class="na">xmlns=</span><span class="s">&quot;http://www.w3.org/1998/Math/MathML&quot;</span> <span class="na">display=</span><span class="s">&#39;inline&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;munder&gt;</span>
    <span class="nt">&lt;mo&gt;</span>lim<span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;rarr;</span><span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;mn&gt;</span>0<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;/munder&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;ApplyFunction;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mfrac&gt;</span>
    <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/mfrac&gt;</span>
<span class="nt">&lt;/math&gt;</span>
equals infinity.<span class="nt">&lt;/p&gt;</span>
</pre></div>


<p>Unfortunately, this won&rsquo;t change the underscript to a subscript. A
<code>display='inline'</code> attribute is just half the story for inline
over/underscripts. We also need to define the <code>movablelimits</code>
attribute on the base of the <code>&lt;munder&gt;</code> tag. If it&rsquo;s set
to <code>true</code>, a MathML renderer is allowed to display limits as
super/subscripts, but if it&rsquo;s <code>false</code>, they <em>must</em> be
displayed as true over/underscripts.</p>

<p>So, let&rsquo;s change the <code>&lt;mo&gt;lim&lt;/mo&gt;</code> element to
the following:</p>

<div class="highlight"><pre><span class="nt">&lt;mo</span> <span class="na">movablelimits=</span><span class="s">&#39;true&#39;</span><span class="nt">&gt;</span>lim<span class="nt">&lt;/mo&gt;</span>
</pre></div>


<p>This should give us the desired result. You&rsquo;re probably wondering why
we didn&rsquo;t have to do this for the summation in the previous step. Certain
characters, including <code>&amp;sum;</code>, implicitly set
<code>movablelimits</code> to <code>true</code>. But, this varies among
rendering engines so it&rsquo;s safer to define it explicitly.</p>

<p>The point to remember is that an inline limit requires two things:</p>

<ol>
	<li><code>display='inline'</code> on the top-level
	<code>&lt;math&gt;</code> element</li>
	<li><code>movablelimits='true'</code> on the base of
	<code>&lt;munder&gt;</code>/<code>&lt;munderover&gt;</code></li>
</ol>

<p>Also note that <code>movablelimits</code> can <em>only</em> be defined on an
<code>&lt;mo&gt;</code> element, so don&rsquo;t try to use an identifier or a
number as a base.</p>


<h2 id='combining-text-and-mathml'>Combining Text and MathML</h2>

<p>In MathML, there are three standard ways to combine mathematical expressions
with text:</p>

<ul>
	<li>Write the text as plain HTML and use an inline
	<code>&lt;math&gt;</code> element.</li>
	<li>Wrap the text in <code>&lt;mo&gt;</code> or <code>&lt;mi&gt;</code>
	tags.</li>
	<li>Define an <code>&lt;mtext&gt;</code> element directly inside of a
	MathML expression.</li>
</ul>

<p>The previous two examples demonstrated the first technique, which is the
preferred way to mix text and formulas when writing articles or books. But, if
the text represents a mathematical entity, it&rsquo;s better to wrap it in
<code>&lt;mo&gt;</code> or <code>&lt;mi&gt;</code> tags. We saw this in the <a
href='calculus.html#over-underscripts'>Over/Underscripts</a> section when we
placed an ellipsis in an <code>&lt;mi&gt;</code> tag and in <a
href='tables.html#piecewise-functions'>Piecewise Functions</a> when we marked
up the <code>if</code> statement as an operator.</p>

<p>MathML&rsquo;s <code>&lt;mtext&gt;</code> element opens up the third
possibility of embedding text directly into a formula. For example, consider
the following expression.</p>

<figure>
	<img style='max-width: 472px'
		 src='media/advanced-formatting/combining-text-and-mathml.png'
		 alt='Theorem 1: if x>1 then x^2>x' />
</figure>

<p><code>Theorem 1</code> can&rsquo;t be marked up as an operator or an
identifier. This leaves either plain HTML and an inline MathML expression, or
an <code>&lt;mtext&gt;</code> element. Examples of both are shown below.</p>

<div class="highlight"><pre><span class="c">&lt;!-- Plain HTML with inline expression --&gt;</span>
<span class="nt">&lt;p&gt;</span>Theorem 1:
<span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&quot;http://www.w3.org/1998/Math/MathML&quot;</span> <span class="na">display=</span><span class="s">&#39;inline&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mo&gt;</span>if<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;gt;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;mo&gt;</span>then<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;msup&gt;</span>
    <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/msup&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;gt;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
<span class="nt">&lt;/math&gt;&lt;/p&gt;</span>
</pre></div>


<p>This inline version is better if you&rsquo;re using the expression within a
paragraph. However, if you want to display the expression in its own formatting
block, you&rsquo;ll want to use an <code>&lt;mtext&gt;</code> element, like
so:</p>

<div class="highlight"><pre><span class="c">&lt;!-- Embedded text with block expression --&gt;</span>
<span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mtext&gt;</span>Theorem 1:<span class="nt">&lt;/mtext&gt;</span>
  <span class="nt">&lt;mo&gt;</span>if<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;gt;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;mo&gt;</span>then<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;msup&gt;</span>
    <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/msup&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;gt;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Either option will work, but it&rsquo;s important to remember that
<code>&lt;mtext&gt;</code> is meant to define commentary or expository
text&mdash;not meaningful mathematical elements. Do <em>not</em> use it to mark
up text that replaces a variable or operator.</p>


<h2 id='custom-spacing'>Custom Spaces</h2>

<p>If you try to put a space inside of an <code>&lt;mtext&gt;</code> element,
you&rsquo;ll quickly realize that MathML strips surrounding whitespace from
elements. To insert a space into an expression, you need to use the empty
<code>&lt;mspace/&gt;</code> element and its <code>width</code> attribute. For
example, if you wanted to add a little bit of space between the words from the
previous step, you could use the following.</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mtext&gt;</span>Theorem 1:<span class="nt">&lt;/mtext&gt;</span>
  <span class="nt">&lt;mspace</span> <span class="na">width=</span><span class="s">&#39;.1em&#39;</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;mo&gt;</span>if<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mspace</span> <span class="na">width=</span><span class="s">&#39;.1em&#39;</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;gt;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;mo&gt;</span>then<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mspace</span> <span class="na">width=</span><span class="s">&#39;.1em&#39;</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;msup&gt;</span>
    <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/msup&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;gt;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>This can also be accomplished by adding an <code>lspace</code> or
<code>rspace</code> attribute to an <code>&lt;mo&gt;</code> element, but
<code>&lt;mspace/&gt;</code> is a little bit more flexible since it can be
defined anywhere in an equation. The result is shown below.</p>

<figure>
	<img style='max-width: 485px'
		 src='media/advanced-formatting/custom-spacing.png'
		 alt='Theorem 1: if x>1 then x^2>x with more spacing between words' />
</figure>

<p>One <code>em</code> is equal to the current font height, so this is a
relatively robust method of adding spaces (the
<code>&lt;mspace/&gt;</code>&rsquo;s will scale with the font size). However,
this kind of &ldquo;tweaking&rdquo; often proves more trouble than it&rsquo;s
worth: due to the inconsistencies in rendering engines and the
medium-independent nature of HTML, your &ldquo;perfect&rdquo; layout
won&rsquo;t look nearly as good in other applications. Unless you know exactly
how your audience will be viewing your MathML, it&rsquo;s usually best to stick
with the default layout.</p>

<p>It&rsquo;s important to understand that spaces should never be used to
convey mathematical meaning&mdash;your formula should read the same even if all
of the <code>&lt;mspace/&gt;</code>&rsquo;s were removed. For this reason, most
rendering engines use a default <code>width</code> of <code>0</code>, so
you&rsquo;ll pretty much always have to define it on your own.</p>


<h2>Custom Linebreaks</h2>

<p>In addition to horizontal space, the <code>&lt;mspace/&gt;</code> element
also lets you declare line breaks with the aptly named <code>linebreak</code>
attribute. This is useful, for example, to manually break a long expression
onto multiple lines:</p>

<figure>
	<img style='max-width: 525px'
		 src='media/advanced-formatting/custom-linebreaks.png'
		 alt='Long expression broken manually' />
</figure>

<p>To create such a linebreak, all you need is the following.</p>

<div class="highlight"><pre><span class="nt">&lt;mspace</span> <span class="na">linebreak=</span><span class="s">&#39;newline&#39;</span> <span class="nt">/&gt;</span>
</pre></div>


<p>If you would rather provide <em>hints</em> to the rendering engine and let
it break lines on its own, you can use the following values for the
<code>linebreak</code> attribute:</p>

<table class='multiline'>
	<thead>
		<tr>
			<th>Value</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><code>nobreak</code></td>
			<td>Forbid a line break at this location.</td>
		</tr>
		<tr>
			<td><code>badbreak</code></td>
			<td>Try to prevent a line break at this location.</td>
		</tr>
		<tr>
			<td><code>goodbreak</code></td>
			<td>Suggest a line break at this location.</td>
		</tr>
	</tbody>
</table>

<p>Since linebreaks often occur at operators, MathML lets you use all of these
attributes on the <code>&lt;mo&gt;</code> element, too. But, since you&rsquo;re
now breaking on a visible symbol, you&rsquo;ll need the
<code>linebreakstyle</code> attribute to specify whether the newline should be
before the operator, after the operator, or if the operator should be
duplicated and appear on both lines. Valid values are (respectively):
<code>before</code>, <code>after</code>, and <code>duplicate</code>.</p>

<p>So, instead of using an <code>&lt;mspace/&gt;</code> as in the previous
example, you could specify it directly on the last operator in the line:</p>

<div class="highlight"><pre><span class="nt">&lt;mo</span> <span class="na">linebreak=</span><span class="s">&#39;newline&#39;</span> <span class="na">linebreakstyle=</span><span class="s">&#39;after&#39;</span><span class="nt">&gt;</span>+<span class="nt">&lt;/mo&gt;</span>
</pre></div>



<h2>Custom Indentation</h2>

<p>The MathML specification gives you control over how subsequent lines are
indented after a linebreak. The <code>indentalign</code> attribute controls the
alignment of the line, and <code>indentshift</code> controls the size of the
indent. Both of these can be defined on <code>&lt;mo&gt;</code>,
<code>&lt;mspace/&gt;</code>, and the top-level <code>&lt;math&gt;</code>
element. They can be used to align multi-step evaluations of an expression,
like so:</p>

<figure>
	<img style='max-width: 364px'
		 src='media/advanced-formatting/custom-indentation.png'
		 alt='Two-line equation lined up at the equal sign' />
</figure>

<p>You&rsquo;re supposed to be doing this kind of alignment with the
<code>indenttarget</code> attribute, but this functionality is poorly
implemented across MathML rendering engines. Instead we&rsquo;re stuck with
manual indents:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&quot;http://www.w3.org/1998/Math/MathML&quot;</span>
      <span class="na">display=</span><span class="s">&#39;block&#39;</span>
      <span class="na">indentalign=</span><span class="s">&#39;left&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;msup&gt;&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/msup&gt;</span>
    <span class="nt">&lt;mo&gt;</span>-<span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mn&gt;</span>9<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;msup&gt;&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/msup&gt;</span>
    <span class="nt">&lt;mo&gt;</span>-<span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;msup&gt;&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/msup&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;mo</span> <span class="na">linebreak=</span><span class="s">&#39;newline&#39;</span>
      <span class="na">linebreakstyle=</span><span class="s">&#39;before&#39;</span>
      <span class="na">indentshift=</span><span class="s">&#39;2.6em&#39;</span><span class="nt">&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;mfenced</span> <span class="na">separators=</span><span class="s">&#39;&#39;</span><span class="nt">&gt;</span>
      <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span>-<span class="nt">&lt;/mo&gt;&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mfenced&gt;</span>
    <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;InvisibleTimes;</span><span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mfenced</span> <span class="na">separators=</span><span class="s">&#39;&#39;</span><span class="nt">&gt;</span>
      <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mfenced&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>First, this snippet changed the alignment of the entire
<code>&lt;math&gt;</code> element to <code>left</code>, since it&rsquo;s a much
easier way to align multiple lines. Then, we specified a line break at the
second <code>=</code> sign and indented it to align with the first
<code>=</code> sign. <code>2.6em</code> is the approximate width of the
left-hand-side of the equation, determined through trial and error.</p>

<p>Yes, this is a tedious way to align equations, but at least it uses
<code>em</code> units so that the alignment will be unaffected by changes in
font sizes. The next section takes a look at a better way to align
expressions.</p>


<h2>Hiding Sub-Expressions</h2>

<p>The <code>&lt;mphantom&gt;</code> element makes the contained mathematical
expression disappear, but the rest of the expression renders as if it were
still there. This is a much more robust way to align equations than manual
indentation. For example, the factorization from the previous step can be
aligned with the following:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&quot;http://www.w3.org/1998/Math/MathML&quot;</span>
      <span class="na">display=</span><span class="s">&#39;block&#39;</span>
      <span class="na">indentalign=</span><span class="s">&#39;left&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;msup&gt;&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/msup&gt;</span>
    <span class="nt">&lt;mo&gt;</span>-<span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mn&gt;</span>9<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;msup&gt;&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/msup&gt;</span>
    <span class="nt">&lt;mo&gt;</span>-<span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;msup&gt;&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/msup&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;mspace</span> <span class="na">linebreak=</span><span class="s">&#39;newline&#39;</span> <span class="nt">/&gt;</span>

  <span class="nt">&lt;mphantom&gt;</span>
    <span class="nt">&lt;msup&gt;</span>
      <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/msup&gt;</span>
    <span class="nt">&lt;mo&gt;</span>-<span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mn&gt;</span>9<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/mphantom&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;mfenced</span> <span class="na">separators=</span><span class="s">&#39;&#39;</span><span class="nt">&gt;</span>
      <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span>-<span class="nt">&lt;/mo&gt;&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mfenced&gt;</span>
    <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;InvisibleTimes;</span><span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mfenced</span> <span class="na">separators=</span><span class="s">&#39;&#39;</span><span class="nt">&gt;</span>
      <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mfenced&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>In the second line, the left-hand-side of the equation still exists in the
MathML, which ensures that the equal sign lines up with the first line. Then,
we simply hide it by wrapping it in <code>&lt;mphantom&gt;</code> tags. Since
we&rsquo;re essentially faking the equal-sign alignment, we also had to use an
<code>&lt;mspace/&gt;</code> to create the line break instead of a adding it to
an <code>&lt;mo&gt;</code>.</p>

<p>From a pedagogical perspective, <code>&lt;mphantom&gt;</code> presents an
intuitive way to mark up fill-in-the-blank questions. It lets you create the
full expression, and then turn some of the elements into blanks:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&quot;http://www.w3.org/1998/Math/MathML&quot;</span>
      <span class="na">display=</span><span class="s">&#39;block&#39;</span>
      <span class="na">indentalign=</span><span class="s">&#39;left&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;msup&gt;&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/msup&gt;</span>
    <span class="nt">&lt;mo&gt;</span>-<span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mn&gt;</span>9<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;msup&gt;</span>
      <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/msup&gt;</span>
    <span class="nt">&lt;mo&gt;</span>-<span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;msup&gt;</span>
      <span class="nt">&lt;mrow&gt;</span>
        <span class="nt">&lt;mphantom</span> <span class="na">style=</span><span class="s">&#39;border: 2px solid #000; padding: 5px;&#39;</span><span class="nt">&gt;</span>
          <span class="nt">&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;</span>
        <span class="nt">&lt;/mphantom&gt;</span>
      <span class="nt">&lt;/mrow&gt;</span>
      <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/msup&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>With the help of some inline CSS, <code>&lt;mphantom&gt;</code> turned the
second term in the right-hand-side into an empty box:</p>

<figure>
	<img style='max-width: 297px'
		 src='media/advanced-formatting/hiding-sub-expressions.png'
		 alt='' />
</figure>

<p>As you might imagine, the fact that MathML can be seamlessly combined with
CSS opens up a vast array of possibilities for formatting equations. But, the
creative side of styling equations and worksheets is a bit outside the scope of
this tutorial, so we&rsquo;ll leave that for you to explore on your own.</p>


<h2>Summary</h2>

<p>In this module, we wrapped up our discussion of MathML by looking at several
advanced formatting features. We learned how to change the color and size of
expressions, create inline elements with <code>display='inline'</code> and
<code>movablelimits='true'</code>, and customize whitespace using
<code>&lt;mspace/&gt;</code>.</p>

<p>Remember that different rendering engines have different default spacing
behavior, so make sure you know your target output format before you start
tweaking equations in search of the perfect layout.</p>

<p>And that&rsquo;s everything you need to know about MathML for typical daily
usage. You should now have all the skills you need to embed complex equations
directly into your web pages. Be sure to check out the final module of this
tutorial for a handy mathematical entity reference, too.</p>

<p>Of course, if you have any questions, comments, or suggestions, please
don&rsquo;t hesitate to <a href='../../contact.html'>contact us</a>.

<p class='sequential-nav'>
  <a href='symbol-reference.html'>Continue to <em>Symbol Reference</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>