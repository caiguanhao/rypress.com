<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's MathML Tutorial | Calculus | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="By combining a few new MathML elements with previous
concepts, we'll learn how to mark up limits, summations, derivatives, and
integrals." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />
  <link rel="stylesheet" href="../../media/stix.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s MathML
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Calculus</h1>

<p>Now that we have the bulk of MathML&rsquo;s typesetting capabilities out of
the way, we can focus on a few elements and entities directly related to
calculus. By combining these new tools with previous concepts, we&rsquo;ll
learn how to mark up limits, summations, derivatives, and integrals.</p>


<h2>Underscripts</h2>

<p>First, we&rsquo;ll look at <code>&lt;mover&gt;</code>&rsquo;s counterpart:
<code>&lt;munder&gt;</code>. Underscripts are often used to draw limits.</p>

<figure>
	<img style='max-width: 204px'
		 src='media/calculus/underscripts.png'
		 alt='lim 1/x as x->0' />
</figure>

<p>This expression can be marked up with the following. Also note the new
<code>&amp;infin;</code> entity for creating the infinity symbol.</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;munder&gt;</span>
    <span class="nt">&lt;mo&gt;</span>lim<span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;rarr;</span><span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;mn&gt;</span>0<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;/munder&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;ApplyFunction;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mfrac&gt;</span>
    <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/mfrac&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi&gt;</span><span class="ni">&amp;infin;</span><span class="nt">&lt;/mi&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Like <code>&lt;mfrac&gt;</code>, this element also includes a few attributes
to control how the underscript is rendered:</p>

<table class='multiline'>
	<thead>
		<tr>
			<th>Attribute</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><code>accentunder</code></td>
			<td>Whether the underscript is the same size as the base or shrunk
			down. Value should be <code>true</code> or <code>false</code>,
			respectively.</td>
		</tr>
		<tr>
			<td><code>align</code></td>
			<td>The alignment of the script with respect to the base. Can be
			<code>left</code>, <code>right</code>, or <code>center</code>.</td>
		</tr>
	</tbody>
</table>


<h2 id='over-underscripts'>Over/Underscripts</h2>

<p>To specify the bounds for summations (<code>&amp;sum;</code>) and products
(<code>&amp;prod;</code>), we&rsquo;ll need <code>&lt;munderover&gt;</code>. It
takes three arguments: a base, an underscript, and an overscript. For
example:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;munderover&gt;</span>
    <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;sum;</span><span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mi&gt;</span>n<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;mn&gt;</span>0<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
    <span class="nt">&lt;mi&gt;</span>k<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/munderover&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Your browser should display this as the following.</p>

<figure>
	<img style='max-width: 91px'
		 src='media/calculus/overunderscripts-1.png'
		 alt='Summation from n=0 to k' />
</figure>

<p>The midline ellipsis entity <code>&amp;ctdot;</code> is very useful for
marking up series. It&rsquo;s typically rendered higher up than the
<code>&amp;hellip;</code> entity, as demonstrated in the following
equation.</p>

<figure>
	<img style='max-width: 412px'
		 src='media/calculus/overunderscripts-2.png'
		 alt='Summation from n=0 to k showing terms a(0) through a(k)' />
</figure>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;munderover&gt;</span>
    <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;sum;</span><span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
        <span class="nt">&lt;mi&gt;</span>n<span class="nt">&lt;/mi&gt;</span>
        <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
        <span class="nt">&lt;mn&gt;</span>0<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
    <span class="nt">&lt;mi&gt;</span>k<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/munderover&gt;</span>
  <span class="nt">&lt;msub&gt;</span>
    <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;&lt;mi&gt;</span>n<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/msub&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;msub&gt;</span>
    <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>0<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/msub&gt;</span>
  <span class="nt">&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;msub&gt;</span>
    <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/msub&gt;</span>
  <span class="nt">&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi&gt;</span><span class="ni">&amp;ctdot;</span><span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;msub&gt;</span>
    <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>k<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/msub&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Notice that the ellipsis is placed in an <code>&lt;mi&gt;</code> tag, not an
<code>&lt;mo&gt;</code> as you might expect. This reflects the underlying
meaning of the elision&mdash;the ellipsis takes the place of symbols that
<em>would</em> be rendered as identifiers.</p>


<h2 id='derivatives'>Derivatives</h2>

<p>Underscripts and overscripts are the only new MathML elements we need for
calculus-related mathematics. All that&rsquo;s left are the HTML entities for
derivatives and integrals. Common symbols for differentiation are listed
below.</p>

<table>
	<thead>
		<tr>
			<th>Symbol</th>
			<th>Entity</th>
			<th>Hex</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class='center stix'>&#x2032;</td>
			<td><code>&amp;prime;</code></td>
			<td><code>&amp;#x2032;</code></td>
			<td>Prime</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2033;</td>
			<td><code>&amp;Prime;</code></td>
			<td><code>&amp;#x2033;</code></td>
			<td>Double prime</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2034;</td>
			<td><code>&amp;tprime;</code></td>
			<td><code>&amp;#x2034;</code></td>
			<td>Triple prime</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2057;</td>
			<td><code>&amp;qprime;</code></td>
			<td><code>&amp;#x2057;</code></td>
			<td>Quadruple prime</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2202;</td>
			<td><code>&amp;part;</code></td>
			<td><code>&amp;#x2202;</code></td>
			<td>Partial Differential</td>
		</tr>
		<tr>
			<td class='center stix'>&#x0394;</td>
			<td><code>&amp;Delta;</code></td>
			<td><code>&amp;#x0394;</code></td>
			<td>Increment</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2207;</td>
			<td><code>&amp;Del;</code></td>
			<td><code>&amp;#x2207;</code></td>
			<td>Gradient</td>
		</tr>
	</tbody>
</table>

<p>For example, we can use these prime entities to render the Taylor series.
Note that when used in this manner, they should be explicitly associated with
their function via an <code>&lt;msup&gt;</code> tag.</p>

<figure>
	<img style='max-width: 626px' src='media/calculus/derivatives-1.png' />
</figure>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;mi&gt;</span>f<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;ApplyFunction;</span><span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mfenced&gt;&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;&lt;/mfenced&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;mfrac&gt;</span>
      <span class="nt">&lt;mrow&gt;</span>
        <span class="nt">&lt;msup&gt;</span>
          <span class="nt">&lt;mi&gt;</span>f<span class="nt">&lt;/mi&gt;</span>
          <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;prime;</span><span class="nt">&lt;/mo&gt;</span>
        <span class="nt">&lt;/msup&gt;</span>
        <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;ApplyFunction;</span><span class="nt">&lt;/mo&gt;</span>
        <span class="nt">&lt;mfenced&gt;&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;&lt;/mfenced&gt;</span>
      <span class="nt">&lt;/mrow&gt;</span>
      <span class="nt">&lt;mrow&gt;</span>
        <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span>!<span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;/mrow&gt;</span>
    <span class="nt">&lt;/mfrac&gt;</span>
    <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;InvisibleTimes;</span><span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mfenced</span> <span class="na">separators=</span><span class="s">&#39;&#39;</span><span class="nt">&gt;</span>
      <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span><span class="ni">&amp;minus;</span><span class="nt">&lt;/mo&gt;&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;/mfenced&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;mfrac&gt;</span>
      <span class="nt">&lt;mrow&gt;</span>
        <span class="nt">&lt;msup&gt;</span>
          <span class="nt">&lt;mi&gt;</span>f<span class="nt">&lt;/mi&gt;</span>
          <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;Prime;</span><span class="nt">&lt;/mo&gt;</span>
        <span class="nt">&lt;/msup&gt;</span>
        <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;ApplyFunction;</span><span class="nt">&lt;/mo&gt;</span>
        <span class="nt">&lt;mfenced&gt;&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;&lt;/mfenced&gt;</span>
      <span class="nt">&lt;/mrow&gt;</span>
      <span class="nt">&lt;mrow&gt;</span>
        <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span>!<span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;/mrow&gt;</span>
    <span class="nt">&lt;/mfrac&gt;</span>
    <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;InvisibleTimes;</span><span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;msup&gt;</span>
      <span class="nt">&lt;mfenced</span> <span class="na">separators=</span><span class="s">&#39;&#39;</span><span class="nt">&gt;</span>
        <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span><span class="ni">&amp;minus;</span><span class="nt">&lt;/mo&gt;&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;/mfenced&gt;</span>
      <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/msup&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi&gt;</span><span class="ni">&amp;ctdot;</span><span class="nt">&lt;/mi&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Or, if you&rsquo;re more interested in multivariable calculus, take a look
at the markup for a two-dimensional gradient:</p>

<figure>
	<img style='max-width: 510px'
		 src='media/calculus/derivatives-2.png'
		 alt='Definition of a 2D gradient' />
</figure>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;Del;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi&gt;</span>f<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mfenced&gt;</span>
    <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mi&gt;</span>y<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/mfenced&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mfenced&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mfrac&gt;</span>
        <span class="nt">&lt;mrow&gt;</span>
          <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;part;</span><span class="nt">&lt;/mo&gt;&lt;mi&gt;</span>f<span class="nt">&lt;/mi&gt;</span>
        <span class="nt">&lt;/mrow&gt;</span>
        <span class="nt">&lt;mrow&gt;</span>
          <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;part;</span><span class="nt">&lt;/mo&gt;&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
        <span class="nt">&lt;/mrow&gt;</span>
      <span class="nt">&lt;/mfrac&gt;</span>
      <span class="nt">&lt;mfenced&gt;</span>
        <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mi&gt;</span>y<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;/mfenced&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mfrac&gt;</span>
        <span class="nt">&lt;mrow&gt;</span>
          <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;part;</span><span class="nt">&lt;/mo&gt;&lt;mi&gt;</span>f<span class="nt">&lt;/mi&gt;</span>
        <span class="nt">&lt;/mrow&gt;</span>
        <span class="nt">&lt;mrow&gt;</span>
          <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;part;</span><span class="nt">&lt;/mo&gt;&lt;mi&gt;</span>y<span class="nt">&lt;/mi&gt;</span>
        <span class="nt">&lt;/mrow&gt;</span>
      <span class="nt">&lt;/mfrac&gt;</span>
      <span class="nt">&lt;mfenced&gt;</span>
        <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mi&gt;</span>y<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;/mfenced&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;/mfenced&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>



<h2>Integrals</h2>

<p>Finally, we come to integrals, of which there are many:</p>

<table style='padding: 0 .5em'>
	<thead>
		<tr>
			<th>Symbol</th>
			<th>Entity</th>
			<th>Hex</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class='center stix'>&#x222B;</td>
			<td><code>&amp;int;</code></td>
			<td><code>&amp;#x222B;</code></td>
			<td>Integral</td>
		</tr>
		<tr>
			<td class='center stix'>&#x222C;</td>
			<td><code>&amp;Int;</code></td>
			<td><code>&amp;#x222C;</code></td>
			<td>Double integral</td>
		</tr>
		<tr>
			<td class='center stix'>&#x222D;</td>
			<td><code>&amp;tint;</code></td>
			<td><code>&amp;#x222D;</code></td>
			<td>Triple integral</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2A0C;</td>
			<td><code>&amp;qint;</code></td>
			<td><code>&amp;#x2A0C;</code></td>
			<td>Quadruple integral</td>
		</tr>
		<tr>
			<td class='center stix'>&#x222E;</td>
			<td><code>&amp;conint;</code></td>
			<td><code>&amp;#x222E;</code></td>
			<td>Contour integral</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2232;</td>
			<td><code>&amp;cwconint;</code></td>
			<td><code>&amp;#x2232;</code></td>
			<td>Clockwise contour integral</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2233;</td>
			<td><code>&amp;awconint;</code></td>
			<td><code>&amp;#x2233;</code></td>
			<td>Anticlockwise contour integral</td>
		</tr>
		<tr>
			<td class='center stix'>&#x222F;</td>
			<td><code>&amp;Conint;</code></td>
			<td><code>&amp;#x222F;</code></td>
			<td>Surface integral</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2230;</td>
			<td><code>&amp;Cconint;</code></td>
			<td><code>&amp;#x2230;</code></td>
			<td>Volume integral</td>
		</tr>
	</tbody>
</table>

<p>Stokes&rsquo; Theorem serves as a fitting example for integral entities:</p>

<figure>
	<img style='max-width: 413px'
		 src='media/calculus/integrals.png'
		 alt="Stokes' Theorem" />
</figure>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;msub&gt;</span>
    <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;conint;</span><span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;part;</span><span class="nt">&lt;/mo&gt;&lt;mi&gt;</span>S<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;/msub&gt;</span>
  <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>F<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;sdot;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;mi&gt;</span>d<span class="nt">&lt;/mi&gt;&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>s<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;msub&gt;</span>
    <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;Int;</span><span class="nt">&lt;/mo&gt;&lt;mi&gt;</span>S<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/msub&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;Del;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;times;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>F<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;sdot;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;mi&gt;</span>d<span class="nt">&lt;/mi&gt;&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>s<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Note that integral bounds are typically marked up as superscript/subscript
pairs, regardless of whether they are drawn as block or inline elements (more
on this in the <a href='advanced-formatting.html#inline-expressions'>Advanced
Formatting</a> section).</p>


<h2>Summary</h2>

<p>This section introduced two new elements, <code>&lt;munder&gt;</code> and
<code>&lt;munderover&gt;</code>, along with several HTML entities for drawing
derivatives and integrals. For the most part, the preceding examples serve as a
useful reference for building up complex expressions, rather than a
demonstration of brand new MathML concepts.</p>

<p>By now, I hope you&rsquo;re relatively comfortable marking up equations.
However, we have yet to discuss one of the most powerful features of MathML:
tables.</p>


<p class='sequential-nav'>
  <a href='tables.html'>Continue to <em>Tables</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>