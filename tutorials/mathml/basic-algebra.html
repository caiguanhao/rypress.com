<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's MathML Tutorial | Basic Algebra | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="This module explores the fundamental elements of MathML:
variables, operators, radicals, and fractions." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />
  <link rel="stylesheet" href="../../media/stix.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s MathML
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Basic Algebra</h1>

<p>Just as HTML uses tags to define headings and paragraphs, MathML uses them
to describe the various parts of a mathematical expression. This module
explores the fundamental elements of MathML: variables, operators, radicals,
and fractions. By the end of this module, you should be able combine these into
basic algebraic expressions.</p>

<figure>
	<img style='max-width: 353px'
		 src='media/basic-algebra/quadratic-formula.png'
		 alt='The Quadratic Formula' />
</figure>

<p>While this tutorial focuses on <em>Presentation</em> Markup, not all of the
elements we&rsquo;ll encounter are purely aesthetic. For instance, the
<code>i</code> in <code>&lt;mi&gt;</code> (discussed below) stands for
<em>identifier</em>, which is really more semantic than presentational.</p>

<p>As we&rsquo;ll see throughout this tutorial, typographic decisions often
carry mathematical meaning. This makes it hard to draw a clear line between
presentation and content.</p>


<h2 id='variables'>Variables</h2>

<p>We&rsquo;ll begin by creating a single variable with MathML. Replace the
<code>&lt;!&#8209;&#8209;&nbsp;Equations will go
here&nbsp;&#8209;&#8209;&gt;</code> comment in the <a
href='introduction.html#setting-up'>template file</a> with the following
code.</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>The <code>&lt;math&gt;</code> tag begins a new MathML expression, and it
should always use the above URI for its <code>xmlns</code> attribute. The
<code>display</code> attribute determines how the expression is rendered. Its
value can be either <code>block</code> or <code>inline</code>, as described
below.</p>

<table class='multiline'>
	<thead>
		<tr>
			<th>Value</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><code>block</code></td>
			<td>Render the expression as its own &ldquo;paragraph&rdquo;</td>
		</tr>
		<tr>
			<td><code>inline</code></td>
			<td>Render the expression in the same line as surrounding text</td>
		</tr>
	</tbody>
</table>

<p>We&rsquo;ll use <code>block</code> expressions for the majority of this
tutorial, deferring inline elements to the <a
href='advanced-formatting.html'>Advanced Formatting</a> module.</p>

<p>Next, the <code>&lt;mi&gt;</code> element defines a MathML
<strong>identifier</strong>, which can be a function name, a constant, or a
variable. In this case, we used the letter <code>x</code> as the
identifier&rsquo;s value, which your browser should render as the
following.</p>

<figure>
	<img style='max-width: 61px'
		 src='media/basic-algebra/x.png'
		 alt='x' />
</figure>


<h2>Numbers</h2>

<p>MathML uses the <code>&lt;mn&gt;</code> element to represent numeric
literals, which can be either integers or real numbers (digits with a decimal
point). Underneath our existing <code>&lt;math&gt;</code> element, try adding
the following expression.</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Notice that your browser rendered the number <code>3</code> with an upright
font, while it decided the <code>x</code> identifier should be italicized:</p>

<figure>
	<img style='max-width: 64px'
		 src='media/basic-algebra/3x.png'
		 alt='3x' />
</figure>

<p>Even though we&rsquo;re working with Presentation Markup, the
<code>&lt;mn&gt;</code> and <code>&lt;mi&gt;</code> elements do carry semantic
meaning. This lets a MathML reader make certain assumptions about how to
typeset their contents (e.g., italicizing variable names). We&rsquo;ll learn
how to control these stylistic assumptions in <a
href='vectors-and-functions.html'>Vectors and Functions</a>.</p>


<h2 id='operators'>Operators</h2>

<p>For now, let&rsquo;s move on to the <code>&lt;mo&gt;</code> element for
marking up operators. Again, the following expression should be added below the
existing examples in your <code>hello-mathml.html</code> file.</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span>-<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;mi&gt;</span>y<span class="nt">&lt;/mi&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>This example uses <code>&lt;mo&gt;</code> for a minus sign, but it&rsquo;s
meant to denote any kind of &ldquo;operator-like&rdquo; entity including
<code>=</code>, <code>+</code>, <code>&times;</code>, <code>&divide;</code>,
and even parentheses and commas. Explicitly delimiting operators helps MathML
viewers make important layout decisions like where to break lines.</p>

<p>It&rsquo;s important to note that traditional typography differentiates
hyphens from minus signs, which are longer. In the above HTML code, we used a
hyphen, but since we put it in an <code>&lt;mo&gt;</code> element, the browser
inferred that we were trying to create a minus sign.</p>

<figure>
	<img style='max-width: 338px'
		 src='media/basic-algebra/operators.png'
		 alt='3x-2y with a short hyphen vs with a longer minus sign' />
</figure>


<h2 id='entities'>Operator Entities</h2>

<p>To represent special symbols like minus signs, HTML offers several character
entities dedicated to mathematics. A few of the most common operators are
included below.</p>

<table>
	<thead>
		<tr>
			<th>Symbol</th>
			<th>Entity</th>
			<th>Hex</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class='center stix'>&#x2212;</td>
			<td><code>&amp;minus;</code></td>
			<td><code>&amp;#x2212;</code></td>
			<td>Subtraction</td>
		</tr>
		<tr>
			<td class='center stix'>&#x00D7;</td>
			<td><code>&amp;times;</code></td>
			<td><code>&amp;#x00D7;</code></td>
			<td>Multiplication</td>
		</tr>
		<tr>
			<td class='center stix'>&#x00F7;</td>
			<td><code>&amp;divide;</code></td>
			<td><code>&amp;#x00F7;</code></td>
			<td>Division</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2260;</td>
			<td><code>&amp;ne;</code></td>
			<td><code>&amp;#x2260;</code></td>
			<td>Not equal</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2248;</td>
			<td><code>&amp;asymp;</code></td>
			<td><code>&amp;#x2248;</code></td>
			<td>Approximately equal</td>
		</tr>
		<tr>
			<td class='center stix'>&#x003C;</td>
			<td><code>&amp;lt;</code></td>
			<td><code>&amp;#x003C;</code></td>
			<td>Less than</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2264;</td>
			<td><code>&amp;le;</code></td>
			<td><code>&amp;#x2264;</code></td>
			<td>Less than or equal</td>
		</tr>
		<tr>
			<td class='center stix'>&#x003E;</td>
			<td><code>&amp;gt;</code></td>
			<td><code>&amp;#x003E;</code></td>
			<td>Greater than</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2265;</td>
			<td><code>&amp;ge;</code></td>
			<td><code>&amp;#x2265;</code></td>
			<td>Greater than or equal</td>
		</tr>
		<tr>
			<td class='center stix'>&#x00B1;</td>
			<td><code>&amp;plusmn;</code></td>
			<td><code>&amp;#x00B1;</code></td>
			<td>Plus or minus</td>
		</tr>
	</tbody>
</table>

<p>In general, HTML entities can be used anywhere normal characters can exist,
though the ones shown above will typically be used as the content of an
<code>&lt;mo&gt;</code> element:

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span>-<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;mi&gt;</span>y<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;ge;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mn&gt;</span>0<span class="nt">&lt;/mn&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<figure>
	<img style='max-width: 212px'
		 src='media/basic-algebra/3x-2y-ge-0.png'
		 alt='3x-2y&ge;0' />
</figure>

<p>Other useful symbols can be found in the <a
href='symbol-reference.html'>Symbol Reference</a>.</p>


<h2 id='superscripts'>Superscripts</h2>

<p>Superscripts are created with the <code>&lt;msup&gt;</code> tag. Inside of
<code>&lt;msup&gt;</code>, you should include the base, followed by the
superscript, like so:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;msup&gt;</span>
    <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/msup&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Like many MathML elements, <code>&lt;msup&gt;</code> acts more like a
function than a normal HTML markup tag. In the above snippet, the base and the
superscript can be thought of as two &ldquo;arguments&rdquo; passed to the
<code>&lt;msup&gt;</code> &ldquo;function.&rdquo; Both arguments need to be a
single MathML element (e.g., <code>&lt;mi&gt;</code> or
<code>&lt;mn&gt;</code>).</p>

<figure>
	<img style='max-width: 76px'
		 src='media/basic-algebra/x2.png'
		 alt='x^2' />
</figure>

<p>This works fine when we&rsquo;re dealing with atomic values like
<code>x</code> and <code>2</code>, but things get more complicated once we
start working with compound expressions.</p>


<h2>Grouping Sub-Expressions</h2>

<figure>
	<img style='max-width: 117px'
		 src='media/basic-algebra/e-2x-plus-1.png'
		 alt='e^(2x+1)' />
</figure>

<p>Because the exponent shown above is comprised of more than one element, we
can&rsquo;t pass it directly into <code>&lt;msup&gt;</code>. Instead, we need
to use an <code>&lt;mrow&gt;</code> tag to group the expression, as shown
below.</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;msup&gt;</span>
    <span class="nt">&lt;mi&gt;</span>e<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
      <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;/msup&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Of course, this works the other way around&mdash;you can wrap the
superscript argument in an <code>&lt;mrow&gt;</code> to create a complex base
expression, too.</p>

<figure>
	<img style='max-width: 197px'
		 src='media/basic-algebra/3x-2y-squared.png'
		 alt='(3x-2y)^2' />
</figure>

<p>Technically, <code>&lt;mrow&gt;</code> should be used to encode the complete
mathematical structure of an expression, meaning <em>every</em> sub-expression
should be contained in an <code>&lt;mrow&gt;</code>. For example:

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;msup&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mo&gt;</span>(<span class="nt">&lt;/mo&gt;</span>
        <span class="nt">&lt;mrow&gt;</span>
          <span class="nt">&lt;mrow&gt;&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;/mrow&gt;</span>
          <span class="nt">&lt;mo&gt;</span>-<span class="nt">&lt;/mo&gt;</span>
          <span class="nt">&lt;mrow&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;mi&gt;</span>y<span class="nt">&lt;/mi&gt;&lt;/mrow&gt;</span>
        <span class="nt">&lt;/mrow&gt;</span>
      <span class="nt">&lt;mo&gt;</span>)<span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
    <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/msup&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Explicit <code>&lt;mrow&gt;</code> tags do help MathML renderers make layout
decisions, and they provide extra semantic information that&rsquo;s useful for
automated interpretation. However, this level of completeness isn&rsquo;t
strictly necessary for most practical purposes.</p>

<p>Also notice that we used <code>&lt;mo&gt;</code> to encode the paretheses.
While this is perfectly legal, MathML does offer a more convenient way to
bracket expressions, which we&rsquo;ll discuss in <a
href='vectors-and-functions.html'>Vectors and Functions</a>.</p>


<h2>Radicals</h2>

<p>MathML provides two elements for drawing radicals:
<code>&lt;msqrt&gt;</code> and <code>&lt;mroot&gt;</code>. The former creates a
square root, taking the base as its only argument, and the latter creates an
indexed root, taking the base and the index as arguments (in that order).</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;msqrt&gt;</span>
    <span class="nt">&lt;mn&gt;</span>4<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/msqrt&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;times;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mroot&gt;</span>
    <span class="nt">&lt;mn&gt;</span>8<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/mroot&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mn&gt;</span>4<span class="nt">&lt;/mn&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Your browser should render this equation as follows.</p>

<figure>
	<img style='max-width: 242px'
		 src='media/basic-algebra/sqrt-4-times-3-root-8.png'
		 alt='sqrt(4)*root(3,8)=4' />
</figure>

<p>As with <code>&lt;msup&gt;</code>, you should make sure
<code>&lt;mroot&gt;</code> has exactly two child elements by wrapping complex
sub-expressions in an <code>&lt;mrow&gt;</code>. This isn&rsquo;t actually
necessary for <code>&lt;msqrt&gt;</code>, since an <code>&lt;mrow&gt;</code>
will be implied if you pass more than one argument.</p>


<h2>Fractions</h2>

<p>The last component we&rsquo;ll look at in <em>Basic Algebra</em> is the
<code>&lt;mfrac&gt;</code> tag for creating fractions. It takes the numerator
as the first argument, followed by the denominator:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mfrac&gt;</span>
    <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/mfrac&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>This should render as an vertical fraction:</p>

<figure>
	<img style='max-width: 77px'
		 src='media/basic-algebra/1-over-x.png'
		 alt='1/x' />
</figure>

<p>The <code>&lt;mfrac&gt;</code> element also contains a few useful attributes
that let you control its appearance:</p>

<table class='multiline'>
	<thead>
		<tr>
			<th>Attribute</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><code>linethickness</code></td>

			<td>The stroke width of the fraction bar. Value should be a length
			measured in <code>px</code>, <code>pt</code>, <code>em</code>, etc.</td>

		</tr>
		<tr>
			<td><code>numalign</code></td>

			<td>The alignment of the numerator. Can be <code>left</code>,
			<code>right</code>, or <code>center</code>.</td>

		</tr>
		<tr>
			<td><code>denomalign</code></td>

			<td>The alignment of the denominator. Can be <code>left</code>,
			<code>right</code>, or <code>center</code>.</td>

		</tr>
		<tr>
			<td><code>bevelled</code></td>

			<td>Whether the fraction should be displayed vertically or inline. Can be
			<code>true</code> or <code>false</code>.</td>

		</tr>
	</tbody>
</table>

<p>These attributes let you manually arrange complex expressions. For
example:</p>

<figure>
	<img style='max-width: 133px'
		 src='media/basic-algebra/1-over-x-over-y-2.png'
		 alt='(1/x)/(y-2)' />
</figure>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mfrac</span> <span class="na">linethickness=</span><span class="s">&#39;3px&#39;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;mfrac</span> <span class="na">bevelled=</span><span class="s">&#39;true&#39;</span><span class="nt">&gt;</span>
      <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
      <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;/mfrac&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mi&gt;</span>y<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;minus;</span><span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;/mfrac&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Setting <code>linethickness</code> to zero is also the preferred way to mark
up binomial coefficients.</p>

<p>Again, like other MathML &ldquo;functions,&rdquo; sub-expressions should be
placed in an <code>&lt;mrow&gt;</code> to ensure only two arguments are
passed to <code>&lt;mfrac&gt;</code>.</p>


<h2>Summary</h2>

<p>This section introduced MathML&rsquo;s three atomic elements:
<code>&lt;mi&gt;</code>, <code>&lt;mn&gt;</code>, and <code>&lt;mo&gt;</code>.
We also saw how these tokens can be combined into complex layouts using
superscripts, radicals, and fractions. Together, these provide everything we
need to mark up basic algebraic equations like the quadratic formula:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mfrac&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mo&gt;</span>-<span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;mi&gt;</span>b<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;plusmn;</span><span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;msqrt&gt;</span>
        <span class="nt">&lt;msup&gt;</span>
          <span class="nt">&lt;mi&gt;</span>b<span class="nt">&lt;/mi&gt;</span>
          <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
        <span class="nt">&lt;/msup&gt;</span>
        <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;minus;</span><span class="nt">&lt;/mo&gt;</span>
        <span class="nt">&lt;mrow&gt;</span>
          <span class="nt">&lt;mn&gt;</span>4<span class="nt">&lt;/mn&gt;</span>
          <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
          <span class="nt">&lt;mi&gt;</span>c<span class="nt">&lt;/mi&gt;</span>
        <span class="nt">&lt;/mrow&gt;</span>
      <span class="nt">&lt;/msqrt&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
      <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;/mfrac&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Towards the end of this module, we saw how attribute values can affect the
appearance of a MathML element. In the next module, we&rsquo;ll discuss several
other attributes that will give us fine-grained control over MathML&rsquo;s
typographic decisions.</p>

<p class='sequential-nav'>
  <a href='vectors-and-functions.html'>Continue to <em>Vectors &amp;
  Functions</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>