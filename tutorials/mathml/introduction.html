<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's MathML Tutorial | Introduction | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="MathML is an XML language for describing mathematical
expressions. It can be embedded directly into HTML pages, and it has an
approachable learning curve." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />
  <link rel="stylesheet" href="../../media/stix.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s MathML
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Introduction</h1>

<p><a href='http://www.w3.org/Math/'>MathML</a> is an XML language for
describing mathematical expressions. It can be embedded directly into HTML
documents, which makes it possible to share complex mathematical concepts using
nothing more than a text editor and a web browser.</p>

<figure>
	<img style='max-width: 413px' src='media/introduction/mathml-demo.png' />
	<figcaption>An equation rendered with
MathML</figcaption>
</figure>

<p>Since it&rsquo;s based on XML, MathML is more verbose than its LaTeX
counterpart. It does, however, have a much more approachable learning curve if
you&rsquo;re already familiar with HTML. In addition, MathML&rsquo;s
predictable structure makes it ideal for machine processing.</p>

<p>The MathML specification defines two separate languages for representing
expressions: <a href='http://www.w3.org/TR/MathML3/chapter4.html'>Presentation
Markup</a> and <a href='http://www.w3.org/TR/MathML3/chapter3.html'>Content
Markup</a>. As the name suggests, Presentation Markup describes expressions in
terms of their visual appearance, whereas Content Markup describes the
underlying mathematical meaning of an expression. This tutorial focuses solely
on Presentation Markup.</p>


<h2>Browser Support</h2>

<p>As of this writing, Firefox, Opera, and Safari provide native MathML
support, while Internet Explorer and Chrome do not. Fortunately, the <a
href='http://www.mathjax.org/'>MathJax</a> library brings MathML to
<em>all</em> modern browsers using some JavaScript magic.</p>


<h2>Font Support</h2>

<p>To display special symbols in MathML, you can use HTML entities like
<code>&amp;times;</code> and <code>&amp;infin;</code> (see the <a
href='symbol-reference.html'>Symbol Reference</a> module for a concise list).
However, many of these characters are not supported by common fonts. The open
source <a href='http://www.stixfonts.org/'>STIX Fonts</a> were created to solve
this problem by providing a comprehensive set of symbols for scientific and
technical publications.</p>

<p>MathJax includes the STIX Fonts as part of its library, so if you use the
template below, no extra work is required. If you want to display mathematical
symbols in a browser <em>without</em> MathJax, you&rsquo;ll need to <a
href='http://sourceforge.net/projects/stixfonts/'>download</a> the STIX Fonts,
<a href='http://www.fontsquirrel.com/fontface/generator'>convert</a> them to a
webfont kit, and embed them through a CSS <a
href='http://www.w3.org/TR/css3-fonts/#font-face-rule'><code>@font-face</code></a>
rule.</p>


<h2 id='setting-up'>Setting Up</h2>

<p>This tutorial is designed to be a hands-on introduction to MathML, which
means it&rsquo;s full of real-world examples for you to experiment with. To get
started, create a new text file called <code>hello-mathml.html</code>, open it
with your favorite text editor, and add the following code.</p>

<div class="highlight"><pre><span class="cp">&lt;!doctype html&gt;</span>
<span class="nt">&lt;html</span> <span class="na">lang=</span><span class="s">&#39;en&#39;</span><span class="nt">&gt;</span>
<span class="nt">&lt;head&gt;</span>
  <span class="nt">&lt;meta</span> <span class="na">charset=</span><span class="s">&#39;UTF-8&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;title&gt;</span>Ry&#39;s MathML Tutorial<span class="nt">&lt;/title&gt;</span>
  <span class="nt">&lt;style&gt;</span>
    <span class="nt">body</span> <span class="p">{</span>
      <span class="k">font-size</span><span class="o">:</span> <span class="m">2em</span><span class="p">;</span>
    <span class="p">}</span>
  <span class="nt">&lt;/style&gt;</span>
  <span class="nt">&lt;script </span><span class="na">type=</span><span class="s">&quot;text/javascript&quot;</span>
          <span class="na">src=</span><span class="s">&quot;http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML&quot;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;/script&gt;</span>
<span class="nt">&lt;/head&gt;</span>
<span class="nt">&lt;body&gt;</span>
  <span class="c">&lt;!-- Equations will go here --&gt;</span>
<span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span>
</pre></div>


<p>This empty HTML5 page will serve as our template for the rest of the
tutorial. Code snippets from the upcoming sections can be pasted into the
<code>&lt;body&gt;</code> element, and you should be able to open the page with
your browser to view the rendered mathematical notation.</p>

<p>Also notice the <code>&lt;script&gt;</code> element that includes the
MathJax library (no download required). If you&rsquo;re using a browser with
MathML support, feel free to remove it&mdash;but, remember that this will make
your content inaccessible to IE and Chrome.</p>

<p class='sequential-nav'>
  <a href='basic-algebra.html'>Continue to <em>Basic Algebra</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>