<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's MathML Tutorial | Tables | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="In this module, we'll explore MathML tables and their
use in matrices, numbered equations, and piecewise functions." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />
  <link rel="stylesheet" href="../../media/stix.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s MathML
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Tables</h1>

<p>Together with our existing elements, MathML tables open up several new
layout possibilities for mathematical expressions. In this module, we&rsquo;ll
see how they can be used to create matrices, numbered equations, and piecewise
functions.</p>

<figure>
	<img style='max-width: 353px'
		 src='media/tables/introduction.png'
		 alt='Generic matrix with dashed lines between each cell' />
</figure>

<p>MathML tables work pretty much the same way native HTML tables do. In fact,
they even use the same tag names, prefixed with the letter <code>m</code>. For
example, instead of HTML&rsquo;s <code>&lt;table&gt;</code> and
<code>&lt;tr&gt;</code> elements, MathML uses <code>&lt;mtable&gt;</code> and
<code>&lt;mtr&gt;</code>.</p>


<h2>Matrices</h2>

<p>Displaying matrices is one of the of the most common applications of MathML
tables. Let&rsquo;s start by adding the following code to our
<code>hello-mathml.html</code> file.</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mi&gt;</span>A<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mfenced</span> <span class="na">open=</span><span class="s">&#39;[&#39;</span> <span class="na">close=</span><span class="s">&#39;]&#39;</span> <span class="na">separators=</span><span class="s">&#39;&#39;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;mtable&gt;</span>
      <span class="nt">&lt;mtr&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span>b<span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
      <span class="nt">&lt;/mtr&gt;</span>
      <span class="nt">&lt;mtr&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span>c<span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span>d<span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
      <span class="nt">&lt;/mtr&gt;</span>
    <span class="nt">&lt;/mtable&gt;</span>
  <span class="nt">&lt;/mfenced&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>The <code>&lt;mtable&gt;</code> tag begins a new MathML table,
<code>&lt;mtr&gt;</code> creates a row, and <code>&lt;mtd&gt;</code> contains
the column data for each row. The content inside each <code>&lt;mtd&gt;</code>
should be an expression marked up with MathML tags. In this case, we just have
variables marked as identifiers, which should look something like:</p>

<figure>
	<img style='max-width: 219px'
		 src='media/tables/matrices.png'
		 alt='A=[[a,b],[c,d]]' />
</figure>


<h2>Determinants</h2>

<p>To create determinants, simply change the <code>open</code> and
<code>close</code> attributes on the surrounding <code>&lt;mfenced&gt;</code>
to a vertical bar:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;mi&gt;</span>det<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;ApplyFunction;</span><span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mfenced&gt;&lt;mi&gt;</span>A<span class="nt">&lt;/mi&gt;&lt;/mfenced&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>

  <span class="nt">&lt;mfenced</span> <span class="na">open=</span><span class="s">&#39;|&#39;</span> <span class="na">close=</span><span class="s">&#39;|&#39;</span> <span class="na">separators=</span><span class="s">&#39;&#39;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;mtable&gt;</span>
      <span class="nt">&lt;mtr&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span>b<span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
      <span class="nt">&lt;/mtr&gt;</span>
      <span class="nt">&lt;mtr&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span>c<span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span>d<span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
      <span class="nt">&lt;/mtr&gt;</span>
    <span class="nt">&lt;/mtable&gt;</span>
  <span class="nt">&lt;/mfenced&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>

  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span><span class="ni">&amp;InvisibleTimes;</span><span class="nt">&lt;/mo&gt;&lt;mi&gt;</span>d<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
    <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;minus;</span><span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mi&gt;</span>b<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span><span class="ni">&amp;InvisibleTimes;</span><span class="nt">&lt;/mo&gt;&lt;mi&gt;</span>c<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Notice how the <code>&lt;mfenced&gt;</code> brackets conveniently stretch to
match the height of the table, regardless of which kind of bracket you use.
This will work for any characters that are allowed to stretch vertically. The
most common ones are <code>[]</code>, <code>||</code>, <code>()</code>,
<code>{}</code>, and <code>&lang;&rang;</code>
(<code>&amp;lang;&amp;rang;</code>).</p>

<figure>
	<img style='max-width: 416px'
		 src='media/tables/determinants.png'
		 alt='det(A)=|[a,b],[c,d]|=ad-bc' />
</figure>

<p>This example also demonstrates technically accurate
<code>&lt;mrow&gt;</code>&rsquo;s for each operand.</p>


<h2>Ellipsis Entities</h2>

<p>The <a href='calculus.html#over-underscripts'>Calculus</a> module
introduced the midline ellipsis for use in a series, but for matrix elision, we
need ellipses for all directions:</p>

<table>
	<thead>
		<tr>
			<th>Symbol</th>
			<th>Entity</th>
			<th>Hex</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class='center stix'>&#x2026;</td>
			<td><code>&amp;hellip;</code></td>
			<td><code>&amp;#x2026;</code></td>
			<td>Horizontal ellipsis</td>
		</tr>
		<tr>
			<td class='center stix'>&#x22EE;</td>
			<td><code>&amp;vellip;</code></td>
			<td><code>&amp;#x22EE;</code></td>
			<td>Vertical ellipsis</td>
		</tr>
		<tr>
			<td class='center stix'>&#x22EF;</td>
			<td><code>&amp;ctdot;</code></td>
			<td><code>&amp;#x22EF;</code></td>
			<td>Midline horizontal ellipsis</td>
		</tr>
		<tr>
			<td class='center stix'>&#x22F0;</td>
			<td><code>&amp;utdot;</code></td>
			<td><code>&amp;#x22F0;</code></td>
			<td>Up right diagonal ellipsis</td>
		</tr>
		<tr>
			<td class='center stix'>&#x22F1;</td>
			<td><code>&amp;dtdot;</code></td>
			<td><code>&amp;#x22F1;</code></td>
			<td>Down right diagonal ellipsis</td>
		</tr>
	</tbody>
</table>

<p>Remember that an ellipsis should always appear in the same kind of element
it&rsquo;s representing&mdash;<code>&lt;mn&gt;</code> for numbers,
<code>&lt;mi&gt;</code> for variables, and <code>&lt;mo&gt;</code> for
operators (though you probably won&rsquo;t need the latter all that often). In
the case of matrices, they should also be placed in the <code>&lt;mtd&gt;</code>
where you want them to appear. For example, a generic matrix could be marked
up as follows.</p>

<figure>
	<img style='max-width: 418px'
		 src='media/tables/ellipsis-entities.png'
		 alt='Box matrix with entries from a(1,1) to a(m,n)' />
</figure>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mi&gt;</span>A<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mfenced</span> <span class="na">open=</span><span class="s">&#39;[&#39;</span> <span class="na">close=</span><span class="s">&#39;]&#39;</span> <span class="na">separators=</span><span class="s">&#39;&#39;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;mtable&gt;</span>
      <span class="nt">&lt;mtr&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;msub&gt;</span>
          <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
          <span class="nt">&lt;mrow&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;&lt;mo&gt;</span><span class="ni">&amp;InvisibleComma;</span><span class="nt">&lt;/mo&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;&lt;/mrow&gt;</span>
        <span class="nt">&lt;/msub&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;msub&gt;</span>
          <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
          <span class="nt">&lt;mrow&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;&lt;mo&gt;</span><span class="ni">&amp;InvisibleComma;</span><span class="nt">&lt;/mo&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/mrow&gt;</span>
        <span class="nt">&lt;/msub&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span><span class="ni">&amp;ctdot;</span><span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;msub&gt;</span>
          <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
          <span class="nt">&lt;mrow&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;&lt;mo&gt;</span><span class="ni">&amp;InvisibleComma;</span><span class="nt">&lt;/mo&gt;&lt;mi&gt;</span>n<span class="nt">&lt;/mi&gt;&lt;/mrow&gt;</span>
        <span class="nt">&lt;/msub&gt;&lt;/mtd&gt;</span>
      <span class="nt">&lt;/mtr&gt;</span>

      <span class="nt">&lt;mtr&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;msub&gt;</span>
          <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
          <span class="nt">&lt;mrow&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;mo&gt;</span><span class="ni">&amp;InvisibleComma;</span><span class="nt">&lt;/mo&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;&lt;/mrow&gt;</span>
        <span class="nt">&lt;/msub&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;msub&gt;</span>
          <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
          <span class="nt">&lt;mrow&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;mo&gt;</span><span class="ni">&amp;InvisibleComma;</span><span class="nt">&lt;/mo&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/mrow&gt;</span>
        <span class="nt">&lt;/msub&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span><span class="ni">&amp;ctdot;</span><span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;msub&gt;</span>
          <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
          <span class="nt">&lt;mrow&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;mo&gt;</span><span class="ni">&amp;InvisibleComma;</span><span class="nt">&lt;/mo&gt;&lt;mi&gt;</span>n<span class="nt">&lt;/mi&gt;&lt;/mrow&gt;</span>
        <span class="nt">&lt;/msub&gt;&lt;/mtd&gt;</span>
      <span class="nt">&lt;/mtr&gt;</span>

      <span class="nt">&lt;mtr&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span><span class="ni">&amp;vellip;</span><span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span><span class="ni">&amp;vellip;</span><span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span><span class="ni">&amp;dtdot;</span><span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span><span class="ni">&amp;vellip;</span><span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
      <span class="nt">&lt;/mtr&gt;</span>

      <span class="nt">&lt;mtr&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;msub&gt;</span>
          <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
          <span class="nt">&lt;mrow&gt;&lt;mi&gt;</span>m<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span><span class="ni">&amp;InvisibleComma;</span><span class="nt">&lt;/mo&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;&lt;/mrow&gt;</span>
        <span class="nt">&lt;/msub&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;msub&gt;</span>
          <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
          <span class="nt">&lt;mrow&gt;&lt;mi&gt;</span>m<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span><span class="ni">&amp;InvisibleComma;</span><span class="nt">&lt;/mo&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/mrow&gt;</span>
        <span class="nt">&lt;/msub&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span><span class="ni">&amp;ctdot;</span><span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;msub&gt;</span>
          <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
          <span class="nt">&lt;mrow&gt;&lt;mi&gt;</span>m<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span><span class="ni">&amp;InvisibleComma;</span><span class="nt">&lt;/mo&gt;&lt;mi&gt;</span>n<span class="nt">&lt;/mi&gt;&lt;/mrow&gt;</span>
        <span class="nt">&lt;/msub&gt;&lt;/mtd&gt;</span>
      <span class="nt">&lt;/mtr&gt;</span>
    <span class="nt">&lt;/mtable&gt;</span>
  <span class="nt">&lt;/mfenced&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>



<h2>Table Formatting</h2>

<p>MathML table elements define <a
href='http://www.w3.org/TR/MathML3/chapter3.html#presm.mtable.attrs'>several
attributes</a> for defining row and column size/alignment, but they are not
fully implemented in either <a href='http://mathjax.org'>MathJax</a> nor
FireFox&rsquo;s native MathML rendering engine. The ones that <em>do</em> work
(at least in MathJax) are listed below.</p>

<table class='multiline'>
	<thead>
		<tr>
			<th>Attribute</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><code>width</code></td>
			<td>The width of the table. Value should be a length measured in
			<code>px</code>, <code>pt</code>, <code>em</code>, etc.</td>
		</tr>
		<tr>
			<td><code>side</code></td>
			<td>The alignment for labels (discussed in <a
			href='tables.html#numbered-equations'>Numbered Equations</a>). Can be
			<code>left</code>, <code>right</code>, <code>leftoverlap</code>, or
			<code>rightoverlap</code>.</td>
		</tr>
		<tr>
			<td><code>frame</code></td>
			<td>The kind of border to add to the table. Can be <code>none</code>,
			<code>solid</code>, or <code>dashed</code>.</td>
		</tr>
		<tr>
			<td><code>framespacing</code></td>
			<td>The space to add between each row. Value should be two lengths
			specifying horizontal spacing and vertical spacing (e.g., <code>24px
			12px</code>).</td>
		</tr>
		<tr>
			<td><code>rowalign</code></td>
			<td>The vertical alignment of each cell with respect to other cells in the
			same row. Can be <code>top</code>, <code>bottom</code>,
			<code>center</code>, <code>baseline</code>, or <code>axis</code>.</td>
		</tr>
		<tr>
			<td><code>rowlines</code></td>
			<td>The kind of border to add between each row. Can be
			<code>none</code>, <code>solid</code>, or <code>dashed</code>.</td>
		</tr>
		<tr>
			<td><code>rowspacing</code></td>
			<td>The space to add between each row. Value should be a length.</td>
		</tr>
		<tr>
			<td><code>columnalign</code></td>
			<td>The horizontal alignment of each cell with respect to other cells in
			the same column. Can be <code>left</code>, <code>right</code>, or
			<code>center</code>.</td>
		</tr>
		<tr>
			<td><code>columnlines</code></td>
			<td>The kind of border to add between each column. Can be
			<code>none</code>, <code>solid</code>, or <code>dashed</code>.</td>
		</tr>
		<tr>
			<td><code>columnspacing</code></td>
			<td>The space to add between each column. Value should be a length.</td>
		</tr>
		<tr>
			<td><code>columnwidth</code></td>
			<td>The explicit width of each column. Value should be a length.</td>
		</tr>
		<tr>
			<td><code>equalrows</code></td>
			<td>Whether each row should be the same height. Can be <code>true</code>
			or <code>false</code>.</td>
		</tr>
		<tr>
			<td><code>equalcolumns</code></td>
			<td>Whether each column should be the same width. Can be <code>true</code>
			or <code>false</code>.</td>
		</tr>
	</tbody>
</table>

<p>Most of these attributes are self-explanatory, so I&rsquo;ll leave them for
you to explore. A few of them will be demonstrated in the upcoming
examples.</p>


<h2>Nested Tables</h2>

<p>Cutting a matrix down a row/column is a common way to create submatrices.
It&rsquo;s possible to draw this in MathML by combining the
<code>rowlines</code> and <code>columnlines</code> attributes from the previous
step with <em>nested</em> tables. For example, the following code splits a
matrix into two identity matrices.</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mfenced</span> <span class="na">open=</span><span class="s">&#39;[&#39;</span> <span class="na">close=</span><span class="s">&#39;]&#39;</span> <span class="na">separators=</span><span class="s">&#39;&#39;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;mtable</span> <span class="na">columnlines=</span><span class="s">&#39;solid&#39;</span><span class="nt">&gt;</span>
      <span class="nt">&lt;mtr&gt;</span>
        <span class="nt">&lt;mtd&gt;</span>
          <span class="c">&lt;!-- Left --&gt;</span>
          <span class="nt">&lt;mtable&gt;</span>
            <span class="nt">&lt;mtr&gt;</span>
              <span class="nt">&lt;mtd&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;&lt;/mtd&gt;</span>
              <span class="nt">&lt;mtd&gt;&lt;mn&gt;</span>0<span class="nt">&lt;/mn&gt;&lt;/mtd&gt;</span>
            <span class="nt">&lt;/mtr&gt;</span>
            <span class="nt">&lt;mtr&gt;</span>
              <span class="nt">&lt;mtd&gt;&lt;mn&gt;</span>0<span class="nt">&lt;/mn&gt;&lt;/mtd&gt;</span>
              <span class="nt">&lt;mtd&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;&lt;/mtd&gt;</span>
            <span class="nt">&lt;/mtr&gt;</span>
          <span class="nt">&lt;/mtable&gt;</span>
        <span class="nt">&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;</span>
          <span class="c">&lt;!-- Right --&gt;</span>
          <span class="nt">&lt;mtable&gt;</span>
            <span class="nt">&lt;mtr&gt;</span>
              <span class="nt">&lt;mtd&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;&lt;/mtd&gt;</span>
              <span class="nt">&lt;mtd&gt;&lt;mn&gt;</span>0<span class="nt">&lt;/mn&gt;&lt;/mtd&gt;</span>
            <span class="nt">&lt;/mtr&gt;</span>
            <span class="nt">&lt;mtr&gt;</span>
              <span class="nt">&lt;mtd&gt;&lt;mn&gt;</span>0<span class="nt">&lt;/mn&gt;&lt;/mtd&gt;</span>
              <span class="nt">&lt;mtd&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;&lt;/mtd&gt;</span>
            <span class="nt">&lt;/mtr&gt;</span>
          <span class="nt">&lt;/mtable&gt;</span>
        <span class="nt">&lt;/mtd&gt;</span>
      <span class="nt">&lt;/mtr&gt;</span>
    <span class="nt">&lt;/mtable&gt;</span>
  <span class="nt">&lt;/mfenced&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>This should display the following if you&rsquo;re using MathJax, but
unfortunately, <code>columnalign</code> is not implemented in FireFox&rsquo;s
native MathML rendering engine.</p>

<figure>
	<img style='max-width: 312px'
		 src='media/tables/nested-tables.png'
		 alt='[[1,0,1,0],[1,0,1,0]] split into two 2x2 identity matrices' />
</figure>

<p>That covers pretty much everything you&rsquo;ll ever have to do with
matrices in MathML. The remaining sections cover a few other applications of
<code>&lt;mtable&gt;</code>.</p>


<h2 id='numbered-equations'>Numbered Equations</h2>

<p>Tables can also be used to display numbered equations. Instead of
<code>&lt;mtr&gt;</code>, we can use an <code>&lt;mlabeledtr&gt;</code> tag.
It&rsquo;s pretty much what it sounds like&mdash;a <em>labeled</em> row. Its
first <code>&lt;mtd&gt;</code> acts as a label, and the remaining cells are
treated as if they were in a normal <code>&lt;mtr&gt;</code>.</p>

<p>So, to create a numbered equation, we simply put the number in the first
<code>&lt;mtd&gt;</code>, like so:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mtable</span> <span class="na">side=</span><span class="s">&#39;left&#39;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;mlabeledtr&gt;</span>
      <span class="nt">&lt;mtd&gt;&lt;mtext&gt;</span>(1.4)<span class="nt">&lt;/mtext&gt;&lt;/mtd&gt;</span>
      <span class="nt">&lt;mtd&gt;</span>
        <span class="nt">&lt;mi&gt;</span>E<span class="nt">&lt;/mi&gt;</span>
        <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
        <span class="nt">&lt;mrow&gt;</span>
          <span class="nt">&lt;mi&gt;</span>m<span class="nt">&lt;/mi&gt;</span>
          <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;InvisibleTimes;</span><span class="nt">&lt;/mo&gt;</span>
          <span class="nt">&lt;msup&gt;&lt;mi&gt;</span>c<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/msup&gt;</span>
        <span class="nt">&lt;/mrow&gt;</span>
      <span class="nt">&lt;/mtd&gt;</span>
    <span class="nt">&lt;/mlabeledtr&gt;</span>
  <span class="nt">&lt;/mtable&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Notice the <code>side</code> attribute defining the label alignment. This
should render as the following in MathJax, but, once again, FireFox&rsquo;s
native engine doesn&rsquo;t support <code>&lt;mlabeledtr&gt;</code>.</p>

<figure>
	<img style='max-width: 394px' src='media/tables/numbered-equations.png' />
</figure>

<p>If you&rsquo;re using an automated numbering scheme,
<code>&lt;mlabeledtr&gt;</code> is particularly useful, since your equation
numbers will always be in the same place: the first child of every
<code>&lt;mlabeledtr&gt;</code> element.</p>


<h2 id='piecewise-functions'>Piecewise Functions</h2>

<figure>
	<img style='max-width: 348px'
		 src='media/tables/piecewise-functions.png'
		 alt='f(x)=(x if x&le;0) or (x^2 if x&gt;0)' />
</figure>

<p>Defining piecewise functions with MathML tables is relatively
straightforward: just put each function and its domain in a separate
<code>&lt;mtr&gt;</code>. For example, the function above can be represented
as:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;mi&gt;</span>f<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;ApplyFunction;</span><span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mfenced&gt;&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;/mfenced&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mfenced</span> <span class="na">open=</span><span class="s">&#39;{&#39;</span> <span class="na">close=</span><span class="s">&#39;&#39;</span> <span class="na">separators=</span><span class="s">&#39;&#39;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;mtable</span> <span class="na">columnalign=</span><span class="s">&#39;left&#39;</span><span class="nt">&gt;</span>
      <span class="nt">&lt;mtr&gt;</span>
        <span class="nt">&lt;mtd&gt;&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;</span>
          <span class="nt">&lt;mo&gt;</span>if<span class="nt">&lt;/mo&gt;</span>
          <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span><span class="ni">&amp;le;</span><span class="nt">&lt;/mo&gt;&lt;mn&gt;</span>0<span class="nt">&lt;/mn&gt;</span>
        <span class="nt">&lt;/mtd&gt;</span>
      <span class="nt">&lt;/mtr&gt;</span>
      <span class="nt">&lt;mtr&gt;</span>
        <span class="nt">&lt;mtd&gt;</span>
          <span class="nt">&lt;msup&gt;&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/msup&gt;</span>
        <span class="nt">&lt;/mtd&gt;</span>
        <span class="nt">&lt;mtd&gt;</span>
          <span class="nt">&lt;mo&gt;</span>if<span class="nt">&lt;/mo&gt;</span>
          <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;mo&gt;</span><span class="ni">&amp;gt;</span><span class="nt">&lt;/mo&gt;&lt;mn&gt;</span>0<span class="nt">&lt;/mn&gt;</span>
        <span class="nt">&lt;/mtd&gt;</span>
      <span class="nt">&lt;/mtr&gt;</span>
    <span class="nt">&lt;/mtable&gt;</span>
  <span class="nt">&lt;/mfenced&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>First, note the <code>columnalign</code> attribute that makes sure the
functions line up correctly. Second, you&rsquo;ve probably noticed that the
<code>if</code> statement is encoded using an <code>&lt;mo&gt;</code> element,
which might be somewhat unexpected.</p>

<p>Remember that <code>&lt;mo&gt;</code> is designed to hold
operator-<em>like</em> content&mdash;it&rsquo;s not strictly limited to symbols
like <code>+</code> and <code>&minus;</code>. Many times, it&rsquo;s the most
appropriate container for logical statements like <code>if,</code> <code>there
exists,</code> <code>not all,</code> etc. We&rsquo;ll look at other ways to
combine text with symbols in the <a
href='advanced-formatting.html#combining-text-and-mathml'>Advanced
Formatting</a> module.


<h2>Summary</h2>

<p>This chapter looked at the MathML table elements, which proved very similar
to the native HTML table tags. By now, you should be comfortable with
<code>&lt;mtable&gt;</code>, <code>&lt;mtr&gt;</code>,
<code>&lt;mlabeledtr&gt;</code>, and <code>&lt;mtd&gt;</code>.</p>

<p>Remember, tables are just a framework for other MathML elements. They are a
generic way to control where elements appear in relation to one another, which
means they can be used for much more than just matrices and numbered equations.
With a bit of finagling, it&rsquo;s even possible to draw worked long division
problems and other elementary math exercises using
<code>&lt;mtable&gt;</code>.</p>

<figure>
	<img style='max-width: 148px'
		 src='media/tables/summary.png'
		 alt='Long division problem showing 172/4=43' />
</figure>

<p>Next, the final installment of <a href='index.html'>Ry&rsquo;s MathML
Tutorial</a> will teach you how to integrate mathematical expressions with
surrounding text. After that, you should have all the skills you need to write
in-depth articles, or even books, using HTML and MathML.</p>


<p class='sequential-nav'>
  <a href='advanced-formatting.html'>Continue to <em>Advanced Formatting</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>