<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's MathML Tutorial | Vectors And Functions | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="This module covers all of the mathematical notation
required for describing vector-valued functions, including font selection,
diacritics, and subscripts." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />
  <link rel="stylesheet" href="../../media/stix.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s MathML
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Vectors &amp; Functions</h1>

<p>This module covers all of the mathematical notation required for describing
vector-valued functions. We&rsquo;ll explore standard typesetting conventions,
including: font selection, diacritics, subscripts, and relevant HTML entities.
We&rsquo;ll also look at a more robust method of grouping expressions.</p>


<h2>Font Faces</h2>

<p>Vectors are typically written using a bold face, rather than the italic face
of real-valued variables or the roman face of scalar constants. MathML lets you
explicitly define the element type with the <code>mathvariant</code>
attribute:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mn</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>0<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;sdot;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>v<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mn&gt;</span>0<span class="nt">&lt;/mn&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Since the variables on the left side of this equation describe a vector, we
used the <code>mathvariant</code> attribute to display them in a bold font, but
the right side is real-valued, so we left it as the default roman face.</p>

<p>The rendered equation is shown below. Also notice the
<code>&amp;sdot;</code> entity, which represents the dot product operator.</p>

<figure>
	<img style='max-width: 167px'
		 src='media/vectors-and-functions/font-faces.png'
		 alt='0 &sdot; v = 0' />
</figure>

<p>The <code>mathvariant</code> attribute can be defined on any of the atomic
MathML elements: <code>&lt;mi&gt;</code>, <code>&lt;mn&gt;</code>, or
<code>&lt;mo&gt;</code>. Accepted values are listed below:</p>

<table>
	<tr>
		<td><code>normal</code></td>
		<td><code>bold</code></td>
	</tr>
	<tr>
		<td><code>italic</code></td>
		<td><code>bold-italic</code></td>
	</tr>
	<tr>
		<td><code>fraktur</code></td>
		<td><code>bold-fraktur</code></td>
	</tr>
	<tr>
		<td><code>script</code></td>
		<td><code>bold-script</code></td>
	</tr>
	<tr>
		<td><code>sans-serif</code></td>
		<td><code>bold-sans-serif</code></td>
	</tr>
	<tr>
		<td><code>sans-serif-italic</code></td>
		<td><code>sans-serif-bold-italic</code></td>
	</tr>
	<tr>
		<td><code>monospace</code></td>
		<td><code>double-struck</code></td>
	</tr>
	<tr>
		<td><code>initial</code></td>
		<td><code>tailed</code></td>
	</tr>
	<tr>
		<td><code>looped</code></td>
		<td><code>stretched</code></td>
	</tr>
</table>

<p>These font faces let us display all sorts of important mathematical
characters without the need for special entities. For example, we can easily
describe 2-dimensional Cartesian space using <code>double-struck</code>:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;msup&gt;</span>
    <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;double-struck&#39;</span><span class="nt">&gt;</span>R<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/msup&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Interestingly, these values are considered <em>logical</em> classes, even
though they&rsquo;re all typographic in nature. Once again, the close
relationship between mathematical notation and underlying meaning makes it hard
separate presentation from semantics.</p>

<h2>Overscripts</h2>

<p>An alternative notation for vectors is an arrow over the variable:</p>

<figure>
	<img style='max-width: 62px'
		 src='media/vectors-and-functions/overscripts.png'
		 alt='Arrow over variable v' />
</figure>

<p>We can create this with the <code>&lt;mover&gt;</code> element, which works
the same as <code>&lt;msup&gt;</code>, except the script is drawn directly over
the base instead of to the right:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mover&gt;</span>
    <span class="nt">&lt;mi&gt;</span>v<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;rarr;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;/mover&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>We used the <code>&amp;rarr;</code> entity to create a rightwards arrow, but
you can use any character for the overscript. For other useful overscripts like
line segments and double arrows, please refer to the <a
href='symbol-reference.html#geometry-entities'>Geometry</a> section of the
<em>Symbol Reference</em>.</p>


<h2 id='dotless-letters'>Dotless Letters</h2>

<p>It&rsquo;s important to remember that you can also use special entities for
the <em>base</em> of an overscript, too. One common case is the
&ldquo;i&#8209;hat&rdquo; and &ldquo;j&#8209;hat&rdquo; vectors. If you were to
use a normal <code>i</code> and <code>j</code> as the base of an
<code>&lt;mover&gt;</code> element, the circumflex accent would appear <em>on
top of</em> the dots:</p>

<figure>
	<img style='max-width: 451px'
		 src='media/vectors-and-functions/dotless-letters.png'
		 alt='3i+2j+5k' />
</figure>

<p>Instead, you should use the <code>&amp;imath;</code> and
<code>&amp;jmath;</code> entities for the base, which are the dotless&#8209;i
and dotless&#8209;j, respectively. An ASCII caret (<code>^</code>) can be used
for the circumflex accent, giving you the following expression:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;mover&gt;</span>
    <span class="nt">&lt;mi&gt;</span><span class="ni">&amp;imath;</span><span class="nt">&lt;/mi&gt;</span>    <span class="c">&lt;!-- Dotless i --&gt;</span>
    <span class="nt">&lt;mo&gt;</span>^<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;/mover&gt;</span>
  <span class="nt">&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;mover&gt;</span>
    <span class="nt">&lt;mi&gt;</span><span class="ni">&amp;jmath;</span><span class="nt">&lt;/mi&gt;</span>    <span class="c">&lt;!-- Dotless j --&gt;</span>
    <span class="nt">&lt;mo&gt;</span>^<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;/mover&gt;</span>
  <span class="nt">&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mn&gt;</span>5<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;mover&gt;</span>
    <span class="nt">&lt;mi&gt;</span>k<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mo&gt;</span>^<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;/mover&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>



<h2 id='subscripts'>Subscripts</h2>

<p>MathML subscripts work exactly like superscripts, except instead of
<code>&lt;msup&gt;</code>, you use <code>&lt;msub&gt;</code>. For example, a
generic three-dimensional vector can be marked up as the following.</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>v<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mo&gt;</span>(<span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;msub&gt;</span>
        <span class="nt">&lt;mi&gt;</span>v<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
      <span class="nt">&lt;/msub&gt;</span>
      <span class="nt">&lt;mo&gt;</span>,<span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;msub&gt;</span>
        <span class="nt">&lt;mi&gt;</span>v<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
      <span class="nt">&lt;/msub&gt;</span>
      <span class="nt">&lt;mo&gt;</span>,<span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;msub&gt;</span>
        <span class="nt">&lt;mi&gt;</span>v<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;</span>
      <span class="nt">&lt;/msub&gt;</span>
      <span class="nt">&lt;mo&gt;</span>)<span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>This should look something like:</p>

<figure>
	<img style='max-width: 247px'
		 src='media/vectors-and-functions/subscripts.png'
		 alt='v=(v1,v2,v3)' />
</figure>

<p>Like <code>&lt;msup&gt;</code> and <code>&lt;mover&gt;</code>,
<code>&lt;msub&gt;</code> requires precisely 2 arguments, so remember to wrap
sub-expressions in an <code>&lt;mrow&gt;</code>.</p>


<h2>Super/Subscript Pairs</h2>

<p>Of course, as soon as we have subscripted elements, we&rsquo;re going to
need superscript/subscript pairs. For this, MathML provides the
<code>&lt;msubsup&gt;</code> element, which takes three arguments: the base,
the subscript, and the superscript. For example, consider the conservation of
kinetic energy equation:</p>

<figure>
	<img style='max-width: 499px'
		 src='media/vectors-and-functions/supersubscripts.png'
		 alt='Conservation of kinetic energy equation' />
</figure>

<p>This can be marked up with the following (right side omitted for
brevity).</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;mfrac&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/mfrac&gt;</span>
    <span class="nt">&lt;msub&gt;&lt;mi&gt;</span>m<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;&lt;/msub&gt;</span>
    <span class="nt">&lt;msubsup&gt;</span>
      <span class="nt">&lt;mi&gt;</span>u<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
      <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/msubsup&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;mo&gt;</span>+<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;mfrac&gt;&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/mfrac&gt;</span>
    <span class="nt">&lt;msub&gt;&lt;mi&gt;</span>m<span class="nt">&lt;/mi&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;/msub&gt;</span>
    <span class="nt">&lt;msubsup&gt;</span>
      <span class="nt">&lt;mi&gt;</span>u<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
      <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/msubsup&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>




<h2>Arguments and Tuples</h2>

<p>The <a href='vectors-and-functions.html#subscripts'>Subscripts</a> section demonstrated a perfectly
acceptable way to define a sequence using <code>&lt;mo&gt;</code>; however,
MathML also includes the <code>&lt;mfenced&gt;</code> tag as a friendlier (and
more semantic) way to manage brackets:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>F<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mfenced&gt;</span>
    <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mi&gt;</span>y<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mi&gt;</span>z<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/mfenced&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>This should display a vector-valued function that takes three arguments:</p>

<figure>
	<img style='max-width: 171px'
		 src='media/vectors-and-functions/arguments-and-tuples-1.png'
		 alt='F(x,y,z)' />
</figure>

<p>Instead of manually adding parentheses and commas with
<code>&lt;mo&gt;</code>, we let <code>&lt;mfenced&gt;</code> do all the work
for us. As you might expect, MathML lets you customize the separator and
opening/closing characters via the <code>separators</code>, <code>open</code>,
and <code>close</code> attributes. For instance, if you&rsquo;d like to mark up
an ordered tuple with angled brackets, you could specify them like so:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mfenced</span> <span class="na">open=</span><span class="s">&#39;&amp;lang;&#39;</span> <span class="na">close=</span><span class="s">&#39;&amp;rang;&#39;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mi&gt;</span>y<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mi&gt;</span>z<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/mfenced&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Since there is no angled bracket character on a keyboard, we had to use the
<code>&amp;lang;</code> and <code>&amp;rang;</code> entities for the attribute
values, but you could use any character (e.g., curly braces or square
brackets).</p>

<figure>
	<img style='max-width: 147px'
		 src='media/vectors-and-functions/arguments-and-tuples-2.png'
		 alt='&lang;x,y,z&rang;' />
</figure>

<p>The <code>&lt;mfenced&gt;</code> tag is a natural way to describe sets and
intervals, and of course, it can also be used to wrap parenthetical groups.
Instead of relying on nested <code>&lt;mrow&gt;</code>&rsquo;s, sub-expressions
can be grouped with <code>&lt;mfenced&gt;</code> by specifying an empty
separator, as shown below.</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;msup&gt;</span>
    <span class="nt">&lt;mfenced</span> <span class="na">separators=</span><span class="s">&#39;&#39;</span><span class="nt">&gt;</span>
      <span class="nt">&lt;mrow&gt;&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;&lt;mi&gt;</span>x<span class="nt">&lt;/mi&gt;&lt;/mrow&gt;</span>
      <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;minus;</span><span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;mrow&gt;&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;&lt;mi&gt;</span>y<span class="nt">&lt;/mi&gt;&lt;/mrow&gt;</span>
    <span class="nt">&lt;/mfenced&gt;</span>
    <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/msup&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>While it&rsquo;s ok to put commas in an <code>&lt;mo&gt;</code> tag, you
should <em>never</em> use arithmetic operators like <code>&minus;</code> or
<code>+</code> as a value for the <code>separator</code> attribute.</p>


<h2>Vector Operators and Entities</h2>

<p>In addition to <code>&amp;sdot;</code>, vector mathematics uses a variety of
special symbols. A few of the most useful ones are listed below, and an
extensive list can be found in the <a href='symbol-reference.html'>Symbol
Reference</a>.</p>

<table>
	<thead>
		<tr>
			<th>Symbol</th>
			<th>Entity</th>
			<th>Hex</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class='center stix'>&#x22C5;</td>
			<td><code>&amp;sdot;</code></td>
			<td><code>&amp;#x22C5;</code></td>
			<td>Dot product</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2A2F;</td>
			<td><code>&amp;Cross;</code></td>
			<td><code>&amp;#x2A2F;</code></td>
			<td>Cross product</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2016;</td>
			<td><code>&amp;Vert;</code></td>
			<td><code>&amp;#x2016;</code></td>
			<td>Norm (magnitude) bars</td>
		</tr>
		<tr>
			<td class='center stix'>&#x27E8;</td>
			<td><code>&amp;lang;</code></td>
			<td><code>&amp;#x27E8;</code></td>
			<td>Left angle bracket</td>
		</tr>
		<tr>
			<td class='center stix'>&#x27E9;</td>
			<td><code>&amp;rang;</code></td>
			<td><code>&amp;#x27E9;</code></td>
			<td>Right angle bracket</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2218;</td>
			<td><code>&amp;compfn;</code></td>
			<td><code>&amp;#x2218;</code></td>
			<td>Function composition</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2192;</td>
			<td><code>&amp;rarr;</code></td>
			<td><code>&amp;#x2192;</code></td>
			<td>General function mapping</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2026;</td>
			<td><code>&amp;hellip;</code></td>
			<td><code>&amp;#x2026;</code></td>
			<td>Horizontal ellipsis</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2208;</td>
			<td><code>&amp;isin;</code></td>
			<td><code>&amp;#x2208;</code></td>
			<td>Member of set</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2286;</td>
			<td><code>&amp;sube;</code></td>
			<td><code>&amp;#x2286;</code></td>
			<td>Subset</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2282;</td>
			<td><code>&amp;sub;</code></td>
			<td><code>&amp;#x2282;</code></td>
			<td>Strict subset</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2287;</td>
			<td><code>&amp;supe;</code></td>
			<td><code>&amp;#x2287;</code></td>
			<td>Superset</td>
		</tr>
		<tr>
			<td class='center stix'>&#x2283;</td>
			<td><code>&amp;sup;</code></td>
			<td><code>&amp;#x2283;</code></td>
			<td>Strict superset</td>
		</tr>
	</tbody>
</table>

<p>These new symbols give us most of the tools we need to talk about vectors
and functions. For instance, we can now map function compositions from one
space to another:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>F<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;compfn;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>G<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span>:<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi&gt;</span>U<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;sube;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;msup&gt;</span>
    <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;double-struck&#39;</span><span class="nt">&gt;</span>R<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/msup&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;rarr;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;msup&gt;</span>
    <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;double-struck&#39;</span><span class="nt">&gt;</span>R<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/msup&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>This should look something like:</p>

<figure>
	<img style='max-width: 350px'
		 src='media/vectors-and-functions/operators-and-entities.png'
		 alt='G(F()):U&sube;R^3&rarr;R^2' />
</figure>


<h2 id='invisible-operators'>Invisible Operators</h2>

<p>Unfortunately, the special characters don&rsquo;t stop there. On top of the
standard mathematical symbols, there are also a handful of <em>invisible</em>
entities that you need to be aware of. All of these are operators, so they
should only appear in an <code>&lt;mo&gt;</code> tag.</p>

<table>
	<thead>
		<tr>
			<th>Entity</th>
			<th>Hex</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><code>&amp;ApplyFunction;</code></td>
			<td><code>&amp;#x2061;</code></td>
			<td>Function application</td>
		</tr>
		<tr>
			<td><code>&amp;InvisibleTimes;</code></td>
			<td><code>&amp;#x2062;</code></td>
			<td>Invisible multiplication</td>
		</tr>
		<tr>
			<td><code>&amp;InvisibleComma;</code></td>
			<td><code>&amp;#x2063;</code></td>
			<td>Invisible separator</td>
		</tr>
		<tr>
			<td><code>(n/a)</code></td>
			<td><code>&amp;#x2064;</code></td>
			<td>Invisible addition</td>
		</tr>
	</tbody>
</table>

<p>Even though you can&rsquo;t <em>see</em> these symbols, they still add
important information to a MathML expression. For example, consider two
interpretations of the ambiguous expression <code>x(y)</code>:</p>

<figure>
	<img style='max-width: 225px'
		 src='media/vectors-and-functions/invisible-operators-1.png'
		 alt='x&times;(y) vs. x(y)' />
</figure>

<p>Without an operator between <code>x</code> and <code>(y)</code>, it&rsquo;s
not clear whether <code>x</code> is a variable that should be multiplied by
<code>y</code> or a function call taking <code>y</code> as its argument. To
explicitly specify the latter, you would use the
<code>&amp;ApplyFunction;</code> operator:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;ApplyFunction;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mrow&gt;</span>
    <span class="nt">&lt;mo&gt;</span>(<span class="nt">&lt;/mo&gt;</span>
    <span class="nt">&lt;mi&gt;</span>b<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mo&gt;</span>)<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;/mrow&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Similarly, <code>&amp;InvisibleComma;</code> resolves subscript ambiguities
when referencing matrix elements (more on matrices in the <a
href='tables.html'>Tables</a> section):</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;msub&gt;</span>
    <span class="nt">&lt;mi&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mrow&gt;</span>
      <span class="nt">&lt;mn&gt;</span>1<span class="nt">&lt;/mn&gt;</span>
      <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;InvisibleComma;</span><span class="nt">&lt;/mo&gt;</span>
      <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;/mrow&gt;</span>
  <span class="nt">&lt;/msub&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>Finally, the invisible addition entity distinguishes a mixed number from an
algebraic multiplication:</p>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;mn&gt;</span>4<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;#x2064;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mfrac&gt;</span>
    <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/mfrac&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;ne;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mn&gt;</span>4<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;sdot;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mfrac&gt;</span>
    <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
    <span class="nt">&lt;mn&gt;</span>3<span class="nt">&lt;/mn&gt;</span>
  <span class="nt">&lt;/mfrac&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<figure>
	<img style='max-width: 212px'
		 src='media/vectors-and-functions/invisible-operators-2.png'
		 alt='4 and 2/3&ne;4&sdot;2/3' />
</figure>

<p>The extra semantic information encoded by invisible entities provides subtle
visual cues to the MathML renderer. For example, putting a line break between a
function name and its opening parenthesis has the potential to confuse readers.
These entities also have a significant impact on spoken renditions of an
expression (&ldquo;Four and two thirds&rdquo; vs. &ldquo;Four times two
over three&rdquo;).</p>

<p>It&rsquo;s good practice to include the above entities whenever applicable.
But, as we&rsquo;ve experienced for most of this tutorial, no serious harm will
come if you forget to add one here or there.</p>


<h2>Summary</h2>

<p>This section presented the typographic tools necessary to mark up vectors
and functions. We used <code>mathvariant</code> to set the font face of token
elements, <code>&lt;mover&gt;</code> to embellish symbols,
<code>&lt;msub&gt;</code> and <code>&lt;msubsup&gt;</code> to script vector
components, <code>&lt;mfenced&gt;</code> to specify argument lists, and several
HTML entities to represent both visible and invisible operators.</p>

<p>The vector projection formula sums up many of these new elements:</p>

<figure>
	<img style='max-width: 278px'
		 src='media/vectors-and-functions/summary.png'
		 alt='Vector projection equation' />
</figure>

<div class="highlight"><pre><span class="nt">&lt;math</span> <span class="na">xmlns=</span><span class="s">&#39;http://www.w3.org/1998/Math/MathML&#39;</span> <span class="na">display=</span><span class="s">&#39;block&#39;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;msub&gt;</span>
    <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;italic&#39;</span><span class="nt">&gt;</span>proj<span class="nt">&lt;/mi&gt;</span>
    <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>v<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;/msub&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;ApplyFunction;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
  <span class="nt">&lt;mo&gt;</span>=<span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mstyle</span> <span class="na">displaystyle=</span><span class="s">&#39;true&#39;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;mfrac&gt;</span>
      <span class="nt">&lt;mrow&gt;</span>
        <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>a<span class="nt">&lt;/mi&gt;</span>
        <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;sdot;</span><span class="nt">&lt;/mo&gt;</span>
        <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>v<span class="nt">&lt;/mi&gt;</span>
      <span class="nt">&lt;/mrow&gt;</span>
      <span class="nt">&lt;msup&gt;</span>
        <span class="nt">&lt;mfenced</span> <span class="na">open=</span><span class="s">&#39;&amp;Vert;&#39;</span> <span class="na">close=</span><span class="s">&#39;&amp;Vert;&#39;</span><span class="nt">&gt;</span>
          <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>v<span class="nt">&lt;/mi&gt;</span>
        <span class="nt">&lt;/mfenced&gt;</span>
        <span class="nt">&lt;mn&gt;</span>2<span class="nt">&lt;/mn&gt;</span>
      <span class="nt">&lt;/msup&gt;</span>
    <span class="nt">&lt;/mfrac&gt;</span>
  <span class="nt">&lt;/mstyle&gt;</span>
  <span class="nt">&lt;mo&gt;</span><span class="ni">&amp;InvisibleTimes;</span><span class="nt">&lt;/mo&gt;</span>
  <span class="nt">&lt;mi</span> <span class="na">mathvariant=</span><span class="s">&#39;bold&#39;</span><span class="nt">&gt;</span>v<span class="nt">&lt;/mi&gt;</span>
<span class="nt">&lt;/math&gt;</span>
</pre></div>


<p>The next section examines a few more typographic nuances related to
calculus, then we&rsquo;ll be ready to move on to more complicated layout
concerns.</p>

<p class='sequential-nav'>
  <a href='calculus.html'>Continue to <em>Calculus</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>