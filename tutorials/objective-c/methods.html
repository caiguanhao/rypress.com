<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Methods | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Objective-C
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Methods</h1>

<p>Methods represent the actions that an object knows how to perform.
They&rsquo;re the logical counterpart to properties, which represent an
object&rsquo;s data. You can think of methods as functions that are attached to
an object; however, they have a very different syntax.</p>

<p>In this module, we&rsquo;ll explore Objective-C&rsquo;s method naming
conventions, which can be frustrating for experienced developers coming from
C++, Java, Python, and similar languages. We&rsquo;ll also briefly discuss
Objective-C&rsquo;s <a href='methods.html#protected-private'>access modifiers</a> (or lack
thereof), and we&rsquo;ll learn how to refer to methods using <a
href='methods.html#selectors'>selectors</a>.</p>


<h2>Naming Conventions</h2>

<p>Objective-C methods are designed to remove all ambiguities from an API. As a
result, method names are horribly verbose, but undeniably descriptive.
Accomplishing this boils down to three simple rules for naming Objective-C
methods:</p>

<ol>
	<li>Don&rsquo;t abbreviate anything.</li>
	<li>Explicitly state parameter names in the method itself.</li>
	<li>Explicitly describe the return value of the method.</li>
</ol>

<p>Keep these rules in mind as you read through the following exemplary
interface for a <code>Car</code> class.</p>

<div class="highlight"><pre><span class="c1">// Car.h</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">Car</span> : <span class="nc">NSObject</span>

<span class="c1">// Accessors</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">BOOL</span><span class="p">)</span><span class="nf">isRunning</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">setRunning:</span><span class="p">(</span><span class="kt">BOOL</span><span class="p">)</span><span class="nv">running</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="p">)</span><span class="nf">model</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">setModel:</span><span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="p">)</span><span class="nv">model</span><span class="p">;</span>

<span class="c1">// Calculated values</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">double</span><span class="p">)</span><span class="nf">maximumSpeed</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">double</span><span class="p">)</span><span class="nf">maximumSpeedUsingLocale:</span><span class="p">(</span><span class="nb">NSLocale</span> <span class="o">*</span><span class="p">)</span><span class="nv">locale</span><span class="p">;</span>

<span class="c1">// Action methods</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">startEngine</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">driveForDistance:</span><span class="p">(</span><span class="kt">double</span><span class="p">)</span><span class="nv">theDistance</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">driveFromOrigin:</span><span class="p">(</span><span class="kt">id</span><span class="p">)</span><span class="nv">theOrigin</span> <span class="nf">toDestination:</span><span class="p">(</span><span class="kt">id</span><span class="p">)</span><span class="nv">theDestination</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">turnByAngle:</span><span class="p">(</span><span class="kt">double</span><span class="p">)</span><span class="nv">theAngle</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">turnToAngle:</span><span class="p">(</span><span class="kt">double</span><span class="p">)</span><span class="nv">theAngle</span><span class="p">;</span>

<span class="c1">// Error handling methods</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">BOOL</span><span class="p">)</span><span class="nf">loadPassenger:</span><span class="p">(</span><span class="kt">id</span><span class="p">)</span><span class="nv">aPassenger</span> <span class="nf">error:</span><span class="p">(</span><span class="nb">NSError</span> <span class="o">**</span><span class="p">)</span><span class="nv">error</span><span class="p">;</span>

<span class="c1">// Constructor methods</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">id</span><span class="p">)</span><span class="nf">initWithModel:</span><span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="p">)</span><span class="nv">aModel</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">id</span><span class="p">)</span><span class="nf">initWithModel:</span><span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="p">)</span><span class="nv">aModel</span> <span class="nf">mileage:</span><span class="p">(</span><span class="kt">double</span><span class="p">)</span><span class="nv">theMileage</span><span class="p">;</span>

<span class="c1">// Comparison methods</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">BOOL</span><span class="p">)</span><span class="nf">isEqualToCar:</span><span class="p">(</span><span class="n">Car</span> <span class="o">*</span><span class="p">)</span><span class="nv">anotherCar</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="n">Car</span> <span class="o">*</span><span class="p">)</span><span class="nf">fasterCar:</span><span class="p">(</span><span class="n">Car</span> <span class="o">*</span><span class="p">)</span><span class="nv">anotherCar</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="n">Car</span> <span class="o">*</span><span class="p">)</span><span class="nf">slowerCar:</span><span class="p">(</span><span class="n">Car</span> <span class="o">*</span><span class="p">)</span><span class="nv">anotherCar</span><span class="p">;</span>

<span class="c1">// Factory methods</span>
<span class="k">+</span> <span class="p">(</span><span class="n">Car</span> <span class="o">*</span><span class="p">)</span><span class="nf">car</span><span class="p">;</span>
<span class="k">+</span> <span class="p">(</span><span class="n">Car</span> <span class="o">*</span><span class="p">)</span><span class="nf">carWithModel:</span><span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="p">)</span><span class="nv">aModel</span><span class="p">;</span>
<span class="k">+</span> <span class="p">(</span><span class="n">Car</span> <span class="o">*</span><span class="p">)</span><span class="nf">carWithModel:</span><span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="p">)</span><span class="nv">aModel</span> <span class="nf">mileage:</span><span class="p">(</span><span class="kt">double</span><span class="p">)</span><span class="nv">theMileage</span><span class="p">;</span>

<span class="c1">// Singleton methods</span>
<span class="k">+</span> <span class="p">(</span><span class="n">Car</span> <span class="o">*</span><span class="p">)</span><span class="nf">sharedCar</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>



<h3>Abbreviations</h3>

<p>The easiest way to make methods understandable and predictable is to simply
avoid abbreviations. Most Objective-C programmer <em>expect</em> methods to be
written out in full, as this is the convention for all of the standard
frameworks, from <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/ObjC_classic/_index.html'>Foundation</a>
to <a
href='http://developer.apple.com/library/ios/#documentation/uikit/reference/UIKit_Framework/_index.html'>UIKit</a>.
This is why the above interface chose <code>maximumSpeed</code> over the more
concise <code>maxSpeed</code>.</p>


<h3>Parameters</h3>

<p>One of the clearest examples of Objective-C&rsquo;s verbose design
philosophy is in the naming conventions for method parameters. Whereas C++,
Java, and other Simula-style languages treat a method as a separate entity
from its parameters, an Objective-C method name actually contains the names of
all its parameters.</p>

<p>For example, to make a <code>Car</code> turn by 90 degrees in C++, you would
call something like <code>turn(90)</code>. But, Objective-C finds this too
ambiguous. It&rsquo;s not clear what kind of argument <code>turn()</code>
should take&mdash;it could be the new orientation, or it could be an angle by
which to increment your current orientation. Objective-C methods make this
explicit by describing the argument with a preposition. The resulting API
ensures the method will never be misinterpreted: it&rsquo;s either
<code>turnByAngle:90</code> or <code>turnToAngle:90</code>.</p>

<p>When a method accepts more than one parameter, the name of each argument is
also included in the method name. For instance, the above
<code>initWithModel:mileage:</code> method explicitly labels both the
<code>Model</code> argument and the <code>mileage</code> argument. As
we&rsquo;ll see in a moment, this makes for very informative method
invocations.</p>


<h3>Return Values</h3>

<p>You&rsquo;ll also notice that any methods returning a value explicitly state
what that value is. Sometimes this is as simple as stating the class of the
return type, but other times you&rsquo;ll need to prefix it with a descriptive
adjective.</p>

<p>For example, the factory methods start with <code>car</code>, which clearly
states that the method will return an instance of the <code>Car</code> class.
The comparison methods <code>fasterCar:</code> and <code>slowerCar:</code>
return the faster/slower of the receiver and the argument, and this is also
clearly expressed in the API. It&rsquo;s worth noting that singleton methods
should also follow this pattern (e.g., <code>sharedCar</code>), since the
conventional <code>instance</code> method name is ambiguous.</p>

<p>For more information about naming conventions, please visit the official <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Conceptual/CodingGuidelines/CodingGuidelines.html'>Cocoa Coding
Guidlines</a>.</p>


<h2>Calling Methods</h2>

<p>As discussed in the <a
href='classes.html#instantiation-and-usage'>Instantiation and Usage</a>
section, you invoke a method by placing the object and the desired method in
square brackets, separated by a space. Arguments are separated from the method
name using a colon:</p>

<div class="highlight"><pre><span class="p">[</span><span class="n">porsche</span> <span class="nf">initWithModel:</span><span class="s">@&quot;Porsche&quot;</span><span class="p">];</span>
</pre></div>


<p>When you have more than one parameter, it comes after the initial argument,
following the same pattern. Each parameter is paired with a label, separated
from other arguments by a space, and set off by a colon:</p>

<div class="highlight"><pre><span class="p">[</span><span class="n">porsche</span> <span class="nf">initWithModel:</span><span class="s">@&quot;Porsche&quot;</span> <span class="nf">mileage:</span><span class="mf">42000.0</span><span class="p">];</span>
</pre></div>


<p>It&rsquo;s a lot easier to see the purpose behind the above naming
conventions when you approach it from an invocation perspective. They make
method calls read more like a human language than a computer one. For example,
compare the following method call from Simula-style languages to
Objective-C&rsquo;s version:</p>

<div class="highlight"><pre><span class="c1">// Python/Java/C++</span>
<span class="n">porsche</span><span class="p">.</span><span class="n">drive</span><span class="p">(</span><span class="s">&quot;Home&quot;</span><span class="p">,</span> <span class="s">&quot;Airport&quot;</span><span class="p">);</span>

<span class="c1">// Objective-C</span>
<span class="p">[</span><span class="n">porsche</span> <span class="nf">driveFromOrigin:</span><span class="s">@&quot;Home&quot;</span> <span class="nf">toDestination:</span><span class="s">@&quot;Airport&quot;</span><span class="p">];</span>
</pre></div>


<p>It might be more to type, but that&rsquo;s why Xcode comes with such a nice
auto-completion feature. You&rsquo;ll appreciate the verbosity when you leave
your code for a few months and come back to fix a bug. This clarity also makes
it much easier to work with third-party libraries and to maintain large code
bases.</p>

<h3>Nested Method Calls</h3>

<p>Nesting method calls is a common pattern in Objective-C programs. It&rsquo;s
a natural way to pass the result of one call to another. Conceptually,
it&rsquo;s the exact same as chaining methods, but the square-bracket syntax
makes them look a little bit different:</p>

<div class="highlight"><pre><span class="c1">// JavaScript</span>
<span class="n">Car</span><span class="p">.</span><span class="n">alloc</span><span class="p">().</span><span class="n">init</span><span class="p">()</span>

<span class="c1">// Objective-C</span>
<span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
</pre></div>


<p>First, the <code>[Car alloc]</code> method is invoked, then the
<code>init</code> method is called on its return value.</p>


<h2 id='protected-private'>Protected and Private Methods</h2>

<p>There are no protected or private access modifiers for Objective-C
methods&mdash;they are all public. However, Objective-C does provide
alternative organizational paradigms that let you <em>emulate</em> these
features.</p>

<p>&ldquo;Private&rdquo; methods can be created by defining them in a
class&rsquo;s implementation file while omitting them from its interface file.
Since other objects (including subclasses) are never supposed to import the
implementation, these methods are effectively hidden from everything but the
class itself.</p>

<p>In lieu of protected methods, Objective-C provides <a
href='categories.html'>categories</a>, which are a more general solution for
isolating portions of an&nbsp;API. A full example can be found in <a
href='categories.html#protected-methods'>&ldquo;Protected&rdquo;
Methods</a>.</p>


<h2 id='selectors'>Selectors</h2>

<p>Selectors are Objective-C&rsquo;s internal representation of a method name.
They let you treat a method as an independent entity, enabling you to separate
an action from the object that needs to perform it. This is the basis of the
Target-Action design pattern, and it is an integral part of Objective-C&rsquo;s
dynamic typing system.</p>

<p>There are two ways to get the selector for a method name. The
<code>@selector()</code> directive lets you convert a source-code method name
to a selector, and the <code>NSSelectorFromString()</code> function lets you
convert a string to a selector (the latter is not as efficient). Both of these
return a special data type for selectors called <code>SEL</code>. You can use
<code>SEL</code> the exact same way as <code>BOOL</code>, <code>int</code>, or
any other data type.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="n">Car</span> <span class="o">*</span><span class="n">porsche</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="n">porsche</span><span class="p">.</span><span class="n">model</span> <span class="o">=</span> <span class="s">@&quot;Porsche 911 Carrera&quot;</span><span class="p">;</span>
        
        <span class="kt">SEL</span> <span class="n">stepOne</span> <span class="o">=</span> <span class="nb">NSSelectorFromString</span><span class="p">(</span><span class="s">@&quot;startEngine&quot;</span><span class="p">);</span>
        <span class="kt">SEL</span> <span class="n">stepTwo</span> <span class="o">=</span> <span class="k">@selector</span><span class="p">(</span><span class="nf">driveForDistance:</span><span class="p">);</span>
        <span class="kt">SEL</span> <span class="n">stepThree</span> <span class="o">=</span> <span class="k">@selector</span><span class="p">(</span><span class="nf">turnByAngle:quickly:</span><span class="p">);</span>
        
        <span class="c1">// This is the same as:</span>
        <span class="c1">// [porsche startEngine];</span>
        <span class="p">[</span><span class="n">porsche</span> <span class="nf">performSelector:</span><span class="n">stepOne</span><span class="p">];</span>

        <span class="c1">// This is the same as:</span>
        <span class="c1">// [porsche driveForDistance:[NSNumber numberWithDouble:5.7]];</span>
        <span class="p">[</span><span class="n">porsche</span> <span class="nf">performSelector:</span><span class="n">stepTwo</span>
                      <span class="nf">withObject:</span><span class="p">[</span><span class="nb">NSNumber</span> <span class="nf">numberWithDouble:</span><span class="mf">5.7</span><span class="p">]];</span>
        
        <span class="k">if</span> <span class="p">([</span><span class="n">porsche</span> <span class="nf">respondsToSelector:</span><span class="n">stepThree</span><span class="p">])</span> <span class="p">{</span>
            <span class="c1">// This is the same as:</span>
            <span class="c1">// [porsche turnByAngle:[NSNumber numberWithDouble:90.0]</span>
            <span class="c1">//              quickly:[NSNumber numberWithBool:YES]];</span>
            <span class="p">[</span><span class="n">porsche</span> <span class="nf">performSelector:</span><span class="n">stepThree</span>
                          <span class="nf">withObject:</span><span class="p">[</span><span class="nb">NSNumber</span> <span class="nf">numberWithDouble:</span><span class="mf">90.0</span><span class="p">]</span>
                          <span class="nf">withObject:</span><span class="p">[</span><span class="nb">NSNumber</span> <span class="nf">numberWithBool:</span><span class="kc">YES</span><span class="p">]];</span>
        <span class="p">}</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Step one: %@&quot;</span><span class="p">,</span> <span class="nb">NSStringFromSelector</span><span class="p">(</span><span class="n">stepOne</span><span class="p">))</span><span class="o">;</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>Selectors can be executed on an arbitrary object via
<code>performSelector:</code> and related methods. The <code>withObject:</code>
versions let you pass an argument (or two) to the method, but require those
arguments to be objects. If this is too limiting for your needs, please see <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Classes/NSInvocation_Class/Reference/Reference.html'><code>NSInvocation</code></a>
for advanced usage. When you&rsquo;re not sure if the target object defines the
method, you should use the <code>respondsToSelector:</code> check before trying
to perform the selector.</p>

<p>The technical name for a method is the primary method name concatenated with
all of its parameter labels, separated by colons. This makes colons an integral
aspect of method names, which can be confusing to Objective-C beginners. Their
usage can be summed up as follows: <em>parameterless methods never contain a
colon, while methods that take a parameter always end in a colon</em>.</p>

<p>A sample interface and implementation for the above <code>Car</code> class
is included below. Notice how we have to use <a
href='data-types/nsnumber.html'><code>NSNumber</code></a> instead of
<code>double</code> for the parameter types, since the
<code>performSelector:withObject:</code> method doesn&rsquo;t let you pass
primitive C data types.</p>

<div class="highlight"><pre><span class="c1">// Car.h</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">Car</span> : <span class="nc">NSObject</span>

<span class="k">@property</span> <span class="p">(</span><span class="k">copy</span><span class="p">)</span> <span class="nb">NSString</span> <span class="o">*</span><span class="n">model</span><span class="p">;</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">startEngine</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">driveForDistance:</span><span class="p">(</span><span class="nb">NSNumber</span> <span class="o">*</span><span class="p">)</span><span class="nv">theDistance</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">turnByAngle:</span><span class="p">(</span><span class="nb">NSNumber</span> <span class="o">*</span><span class="p">)</span><span class="nv">theAngle</span>
            <span class="nf">quickly:</span><span class="p">(</span><span class="nb">NSNumber</span> <span class="o">*</span><span class="p">)</span><span class="nv">useParkingBrake</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<div class="highlight"><pre><span class="c1">// Car.m</span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="k">@implementation</span> <span class="nc">Car</span>

<span class="k">@synthesize</span> <span class="n">model</span> <span class="o">=</span> <span class="n">_model</span><span class="p">;</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">startEngine</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Starting the %@&#39;s engine&quot;</span><span class="p">,</span> <span class="n">_model</span><span class="p">);</span>
<span class="p">}</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">driveForDistance:</span><span class="p">(</span><span class="nb">NSNumber</span> <span class="o">*</span><span class="p">)</span><span class="nv">theDistance</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The %@ just drove %0.1f miles&quot;</span><span class="p">,</span>
          <span class="n">_model</span><span class="p">,</span> <span class="p">[</span><span class="n">theDistance</span> <span class="nf">doubleValue</span><span class="p">]);</span>
<span class="p">}</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">turnByAngle:</span><span class="p">(</span><span class="nb">NSNumber</span> <span class="o">*</span><span class="p">)</span><span class="nv">theAngle</span>
            <span class="nf">quickly:</span><span class="p">(</span><span class="nb">NSNumber</span> <span class="o">*</span><span class="p">)</span><span class="nv">useParkingBrake</span> <span class="p">{</span>
    <span class="k">if</span> <span class="p">([</span><span class="n">useParkingBrake</span> <span class="nf">boolValue</span><span class="p">])</span> <span class="p">{</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The %@ is drifting around the corner!&quot;</span><span class="p">,</span> <span class="n">_model</span><span class="p">);</span>
    <span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The %@ is making a gentle %0.1f degree turn&quot;</span><span class="p">,</span>
              <span class="n">_model</span><span class="p">,</span> <span class="p">[</span><span class="n">theAngle</span> <span class="nf">doubleValue</span><span class="p">]);</span>
    <span class="p">}</span>
<span class="p">}</span>

<span class="k">@end</span>
</pre></div>



<h2>Summary</h2>

<p>This module explained the reasoning behind Objective-C&rsquo;s method naming
conventions. We also learned that there are no access modifiers for Objective-C
methods, and how to use <code>@selector</code> to dynamically invoke
methods.</p>

<p>Adapting to new conventions can be a frustrating process, and the dramatic
syntactic differences between Objective-C and other OOP languages won&rsquo;t
make your life any easier. Instead of forcing Objective-C into your existing
mental model of the programming universe, it helps to approach it in its own
right. Try designing a few simple programs before passing judgement on
Objective-C&rsquo;s verbose philosophy.</p>

<p>That covers the basics of object-oriented programming in Objective-C. The
rest of this tutorial explores more advanced ways to organize your code. First
on the list are protocols, which let you share an API between several
classes.</p>

<p class='sequential-nav'>
	<a href='protocols.html'>Continue to <em>Protocols</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>