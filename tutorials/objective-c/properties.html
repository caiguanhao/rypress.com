<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Properties | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Objective-C
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Properties</h1>

<p>An object&rsquo;s properties let other objects inspect or change its state.
But, in a well-designed object-oriented program, it&rsquo;s not possible to
directly access the internal state of an object. Instead, <strong>accessor
methods</strong> (getters and setters) are used as an abstraction for
interacting with the object&rsquo;s underlying data.</p>

<figure>
	<img style='max-width: 320px' src='media/properties/accessor-methods.png' />
	<figcaption>Interacting with a
property via accessor methods</figcaption>
</figure>

<p>The goal of the <code>@property</code> directive is to make it easy to
create and configure properties by automatically generating these accessor
methods. It allows you to specify the behavior of a public property on a
semantic level, and it takes care of the implementation details for you.</p>

<p>This module surveys the various attributes that let you alter getter and
setter behavior. Some of these attributes determine how properties handle their
underlying memory, so this module also serves as a practical introduction to
memory management in Objective-C. For a more detailed discussion, please refer
to <a href='memory-management.html'>Memory Management</a>.</p>


<h2>The @property Directive</h2>

<p>First, let&rsquo;s take at what&rsquo;s going on under the hood when we use
the <code>@property</code> directive. Consider the following interface for a
simple <code>Car</code> class and it&rsquo;s corresponding implementation.</p>

<div class="highlight"><pre><span class="c1">// Car.h</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">Car</span> : <span class="nc">NSObject</span>

<span class="k">@property</span> <span class="kt">BOOL</span> <span class="n">running</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<div class="highlight"><pre><span class="c1">// Car.m</span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="k">@implementation</span> <span class="nc">Car</span>

<span class="k">@synthesize</span> <span class="n">running</span> <span class="o">=</span> <span class="n">_running</span><span class="p">;</span>    <span class="c1">// Optional for Xcode 4.4+</span>

<span class="k">@end</span>
</pre></div>


<p>The compiler generates a getter and a setter for the <code>running</code>
property. The default naming convention is to use the property itself as the
getter, prefix it with <code>set</code> for the setter, and prefix it with an
underscore for the instance variable, like so:</p>

<div class="highlight"><pre><span class="k">-</span> <span class="p">(</span><span class="kt">BOOL</span><span class="p">)</span><span class="nf">running</span> <span class="p">{</span>
    <span class="k">return</span> <span class="n">_running</span><span class="p">;</span>
<span class="p">}</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">setRunning:</span><span class="p">(</span><span class="kt">BOOL</span><span class="p">)</span><span class="nv">newValue</span> <span class="p">{</span>
    <span class="n">_running</span> <span class="o">=</span> <span class="n">newValue</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>After declaring the property with the <code>@property</code> directive, you
can call these methods as if they were included in your class&rsquo;s interface
and implementation files. You can also override them in <code>Car.m</code> to
supply custom getter/setters, but this makes the <code>@synthesize</code>
directive mandatory. However, you should rarely need custom accessors, since
<code>@property</code> attributes let you do this on an abstract level.</p>

<p>Properties accessed via dot-notation get translated to the above accessor
methods behind the scenes, so the following <code>honda.running</code> code
actually calls <code>setRunning:</code> when you assign a value to it and the
<code>running</code> method when you read a value from it:</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="n">Car</span> <span class="o">*</span><span class="n">honda</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="n">honda</span><span class="p">.</span><span class="n">running</span> <span class="o">=</span> <span class="kc">YES</span><span class="p">;</span>                <span class="c1">// [honda setRunning:YES]</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%d&quot;</span><span class="p">,</span> <span class="n">honda</span><span class="p">.</span><span class="n">running</span><span class="p">);</span>        <span class="c1">// [honda running]</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>To change the behavior of the generated accessors, you can specify
attributes in parentheses after the <code>@property</code> directive. The rest
of this module introduces the available attributes.</p>


<h2>The getter= and setter= Attributes</h2>

<p>If you don&rsquo;t like <code>@property</code>&rsquo;s default naming
conventions, you can change the getter/setter method names with the
<code>getter=</code> and <code>setter=</code> attributes. A common use case for
this is Boolean properties, whose getters are conventionally prefixed with
<code>is</code>. Try changing the property declaration in <code>Car.h</code> to
the following.</p>

<div class="highlight"><pre><span class="k">@property</span> <span class="p">(</span><span class="k">getter</span><span class="o">=</span><span class="n">isRunning</span><span class="p">)</span> <span class="kt">BOOL</span> <span class="n">running</span><span class="p">;</span>
</pre></div>


<p>The generated accessors are now called <code>isRunning</code> and
<code>setRunning</code>. Note that the public property is still called
<code>running</code>, and this is what you should use for dot-notation:</p>

<div class="highlight"><pre><span class="n">Car</span> <span class="o">*</span><span class="n">honda</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
<span class="n">honda</span><span class="p">.</span><span class="n">running</span> <span class="o">=</span> <span class="kc">YES</span><span class="p">;</span>                <span class="c1">// [honda setRunning:YES]</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%d&quot;</span><span class="p">,</span> <span class="n">honda</span><span class="p">.</span><span class="n">running</span><span class="p">)</span><span class="o">;</span>        <span class="c1">// [honda isRunning]</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%d&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">honda</span> <span class="nf">running</span><span class="p">])</span><span class="o">;</span>      <span class="c1">// Error: method no longer exists</span>
</pre></div>


<p>These are the only attributes that take an argument (the accessor method
name)&mdash;all of the others are Boolean flags.</p>


<h2>The readonly Attribute</h2>

<p>The <code>readonly</code> attribute is an easy way to make a property
read-only. It omits the setter method and prevents assignment via dot-notation,
but the getter is unaffected. As an example, let&rsquo;s change our
<code>Car</code> interface to the following. Notice how you can specify
multiple attributes by separating them with a comma.</p>

<div class="highlight"><pre><span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">Car</span> : <span class="nc">NSObject</span>

<span class="k">@property</span> <span class="p">(</span><span class="k">getter</span><span class="o">=</span><span class="n">isRunning</span><span class="p">,</span> <span class="k">readonly</span><span class="p">)</span> <span class="kt">BOOL</span> <span class="n">running</span><span class="p">;</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">startEngine</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">stopEngine</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<p>Instead of letting other objects change the <code>running</code> property,
we&rsquo;ll set it internally via the <code>startEngine</code> and
<code>stopEngine</code> methods. The corresponding implementation can be found
below.</p>

<div class="highlight"><pre><span class="c1">// Car.m</span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="k">@implementation</span> <span class="nc">Car</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">startEngine</span> <span class="p">{</span>
    <span class="n">_running</span> <span class="o">=</span> <span class="kc">YES</span><span class="p">;</span>
<span class="p">}</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">stopEngine</span> <span class="p">{</span>
    <span class="n">_running</span> <span class="o">=</span> <span class="kc">NO</span><span class="p">;</span>
<span class="p">}</span>

<span class="k">@end</span>
</pre></div>


<p>Remember that <code>@property</code> also generates an instance variable for
us, which is why we can access <code>_running</code> without declaring it
anywhere (we can&rsquo;t use <code>self.running</code> here because the
property is read-only). Let&rsquo;s test this new <code>Car</code> class by
adding the following snippet to <code>main.m</code>.</p>

<div class="highlight"><pre><span class="n">Car</span> <span class="o">*</span><span class="n">honda</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
<span class="p">[</span><span class="n">honda</span> <span class="nf">startEngine</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Running: %d&quot;</span><span class="p">,</span> <span class="n">honda</span><span class="p">.</span><span class="n">running</span><span class="p">)</span><span class="o">;</span>
<span class="n">honda</span><span class="p">.</span><span class="n">running</span> <span class="o">=</span> <span class="kc">NO</span><span class="p">;</span>                      <span class="c1">// Error: read-only property</span>
</pre></div>


<p>Up until this point, properties have really just been convenient shortcuts
that let us avoid writing boilerplate getter and setter methods. This will not
be the case for the remaining attributes, which significantly alter the
behavior of their properties. They also only apply to properties that store
Objective-C objects (opposed to primitive C data types).</p>


<h2>The nonatomic Attribute</h2>

<p><strong>Atomicity</strong> has to do with how properties behave in a
threaded environment. When you have more than one thread, it&rsquo;s possible
for the setter and the getter to be called at the same time. This means that
the getter/setter can be interrupted by another operation, possibly resulting
in corrupted data.</p>

<p>Atomic properties lock the underlying object to prevent this from happening,
guaranteeing that the get or set operation is working with a complete value.
However, it&rsquo;s important to understand that this is only one aspect of
thread-safety&mdash;using atomic properties does not necessarily mean that your
code is thread-safe.</p>

<p>Properties declared with <code>@property</code> are atomic by default, and
this does incur some overhead. So, if you&rsquo;re not in a multi-threaded
environment (or you&rsquo;re implementing your own thread-safety), you&rsquo;ll
want to override this behavior with the <code>nonatomic</code> attribute, like
so:</p>

<div class="highlight"><pre><span class="k">@property</span> <span class="p">(</span><span class="k">nonatomic</span><span class="p">)</span> <span class="nb">NSString</span> <span class="o">*</span><span class="n">model</span><span class="p">;</span>
</pre></div>


<p>There is also a small, practical caveat with atomic properties. Accessors
for atomic properties must <em>both</em> be either generated or user-defined.
Only non-atomic properties let you mix-and-match synthesized accessors with
custom ones. You can see this by removing <code>nonatomic</code> from the above
code and adding a custom getter in <code>Car.m</code>.</p>


<h2 id='memory-management'>Memory Management</h2>

<p>In any OOP language, objects reside in the computer&rsquo;s memory,
and&mdash;especially on mobile devices&mdash;this is a scarce resource. The
goal of a memory management system is to make sure that programs don&rsquo;t
take up any more space than they need to by creating and destroying objects in
an efficient manner.</p>

<p>Many languages accomplish this through garbage collection, but Objective-C
uses a more efficient alternative called <strong>object ownership</strong>.
When you start interacting with an object, you&rsquo;re said to <em>own</em>
that object, which means that it&rsquo;s guaranteed to exist as long as
you&rsquo;re using it. When you&rsquo;re done with it, you relinquish
ownership, and&mdash;if the object has no other owners&mdash;the operating
system destroys the object and frees up the underlying memory.</p>

<figure>
	<img style='max-width: 400px' src='media/properties/object-ownership.png' />
	<figcaption>Destroying an object with
no owners</figcaption>
</figure>

<p>With the advent of <a
href='memory-management.html#automatic-reference-counting'>Automatic Reference
Counting</a>, the compiler manages all of your object ownership automatically.
For the most part, this means that you&rsquo;ll never to worry about how the
memory management system actually works. But, you do have to understand the
<code>strong</code>, <code>weak</code> and <code>copy</code> attributes of
<code>@property</code>, since they tell the compiler what kind of relationship
objects should have.</p>


<h3 id='the-strong-attribute'>The strong Attribute</h3>

<p>The <code>strong</code> attribute creates an owning relationship to whatever
object is assigned to the property. This is the implicit behavior for all
object properties, which is a safe default because it makes sure the value
exists as long as it&rsquo;s assigned to the property.</p>

<p>Let&rsquo;s take a look at how this works by creating another class called
<code>Person</code>. It&rsquo;s interface just declares a <code>name</code>
property:</p>

<div class="highlight"><pre><span class="c1">// Person.h</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">Person</span> : <span class="nc">NSObject</span>

<span class="k">@property</span> <span class="p">(</span><span class="k">nonatomic</span><span class="p">)</span> <span class="nb">NSString</span> <span class="o">*</span><span class="n">name</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<p>The implementation is shown below. It uses the default accessors generated
by <code>@property</code>. It also overrides <code>NSObject</code>&rsquo;s
<code>description</code> method, which returns the string representation of the
object.</p>

<div class="highlight"><pre><span class="c1">// Person.m</span>
<span class="cp">#import</span> <span class="l">&quot;Person.h&quot;</span><span class="cp"></span>

<span class="k">@implementation</span> <span class="nc">Person</span>

<span class="k">-</span> <span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="p">)</span><span class="nf">description</span> <span class="p">{</span>
    <span class="k">return</span> <span class="kc">self</span><span class="p">.</span><span class="n">name</span><span class="p">;</span>
<span class="p">}</span>

<span class="k">@end</span>
</pre></div>


<p>Next, let&rsquo;s add a <code>Person</code> property to the <code>Car</code>
class. Change <code>Car.h</code> to the following.</p>

<div class="highlight"><pre><span class="c1">// Car.h</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Person.h&quot;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">Car</span> : <span class="nc">NSObject</span>

<span class="k">@property</span> <span class="p">(</span><span class="k">nonatomic</span><span class="p">)</span> <span class="nb">NSString</span> <span class="o">*</span><span class="n">model</span><span class="p">;</span>
<span class="k">@property</span> <span class="p">(</span><span class="k">nonatomic</span><span class="p">,</span> <span class="k">strong</span><span class="p">)</span> <span class="n">Person</span> <span class="o">*</span><span class="n">driver</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<p>Then, consider the following iteration of <code>main.m</code>:</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Person.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="n">Person</span> <span class="o">*</span><span class="n">john</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Person</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="n">john</span><span class="p">.</span><span class="n">name</span> <span class="o">=</span> <span class="s">@&quot;John&quot;</span><span class="p">;</span>
        
        <span class="n">Car</span> <span class="o">*</span><span class="n">honda</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="n">honda</span><span class="p">.</span><span class="n">model</span> <span class="o">=</span> <span class="s">@&quot;Honda Civic&quot;</span><span class="p">;</span>
        <span class="n">honda</span><span class="p">.</span><span class="n">driver</span> <span class="o">=</span> <span class="n">john</span><span class="p">;</span>
        
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@ is driving the %@&quot;</span><span class="p">,</span> <span class="n">honda</span><span class="p">.</span><span class="n">driver</span><span class="p">,</span> <span class="n">honda</span><span class="p">.</span><span class="n">model</span><span class="p">);</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>Since <code>driver</code> is a strong relationship, the <code>honda</code>
object takes ownership of <code>john</code>. This ensures that it will be valid
as long as <code>honda</code> needs it.</p>

<h3 id='the-weak-attribute'>The weak Attribute</h3>

<p>Most of the time, the <code>strong</code> attribute is intuitively what you
want for object properties. However, strong references pose a problem if, for
example, we need a reference from <code>driver</code> back to the
<code>Car</code> object he&rsquo;s driving. First, let&rsquo;s add a
<code>car</code> property to <code>Person.h</code>:</p>

<div class="highlight"><pre><span class="c1">// Person.h</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="k">@class</span> <span class="nc">Car</span>;

<span class="k">@interface</span> <span class="nc">Person</span> : <span class="nc">NSObject</span>

<span class="k">@property</span> <span class="p">(</span><span class="k">nonatomic</span><span class="p">)</span> <span class="nb">NSString</span> <span class="o">*</span><span class="n">name</span><span class="p">;</span>
<span class="k">@property</span> <span class="p">(</span><span class="k">nonatomic</span><span class="p">,</span> <span class="k">strong</span><span class="p">)</span> <span class="n">Car</span> <span class="o">*</span><span class="n">car</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<p>The <code>@class Car</code> line is a forward declaration of the
<code>Car</code> class. It&rsquo;s like telling the compiler, &ldquo;Trust me,
the <code>Car</code> class exists, so don&rsquo;t try to find it right
now.&rdquo; We have to do this instead of our usual <code>#import</code>
statement because <code>Car</code> also imports <code>Person.h</code>, and we
would have an endless loop of imports. (Compilers don&rsquo;t like endless
loops.)</p>

<p>Next, add the following line to <code>main.m</code> right after the
<code>honda.driver</code> assignment:</p>

<div class="highlight"><pre><span class="n">honda</span><span class="p">.</span><span class="n">driver</span> <span class="o">=</span> <span class="n">john</span><span class="p">;</span>
<span class="n">john</span><span class="p">.</span><span class="n">car</span> <span class="o">=</span> <span class="n">honda</span><span class="p">;</span>       <span class="c1">// Add this line</span>
</pre></div>


<p>We now have an owning relationship from <code>honda</code> to
<code>john</code> and another owning relationship from <code>john</code> to
<code>honda</code>. This means that both objects will <em>always</em> be owned
by another object, so the memory management system won&rsquo;t be able to
destroy them even if they&rsquo;re no longer needed.</p>

<figure>
	<img style='max-width: 280px' src='media/properties/retain-cycle.png' />
	<figcaption>A retain cycle between the
<code>Car</code> and <code>Person</code> classes</figcaption>
</figure>

<p>This is called a <strong>retain cycle</strong>, which is a form of memory
leak, and memory leaks are bad. Fortunately, it&rsquo;s very easy to fix this
problem&mdash;just tell one of the properties to maintain a <strong>weak
reference</strong> to the other object. In <code>Person.h</code>, change the
<code>car</code> declaration to the following:</p>

<div class="highlight"><pre><span class="k">@property</span> <span class="p">(</span><span class="k">nonatomic</span><span class="p">,</span> <span class="k">weak</span><span class="p">)</span> <span class="n">Car</span> <span class="o">*</span><span class="n">car</span><span class="p">;</span>
</pre></div>


<p>The <code>weak</code> attribute creates a non-owning relationship to
<code>car</code>. This allows <code>john</code> to have a reference to
<code>honda</code> while avoiding a retain cycle. But, this also means that
there is a possibility that <code>honda</code> will be destroyed while
<code>john</code> still has a reference to it. Should this happen, the
<code>weak</code> attribute will conveniently set <code>car</code> to
<code>nil</code> in order to avoid a dangling pointer.</p>

<figure>
	<img style='max-width: 280px' src='media/properties/weak-reference.png' />
	<figcaption>A weak reference from the
<code>Person</code> class to <code>Car</code></figcaption>
</figure>

<p>A common use case for the <code>weak</code> attribute is parent-child data
structures. By convention, the parent object should maintain a strong reference
with it&rsquo;s children, and the children should store a weak reference back
to the parent. Weak references are also an inherent part of the delegate design
pattern.</p>

<p>The point to take away is that two objects should never have strong
references to each other. The <code>weak</code> attribute makes it possible to
maintain a cyclical relationship without creating a retain cycle.</p>


<h3 id='the-copy-attribute'>The copy Attribute</h3>

<p>The <code>copy</code> attribute is an alternative to <code>strong</code>.
Instead of taking ownership of the existing object, it creates a copy of
whatever you assign to the property, then takes ownership of that. Only objects
that conform to the <a
href='http://developer.apple.com/library/ios/#documentation/cocoa/Reference/Foundation/Protocols/NSCopying_Protocol/Reference/Reference.html%23/apple_ref/occ/intf/NSCopying'><code>NSCopying</code>
protocol</a> can use this attribute.</p>

<p>Properties that represent values (opposed to connections or relationships)
are good candidates for copying. For example, developers usually copy
<code>NSString</code> properties instead of strongly reference them:</p>

<div class="highlight"><pre><span class="c1">// Car.h</span>
<span class="k">@property</span> <span class="p">(</span><span class="k">nonatomic</span><span class="p">,</span> <span class="k">copy</span><span class="p">)</span> <span class="nb">NSString</span> <span class="o">*</span><span class="n">model</span><span class="p">;</span>
</pre></div>


<p>Now, <code>Car</code> will store a brand new instance of whatever value we
assign to <code>model</code>. If you&rsquo;re working with mutable values, this
has the added perk of freezing the object at whatever value it had when it was
assigned. This is demonstrated below:</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="n">Car</span> <span class="o">*</span><span class="n">honda</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="nb">NSMutableString</span> <span class="o">*</span><span class="n">model</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableString</span> <span class="nf">stringWithString:</span><span class="s">@&quot;Honda Civic&quot;</span><span class="p">];</span>
        <span class="n">honda</span><span class="p">.</span><span class="n">model</span> <span class="o">=</span> <span class="n">model</span><span class="p">;</span>
        
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">honda</span><span class="p">.</span><span class="n">model</span><span class="p">);</span>
        <span class="p">[</span><span class="n">model</span> <span class="nf">setString:</span><span class="s">@&quot;Nissa Versa&quot;</span><span class="p">];</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">honda</span><span class="p">.</span><span class="n">model</span><span class="p">);</span>            <span class="c1">// Still &quot;Honda Civic&quot;</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p><a
href='data-types/nsstring.html#nsmutablestring'><code>NSMutableString</code></a>
is a subclass of <code>NSString</code> that can be edited in-place. If the
<code>model</code> property didn&rsquo;t create a copy of the original
instance, we would be able to see the altered string (<code>Nissan
Versa</code>) in the second <code>NSLog()</code> output.</p>


<h2>Other Attributes</h2>

<p>The above <code>@property</code> attributes are all you should need for
modern Objective-C applications (iOS&nbsp;5+), but there are a few others that
you may encounter in older libraries or documentation.</p>

<h3 id='the-retain-attribute'>The retain Attribute</h3>

<p>The <code>retain</code> attribute is the <a
href='memory-management.html#manual-retain-release'>Manual Retain Release</a>
version of <code>strong</code>, and it has the exact same effect: claiming
ownership of assigned values. You shouldn&rsquo;t use this in an Automatic
Reference Counted environment.</p>

<h3>The unsafe_unretained Attribute</h3>

<p>Properties with the <code>unsafe_unretained</code> attribute behave similar
to <code>weak</code> properties, but they don&rsquo;t automatically set their
value to <code>nil</code> if the referenced object is destroyed. The only
reason you should need to use <code>unsafe_unretained</code> is to make your
class compatible with code that doesn&rsquo;t support the <code>weak</code>
property.</p>

<h3>The assign Attribute</h3>

<p>The <code>assign</code> attribute doesn&rsquo;t perform any kind of
memory-management call when assigning a new value to the property. This is the
default behavior for primitive data types, and it used to be a way to implement
weak references before iOS&nbsp;5. Like <code>retain</code>, you
shouldn&rsquo;t ever need to explicitly use this in modern applications.</p>

<h2>Summary</h2>

<p>This module presented the entire selection of <code>@property</code>
attributes, and we hope that you&rsquo;re feeling relatively comfortable
modifying the behavior of generated accessor methods. Remember that the goal of
all these attributes is to help you focus on <em>what</em> data needs to be
recorded by letting the compiler automatically determine <em>how</em>
it&rsquo;s represented. They are summarized below.</p>

<table class='multiline'>
<thead>
	<tr>
		<th>Attribute</th>
		<th>Description</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td><code>getter=</code></td>
		<td>Use a custom name for the getter method.</td>
	</tr>
	<tr>
		<td><code>setter=</code></td>
		<td>Use a custom name for the setter method.</td>
	</tr>
	<tr>
		<td><code>readonly</code></td>
		<td>Don&rsquo;t synthesize a setter method.</td>
	</tr>
	<tr>
		<td><code>nonatomic</code></td>
		<td>Don&rsquo;t guarantee the integrity of accessors in a
		multi-threaded environment. This is more efficient than the default
		atomic behavior.</td>
	</tr>
	<tr>
		<td><code>strong</code></td>
		<td>Create an owning relationship between the property and the assigned
		value. This is the default for object properties.</td>
	</tr>
	<tr>
		<td><code>weak</code></td>
		<td>Create a non-owning relationship between the property and the
		assigned value. Use this to prevent retain cycles.</td>
	</tr>
	<tr>
		<td><code>copy</code></td>
		<td>Create a copy of the assigned value instead of referencing the
		existing instance.</td>
	</tr>
</tbody>
</table>

<p>Now that we&rsquo;ve got properties out of the way, we can take an in-depth
look at the other half of Objective-C classes: methods. We&rsquo;ll explore
everything from the quirks behind their naming conventions to dynamic method
calls.</p>

<p class='sequential-nav'>
	<a href='methods.html'>Continue to <em>Methods</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>