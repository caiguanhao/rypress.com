<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Protocols | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Objective-C
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Protocols</h1>

<p>A protocol is a group of related properties and methods that can be
implemented by <em>any</em> class. They are more flexible than a normal class
interface, since they let you reuse a single API declaration in completely
unrelated classes. This makes it possible to represent horizontal relationships
on top of an existing class hierarchy.</p>

<figure>
	<img style='max-width: 470px' src='media/protocols/protocol-overview.png' />
	<figcaption>Unrelated classes adopting
the <code>StreetLegal</code> protocol</figcaption>
</figure>

<p>This is a relatively short module covering the basics behind working with
protocols. We&rsquo;ll also see how they fit into Objective-C&rsquo;s dynamic
typing system.</p>


<h2>Creating Protocols</h2>

<p>Like class interfaces, protocols typically reside in a <code>.h</code>
file. To add a protocol to your Xcode project, navigate to
<em>File&nbsp;&gt;&nbsp;New&gt;&nbsp;File&hellip;</em> or use the
<em>Cmd+N</em> shortcut. Select <em>Objective-C protocol</em> under the
<em>iOS&nbsp;&gt;&nbsp;Cocoa&nbsp;Touch</em> category.</p>

<figure>
	<img style='max-width: 500px' src='media/protocols/creating-a-protocol.png' />
	<figcaption>Creating a protocol in Xcode</figcaption>
</figure>

<p>In this module, we&rsquo;ll be working with a protocol called
<code>StreetLegal</code>. Enter this in the next window, and save it in the
project root.</p>

<p>Our protocol will capture the necessary behaviors of a street-legal vehicle.
Defining these characteristics in a protocol lets you apply them to arbitrary
objects instead of forcing them to inherit from the same abstract superclass. A
simple version of the <code>StreetLegal</code> protocol might look something
like the following:</p>

<div class="highlight"><pre><span class="c1">// StreetLegal.h</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="k">@protocol</span> <span class="nc">StreetLegal</span> <span class="nl">&lt;NSObject&gt;</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">signalStop</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">signalLeftTurn</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">signalRightTurn</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<p>Any objects that adopt this protocol are <em>guaranteed</em> to implement
all of the above methods. The <code>&lt;NSObject&gt;</code> after the protocol
name incorporates the <a
href='http://developer.apple.com/library/ios/#documentation/cocoa/reference/foundation/Protocols/NSObject_Protocol/Reference/NSObject.html'><code>NSObject</code>
protocol</a> (not to be confused with the <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Classes/nsobject_Class/Reference/Reference.html'><code>NSObject</code>
class</a>) into the <code>StreetLegal</code> protocol. That is, any objects
conforming to the <code>StreetLegal</code> protocol are required to conform to
the <code>NSObject</code> protocol, too.</p>


<h2>Adopting Protocols</h2>

<p>The above API can be adopted by a class by adding it in angled brackets
after the class/superclass name. Create a new classed called
<code>Bicycle</code> and change its header to the following. Note that you need
to import the protocol before you can use it.</p>

<div class="highlight"><pre><span class="c1">// Bicycle.h</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;StreetLegal.h&quot;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">Bicycle</span> : <span class="nc">NSObject</span> <span class="nl">&lt;StreetLegal&gt;</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">startPedaling</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">removeFrontWheel</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">lockToStructure:</span><span class="p">(</span><span class="kt">id</span><span class="p">)</span><span class="nv">theStructure</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<p>Adopting the protocol is like adding all of the methods in
<code>StreetLegal.h</code> to <code>Bicycle.h</code>. This would work the exact
same way even if <code>Bicycle</code> inherited from a different superclass.
Multiple protocols can be adopted by separating them with commas (e.g.,
<code>&lt;StreetLegal, SomeOtherProtocol&gt;</code>).</p>

<p>There&rsquo;s nothing special about the <code>Bicycle</code>
implementation&mdash;it just has to make sure all of the methods declared by
<code>Bicycle.h</code> <em>and</em> <code>StreetLegal.h</code> are
implemented:</p>

<div class="highlight"><pre><span class="c1">// Bicycle.m</span>
<span class="cp">#import</span> <span class="l">&quot;Bicycle.h&quot;</span><span class="cp"></span>

<span class="k">@implementation</span> <span class="nc">Bicycle</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">signalStop</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Bending left arm downwards&quot;</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">signalLeftTurn</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Extending left arm outwards&quot;</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">signalRightTurn</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Bending left arm upwards&quot;</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">startPedaling</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Here we go!&quot;</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">removeFrontWheel</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Front wheel is off.&quot;</span>
          <span class="s">&quot;Should probably replace that before pedaling...&quot;</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">lockToStructure:</span><span class="p">(</span><span class="kt">id</span><span class="p">)</span><span class="nv">theStructure</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Locked to structure. Don&#39;t forget the combination!&quot;</span><span class="p">);</span>
<span class="p">}</span>

<span class="k">@end</span>
</pre></div>


<p>Now, when you use the <code>Bicycle</code> class, you can assume it responds
to the API defined by the protocol. It&rsquo;s as though
<code>signalStop</code>, <code>signalLeftTurn</code>, and
<code>signalRightTurn</code> were declared in <code>Bicycle.h</code>:</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Bicycle.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="n">Bicycle</span> <span class="o">*</span><span class="n">bike</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Bicycle</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="p">[</span><span class="n">bike</span> <span class="nf">startPedaling</span><span class="p">];</span>
        <span class="p">[</span><span class="n">bike</span> <span class="nf">signalLeftTurn</span><span class="p">];</span>
        <span class="p">[</span><span class="n">bike</span> <span class="nf">signalStop</span><span class="p">];</span>
        <span class="p">[</span><span class="n">bike</span> <span class="nf">lockToStructure:</span><span class="kc">nil</span><span class="p">];</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>



<h2>Type Checking With Protocols</h2>

<p>Just like classes, protocols can be used to type check variables. To make
sure an object adopts a protocol, put the protocol name after the data type in
the variable declaration, as shown below. The next code snippet also assumes
that you have created a <code>Car</code> class that adopts the
<code>StreetLegal</code> protocol:</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Bicycle.h&quot;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;StreetLegal.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="kt">id</span> <span class="nl">&lt;StreetLegal&gt;</span> <span class="n">mysteryVehicle</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="p">[</span><span class="n">mysteryVehicle</span> <span class="nf">signalLeftTurn</span><span class="p">];</span>
        
        <span class="n">mysteryVehicle</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Bicycle</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="p">[</span><span class="n">mysteryVehicle</span> <span class="nf">signalLeftTurn</span><span class="p">];</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>It doesn&rsquo;t matter if <code>Car</code> and <code>Bicycle</code> inherit
from the same superclass&mdash;the fact that they both adopt the
<code>StreetLegal</code> protocol lets us store either of them in a variable
declared with <code>id &lt;StreetLegal&gt;</code>. This is an example of how
protocols can capture common functionality between unrelated classes.</p>

<p>Objects can also be checked against a protocol using the
<code>conformsToProtocol:</code> method defined by the <code>NSObject</code>
protocol. It takes a protocol object as an argument, which can be obtained via
the <code>@protocol()</code> directive. This works much like the <a
href='methods.html#selectors'><code>@selector()</code></a> directive, but you
pass the protocol name instead of a method name, like so:</p>

<div class="highlight"><pre><span class="k">if</span> <span class="p">([</span><span class="n">mysteryVehicle</span> <span class="nf">conformsToProtocol:</span><span class="k">@protocol</span><span class="p">(</span><span class="n">StreetLegal</span><span class="p">)])</span> <span class="p">{</span>
    <span class="p">[</span><span class="n">mysteryVehicle</span> <span class="nf">signalStop</span><span class="p">];</span>
    <span class="p">[</span><span class="n">mysteryVehicle</span> <span class="nf">signalLeftTurn</span><span class="p">];</span>
    <span class="p">[</span><span class="n">mysteryVehicle</span> <span class="nf">signalRightTurn</span><span class="p">];</span>
<span class="p">}</span>
</pre></div>


<p>Using protocols in this manner is like saying, &ldquo;Make sure this object
has this particular set of functionality.&rdquo; This is a very powerful tool
for dynamic typing, as it lets you use a well-defined API without worrying
about what kind of object you&rsquo;re dealing with.</p>


<h2>Protocols In The Real World</h2>

<p>A more realistic use case can be seen in your everyday iOS application
development. The entry point into any iOS app is an &ldquo;application
delegate&rdquo; object that handles the major events in a program&rsquo;s life
cycle. Instead of forcing the delegate to inherit from any particular
superclass, the <a
href='http://developer.apple.com/library/ios/#documentation/uikit/reference/UIKit_Framework/_index.html'>UIKit
Framework</a> just makes you adopt a protocol:</p>

<div class="highlight"><pre><span class="k">@interface</span> <span class="nc">YourAppDelegate</span> : <span class="nc">UIResponder</span> <span class="nl">&lt;UIApplicationDelegate&gt;</span>
</pre></div>


<p>As long as it responds to the methods defined by <a
href='http://developer.apple.com/library/ios/#documentation/uikit/reference/UIApplicationDelegate_Protocol/Reference/Reference.html%23/apple_ref/doc/uid/TP40006786'><code>UIApplicationDelegate</code></a>,
you can use <em>any</em> object as your application delegate. Implementing the
delegate design pattern through protocols instead of subclassing gives
developers much more leeway when it comes to organizing their applications.</p>


<h2>Summary</h2>

<p>In this module, we added another organizational tool to our collection.
Protocols are a way to abstract shared properties and methods into a dedicated
file. This helps reduce redundant code and lets you dynamically check if an
object supports an arbitrary set of functionality. You&rsquo;ll find many
protocols throughout the Cocoa frameworks.</p>

<p>The next module introduces categories, which are a flexible option for
modularizing classes and providing opt-in support for an API.</p>

<p class='sequential-nav'>
	<a href='categories.html'>Continue to <em>Categories</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>