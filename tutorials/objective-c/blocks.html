<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Blocks | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Objective-C
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Blocks</h1>

<p>Blocks are Objective-C&rsquo;s anonymous functions. They let you pass
arbitrary statements between objects as you would data, which is often more
intuitive than referencing functions defined elsewhere. In addition, blocks are
implemented as <em>closures</em>, making it trivial to capture the surrounding
state.</p>


<h2>Creating Blocks</h2>

<p>Blocks use all the same mechanics as normal <a
href='functions.html'>functions</a>. You can declare a block variable just like
you would declare a function, define the block as though you would implement a
function, and then call the block as if it were a function:</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="c1">// Declare the block variable</span>
        <span class="kt">double</span> <span class="p">(</span><span class="o">^</span><span class="n">distanceFromRateAndTime</span><span class="p">)(</span><span class="kt">double</span> <span class="n">rate</span><span class="p">,</span> <span class="kt">double</span> <span class="n">time</span><span class="p">);</span>
        
        <span class="c1">// Create and assign the block</span>
        <span class="n">distanceFromRateAndTime</span> <span class="o">=</span> <span class="o">^</span><span class="kt">double</span><span class="p">(</span><span class="kt">double</span> <span class="n">rate</span><span class="p">,</span> <span class="kt">double</span> <span class="n">time</span><span class="p">)</span> <span class="p">{</span>
            <span class="k">return</span> <span class="n">rate</span> <span class="o">*</span> <span class="n">time</span><span class="p">;</span>
        <span class="p">};</span>
        <span class="c1">// Call the block</span>
        <span class="kt">double</span> <span class="n">dx</span> <span class="o">=</span> <span class="n">distanceFromRateAndTime</span><span class="p">(</span><span class="mi">35</span><span class="p">,</span> <span class="mf">1.5</span><span class="p">);</span>
        
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;A car driving 35 mph will travel &quot;</span>
              <span class="s">@&quot;%.2f miles in 1.5 hours.&quot;</span><span class="p">,</span> <span class="n">dx</span><span class="p">);</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>The caret (<code>^</code>) is used to mark the
<code>distanceFromRateAndTime</code> variable as a block. Like a function
declaration, you need to include the return type and parameter types so the
compiler can enforce type safety. The <code>^</code> behaves in a similar
manner to the asterisk before a pointer (e.g., <code>int *aPointer</code>) in
that it is only required when <em>declaring</em> the block, after which you can
use it like a normal variable.</p>

<p>The block itself is essentially a function definition&mdash;without the
function name. The <code>^double(double rate, double time)</code> signature
begins a block literal that returns a <code>double</code> and has two
parameters that are also doubles (the return type can be omitted if desired).
Arbitrary statements can go inside the curly braces (<code>{}</code>), just
like a normal function.</p>

<p>After assigning the block literal to the
<code>distanceFromRateAndTime</code> variable, we can <em>call</em> that
variable as though it were a function.</p>


<h3>Parameterless Blocks</h3>

<p>If a block doesn&rsquo;t take any parameters, you can omit the argument list
in its entirety. And again, specifying the return type of a block literal is
always optional, so you can shorten the notation to <code>^ { ... }</code>:</p>

<div class="highlight"><pre><span class="kt">double</span> <span class="p">(</span><span class="o">^</span><span class="n">randomPercent</span><span class="p">)(</span><span class="kt">void</span><span class="p">)</span> <span class="o">=</span> <span class="o">^</span> <span class="p">{</span>
    <span class="k">return</span> <span class="p">(</span><span class="kt">double</span><span class="p">)</span><span class="nb">arc4random</span><span class="p">()</span> <span class="o">/</span> <span class="mi">4294967295</span><span class="p">;</span>
<span class="p">};</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Gas tank is %.1f%% full&quot;</span><span class="p">,</span>
      <span class="n">randomPercent</span><span class="p">()</span> <span class="o">*</span> <span class="mi">100</span><span class="p">);</span>
</pre></div>


<p>The built-in <a
href='https://developer.apple.com/library/mac/#documentation/Darwin/Reference/ManPages/man3/arc4random.3.html'><code>arc4random()</code></a>
function returns a random 32-bit integer. By dividing by the maximum possible
value of <code>arc4random()</code> (<code>4294967295</code>), we get a decimal
value between <code>0</code> and <code>1</code>.</p>

<p>So far, it might seem like blocks are just a complicated way of defining
functions. But, the fact that they&rsquo;re implemented as <em>closures</em>
opens the door to exciting new programming opportunities.</p>


<h2>Closures</h2>

<p>Inside of a block, you have access to same data as in a normal function:
local variables, parameters passed to the block, and global
variables/functions. But, blocks are implented as <strong>closures</strong>,
which means that you also have access to <strong>non-local variables</strong>.
Non-local variables are variables defined in the block&rsquo;s enclosing
lexical scope, but outside the block itself. For example,
<code>getFullCarName</code> can reference the <code>make</code> variable
defined before the block:</p>

<div class="highlight"><pre><span class="nb">NSString</span> <span class="o">*</span><span class="n">make</span> <span class="o">=</span> <span class="s">@&quot;Honda&quot;</span><span class="p">;</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="p">(</span><span class="o">^</span><span class="n">getFullCarName</span><span class="p">)(</span><span class="nb">NSString</span> <span class="o">*</span><span class="p">)</span> <span class="o">=</span> <span class="o">^</span><span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="n">model</span><span class="p">)</span> <span class="p">{</span>
    <span class="k">return</span> <span class="p">[</span><span class="n">make</span> <span class="nf">stringByAppendingFormat:</span><span class="s">@&quot; %@&quot;</span><span class="p">,</span> <span class="n">model</span><span class="p">];</span>
<span class="p">};</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">getFullCarName</span><span class="p">(</span><span class="s">@&quot;Accord&quot;</span><span class="p">))</span><span class="o">;</span>    <span class="c1">// Honda Accord</span>
</pre></div>


<p>Non-local variables are copied and stored with the block as
<code>const</code> variables, which means they are read-only. Trying to assign
a new value to the <code>make</code> variable from inside the block will throw
a compiler error.</p>

<figure>
	<img style='max-width: 330px' src='media/blocks/const-non-local-variables.png' />
	<figcaption>Accessing non-local
variables as <code>const</code> copies</figcaption>
</figure>

<p>The fact that non-local variables are copied as constants means that a block
doesn&rsquo;t just have <em>access</em> to non-local variables&mdash;it creates
a <em>snapshot</em> of them. Non-local variables are frozen at whatever value
they contain when the block is defined, and the block <em>always</em> uses that
value, even if the non-local variable changes later on in the program. Watch
what happens when we try to change the <code>make</code> variable after
creating the block:</p>

<div class="highlight"><pre><span class="nb">NSString</span> <span class="o">*</span><span class="n">make</span> <span class="o">=</span> <span class="s">@&quot;Honda&quot;</span><span class="p">;</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="p">(</span><span class="o">^</span><span class="n">getFullCarName</span><span class="p">)(</span><span class="nb">NSString</span> <span class="o">*</span><span class="p">)</span> <span class="o">=</span> <span class="o">^</span><span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="n">model</span><span class="p">)</span> <span class="p">{</span>
    <span class="k">return</span> <span class="p">[</span><span class="n">make</span> <span class="nf">stringByAppendingFormat:</span><span class="s">@&quot; %@&quot;</span><span class="p">,</span> <span class="n">model</span><span class="p">];</span>
<span class="p">};</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">getFullCarName</span><span class="p">(</span><span class="s">@&quot;Accord&quot;</span><span class="p">))</span><span class="o">;</span>    <span class="c1">// Honda Accord</span>

<span class="c1">// Try changing the non-local variable (it won&#39;t change the block)</span>
<span class="n">make</span> <span class="o">=</span> <span class="s">@&quot;Porsche&quot;</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">getFullCarName</span><span class="p">(</span><span class="s">@&quot;911 Turbo&quot;</span><span class="p">))</span><span class="o">;</span> <span class="c1">// Honda 911 Turbo</span>
</pre></div>


<p>Closures are an incredibly convenient way to work with the surrounding
state, as it eliminates the need to pass in extra values as
parameters&mdash;you simply use non-local variables as if they were defined in
the block itself.</p>


<h3>Mutable Non-Local Variables</h3>

<p>Freezing non-local variables as constant values is a safe default behavior
in that it prevents you from accidentally changing them from within the block;
however, there are occasions when this is desirable. You can override the
<code>const</code> copy behavior by declaring a non-local variable with the
<code>__block</code> storage modifier:</p>

<div class="highlight"><pre><span class="kt">__block</span> <span class="nb">NSString</span> <span class="o">*</span><span class="n">make</span> <span class="o">=</span> <span class="s">@&quot;Honda&quot;</span><span class="p">;</span>
</pre></div>


<p>This tells the block to capture the variable <em>by reference</em>, creating
a direct link between the <code>make</code> variable outside the block and the
one inside the block. You can now assign a new value to <code>make</code> from
outside the block, and it will be reflected in the block, and vice versa.</p>

<figure>
	<img style='max-width: 260px' src='media/blocks/mutable-non-local-variables.png' />
	<figcaption>Accessing non-local
variables by reference</figcaption>
</figure>

<p>Like <a href='functions.html#static-local-variables'><code>static</code>
local variables</a> in normal functions, the <code>__block</code> modifier
serves as a &ldquo;memory&rdquo; between multiple calls to a block. This makes
it possible to use blocks as generators. For example, the following snippet
creates a block that &ldquo;remembers&rdquo; the value of <code>i</code> over
subsequent invocations.</p>

<div class="highlight"><pre><span class="kt">__block</span> <span class="kt">int</span> <span class="n">i</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span>
<span class="kt">int</span> <span class="p">(</span><span class="o">^</span><span class="n">count</span><span class="p">)(</span><span class="kt">void</span><span class="p">)</span> <span class="o">=</span> <span class="o">^</span> <span class="p">{</span>
    <span class="n">i</span> <span class="o">+=</span> <span class="mi">1</span><span class="p">;</span>
    <span class="k">return</span> <span class="n">i</span><span class="p">;</span>
<span class="p">};</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%d&quot;</span><span class="p">,</span> <span class="n">count</span><span class="p">())</span><span class="o">;</span>    <span class="c1">// 1</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%d&quot;</span><span class="p">,</span> <span class="n">count</span><span class="p">())</span><span class="o">;</span>    <span class="c1">// 2</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%d&quot;</span><span class="p">,</span> <span class="n">count</span><span class="p">())</span><span class="o">;</span>    <span class="c1">// 3</span>
</pre></div>



<h2>Blocks as Method Parameters</h2>

<p>Storing blocks in variables is occasionally useful, but in the real world,
they&rsquo;re more likely to be used as method parameters. They solve the same
problem as function pointers, but the fact that they can be defined inline
makes the resulting code much easier to read.</p>

<p>For example, the following <code>Car</code> interface declares a method that
tallies the distance traveled by the car. Instead of forcing the caller to pass
a constant speed, it accepts a block that defines the car&rsquo;s speed as a
function of time.</p>

<div class="highlight"><pre><span class="c1">// Car.h</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">Car</span> : <span class="nc">NSObject</span>

<span class="k">@property</span> <span class="kt">double</span> <span class="n">odometer</span><span class="p">;</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">driveForDuration:</span><span class="p">(</span><span class="kt">double</span><span class="p">)</span><span class="nv">duration</span>
       <span class="nf">withVariableSpeed:</span><span class="p">(</span><span class="kt">double</span> <span class="p">(</span><span class="o">^</span><span class="p">)(</span><span class="kt">double</span> <span class="n">time</span><span class="p">))</span><span class="nv">speedFunction</span>
                   <span class="nf">steps:</span><span class="p">(</span><span class="kt">int</span><span class="p">)</span><span class="nv">numSteps</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<p>The data type for the block is <code>double (^)(double time)</code>, which
states that whatever block the caller passes to the method should return a
<code>double</code> and accept a single <code>double</code> parameter. Note
that this is almost the exact same syntax as the block variable declaration
discussed at the beginning of this module, but without the variable name.</p>

<p>The implementation can then call the block via <code>speedFunction</code>.
The following example uses a na&#239;ve right-handed Riemann sum to approximate
the distance traveled over <code>duration</code>. The <code>steps</code>
argument is used to let the caller determine the precision of the estimate.</p>

<div class="highlight"><pre><span class="c1">// Car.m</span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="k">@implementation</span> <span class="nc">Car</span>

<span class="k">@synthesize</span> <span class="n">odometer</span> <span class="o">=</span> <span class="n">_odometer</span><span class="p">;</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">driveForDuration:</span><span class="p">(</span><span class="kt">double</span><span class="p">)</span><span class="nv">duration</span>
       <span class="nf">withVariableSpeed:</span><span class="p">(</span><span class="kt">double</span> <span class="p">(</span><span class="o">^</span><span class="p">)(</span><span class="kt">double</span> <span class="n">time</span><span class="p">))</span><span class="nv">speedFunction</span>
                   <span class="nf">steps:</span><span class="p">(</span><span class="kt">int</span><span class="p">)</span><span class="nv">numSteps</span> <span class="p">{</span>
    <span class="kt">double</span> <span class="n">dt</span> <span class="o">=</span> <span class="n">duration</span> <span class="o">/</span> <span class="n">numSteps</span><span class="p">;</span>
    <span class="k">for</span> <span class="p">(</span><span class="kt">int</span> <span class="n">i</span><span class="o">=</span><span class="mi">1</span><span class="p">;</span> <span class="n">i</span><span class="o">&lt;=</span><span class="n">numSteps</span><span class="p">;</span> <span class="n">i</span><span class="o">++</span><span class="p">)</span> <span class="p">{</span>
        <span class="n">_odometer</span> <span class="o">+=</span> <span class="n">speedFunction</span><span class="p">(</span><span class="n">i</span><span class="o">*</span><span class="n">dt</span><span class="p">)</span> <span class="o">*</span> <span class="n">dt</span><span class="p">;</span>
    <span class="p">}</span>
<span class="p">}</span>

<span class="k">@end</span>
</pre></div>


<p>As you can see in the <code>main()</code> function included below, block
literals can be defined <em>within</em> a method invocation. While it might
take a second to parse the syntax, this is still much more intuitive than
creating a dedicated top-level function to define the
<code>withVariableSpeed</code> parameter.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="n">Car</span> <span class="o">*</span><span class="n">theCar</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        
        <span class="c1">// Drive for awhile with constant speed of 5.0 m/s</span>
        <span class="p">[</span><span class="n">theCar</span> <span class="nf">driveForDuration:</span><span class="mf">10.0</span>
               <span class="nf">withVariableSpeed:</span><span class="o">^</span><span class="p">(</span><span class="kt">double</span> <span class="n">time</span><span class="p">)</span> <span class="p">{</span>
                           <span class="k">return</span> <span class="mf">5.0</span><span class="p">;</span>
                       <span class="p">}</span> <span class="nf">steps:</span><span class="mi">100</span><span class="p">];</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The car has now driven %.2f meters&quot;</span><span class="p">,</span> <span class="n">theCar</span><span class="p">.</span><span class="n">odometer</span><span class="p">)</span><span class="o">;</span>
        
        <span class="c1">// Start accelerating at a rate of 1.0 m/s^2</span>
        <span class="p">[</span><span class="n">theCar</span> <span class="nf">driveForDuration:</span><span class="mf">10.0</span>
               <span class="nf">withVariableSpeed:</span><span class="o">^</span><span class="p">(</span><span class="kt">double</span> <span class="n">time</span><span class="p">)</span> <span class="p">{</span>
                           <span class="k">return</span> <span class="n">time</span> <span class="o">+</span> <span class="mf">5.0</span><span class="p">;</span>
                       <span class="p">}</span> <span class="nf">steps:</span><span class="mi">100</span><span class="p">];</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The car has now driven %.2f meters&quot;</span><span class="p">,</span> <span class="n">theCar</span><span class="p">.</span><span class="n">odometer</span><span class="p">)</span><span class="o">;</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>This is a simple example of the versatility of blocks, but the standard
frameworks are chock-full of other use cases. <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Classes/NSArray_Class/NSArray.html'><code>NSArray</code></a>
lets you sort elements with a block via the
<code>sortedArrayUsingComparator:</code> method, and <a
href='http://developer.apple.com/library/ios/#documentation/uikit/reference/uiview_class/uiview/uiview.html'><code>UIView</code></a>
uses a block to define the final state of an animation via the
<code>animateWithDuration:animations:</code> method. Blocks are also a powerful
tool for concurrent programming.</p>


<h2>Defining Block Types</h2>

<p>Since the syntax for block data types can quickly clutter up your method
declarations, it&rsquo;s often useful to <code>typedef</code> common block
signatures. For instance, the following code creates a new type called
<code>SpeedFunction</code> that we can use as a more semantic data type for the
<code>withVariableSpeed</code> argument.</p>

<div class="highlight"><pre><span class="c1">// Car.h</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="c1">// Define a new type for the block</span>
<span class="k">typedef</span> <span class="nf">double</span> <span class="p">(</span><span class="o">^</span><span class="kt">SpeedFunction</span><span class="p">)(</span><span class="kt">double</span><span class="p">)</span><span class="o">;</span>

<span class="k">@interface</span> <span class="nc">Car</span> : <span class="nc">NSObject</span>

<span class="k">@property</span> <span class="kt">double</span> <span class="n">odometer</span><span class="p">;</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">driveForDuration:</span><span class="p">(</span><span class="kt">double</span><span class="p">)</span><span class="nv">duration</span>
       <span class="nf">withVariableSpeed:</span><span class="p">(</span><span class="kt">SpeedFunction</span><span class="p">)</span><span class="nv">speedFunction</span>
                   <span class="nf">steps:</span><span class="p">(</span><span class="kt">int</span><span class="p">)</span><span class="nv">numSteps</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<p>Many of the standard Objective-C frameworks also use this technique (e.g.,
<a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Miscellaneous/Foundation_DataTypes/Reference/reference.html%23/apple_ref/doc/c_ref/NSComparator'><code>NSComparator</code></a>).</p>


<h2>Summary</h2>

<p>Blocks provide much the same functionality as C functions, but they are much
more intuitive to work with (after you get used to the syntax). The fact that
they can be defined inline makes it easy to use them inside of method calls,
and since they are closures, it&rsquo;s possible to capture the value of
surrounding variables with literally no additional effort.</p>

<p>The next module switches gears a little bit and delves into iOS&rsquo;s and
OS&nbsp;X&rsquo;s error-handling capabilities. We&rsquo;ll explore two
important classes for representing errors: <code>NSException</code> and
<code>NSError</code>.</p>

<p class='sequential-nav'>
	<a href='exceptions.html'>Continue to <em>Exceptions &amp; Errors</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>