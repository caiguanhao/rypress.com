<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Objective-C Data Types | Primitives | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../../media/style.css" />
  <link rel="icon" type="image/png" href="../../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../../media/single.css" />
  <link rel="stylesheet" href="../../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../../index.html'>RyPress</a></li><li><a href='../../../tutorials.html'>Tutorials</a></li><li><a href='../../../sponsors.html'>Sponsors</a></li><li><a href='../../../about.html'>About</a></li><li><a href='../../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='../data-types.html'><span>&lsaquo;</span> Back to <em>Objective-C Data Types</em></a>
</p>

<h1 class='back-heading'>C Primitives</h1>

<p>The vast majority of Objective-C&rsquo;s primitive data types are adopted
from C, although it does define a few of its own to facilitate its
object-oriented capabilities. The first part of this module provides a
practical introduction to C&rsquo;s data types, and the <a
href='primitives.html#objective-c'>second part</a> covers three more primitives that are
specific to Objective-C.</p>

<p>The examples in this module use <code>NSLog()</code> to inspect variables.
In order for them to display correctly, you need to use the correct <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Conceptual/Strings/Articles/formatSpecifiers.html'>format
specifier</a> in <code>NSLog()</code>&rsquo;s first argument. The various
specifiers for C primitives are presented alongside the data types themselves,
and all Objective-C objects can be displayed with the <code>%@</code>
specifier.</p>


<h2>The void Type</h2>

<p>The <code>void</code> type is C&rsquo;s empty data type. Its most common use
case is to specify the return type for <a
href='../functions.html'>functions</a> that don&rsquo;t return anything. For
example:</p>

<div class="highlight"><pre><span class="kt">void</span> <span class="nf">sayHello</span><span class="p">()</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;This function doesn&#39;t return anything&quot;</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>


<p>The <code>void</code> type is not to be confused with the <a
href='../c-basics.html#void-pointers'>void <em>pointer</em></a>. The former
indicates the <em>absence</em> of a value, while the latter represents
<em>any</em> value (well, any pointer, at least).</p>


<h2>Integer Types</h2>

<p>Integer data types are characterized by their size and whether they are
signed or unsigned. The <code>char</code> type is always 1 byte, but it&rsquo;s
very important to understand that the exact size of the other integer types is
implementation-dependent. Instead of being defined as an <em>absolute</em>
number of bytes, they are defined relative to each other.  The only guarantee
is that <code>short &lt;= int &lt;= long &lt;= long long</code>; however it is
possible to <a href='primitives.html#determining-type-sizes'>determine their exact sizes</a>
at runtime.</p>

<p>C was designed to work closely with the underlying architecture, and
different systems support different variable sizes. A relative definition
provides the flexibility to, for example, define <code>short</code>,
<code>int</code>, and <code>long</code> as the same number of bytes when the
target chipset can&rsquo;t differentiate between them.</p>

<div class="highlight"><pre><span class="kt">BOOL</span> <span class="n">isBool</span> <span class="o">=</span> <span class="kc">YES</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%d&quot;</span><span class="p">,</span> <span class="n">isBool</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">isBool</span> <span class="o">?</span> <span class="s">@&quot;YES&quot;</span> <span class="o">:</span> <span class="s">@&quot;NO&quot;</span><span class="p">)</span><span class="o">;</span>

<span class="kt">char</span> <span class="n">aChar</span> <span class="o">=</span> <span class="sc">&#39;a&#39;</span><span class="p">;</span>
<span class="kt">unsigned</span> <span class="kt">char</span> <span class="n">anUnsignedChar</span> <span class="o">=</span> <span class="mi">255</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The letter %c is ASCII number %hhd&quot;</span><span class="p">,</span> <span class="n">aChar</span><span class="p">,</span> <span class="n">aChar</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%hhu&quot;</span><span class="p">,</span> <span class="n">anUnsignedChar</span><span class="p">)</span><span class="o">;</span>   

<span class="kt">short</span> <span class="n">aShort</span> <span class="o">=</span> <span class="o">-</span><span class="mi">32768</span><span class="p">;</span>
<span class="kt">unsigned</span> <span class="kt">short</span> <span class="n">anUnsignedShort</span> <span class="o">=</span> <span class="mi">65535</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%hd&quot;</span><span class="p">,</span> <span class="n">aShort</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%hu&quot;</span><span class="p">,</span> <span class="n">anUnsignedShort</span><span class="p">)</span><span class="o">;</span>

<span class="kt">int</span> <span class="n">anInt</span> <span class="o">=</span> <span class="o">-</span><span class="mi">2147483648</span><span class="p">;</span>
<span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">anUnsignedInt</span> <span class="o">=</span> <span class="mi">4294967295</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%d&quot;</span><span class="p">,</span> <span class="n">anInt</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%u&quot;</span><span class="p">,</span> <span class="n">anUnsignedInt</span><span class="p">)</span><span class="o">;</span>

<span class="kt">long</span> <span class="n">aLong</span> <span class="o">=</span> <span class="o">-</span><span class="mi">9223372036854775808</span><span class="p">;</span>
<span class="kt">unsigned</span> <span class="kt">long</span> <span class="n">anUnsignedLong</span> <span class="o">=</span> <span class="mi">18446744073709551615</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%ld&quot;</span><span class="p">,</span> <span class="n">aLong</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%lu&quot;</span><span class="p">,</span> <span class="n">anUnsignedLong</span><span class="p">)</span><span class="o">;</span>

<span class="kt">long</span> <span class="kt">long</span> <span class="n">aLongLong</span> <span class="o">=</span> <span class="o">-</span><span class="mi">9223372036854775808</span><span class="p">;</span>
<span class="kt">unsigned</span> <span class="kt">long</span> <span class="kt">long</span> <span class="n">anUnsignedLongLong</span> <span class="o">=</span> <span class="mi">18446744073709551615</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%lld&quot;</span><span class="p">,</span> <span class="n">aLongLong</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%llu&quot;</span><span class="p">,</span> <span class="n">anUnsignedLongLong</span><span class="p">)</span><span class="o">;</span>
</pre></div>


<p>The <code>%d</code> and <code>%u</code> characters are the core specifiers
for displaying signed and unsigned integers, respectively. The <code>hh</code>,
<code>h</code>, <code>l</code> and <code>ll</code> characters are modifiers
that tell <code>NSLog()</code> to treat the associated integer as a
<code>char</code>, <code>short</code>, <code>long</code>, or <code>long
long</code>, respectively.</p>

<p>It&rsquo;s also worth noting that the <code>BOOL</code> type is actually
part of Objective-C, not C. Objective-C uses <code>YES</code> and
<code>NO</code> for its Boolean values instead of the <code>true</code> and
<code>false</code> macros used by C.</p>


<h3>Fixed-Width Integers</h3>

<p>While the basic types presented above are satisfactory for most purposes, it
is sometimes necessary to declare a variable that stores a specific number of
bytes. This is particularly relevant for algorithms like <a
href='https://developer.apple.com/library/mac/#documentation/Darwin/Reference/ManPages/man3/arc4random.3.html'><code>arc4random()</code></a>
that operate on a fixed-width integer.

<p>The <code>int&lt;n&gt;_t</code> data types allow you to represent signed and
unsigned integers that are exactly 1, 2, 4, or 8 bytes, and the
<code>int_least&lt;n&gt;_t</code> variants let you constrain the
<em>minimum</em> size of a variable without specifying an exact number of
bytes. In addition, <code>intmax_t</code> is an alias for the largest integer
type that the system can handle.</p>

<div class="highlight"><pre><span class="c1">// Exact integer types</span>
<span class="kt">int8_t</span> <span class="n">aOneByteInt</span> <span class="o">=</span> <span class="mi">127</span><span class="p">;</span>
<span class="kt">uint8_t</span> <span class="n">aOneByteUnsignedInt</span> <span class="o">=</span> <span class="mi">255</span><span class="p">;</span>
<span class="kt">int16_t</span> <span class="n">aTwoByteInt</span> <span class="o">=</span> <span class="mi">32767</span><span class="p">;</span>
<span class="kt">uint16_t</span> <span class="n">aTwoByteUnsignedInt</span> <span class="o">=</span> <span class="mi">65535</span><span class="p">;</span>
<span class="kt">int32_t</span> <span class="n">aFourByteInt</span> <span class="o">=</span> <span class="mi">2147483647</span><span class="p">;</span>
<span class="kt">uint32_t</span> <span class="n">aFourByteUnsignedInt</span> <span class="o">=</span> <span class="mi">4294967295</span><span class="p">;</span>
<span class="kt">int64_t</span> <span class="n">anEightByteInt</span> <span class="o">=</span> <span class="mi">9223372036854775807</span><span class="p">;</span>
<span class="kt">uint64_t</span> <span class="n">anEightByteUnsignedInt</span> <span class="o">=</span> <span class="mi">18446744073709551615</span><span class="p">;</span>

<span class="c1">// Minimum integer types</span>
<span class="kt">int_least8_t</span> <span class="n">aTinyInt</span> <span class="o">=</span> <span class="mi">127</span><span class="p">;</span>
<span class="kt">uint_least8_t</span> <span class="n">aTinyUnsignedInt</span> <span class="o">=</span> <span class="mi">255</span><span class="p">;</span>
<span class="kt">int_least16_t</span> <span class="n">aMediumInt</span> <span class="o">=</span> <span class="mi">32767</span><span class="p">;</span>
<span class="kt">uint_least16_t</span> <span class="n">aMediumUnsignedInt</span> <span class="o">=</span> <span class="mi">65535</span><span class="p">;</span>
<span class="kt">int_least32_t</span> <span class="n">aNormalInt</span> <span class="o">=</span> <span class="mi">2147483647</span><span class="p">;</span>
<span class="kt">uint_least32_t</span> <span class="n">aNormalUnsignedInt</span> <span class="o">=</span> <span class="mi">4294967295</span><span class="p">;</span>
<span class="kt">int_least64_t</span> <span class="n">aBigInt</span> <span class="o">=</span> <span class="mi">9223372036854775807</span><span class="p">;</span>
<span class="kt">uint_least64_t</span> <span class="n">aBigUnsignedInt</span> <span class="o">=</span> <span class="mi">18446744073709551615</span><span class="p">;</span>

<span class="c1">// The largest supported integer type</span>
<span class="kt">intmax_t</span> <span class="n">theBiggestInt</span> <span class="o">=</span> <span class="mi">9223372036854775807</span><span class="p">;</span>
<span class="kt">uintmax_t</span> <span class="n">theBiggestUnsignedInt</span> <span class="o">=</span> <span class="mi">18446744073709551615</span><span class="p">;</span>
</pre></div>



<h2>Floating-Point Types</h2>

<p>C provides three floating-point types. Like the integer data types, they are
defined as relative sizes, where <code>float &lt;= double &lt;= long
double</code>. Literal decimal values are represented as doubles&mdash;floats
must be explicitly marked with a trailing <code>f</code>, and long doubles must
be marked with an <code>L</code>, as shown below.</p>

<div class="highlight"><pre><span class="c1">// Single precision floating-point</span>
<span class="kt">float</span> <span class="n">aFloat</span> <span class="o">=</span> <span class="o">-</span><span class="mf">21.09f</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%f&quot;</span><span class="p">,</span> <span class="n">aFloat</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%8.2f&quot;</span><span class="p">,</span> <span class="n">aFloat</span><span class="p">)</span><span class="o">;</span>

<span class="c1">// Double precision floating-point</span>
<span class="kt">double</span> <span class="n">aDouble</span> <span class="o">=</span> <span class="o">-</span><span class="mf">21.09</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%8.2f&quot;</span><span class="p">,</span> <span class="n">aDouble</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%e&quot;</span><span class="p">,</span> <span class="n">aDouble</span><span class="p">)</span><span class="o">;</span>

<span class="c1">// Extended precision floating-point</span>
<span class="kt">long</span> <span class="kt">double</span> <span class="n">aLongDouble</span> <span class="o">=</span> <span class="o">-</span><span class="mf">21.09e8L</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%Lf&quot;</span><span class="p">,</span> <span class="n">aLongDouble</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%Le&quot;</span><span class="p">,</span> <span class="n">aLongDouble</span><span class="p">)</span><span class="o">;</span>
</pre></div>


<p>The <code>%f</code> format specifier is used to display floats and doubles
as decimal values, and the <code>%8.2f</code> syntax determines the padding and
the number of points after the decimal. In this case, we pad the output to fill
8 digits and display 2 decimal places. Alternatively, you can format the value
as scientific notation with the <code>%e</code> specifier. Long doubles require
the <code>L</code> modifier (similar to <code>hh</code>, <code>l</code>,
etc).</p>


<h2 id='determining-type-sizes'>Determining Type Sizes</h2>

<p>It&rsquo;s possible to determine the exact size of any data type by passing
it to the <code>sizeof()</code> function, which returns the number of bytes
used to represent the specified type. Running the following snippet is an easy
way to see the size of the basic data types on any given architecture.</p>

<div class="highlight"><pre><span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Size of char: %zu&quot;</span><span class="p">,</span> <span class="k">sizeof</span><span class="p">(</span><span class="kt">char</span><span class="p">))</span><span class="o">;</span>   <span class="c1">// This will always be 1</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Size of short: %zu&quot;</span><span class="p">,</span> <span class="k">sizeof</span><span class="p">(</span><span class="kt">short</span><span class="p">))</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Size of int: %zu&quot;</span><span class="p">,</span> <span class="k">sizeof</span><span class="p">(</span><span class="kt">int</span><span class="p">))</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Size of long: %zu&quot;</span><span class="p">,</span> <span class="k">sizeof</span><span class="p">(</span><span class="kt">long</span><span class="p">))</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Size of long long: %zu&quot;</span><span class="p">,</span> <span class="k">sizeof</span><span class="p">(</span><span class="kt">long</span> <span class="kt">long</span><span class="p">))</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Size of float: %zu&quot;</span><span class="p">,</span> <span class="k">sizeof</span><span class="p">(</span><span class="kt">float</span><span class="p">))</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Size of double: %zu&quot;</span><span class="p">,</span> <span class="k">sizeof</span><span class="p">(</span><span class="kt">double</span><span class="p">))</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Size of size_t: %zu&quot;</span><span class="p">,</span> <span class="k">sizeof</span><span class="p">(</span><span class="kt">size_t</span><span class="p">))</span><span class="o">;</span>
</pre></div>


<p>Note that <code>sizeof()</code> can also be used with an array, in which
case it returns the number of bytes used by the array. This presents a new
problem: the programmer has no idea which data type is required to store the
maximum size of an array. Instead of forcing you to guess, the
<code>sizeof()</code> function returns a special data type called
<code>size_t</code>. This is why we used the <code>%zu</code> format specifier
in the above example.</p>

<p>The <code>size_t</code> type is dedicated solely to representing
memory-related values, and it is guaranteed to be able to store the maximum
size of an array.  Aside from being the return type for <code>sizeof()</code>
and other memory utilities, this makes <code>size_t</code> an appropriate data
type for storing the indices of very large arrays. As with any other type, you
can pass it to <code>sizeof()</code> to get its exact size at runtime, as shown
in the above example.</p>

<p>If your Objective-C programs interact with a lot of C libraries,
you&rsquo;re likely to encounter the following application of
<code>sizeof()</code>:</p>

<div class="highlight"><pre><span class="kt">size_t</span> <span class="n">numberOfElements</span> <span class="o">=</span> <span class="k">sizeof</span><span class="p">(</span><span class="n">anArray</span><span class="p">)</span><span class="o">/</span><span class="k">sizeof</span><span class="p">(</span><span class="n">anArray</span><span class="p">[</span><span class="mi">0</span><span class="p">]);</span>
</pre></div>


<p>This is the canonical way to determine the number of elements in a
primitive C array. It simply divides the size of the array,
<code>sizeof(anArray)</code>, by the size of each
element, <code>sizeof(anArray[0])</code>.</p>


<h3>Limit Macros</h3>

<p>While it&rsquo;s trivial to determine the potential range of an integer type
once you know how how many bytes it is, C implementations provide convenient
macros for accessing the minimum and maximum values that each type can
represent:</p>

<div class="highlight"><pre><span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Smallest signed char: %d&quot;</span><span class="p">,</span> <span class="n">SCHAR_MIN</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Largest signed char: %d&quot;</span><span class="p">,</span> <span class="n">SCHAR_MAX</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Largest unsigned char: %u&quot;</span><span class="p">,</span> <span class="n">UCHAR_MAX</span><span class="p">)</span><span class="o">;</span>

<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Smallest signed short: %d&quot;</span><span class="p">,</span> <span class="n">SHRT_MIN</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Largest signed short: %d&quot;</span><span class="p">,</span> <span class="n">SHRT_MAX</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Largest unsigned short: %u&quot;</span><span class="p">,</span> <span class="n">USHRT_MAX</span><span class="p">)</span><span class="o">;</span>

<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Smallest signed int: %d&quot;</span><span class="p">,</span> <span class="n">INT_MIN</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Largest signed int: %d&quot;</span><span class="p">,</span> <span class="n">INT_MAX</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Largest unsigned int: %u&quot;</span><span class="p">,</span> <span class="n">UINT_MAX</span><span class="p">)</span><span class="o">;</span>

<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Smallest signed long: %ld&quot;</span><span class="p">,</span> <span class="n">LONG_MIN</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Largest signed long: %ld&quot;</span><span class="p">,</span> <span class="n">LONG_MAX</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Largest unsigned long: %lu&quot;</span><span class="p">,</span> <span class="n">ULONG_MAX</span><span class="p">)</span><span class="o">;</span>

<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Smallest signed long long: %lld&quot;</span><span class="p">,</span> <span class="n">LLONG_MIN</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Largest signed long long: %lld&quot;</span><span class="p">,</span> <span class="n">LLONG_MAX</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Largest unsigned long long: %llu&quot;</span><span class="p">,</span> <span class="n">ULLONG_MAX</span><span class="p">)</span><span class="o">;</span>

<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Smallest float: %e&quot;</span><span class="p">,</span> <span class="n">FLT_MIN</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Largest float: %e&quot;</span><span class="p">,</span> <span class="n">FLT_MAX</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Smallest double: %e&quot;</span><span class="p">,</span> <span class="n">DBL_MIN</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Largest double: %e&quot;</span><span class="p">,</span> <span class="n">DBL_MAX</span><span class="p">)</span><span class="o">;</span>

<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Largest possible array index: %llu&quot;</span><span class="p">,</span> <span class="n">SIZE_MAX</span><span class="p">)</span><span class="o">;</span>
</pre></div>


<p>The <code>SIZE_MAX</code> macro defines the maximum value that can be stored
in a <code>size_t</code> variable.</p>


<h2>Working With C Primitives</h2>

<p>This section takes a look at some common &ldquo;gotchas&ldquo; when working
with C&rsquo;s primitive data types. Keep in mind that these are merely brief
overviews of computational topics that often involve a great deal of
subtlety.</p>


<h3>Choosing an Integer Type</h3>

<p>The variety of integer types offered by C can make it hard to know which one
to use in any given situation, but the answer is quite simple:
use <code>int</code>&rsquo;s unless you have a compelling reason not to.</p>

<p>Traditionally, an <code>int</code> is defined to be the native word size of
the underlying architecture, so it&rsquo;s (generally) the most efficient
integer type. The only reason to use a <code>short</code> is when you want to
reduce the memory footprint of very large arrays (e.g., an OpenGL index
buffer). The <code>long</code> types should only be used when you need to store
values that don&rsquo;t fit into an <code>int</code>.</p>


<h3 id='integer-division'>Integer Division</h3>

<p>Like most programming languages, C differentiates between integer and
floating-point operations. If both operands are integers, the calculation
uses integer arithmetic, but if at least one of them is a floating-point type,
it uses floating-point arithmetic. This is important to keep in mind for
division:</p>

<div class="highlight"><pre><span class="kt">int</span> <span class="n">integerResult</span> <span class="o">=</span> <span class="mi">5</span> <span class="o">/</span> <span class="mi">4</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Integer division: %d&quot;</span><span class="p">,</span> <span class="n">integerResult</span><span class="p">)</span><span class="o">;</span>        <span class="c1">// 1</span>
<span class="kt">double</span> <span class="n">doubleResult</span> <span class="o">=</span> <span class="mf">5.0</span> <span class="o">/</span> <span class="mi">4</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Floating-point division: %f&quot;</span><span class="p">,</span> <span class="n">doubleResult</span><span class="p">)</span><span class="o">;</span>  <span class="c1">// 1.25</span>
</pre></div>


<p>Note that the decimal will always be truncated when dividing two
integers, so be sure to use (or cast to) a <code>float</code> or a
<code>double</code> if you need the remainder.</p>


<h3>Floating-Point Equality</h3>

<p>Floating-point numbers are inherently <em>not</em> precise, and certain
values simply cannot be represented as a floating-point value. For example, we
can inspect the imprecision of the number <code>0.1</code> by displaying
several decimal places:</p>

<div class="highlight"><pre><span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%.17f&quot;</span><span class="p">,</span> <span class="mf">.1</span><span class="p">)</span><span class="o">;</span>        <span class="c1">// 0.10000000000000001</span>
</pre></div>


<p>As you can see, <code>0.1</code> is not actually represented as
<code>0.1</code> by your computer. That extra <code>1</code> in the 17th digit
occurs because converting <code>1/10</code> to binary results in a repeating
decimal. Of course, a computer cannot store the infinitely many digits required
for the exact value, so this introduces a rounding error. The error gets
magnified when you start doing calculations with the value, resulting in
counterintuitive situations like the following:</p>

<div class="highlight"><pre><span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%.17f&quot;</span><span class="p">,</span> <span class="mf">4.2</span> <span class="o">-</span> <span class="mf">4.1</span><span class="p">)</span><span class="o">;</span> <span class="c1">// 0.10000000000000053</span>
<span class="k">if</span> <span class="p">(</span><span class="mf">4.2</span> <span class="o">-</span> <span class="mf">4.1</span> <span class="o">==</span> <span class="mf">.1</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;This math is perfect!&quot;</span><span class="p">);</span>
<span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
    <span class="c1">// You&#39;ll see this message</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;This math is just a tiny bit off...&quot;</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>


<p>The lesson here is: don&rsquo;t try to check if two floating-point values
are exactly equal, and definitely don&rsquo;t use a floating-point type to
store precision-sensitive data (e.g., monetary values). To represent exact
quantities, you should use the fixed-point <a
href='nsdecimalnumber.html'><code>NSDecimalNumber</code></a> class.</p>

<p>For a comprehensive discussion of the issues surrounding floating-point
math, please see <a
href='http://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html'>What
Every Computer Scientist Should Know About Floating-Point Arithmetic</a> by
David Goldberg.</p>



<h1 id='objective-c'>Objective-C Primitives</h1>

<p>In addition to the data types discussed above, Objective-C defines three of
its own primitive types: <code>id</code>, <code>Class</code>, and
<code>SEL</code>. These are the basis for Objective-C&rsquo;s dynamic typing
capabilities. This section also introduces the anomalous <code>NSInteger</code>
and <code>NSUInteger</code> types.</p>


<h2 id='id'>The id Type</h2>

<p>The <code>id</code> type is the generic type for all Objective-C objects.
You can think of it as the object-oriented version of C&rsquo;s void pointer.
And, like a void pointer, it can store a reference to <em>any</em> type of
object. The following example uses the same <code>id</code> variable to hold a
string and a dictionary.</p>

<div class="highlight"><pre><span class="kt">id</span> <span class="n">mysteryObject</span> <span class="o">=</span> <span class="s">@&quot;An NSString object&quot;</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">mysteryObject</span> <span class="nf">description</span><span class="p">])</span><span class="o">;</span>
<span class="n">mysteryObject</span> <span class="o">=</span> <span class="p">@{</span><span class="s">@&quot;model&quot;</span><span class="o">:</span> <span class="s">@&quot;Ford&quot;</span><span class="p">,</span> <span class="s">@&quot;year&quot;</span><span class="o">:</span> <span class="m">@1967</span><span class="p">};</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">mysteryObject</span> <span class="nf">description</span><span class="p">])</span><span class="o">;</span>
</pre></div>


<p>Recall that all Objective-C objects are referenced as pointers, so when they
are statically typed, they must be declared with pointer notation:
<code>NSString&nbsp;*mysteryObject</code>. However, the <code>id</code> type
automatically implies that the variable is a pointer, so this is not necessary:
<code>id&nbsp;mysteryObject</code> (without the asterisk).</p>


<h2>The Class Type</h2>

<p>Objective-C classes are represented as objects themselves, using a special
data type called <code>Class</code>. This lets you, for example, dynamically
check an object&rsquo;s type at runtime:</p>

<div class="highlight"><pre><span class="kt">Class</span> <span class="n">targetClass</span> <span class="o">=</span> <span class="p">[</span><span class="n">NSString</span> <span class="nf">class</span><span class="p">];</span>
<span class="kt">id</span> <span class="n">mysteryObject</span> <span class="o">=</span> <span class="s">@&quot;An NSString object&quot;</span><span class="p">;</span>
<span class="k">if</span> <span class="p">([</span><span class="n">mysteryObject</span> <span class="nf">isKindOfClass:</span><span class="n">targetClass</span><span class="p">])</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Yup! That&#39;s an instance of the target class&quot;</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>


<p>All classes implement a class-level method called <code>class</code> that
returns its associated class object (apologies for the redundant terminology).
This object can be used for introspection, which we see with the
<code>isKindOfClass:</code> method above.</p>

<h2>The SEL Type</h2>

<p>The <code>SEL</code> data type is used to store selectors, which are
Objective-C&rsquo;s internal representation of a method name. For example, the
following snippet stores a method called <code>sayHello</code> in the
<code>someMethod</code> variable. This variable could be used to dynamically
call a method at runtime.</p>

<div class="highlight"><pre><span class="kt">SEL</span> <span class="n">someMethod</span> <span class="o">=</span> <span class="k">@selector</span><span class="p">(</span><span class="n">sayHello</span><span class="p">);</span>
</pre></div>


<p>Please refer to the <a href='../methods.html#selectors'>Methods</a> module
for a thorough discussion of Objective-C&rsquo;s selectors.</p>


<h2>NSInteger and NSUInteger</h2>

<p>While they aren&rsquo;t technically native Objective-C types, this is a good
time to discuss the <a
href='http://developer.apple.com/library/mac/#documentation/cocoa/reference/foundation/ObjC_classic/_index.html'>Foundation
Framework</a>&rsquo;s custom integers, <code>NSInteger</code> and
<code>NSUInteger</code>. On 32-bit systems, these are defined to be 32-bit
signed/unsigned integers, respectively, and on 64-bit systems, they are 64-bit
integers. In other words, they are guaranteed to be the natural word size on
any given architecture.</p>

<p>The original purpose of these Apple-specific types was to facilitate the
transition from 32-bit architectures to 64-bit, but it&rsquo;s up to you
whether or not you want to use <code>NSInteger</code> over the basic types
(<code>int</code>, <code>long</code>, <code>long long</code>) and the
<code>int&lt;n&gt;_t</code> variants. A sensible convention is to use
<code>NSInteger</code> and <code>NSUInteger</code> when interacting with
Apple&rsquo;s APIs and use the standard C types for everything else.</p>

<p>Either way, it&rsquo;s still important to understand what
<code>NSInteger</code> and <code>NSUInteger</code> represent, as they are used
extensively in Foundation, <a
href='http://developer.apple.com/library/ios/#documentation/uikit/reference/UIKit_Framework/_index.html'>UIKit</a>,
and several other frameworks.</p>

<p class='sequential-nav'>
	<a href='nsnumber.html'>Continue to <em>NSNumber</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>