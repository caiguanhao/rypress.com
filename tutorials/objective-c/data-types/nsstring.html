<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Objective-C Data Types | NSString | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../../media/style.css" />
  <link rel="icon" type="image/png" href="../../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../../media/single.css" />
  <link rel="stylesheet" href="../../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../../index.html'>RyPress</a></li><li><a href='../../../tutorials.html'>Tutorials</a></li><li><a href='../../../sponsors.html'>Sponsors</a></li><li><a href='../../../about.html'>About</a></li><li><a href='../../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='../data-types.html'><span>&lsaquo;</span> Back to <em>Objective-C Data Types</em></a>
</p>

<h1 class='back-heading'>NSString</h1>

<p>As we&rsquo;ve already seen several times throughout this tutorial, the <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Classes/NSString_Class/Reference/NSString.html'><code>NSString</code></a>
class is the basic tool for representing text in an Objective-C application.
Aside from providing an object-oriented wrapper for strings,
<code>NSString</code> provides many powerful methods for searching and
manipulating its contents. It also comes with native Unicode support.</p>

<p>Like <code>NSNumber</code> and <code>NSDecimalNumber</code>,
<code>NSString</code> is an immutable type, so you cannot change it after
it&rsquo;s been instantiated. It does, however, have a mutable counterpart
called <code>NSMutableString</code>, which will be discussed at the <a
href='nsstring.html#nsmutablestring'>end of this module</a>.</p>


<h2>Creating Strings</h2>

<p>The most common way to create strings is using the literal <code>@"Some
String"</code> syntax, but the <code>stringWithFormat:</code> class method is
also useful for generating strings that are composed of variable values. It
takes the same kind of format string as <code>NSLog()</code>:</p>

<div class="highlight"><pre><span class="nb">NSString</span> <span class="o">*</span><span class="n">make</span> <span class="o">=</span> <span class="s">@&quot;Porsche&quot;</span><span class="p">;</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="n">model</span> <span class="o">=</span> <span class="s">@&quot;911&quot;</span><span class="p">;</span>
<span class="kt">int</span> <span class="n">year</span> <span class="o">=</span> <span class="mi">1968</span><span class="p">;</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="n">message</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSString</span> <span class="nf">stringWithFormat:</span><span class="s">@&quot;That&#39;s a %@ %@ from %d!&quot;</span><span class="p">,</span>
                     <span class="n">make</span><span class="p">,</span> <span class="n">model</span><span class="p">,</span> <span class="n">year</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">message</span><span class="p">)</span><span class="o">;</span>
</pre></div>


<p>Notice that we used the <code>@"%@"</code> format specifier in the
<code>NSLog()</code> call instead of passing the string directly with
<code>NSLog(message)</code>. Using a literal for the first argument of
<code>NSLog()</code> is a best practice, as it sidesteps potential bugs when
the string you want to display contains <code>%</code> signs. Think about what
would happen when <code>message = @"The tank is 50% full"</code>.</p>

<p><code>NSString</code> provides built-in support for Unicode, which means
that you can include UTF-8 characters directly in string literals. For
example, you can paste the following into Xcode and use it the same way as any
other <code>NSString</code> object.</p>

<div class="highlight"><pre><span class="nb">NSString</span> <span class="o">*</span><span class="n">make</span> <span class="o">=</span> <span class="s">@&quot;C&#xF4;te d&#39;Ivoire&quot;</span><span class="p">;</span>
</pre></div>



<h2>Enumerating Strings</h2>

<p>The two most basic <code>NSString</code> methods are <code>length</code> and
<code>characterAtIndex:</code>, which return the number of characters in the
string and the character at a given index, respectively. You probably
won&rsquo;t have to use these methods unless you&rsquo;re doing low-level
string manipulation, but they&rsquo;re still good to know:</p>

<div class="highlight"><pre><span class="nb">NSString</span> <span class="o">*</span><span class="n">make</span> <span class="o">=</span> <span class="s">@&quot;Porsche&quot;</span><span class="p">;</span>
<span class="k">for</span> <span class="p">(</span><span class="kt">int</span> <span class="n">i</span><span class="o">=</span><span class="mi">0</span><span class="p">;</span> <span class="n">i</span><span class="o">&lt;</span><span class="p">[</span><span class="n">make</span> <span class="nf">length</span><span class="p">];</span> <span class="n">i</span><span class="o">++</span><span class="p">)</span> <span class="p">{</span>
    <span class="kt">unichar</span> <span class="n">letter</span> <span class="o">=</span> <span class="p">[</span><span class="n">make</span> <span class="nf">characterAtIndex:</span><span class="n">i</span><span class="p">];</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%d: %hu&quot;</span><span class="p">,</span> <span class="n">i</span><span class="p">,</span> <span class="n">letter</span><span class="p">)</span><span class="o">;</span>
<span class="p">}</span>
</pre></div>


<p>As you can see, <code>characterAtIndex:</code> has a return type of
<code>unichar</code>, which is a <a
href='../c-basics.html#typedef'><code>typedef</code></a> for <code>unsigned
short</code>. This value represents the Unicode decimal number for the
character.</p>


<h2>Comparing Strings</h2>

<p>String comparisons present the same issues as <code>NSNumber</code>
comparisons. Instead of comparing <em>pointers</em> with the <code>==</code>
operator, you should always use the <code>isEqualToString:</code> method for a
more robust <em>value</em> comparison. The following example shows you how this
works, along with the useful <code>hasPrefix:</code> and
<code>hasSuffix:</code> methods for partial comparisons.</p>

<div class="highlight"><pre><span class="nb">NSString</span> <span class="o">*</span><span class="n">car</span> <span class="o">=</span> <span class="s">@&quot;Porsche Boxster&quot;</span><span class="p">;</span>
<span class="k">if</span> <span class="p">([</span><span class="n">car</span> <span class="nf">isEqualToString:</span><span class="s">@&quot;Porsche Boxster&quot;</span><span class="p">])</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;That car is a Porsche Boxster&quot;</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">if</span> <span class="p">([</span><span class="n">car</span> <span class="nf">hasPrefix:</span><span class="s">@&quot;Porsche&quot;</span><span class="p">])</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;That car is a Porsche of some sort&quot;</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">if</span> <span class="p">([</span><span class="n">car</span> <span class="nf">hasSuffix:</span><span class="s">@&quot;Carrera&quot;</span><span class="p">])</span> <span class="p">{</span>
    <span class="c1">// This won&#39;t execute</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;That car is a Carrera&quot;</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>


<p>And, just like <code>NSNumber</code>, <code>NSString</code> has a
<code>compare:</code> method that can be useful for alphabetically sorting
strings:</p>

<div class="highlight"><pre><span class="nb">NSString</span> <span class="o">*</span><span class="n">otherCar</span> <span class="o">=</span> <span class="s">@&quot;Ferrari&quot;</span><span class="p">;</span>
<span class="nb">NSComparisonResult</span> <span class="n">result</span> <span class="o">=</span> <span class="p">[</span><span class="n">car</span> <span class="nf">compare:</span><span class="n">otherCar</span><span class="p">];</span>
<span class="k">if</span> <span class="p">(</span><span class="n">result</span> <span class="o">==</span> <span class="nb">NSOrderedAscending</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The letter &#39;P&#39; comes before &#39;F&#39;&quot;</span><span class="p">);</span>
<span class="p">}</span> <span class="k">else</span> <span class="k">if</span> <span class="p">(</span><span class="n">result</span> <span class="o">==</span> <span class="nb">NSOrderedSame</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;We&#39;re comparing the same string&quot;</span><span class="p">);</span>
<span class="p">}</span> <span class="k">else</span> <span class="k">if</span> <span class="p">(</span><span class="n">result</span> <span class="o">==</span> <span class="nb">NSOrderedDescending</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The letter &#39;P&#39; comes after &#39;F&#39;&quot;</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>


<p>Note that this is a case-sensitive comparison, so uppercase letters will
always be <em>before</em> their lowercase counterparts. If you want to ignore
case, you can use the related <code>caseInsensitiveCompare:</code> method.</p>


<h2>Combining Strings</h2>

<p>The two methods presented below are a way to concatenate
<code>NSString</code> objects. But, remember that <code>NSString</code> is an
immutable type, so these methods actually return a <em>new</em> string and
leave the original arguments unchanged.</p>

<div class="highlight"><pre><span class="nb">NSString</span> <span class="o">*</span><span class="n">make</span> <span class="o">=</span> <span class="s">@&quot;Ferrari&quot;</span><span class="p">;</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="n">model</span> <span class="o">=</span> <span class="s">@&quot;458 Spider&quot;</span><span class="p">;</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="n">car</span> <span class="o">=</span> <span class="p">[</span><span class="n">make</span> <span class="nf">stringByAppendingString:</span><span class="n">model</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">car</span><span class="p">)</span><span class="o">;</span>        <span class="c1">// Ferrari458 Spider</span>
<span class="n">car</span> <span class="o">=</span> <span class="p">[</span><span class="n">make</span> <span class="nf">stringByAppendingFormat:</span><span class="s">@&quot; %@&quot;</span><span class="p">,</span> <span class="n">model</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">car</span><span class="p">)</span><span class="o">;</span>        <span class="c1">// Ferrari 458 Spider (note the space)</span>
</pre></div>



<h2>Searching Strings</h2>

<p><code>NSString</code>&rsquo;s search methods all return an
<code>NSRange</code> struct, which defines a <code>location</code> and a
<code>length</code> field. The <code>location</code> is the index of the
beginning of the match, and the <code>length</code> is the number of characters
in the match. If no match was found, <code>location</code> will contain
<code>NSNotFound</code>. For example, the following snippet searches for the
<code>Cabrio</code> substring.</p>

<div class="highlight"><pre><span class="nb">NSString</span> <span class="o">*</span><span class="n">car</span> <span class="o">=</span> <span class="s">@&quot;Maserati GranCabrio&quot;</span><span class="p">;</span>
<span class="nb">NSRange</span> <span class="n">searchResult</span> <span class="o">=</span> <span class="p">[</span><span class="n">car</span> <span class="nf">rangeOfString:</span><span class="s">@&quot;Cabrio&quot;</span><span class="p">];</span>
<span class="k">if</span> <span class="p">(</span><span class="n">searchResult</span><span class="p">.</span><span class="n">location</span> <span class="o">==</span> <span class="nb">NSNotFound</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Search string was not found&quot;</span><span class="p">);</span>
<span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;&#39;Cabrio&#39; starts at index %lu and is %lu characters long&quot;</span><span class="p">,</span>
          <span class="n">searchResult</span><span class="p">.</span><span class="n">location</span><span class="p">,</span>        <span class="c1">// 13</span>
          <span class="n">searchResult</span><span class="p">.</span><span class="n">length</span><span class="p">);</span>         <span class="c1">// 6</span>
<span class="p">}</span>
</pre></div>


<p>The next section shows you how to create <code>NSRange</code> structs from
scratch.</p>


<h2>Subdividing Strings</h2>

<p>You can divide an existing string by specifying the first/last index of the
desired substring. Again, since <code>NSString</code> is immutable, the
following methods return a new object, leaving the original intact.</p>

<div class="highlight"><pre><span class="nb">NSString</span> <span class="o">*</span><span class="n">car</span> <span class="o">=</span> <span class="s">@&quot;Maserati GranTurismo&quot;</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">car</span> <span class="nf">substringToIndex:</span><span class="mi">8</span><span class="p">])</span><span class="o">;</span>               <span class="c1">// Maserati</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">car</span> <span class="nf">substringFromIndex:</span><span class="mi">9</span><span class="p">])</span><span class="o">;</span>             <span class="c1">// GranTurismo</span>
<span class="nb">NSRange</span> <span class="n">range</span> <span class="o">=</span> <span class="nb">NSMakeRange</span><span class="p">(</span><span class="mi">9</span><span class="p">,</span> <span class="mi">4</span><span class="p">);</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">car</span> <span class="nf">substringWithRange:</span><span class="n">range</span><span class="p">])</span><span class="o">;</span>         <span class="c1">// Gran</span>
</pre></div>


<p>The global <code>NSMakeRange()</code> method creates an <code>NSRange</code>
struct. The first argument specifies the <code>location</code> field, and the
second defines the <code>length</code> field. The
<code>substringWithRange:</code> method interprets these as the first index of
the substring and the number of characters to include, respectively.</p>

<p>It&rsquo;s also possible to split a string into an <code>NSArray</code>
using the <code>componentsSeparatedByString:</code> method, as shown below.</p>

<div class="highlight"><pre><span class="nb">NSString</span> <span class="o">*</span><span class="n">models</span> <span class="o">=</span> <span class="s">@&quot;Porsche,Ferrari,Maserati&quot;</span><span class="p">;</span>
<span class="nb">NSArray</span> <span class="o">*</span><span class="n">modelsAsArray</span> <span class="o">=</span> <span class="p">[</span><span class="n">models</span> <span class="nf">componentsSeparatedByString:</span><span class="s">@&quot;,&quot;</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">modelsAsArray</span> <span class="nf">objectAtIndex:</span><span class="mi">1</span><span class="p">])</span><span class="o">;</span>        <span class="c1">// Ferrari</span>
</pre></div>



<h2>Replacing Substrings</h2>

<p>Replacing part of a string is just like subdividing a string, except you
provide a replacement along with the substring you&rsquo;re looking for. The
following snippet demonstrates the two most common substring replacement
methods.</p>

<div class="highlight"><pre><span class="nb">NSString</span> <span class="o">*</span><span class="n">elise</span> <span class="o">=</span> <span class="s">@&quot;Lotus Elise&quot;</span><span class="p">;</span>
<span class="nb">NSRange</span> <span class="n">range</span> <span class="o">=</span> <span class="nb">NSMakeRange</span><span class="p">(</span><span class="mi">6</span><span class="p">,</span> <span class="mi">5</span><span class="p">);</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="n">exige</span> <span class="o">=</span> <span class="p">[</span><span class="n">elise</span> <span class="nf">stringByReplacingCharactersInRange:</span><span class="n">range</span>
                                                 <span class="nf">withString:</span><span class="s">@&quot;Exige&quot;</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">exige</span><span class="p">)</span><span class="o">;</span>          <span class="c1">// Lotus Exige</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="n">evora</span> <span class="o">=</span> <span class="p">[</span><span class="n">exige</span> <span class="nf">stringByReplacingOccurrencesOfString:</span><span class="s">@&quot;Exige&quot;</span>
                                                   <span class="nf">withString:</span><span class="s">@&quot;Evora&quot;</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">evora</span><span class="p">)</span><span class="o">;</span>          <span class="c1">// Lotus Evora</span>
</pre></div>



<h2>Changing Case</h2>

<p>The <code>NSString</code> class also provides a few convenient methods for
changing the case of a string. This can be used to normalize user-submitted
values. As with all <code>NSString</code> manipulation methods, these return
new strings instead of changing the existing instance.</p>

<div class="highlight"><pre><span class="nb">NSString</span> <span class="o">*</span><span class="n">car</span> <span class="o">=</span> <span class="s">@&quot;lotUs beSpoKE&quot;</span><span class="p">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">car</span> <span class="nf">lowercaseString</span><span class="p">])</span><span class="o">;</span>      <span class="c1">// lotus bespoke</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">car</span> <span class="nf">uppercaseString</span><span class="p">])</span><span class="o">;</span>      <span class="c1">// LOTUS BESPOKE</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">car</span> <span class="nf">capitalizedString</span><span class="p">])</span><span class="o">;</span>    <span class="c1">// Lotus Bespoke</span>
</pre></div>



<h2>Numerical Conversions</h2>

<p><code>NSString</code> defines several conversion methods for interpreting
strings as primitive values. These are occasionally useful for very simple
string processing, but you should really consider <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Classes/NSScanner_Class/Reference/Reference.html'><code>NSScanner</code></a>
or <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Classes/NSNumberFormatter_Class/Reference/Reference.html'><code>NSNumberFormatter</code></a>
if you need a robust string-to-number conversion tool.</p>

<div class="highlight"><pre><span class="nb">NSString</span> <span class="o">*</span><span class="n">year</span> <span class="o">=</span> <span class="s">@&quot;2012&quot;</span><span class="p">;</span>
<span class="kt">BOOL</span> <span class="n">asBool</span> <span class="o">=</span> <span class="p">[</span><span class="n">year</span> <span class="nf">boolValue</span><span class="p">];</span>
<span class="kt">int</span> <span class="n">asInt</span> <span class="o">=</span> <span class="p">[</span><span class="n">year</span> <span class="nf">intValue</span><span class="p">];</span>
<span class="nb">NSInteger</span> <span class="n">asInteger</span> <span class="o">=</span> <span class="p">[</span><span class="n">year</span> <span class="nf">integerValue</span><span class="p">];</span>
<span class="kt">long</span> <span class="kt">long</span> <span class="n">asLongLong</span> <span class="o">=</span> <span class="p">[</span><span class="n">year</span> <span class="nf">longLongValue</span><span class="p">];</span>
<span class="kt">float</span> <span class="n">asFloat</span> <span class="o">=</span> <span class="p">[</span><span class="n">year</span> <span class="nf">floatValue</span><span class="p">];</span>
<span class="kt">double</span> <span class="n">asDouble</span> <span class="o">=</span> <span class="p">[</span><span class="n">year</span> <span class="nf">doubleValue</span><span class="p">];</span>
</pre></div>



<h1 id='nsmutablestring'>NSMutableString</h1>

<p>The <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Classes/NSMutableString_Class/Reference/Reference.html'><code>NSMutableString</code></a>
class is a mutable version of <code>NSString</code>. Unlike immutable strings,
it&rsquo;s possible to alter individual characters of a mutable string without
creating a brand new object. This makes <code>NSMutableString</code> the
preferred data structure when you&rsquo;re performing several small edits on
the same string.</p>

<p><code>NSMutableString</code> inherits from <code>NSString</code>, so aside
from the ability to manipulate it in place, you can use a mutable string just
like you would an immutable string. That is to say, the API discussed above
will still work with an <code>NSMutableString</code> instance, although methods
like <code>stringByAppendingString:</code> will still return a
<code>NSString</code> object&mdash;not an <code>NSMutableString</code>.</p>

<p>The remaining sections present several methods defined by the
<code>NSMutableString</code> class. You&rsquo;ll notice that the fundamental
workflow for mutable strings is different than that of immutable ones. Instead
of creating a new object and replacing the old value,
<code>NSMutableString</code> methods operate directly on the existing
instance.</p>


<h2>Creating Mutable Strings</h2>

<p>Mutable strings can be created through the <code>stringWithString:</code>
class method, which turns a literal string or an existing <code>NSString</code>
object into a mutable one:</p>

<div class="highlight"><pre><span class="nb">NSMutableString</span> <span class="o">*</span><span class="n">car</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableString</span> <span class="nf">stringWithString:</span><span class="s">@&quot;Porsche 911&quot;</span><span class="p">];</span>
</pre></div>


<p>After you&rsquo;ve created a mutable string, the <code>setString:</code>
method lets you assign a new value to the instance:</p>

<div class="highlight"><pre><span class="p">[</span><span class="n">car</span> <span class="nf">setString:</span><span class="s">@&quot;Porsche Boxster&quot;</span><span class="p">];</span>
</pre></div>


<p>Compare this to <code>NSString</code>, where you re-assign a new value to
the variable. With mutable strings, we don&rsquo;t change the instance
reference, but rather manipulate its contents through the mutable API.</p>


<h2>Expanding Mutable Strings</h2>

<p><code>NSMutableString</code> provides mutable alternatives to many of the
<code>NSString</code> manipulation methods discussed above. Again, the mutable
versions don&rsquo;t need to copy the resulting string into a new memory
location and return a new reference to it. Instead, they directly change the
existing object&rsquo;s underlying value.</p>

<div class="highlight"><pre><span class="nb">NSMutableString</span> <span class="o">*</span><span class="n">car</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableString</span> <span class="nf">stringWithCapacity:</span><span class="mi">20</span><span class="p">];</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="n">model</span> <span class="o">=</span> <span class="s">@&quot;458 Spider&quot;</span><span class="p">;</span>

<span class="p">[</span><span class="n">car</span> <span class="nf">setString:</span><span class="s">@&quot;Ferrari&quot;</span><span class="p">];</span>
<span class="p">[</span><span class="n">car</span> <span class="nf">appendString:</span><span class="n">model</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">car</span><span class="p">)</span><span class="o">;</span>                    <span class="c1">// Ferrari458 Spider</span>

<span class="p">[</span><span class="n">car</span> <span class="nf">setString:</span><span class="s">@&quot;Ferrari&quot;</span><span class="p">];</span>
<span class="p">[</span><span class="n">car</span> <span class="nf">appendFormat:</span><span class="s">@&quot; %@&quot;</span><span class="p">,</span> <span class="n">model</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">car</span><span class="p">)</span><span class="o">;</span>                    <span class="c1">// Ferrari 458 Spider</span>

<span class="p">[</span><span class="n">car</span> <span class="nf">setString:</span><span class="s">@&quot;Ferrari Spider&quot;</span><span class="p">];</span>
<span class="p">[</span><span class="n">car</span> <span class="nf">insertString:</span><span class="s">@&quot;458 &quot;</span> <span class="nf">atIndex:</span><span class="mi">8</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">car</span><span class="p">)</span><span class="o">;</span>                    <span class="c1">// Ferrari 458 Spider</span>
</pre></div>


<p>Also note that, like any well-designed Objective-C class, the method names
of <code>NSString</code> and <code>NSMutableString</code> reflect exactly what
they do. The former creates and returns a brand new string, so it uses names
like <code>stringByAppendingString:</code>. On the other hand, the latter
operates on the object itself, so it uses verbs like
<code>appendString:</code>.</p>


<h2>Replacing/Deleting Substrings</h2>

<p>It&rsquo;s possible to replace or delete substrings via the
<code>replaceCharactersInRange:withString:</code> and
<code>deleteCharactersInRange:</code> methods, as shown below.</p>

<div class="highlight"><pre><span class="nb">NSMutableString</span> <span class="o">*</span><span class="n">car</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableString</span> <span class="nf">stringWithCapacity:</span><span class="mi">20</span><span class="p">];</span>
<span class="p">[</span><span class="n">car</span> <span class="nf">setString:</span><span class="s">@&quot;Lotus Elise&quot;</span><span class="p">];</span>
<span class="p">[</span><span class="n">car</span> <span class="nf">replaceCharactersInRange:</span><span class="nb">NSMakeRange</span><span class="p">(</span><span class="mi">6</span><span class="p">,</span> <span class="mi">5</span><span class="p">)</span>
                   <span class="nf">withString:</span><span class="s">@&quot;Exige&quot;</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">car</span><span class="p">)</span><span class="o">;</span>                               <span class="c1">// Lotus Exige</span>
<span class="p">[</span><span class="n">car</span> <span class="nf">deleteCharactersInRange:</span><span class="nb">NSMakeRange</span><span class="p">(</span><span class="mi">5</span><span class="p">,</span> <span class="mi">6</span><span class="p">)];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">car</span><span class="p">)</span><span class="o">;</span>                               <span class="c1">// Lotus</span>
</pre></div>



<h2>When To Use Mutable Strings</h2>

<p>Since <code>NSString</code> and <code>NSMutableString</code> provide such
similar functionality, it can be hard to know when to use one over the other.
In general, the static nature of <code>NSString</code> makes it more efficient
for most tasks; however, the fact that an immutable string can&rsquo;t be
changed without generating a new object makes it less than ideal when
you&rsquo;re trying to perform several small edits.</p>

<p>The two examples presented in this section demonstrate the advantages of
mutable strings. First, let&rsquo;s take a look at an anti-pattern for
immutable strings. The following loop generates a string containing all of the
numbers between 0 and 999 using <code>NSString</code>.</p>

<div class="highlight"><pre><span class="c1">// DO NOT DO THIS. EVER.</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="n">indices</span> <span class="o">=</span> <span class="s">@&quot;&quot;</span><span class="p">;</span>
<span class="k">for</span> <span class="p">(</span><span class="kt">int</span> <span class="n">i</span><span class="o">=</span><span class="mi">0</span><span class="p">;</span> <span class="n">i</span><span class="o">&lt;</span><span class="mi">1000</span><span class="p">;</span> <span class="n">i</span><span class="o">++</span><span class="p">)</span> <span class="p">{</span>
    <span class="n">indices</span> <span class="o">=</span> <span class="p">[</span><span class="n">indices</span> <span class="nf">stringByAppendingFormat:</span><span class="s">@&quot;%d&quot;</span><span class="p">,</span> <span class="n">i</span><span class="p">];</span>
<span class="p">}</span>
</pre></div>


<p>Remember that <code>stringByAppendingFormat:</code> creates a new
<code>NSString</code> instance, which means that in each iteration, the entire
string gets copied to a new block of memory. The above code allocates 999
string objects that serve only as intermediary values, resulting in an
application that requires a whopping 1.76 MB of memory. Needless to say, this
is incredibly inefficient.</p>

<p>Now, let&rsquo;s take a look at the mutable version of this snippet:</p>

<div class="highlight"><pre><span class="nb">NSMutableString</span> <span class="o">*</span><span class="n">indices</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableString</span> <span class="nf">stringWithCapacity:</span><span class="mi">1</span><span class="p">];</span>
<span class="k">for</span> <span class="p">(</span><span class="kt">int</span> <span class="n">i</span><span class="o">=</span><span class="mi">0</span><span class="p">;</span> <span class="n">i</span><span class="o">&lt;</span><span class="mi">1000</span><span class="p">;</span> <span class="n">i</span><span class="o">++</span><span class="p">)</span> <span class="p">{</span>
    <span class="p">[</span><span class="n">indices</span> <span class="nf">appendFormat:</span><span class="s">@&quot;%d&quot;</span><span class="p">,</span> <span class="n">i</span><span class="p">];</span>
<span class="p">}</span>
</pre></div>


<p>Since mutable strings manipulate their contents in place, no copying is
involved, and we completely avoid the 999 unnecessary allocations. Internally,
the mutable string&rsquo;s storage dynamically expands to accommodate longer
values. This reduces the memory footprint to around 19 KB, which is much more
reasonable.</p>

<p>So, a good rule of thumb is to use a mutable string whenever you&rsquo;re
running any kind of algorithm that edits or assembles a string in several
passes and to use an immutable string for everything else. This also applies to
<a href='nsset.html'>sets</a>, <a href='nsarray.html'>arrays</a>, and <a
href='nsdictionary.html'>dictionaries</a>.</p>

<p class='sequential-nav'>
	<a href='nsset.html'>Continue to <em>NSSet</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>