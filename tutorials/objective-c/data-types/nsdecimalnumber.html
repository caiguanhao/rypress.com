<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Objective-C Data Types | NSDecimalNumber | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../../media/style.css" />
  <link rel="icon" type="image/png" href="../../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../../media/single.css" />
  <link rel="stylesheet" href="../../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../../index.html'>RyPress</a></li><li><a href='../../../tutorials.html'>Tutorials</a></li><li><a href='../../../sponsors.html'>Sponsors</a></li><li><a href='../../../about.html'>About</a></li><li><a href='../../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='../data-types.html'><span>&lsaquo;</span> Back to <em>Objective-C Data Types</em></a>
</p>

<h1 class='back-heading'>NSDecimalNumber</h1>

<p>The <code>NSDecimalNumber</code> class provides fixed-point arithmetic
capabilities to Objective-C programs. They&rsquo;re designed to perform base-10
calculations without loss of precision and with predictable rounding behavior.
This makes it a better choice for representing currency than floating-point
data types like <code>double</code>. However, the trade-off is that they are
more complicated to work with.</p>

<figure>
	<img style='max-width: 410px' src='../media/data-types/fixed-point-representation.png' />
</figure>

<p>Internally, a fixed-point number is expressed as <code>sign mantissa x
10^exponent</code>. The sign defines whether it&rsquo;s positive or negative,
the mantissa is an unsigned integer representing the significant digits, and
the exponent determines where the decimal point falls in the mantissa.</p>

<p>It&rsquo;s possible to manually assemble an <code>NSDecimalNumber</code>
from a mantissa, exponent, and sign, but it&rsquo;s often easier to convert it
from a string representation. The following snippet creates the value
<code>15.99</code> using both methods.</p>

<div class="highlight"><pre><span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">price</span><span class="p">;</span>
<span class="n">price</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithMantissa:</span><span class="mi">1599</span>
                                          <span class="nf">exponent:</span><span class="o">-</span><span class="mi">2</span>
                                          <span class="nf">isNegative:</span><span class="kc">NO</span><span class="p">];</span>
<span class="n">price</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;15.99&quot;</span><span class="p">];</span>
</pre></div>


<p>Like <code>NSNumber</code>, all <code>NSDecimalNumber</code> objects are
immutable, which means you cannot change their value after they&rsquo;ve been
created.</p>


<h2 id='arithmetic'>Arithmetic</h2>

<p>The main job of <code>NSDecimalNumber</code> is to provide fixed-point
alternatives to C&rsquo;s native arithmetic operations. All five of
<code>NSDecimalNumber</code>&rsquo;s arithmetic methods are demonstrated
below.</p>

<div class="highlight"><pre><span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">price1</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;15.99&quot;</span><span class="p">];</span>
<span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">price2</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;29.99&quot;</span><span class="p">];</span>
<span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">coupon</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;5.00&quot;</span><span class="p">];</span>
<span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">discount</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;.90&quot;</span><span class="p">];</span>
<span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">numProducts</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;2.0&quot;</span><span class="p">];</span>

<span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">subtotal</span> <span class="o">=</span> <span class="p">[</span><span class="n">price1</span> <span class="nf">decimalNumberByAdding:</span><span class="n">price2</span><span class="p">];</span>
<span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">afterCoupon</span> <span class="o">=</span> <span class="p">[</span><span class="n">subtotal</span> <span class="nf">decimalNumberBySubtracting:</span><span class="n">coupon</span><span class="p">];</span>
<span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">afterDiscount</span> <span class="o">=</span> <span class="p">[</span><span class="n">afterCoupon</span> <span class="nf">decimalNumberByMultiplyingBy:</span><span class="n">discount</span><span class="p">];</span>
<span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">average</span> <span class="o">=</span> <span class="p">[</span><span class="n">afterDiscount</span> <span class="nf">decimalNumberByDividingBy:</span><span class="n">numProducts</span><span class="p">];</span>
<span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">averageSquared</span> <span class="o">=</span> <span class="p">[</span><span class="n">average</span> <span class="nf">decimalNumberByRaisingToPower:</span><span class="mi">2</span><span class="p">];</span>

<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Subtotal: %@&quot;</span><span class="p">,</span> <span class="n">subtotal</span><span class="p">)</span><span class="o">;</span>                    <span class="c1">// 45.98</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;After coupon: %@&quot;</span><span class="p">,</span> <span class="n">afterCoupon</span><span class="p">)</span><span class="o">;</span>           <span class="c1">// 40.98</span>
<span class="nb">NSLog</span><span class="p">((</span><span class="s">@&quot;After discount: %@&quot;</span><span class="p">),</span> <span class="n">afterDiscount</span><span class="p">)</span><span class="o">;</span>       <span class="c1">// 36.882</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Average price per product: %@&quot;</span><span class="p">,</span> <span class="n">average</span><span class="p">)</span><span class="o">;</span>    <span class="c1">// 18.441</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Average price squared: %@&quot;</span><span class="p">,</span> <span class="n">averageSquared</span><span class="p">)</span><span class="o">;</span> <span class="c1">// 340.070481</span>
</pre></div>


<p>Unlike their floating-point counterparts, these operations are guaranteed to
be accurate. However, you&rsquo;ll notice that many of the above calculations
result in extra decimal places. Depending on the application, this may or may
not be desirable (e.g., you might want to constrain currency values to 2
decimal places). This is where custom rounding behavior comes in.</p>


<h2>Rounding Behavior</h2>

<p>Each of the above arithmetic methods have an alternate
<code>withBehavior:</code> form that let you define how the operation rounds
the resulting value. The <code>NSDecimalNumberHandler</code> class encapsulates
a particular rounding behavior and can be instantiated as follows:</p>

<div class="highlight"><pre><span class="nb">NSDecimalNumberHandler</span> <span class="o">*</span><span class="n">roundUp</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSDecimalNumberHandler</span>
                                   <span class="nf">decimalNumberHandlerWithRoundingMode:</span><span class="nb">NSRoundUp</span>
                                   <span class="nf">scale:</span><span class="mi">2</span>
                                   <span class="nf">raiseOnExactness:</span><span class="kc">NO</span>
                                   <span class="nf">raiseOnOverflow:</span><span class="kc">NO</span>
                                   <span class="nf">raiseOnUnderflow:</span><span class="kc">NO</span>
                                   <span class="nf">raiseOnDivideByZero:</span><span class="kc">YES</span><span class="p">];</span>
</pre></div>


<p>The <code>NSRoundUp</code> argument makes all operations round up to the
nearest place. Other rounding options are <code>NSRoundPlain</code>,
<code>NSRoundDown</code>, and <code>NSRoundBankers</code>, all of which are
defined by <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Protocols/NSDecimalNumberBehaviors_Protocol/Reference/Reference.html%23/apple_ref/doc/c_ref/NSRoundPlain'><code>NSRoundingMode</code></a>.
The <code>scale:</code> parameter defines the number of decimal places the
resulting value should have, and the rest of the parameters define the
exception-handling behavior of any operations. In this case,
<code>NSDecimalNumber</code> will only raise an exception if you try to divide
by zero.</p>

<p>This rounding behavior can then be passed to the
<code>decimalNumberByMultiplyingBy:withBehavior:</code> method (or any of the
other arithmetic methods), as shown below.</p>

<div class="highlight"><pre><span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">subtotal</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;40.98&quot;</span><span class="p">];</span>
<span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">discount</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;.90&quot;</span><span class="p">];</span>

<span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">total</span> <span class="o">=</span> <span class="p">[</span><span class="n">subtotal</span> <span class="nf">decimalNumberByMultiplyingBy:</span><span class="n">discount</span>
                                                   <span class="nf">withBehavior:</span><span class="n">roundUp</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Rounded total: %@&quot;</span><span class="p">,</span> <span class="n">total</span><span class="p">)</span><span class="o">;</span>
</pre></div>


<p>Now, instead of <code>36.882</code>, the <code>total</code> gets rounded up
to two decimal points, resulting in <code>36.89</code>.</p>


<h2>Comparing NSDecimalNumbers</h2>

<p>Like <code>NSNumber</code>, <code>NSDecimalNumber</code> objects should use
the <code>compare:</code> method instead of the native inequality operators.
Again, this ensures that <em>values</em> are compared, even if they are stored
in different <em>instances</em>. For example:</p>

<div class="highlight"><pre><span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">discount1</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;.85&quot;</span><span class="p">];</span>
<span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">discount2</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;.9&quot;</span><span class="p">];</span>
<span class="nb">NSComparisonResult</span> <span class="n">result</span> <span class="o">=</span> <span class="p">[</span><span class="n">discount1</span> <span class="nf">compare:</span><span class="n">discount2</span><span class="p">];</span>
<span class="k">if</span> <span class="p">(</span><span class="n">result</span> <span class="o">==</span> <span class="nb">NSOrderedAscending</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;85%% &lt; 90%%&quot;</span><span class="p">);</span>
<span class="p">}</span> <span class="k">else</span> <span class="k">if</span> <span class="p">(</span><span class="n">result</span> <span class="o">==</span> <span class="nb">NSOrderedSame</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;85%% == 90%%&quot;</span><span class="p">);</span>
<span class="p">}</span> <span class="k">else</span> <span class="k">if</span> <span class="p">(</span><span class="n">result</span> <span class="o">==</span> <span class="nb">NSOrderedDescending</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;85%% &gt; 90%%&quot;</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>


<p><code>NSDecimalNumber</code> also inherits the <code>isEqualToNumber:</code>
method from <code>NSNumber</code>.</p>


<h1>Decimal Numbers in C</h1>

<p>For most practical purposes, the <code>NSDecimalNumber</code> class should
satisfy your fixed-point needs; however, it&rsquo;s worth noting that there is
also a function-based alternative available in pure C. This provides increased
efficiency over the OOP interface discussed above and is thus preferred for
high-performance applications dealing with a large number of calculations.</p>


<h2>NSDecimal</h2>

<p>Instead of an <code>NSDecimalNumber</code> object, the C interface is built
around the <a
href='http://developer.apple.com/library/mac/#documentation/cocoa/reference/foundation/Miscellaneous/Foundation_DataTypes/Reference/reference.html%23/apple_ref/doc/uid/TP40003794'><code>NSDecimal</code></a>
<code>struct</code>. Unfortunately, the Foundation Framework doesn&rsquo;t make
it easy to create an <code>NSDecimal</code> from scratch. You need to generate
one from a full-fledged <code>NSDecimalNumber</code> using its
<code>decimalValue</code> method. There is a corresponding factory method, also
shown below.</p>

<div class="highlight"><pre><span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">price</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;15.99&quot;</span><span class="p">];</span>
<span class="nb">NSDecimal</span> <span class="n">asStruct</span> <span class="o">=</span> <span class="p">[</span><span class="n">price</span> <span class="nf">decimalValue</span><span class="p">];</span>
<span class="nb">NSDecimalNumber</span> <span class="o">*</span><span class="n">asNewObject</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithDecimal:</span><span class="n">asStruct</span><span class="p">];</span>
</pre></div>


<p>This isn&rsquo;t exactly an ideal way to create
<code>NSDecimal</code>&rsquo;s, but once you have a <code>struct</code>
representation of your initial values, you can stick to the functional API
presented below. All of these functions use <code>struct</code>&rsquo;s as
inputs and outputs.</p>


<h2>Arithmetic Functions</h2>

<p>In lieu of the arithmetic methods of <code>NSDecimalNumber</code>, the C
interface uses functions like <code>NSDecimalAdd()</code>,
<code>NSDecimalSubtract()</code>, etc. Instead of returning the result, these
functions populate the first argument with the calculated value. This makes it
possible to reuse an existing <code>NSDecimal</code> in several
operations and avoid allocating unnecessary structs just to hold intermediary
values.</p>

<p>For example, the following snippet uses a single <code>result</code>
variable across 5 function calls. Compare this to the <a
href='nsdecimalnumber.html#arithmetic'>Arithmetic</a> section, which created a new
<code>NSDecimalNumber</code> object for each calculation.</p>

<div class="highlight"><pre><span class="nb">NSDecimal</span> <span class="n">price1</span> <span class="o">=</span> <span class="p">[[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;15.99&quot;</span><span class="p">]</span> <span class="n">decimalValue</span><span class="p">];</span>
<span class="nb">NSDecimal</span> <span class="n">price2</span> <span class="o">=</span> <span class="p">[[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;29.99&quot;</span><span class="p">]</span> <span class="n">decimalValue</span><span class="p">];</span>
<span class="nb">NSDecimal</span> <span class="n">coupon</span> <span class="o">=</span> <span class="p">[[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;5.00&quot;</span><span class="p">]</span> <span class="n">decimalValue</span><span class="p">];</span>
<span class="nb">NSDecimal</span> <span class="n">discount</span> <span class="o">=</span> <span class="p">[[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;.90&quot;</span><span class="p">]</span> <span class="n">decimalValue</span><span class="p">];</span>
<span class="nb">NSDecimal</span> <span class="n">numProducts</span> <span class="o">=</span> <span class="p">[[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;2.0&quot;</span><span class="p">]</span> <span class="n">decimalValue</span><span class="p">];</span>
<span class="nb">NSLocale</span> <span class="o">*</span><span class="n">locale</span> <span class="o">=</span> <span class="p">[</span><span class="n">NSLocale</span> <span class="nf">currentLocale</span><span class="p">];</span>
<span class="nb">NSDecimal</span> <span class="n">result</span><span class="p">;</span>

<span class="nb">NSDecimalAdd</span><span class="p">(</span><span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">price1</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">price2</span><span class="p">,</span> <span class="nb">NSRoundUp</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Subtotal: %@&quot;</span><span class="p">,</span> <span class="nb">NSDecimalString</span><span class="p">(</span><span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="n">locale</span><span class="p">))</span><span class="o">;</span>
<span class="nb">NSDecimalSubtract</span><span class="p">(</span><span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">coupon</span><span class="p">,</span> <span class="nb">NSRoundUp</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;After coupon: %@&quot;</span><span class="p">,</span> <span class="nb">NSDecimalString</span><span class="p">(</span><span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="n">locale</span><span class="p">))</span><span class="o">;</span>
<span class="nb">NSDecimalMultiply</span><span class="p">(</span><span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">discount</span><span class="p">,</span> <span class="nb">NSRoundUp</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;After discount: %@&quot;</span><span class="p">,</span> <span class="nb">NSDecimalString</span><span class="p">(</span><span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="n">locale</span><span class="p">))</span><span class="o">;</span>
<span class="nb">NSDecimalDivide</span><span class="p">(</span><span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">numProducts</span><span class="p">,</span> <span class="nb">NSRoundUp</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Average price per product: %@&quot;</span><span class="p">,</span> <span class="nb">NSDecimalString</span><span class="p">(</span><span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="n">locale</span><span class="p">))</span><span class="o">;</span>
<span class="nb">NSDecimalPower</span><span class="p">(</span><span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="mi">2</span><span class="p">,</span> <span class="nb">NSRoundUp</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Average price squared: %@&quot;</span><span class="p">,</span> <span class="nb">NSDecimalString</span><span class="p">(</span><span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="n">locale</span><span class="p">))</span><span class="o">;</span>
</pre></div>


<p>Notice that these functions accept <em>references</em> to
<code>NSDecimal</code> structs, which is why we need to use the reference
operator (<code>&amp;</code>) instead of passing them directly. Also note that
rounding is an inherent part of each operation&mdash;it&rsquo;s not
encapsulated in a separate entity like <code>NSDecimalNumberHandler</code>.</p>

<p>The <code>NSLocale</code> instance defines the formatting of
<code>NSDecimalString()</code>, and is discussed more thoroughly in the
<a href='dates.html#nslocale'>Dates</a> module.</p>


<h2>Error Checking</h2>

<p>Unlike their OOP counterparts, the arithmetic functions don&rsquo;t raise
exceptions when a calculation error occurs. Instead, they follow the common C
pattern of using the return value to indicate success or failure. All of the
above functions return an <a
href='http://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Protocols/NSDecimalNumberBehaviors_Protocol/Reference/Reference.html%23/apple_ref/doc/c_ref/NSCalculationError'><code>NSCalculationError</code></a>,
which defines what kind of error occurred. The potential scenarios are
demonstrated below.</p>


<div class="highlight"><pre><span class="nb">NSDecimal</span> <span class="n">a</span> <span class="o">=</span> <span class="p">[[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;1.0&quot;</span><span class="p">]</span> <span class="n">decimalValue</span><span class="p">];</span>
<span class="nb">NSDecimal</span> <span class="n">b</span> <span class="o">=</span> <span class="p">[[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;0.0&quot;</span><span class="p">]</span> <span class="n">decimalValue</span><span class="p">];</span>
<span class="nb">NSDecimal</span> <span class="n">result</span><span class="p">;</span>
<span class="nb">NSCalculationError</span> <span class="n">success</span> <span class="o">=</span> <span class="nb">NSDecimalDivide</span><span class="p">(</span><span class="o">&amp;</span><span class="n">result</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">a</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">b</span><span class="p">,</span> <span class="nb">NSRoundPlain</span><span class="p">);</span>
<span class="k">switch</span> <span class="p">(</span><span class="n">success</span><span class="p">)</span> <span class="p">{</span>
    <span class="k">case</span> <span class="nf">NSCalculationNoError:</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Operation successful&quot;</span><span class="p">);</span>
        <span class="k">break</span><span class="p">;</span>
    <span class="k">case</span> <span class="nf">NSCalculationLossOfPrecision:</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Error: Operation resulted in loss of precision&quot;</span><span class="p">);</span>
        <span class="k">break</span><span class="p">;</span>
    <span class="k">case</span> <span class="nf">NSCalculationUnderflow:</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Error: Operation resulted in underflow&quot;</span><span class="p">);</span>
        <span class="k">break</span><span class="p">;</span>
    <span class="k">case</span> <span class="nf">NSCalculationOverflow:</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Error: Operation resulted in overflow&quot;</span><span class="p">);</span>
        <span class="k">break</span><span class="p">;</span>
    <span class="k">case</span> <span class="nf">NSCalculationDivideByZero:</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Error: Tried to divide by zero&quot;</span><span class="p">);</span>
        <span class="k">break</span><span class="p">;</span>
    <span class="k">default</span><span class="o">:</span>
        <span class="k">break</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p></p>


<h2>Comparing NSDecimals</h2>

<p>Comparing <code>NSDecimal</code>&rsquo;s works exactly like the OOP interface,
except you use the <code>NSDecimalCompare()</code> function:</p>

<div class="highlight"><pre><span class="nb">NSDecimal</span> <span class="n">discount1</span> <span class="o">=</span> <span class="p">[[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;.85&quot;</span><span class="p">]</span> <span class="n">decimalValue</span><span class="p">];</span>
<span class="nb">NSDecimal</span> <span class="n">discount2</span> <span class="o">=</span> <span class="p">[[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;.9&quot;</span><span class="p">]</span> <span class="n">decimalValue</span><span class="p">];</span>
<span class="nb">NSComparisonResult</span> <span class="n">result</span> <span class="o">=</span> <span class="nb">NSDecimalCompare</span><span class="p">(</span><span class="o">&amp;</span><span class="n">discount1</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">discount2</span><span class="p">);</span>
<span class="k">if</span> <span class="p">(</span><span class="n">result</span> <span class="o">==</span> <span class="nb">NSOrderedAscending</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;85%% &lt; 90%%&quot;</span><span class="p">);</span>
<span class="p">}</span> <span class="k">else</span> <span class="k">if</span> <span class="p">(</span><span class="n">result</span> <span class="o">==</span> <span class="nb">NSOrderedSame</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;85%% == 90%%&quot;</span><span class="p">);</span>
<span class="p">}</span> <span class="k">else</span> <span class="k">if</span> <span class="p">(</span><span class="n">result</span> <span class="o">==</span> <span class="nb">NSOrderedDescending</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;85%% &gt; 90%%&quot;</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>


<p class='sequential-nav'>
	<a href='nsstring.html'>Continue to <em>NSString</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>