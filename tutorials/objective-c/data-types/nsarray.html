<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Objective-C Data Types | NSArray | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../../media/style.css" />
  <link rel="icon" type="image/png" href="../../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../../media/single.css" />
  <link rel="stylesheet" href="../../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../../index.html'>RyPress</a></li><li><a href='../../../tutorials.html'>Tutorials</a></li><li><a href='../../../sponsors.html'>Sponsors</a></li><li><a href='../../../about.html'>About</a></li><li><a href='../../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='../data-types.html'><span>&lsaquo;</span> Back to <em>Objective-C Data Types</em></a>
</p>

<h1 class='back-heading'>NSArray</h1>

<p><code>NSArray</code> is Objective-C&rsquo;s general-purpose array type. It
represents an ordered collection of objects, and it provides a high-level
interface for sorting and otherwise manipulating lists of data. Arrays
aren&rsquo;t as efficient at membership checking as <a
href='nsset.html'>sets</a>, but the trade-off is that they reliably record the
order of their elements.</p>

<figure>
	<img style='max-width: 500px' src='../media/data-types/nsarray.png' />
	<figcaption>The basic collection classes of
the Foundation Framework</figcaption>
</figure>

<p>Like <code>NSSet</code>, <code>NSArray</code> is immutable, so you cannot
dynamically add or remove items. Its mutable counterpart,
<code>NSMutableArray</code>, is discussed in the <a
href='nsarray.html#nsmutablearray'>second part</a> of this module.</p>


<h2>Creating Arrays</h2>

<p>Immutable arrays can be defined as literals using the <code>@[]</code>
syntax. This was added relatively late in the evolution of the language (Xcode
4.4), so you&rsquo;re likely to encounter the more verbose
<code>arrayWithObjects:</code> factory method at some point in your Objective-C
career. Both options are included below.</p>

<div class="highlight"><pre><span class="nb">NSArray</span> <span class="o">*</span><span class="n">germanMakes</span> <span class="o">=</span> <span class="p">@[</span><span class="s">@&quot;Mercedes-Benz&quot;</span><span class="p">,</span> <span class="s">@&quot;BMW&quot;</span><span class="p">,</span> <span class="s">@&quot;Porsche&quot;</span><span class="p">,</span>
                         <span class="s">@&quot;Opel&quot;</span><span class="p">,</span> <span class="s">@&quot;Volkswagen&quot;</span><span class="p">,</span> <span class="s">@&quot;Audi&quot;</span><span class="p">];</span>
<span class="nb">NSArray</span> <span class="o">*</span><span class="n">ukMakes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSArray</span> <span class="nf">arrayWithObjects:</span><span class="s">@&quot;Aston Martin&quot;</span><span class="p">,</span>
                    <span class="s">@&quot;Lotus&quot;</span><span class="p">,</span> <span class="s">@&quot;Jaguar&quot;</span><span class="p">,</span> <span class="s">@&quot;Bentley&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>

<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;First german make: %@&quot;</span><span class="p">,</span> <span class="n">germanMakes</span><span class="p">[</span><span class="mi">0</span><span class="p">])</span><span class="o">;</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;First U.K. make: %@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">ukMakes</span> <span class="nf">objectAtIndex:</span><span class="mi">0</span><span class="p">])</span><span class="o">;</span>
</pre></div>


<p>As you can see, individual items can be accessed through the square-bracket
subscripting syntax (<code>germanMakes[0]</code>) or the
<code>objectAtIndex:</code> method. Prior to Xcode 4.4,
<code>objectAtIndex:</code> was the standard way to access array elements.</p>


<h2>Enumerating Arrays</h2>

<p>Fast-enumeration is the most efficient way to iterate over an
<code>NSArray</code>, and its contents are guaranteed to appear in the correct
order. It&rsquo;s also possible to use the <code>count</code> method with a
traditional for-loop to step through each element in the array:</p>

<div class="highlight"><pre><span class="nb">NSArray</span> <span class="o">*</span><span class="n">germanMakes</span> <span class="o">=</span> <span class="p">@[</span><span class="s">@&quot;Mercedes-Benz&quot;</span><span class="p">,</span> <span class="s">@&quot;BMW&quot;</span><span class="p">,</span> <span class="s">@&quot;Porsche&quot;</span><span class="p">,</span>
                         <span class="s">@&quot;Opel&quot;</span><span class="p">,</span> <span class="s">@&quot;Volkswagen&quot;</span><span class="p">,</span> <span class="s">@&quot;Audi&quot;</span><span class="p">];</span>
<span class="c1">// With fast-enumeration</span>
<span class="k">for</span> <span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="n">item</span> <span class="k">in</span> <span class="n">germanMakes</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">item</span><span class="p">);</span>
<span class="p">}</span>
<span class="c1">// With a traditional for loop</span>
<span class="k">for</span> <span class="p">(</span><span class="kt">int</span> <span class="n">i</span><span class="o">=</span><span class="mi">0</span><span class="p">;</span> <span class="n">i</span><span class="o">&lt;</span><span class="p">[</span><span class="n">germanMakes</span> <span class="nf">count</span><span class="p">];</span> <span class="n">i</span><span class="o">++</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%d: %@&quot;</span><span class="p">,</span> <span class="n">i</span><span class="p">,</span> <span class="n">germanMakes</span><span class="p">[</span><span class="n">i</span><span class="p">]);</span>
<span class="p">}</span>
</pre></div>


<p>If you&rsquo;re fond of blocks, you can use the
<code>enumerateObjectsUsingBlock:</code> method. It works the same as <a
href='nsset.html#enumerating-sets'><code>NSSet</code>&rsquo;s version</a>,
except the index of each item is also passed to the block, so its signature
looks like <code>^(id obj, NSUInteger idx, BOOL *stop)</code>. And of course,
the objects are passed to the block in the same order as they appear in the
array.</p>

<div class="highlight"><pre><span class="p">[</span><span class="n">germanMakes</span> <span class="nf">enumerateObjectsUsingBlock:</span><span class="o">^</span><span class="p">(</span><span class="kt">id</span> <span class="n">obj</span><span class="p">,</span>
                                          <span class="nb">NSUInteger</span> <span class="n">idx</span><span class="p">,</span>
                                          <span class="kt">BOOL</span> <span class="o">*</span><span class="n">stop</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%ld: %@&quot;</span><span class="p">,</span> <span class="n">idx</span><span class="p">,</span> <span class="n">obj</span><span class="p">);</span>
<span class="p">}];</span>
</pre></div>



<h2>Comparing Arrays</h2>

<p>Arrays can be compared for equality with the aptly named
<code>isEqualToArray:</code> method, which returns <code>YES</code> when both
arrays have the same number of elements and every pair pass an
<code>isEqual:</code> comparison. <code>NSArray</code> does not offer the
same subset and intersection comparisons as <code>NSSet</code>.</p>

<div class="highlight"><pre><span class="nb">NSArray</span> <span class="o">*</span><span class="n">germanMakes</span> <span class="o">=</span> <span class="p">@[</span><span class="s">@&quot;Mercedes-Benz&quot;</span><span class="p">,</span> <span class="s">@&quot;BMW&quot;</span><span class="p">,</span> <span class="s">@&quot;Porsche&quot;</span><span class="p">,</span>
                         <span class="s">@&quot;Opel&quot;</span><span class="p">,</span> <span class="s">@&quot;Volkswagen&quot;</span><span class="p">,</span> <span class="s">@&quot;Audi&quot;</span><span class="p">];</span>
<span class="nb">NSArray</span> <span class="o">*</span><span class="n">sameGermanMakes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSArray</span> <span class="nf">arrayWithObjects:</span><span class="s">@&quot;Mercedes-Benz&quot;</span><span class="p">,</span>
                            <span class="s">@&quot;BMW&quot;</span><span class="p">,</span> <span class="s">@&quot;Porsche&quot;</span><span class="p">,</span> <span class="s">@&quot;Opel&quot;</span><span class="p">,</span>
                            <span class="s">@&quot;Volkswagen&quot;</span><span class="p">,</span> <span class="s">@&quot;Audi&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>

<span class="k">if</span> <span class="p">([</span><span class="n">germanMakes</span> <span class="nf">isEqualToArray:</span><span class="n">sameGermanMakes</span><span class="p">])</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Oh good, literal arrays are the same as NSArrays&quot;</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>



<h2>Membership Checking</h2>

<p><code>NSArary</code> provides similar membership checking utilities to
<code>NSSet</code>. The <code>containsObject:</code> method works the exact
same (it returns <code>YES</code> if the object is in the array,
<code>NO</code> otherwise), but instead of <code>member:</code>,
<code>NSArray</code> uses <code>indexOfObject:</code>. This either returns the
index of the first occurrence of the requested object or
<code>NSNotFound</code> if it&rsquo;s not in the array.</p>

<div class="highlight"><pre><span class="nb">NSArray</span> <span class="o">*</span><span class="n">germanMakes</span> <span class="o">=</span> <span class="p">@[</span><span class="s">@&quot;Mercedes-Benz&quot;</span><span class="p">,</span> <span class="s">@&quot;BMW&quot;</span><span class="p">,</span> <span class="s">@&quot;Porsche&quot;</span><span class="p">,</span>
                         <span class="s">@&quot;Opel&quot;</span><span class="p">,</span> <span class="s">@&quot;Volkswagen&quot;</span><span class="p">,</span> <span class="s">@&quot;Audi&quot;</span><span class="p">];</span>
<span class="c1">// BOOL checking</span>
<span class="k">if</span> <span class="p">([</span><span class="n">germanMakes</span> <span class="nf">containsObject:</span><span class="s">@&quot;BMW&quot;</span><span class="p">])</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;BMW is a German auto maker&quot;</span><span class="p">);</span>
<span class="p">}</span>
<span class="c1">// Index checking</span>
<span class="nb">NSUInteger</span> <span class="n">index</span> <span class="o">=</span> <span class="p">[</span><span class="n">germanMakes</span> <span class="nf">indexOfObject:</span><span class="s">@&quot;BMW&quot;</span><span class="p">];</span>
<span class="k">if</span> <span class="p">(</span><span class="n">index</span> <span class="o">==</span> <span class="nb">NSNotFound</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Well that&#39;s not quite right...&quot;</span><span class="p">);</span>
<span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;BMW is a German auto maker and is at index %ld&quot;</span><span class="p">,</span> <span class="n">index</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>


<p>Since arrays can contain more than one reference to the same object,
it&rsquo;s possible that the first occurrence isn&rsquo;t the only one. To find
other occurrences, you can use the related <code>indexOfObject:inRange:</code>
method.</p>

<p>Remember that <a href='nsset.html'>sets</a> are more efficient
for membership checking, so if you&rsquo;re querying against a large collection
of objects, you should probably be using a set instead of an array.</p>


<h2 id='sorting-arrays'>Sorting Arrays</h2>

<p>Sorting is one of the main advantages of arrays. One of the most flexible
ways to sort an array is with the <code>sortedArrayUsingComparator:</code>
method. This accepts an <code>^NSComparisonResult(id obj1, id obj2)</code>
block, which should return one of the following enumerators depending on the
relationship between <code>obj1</code> and <code>obj2</code>:

<table>
<thead>
<tr>
	<th>Return Value</th>
	<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
	<td><code>NSOrderedAscending</code></td>
	<td><code>obj1</code> comes before <code>obj2</code></td>
</tr>
<tr>
	<td><code>NSOrderedSame</code></td>
	<td><code>obj1</code> and <code>obj2</code> have no order</td>
</tr>
<tr>
	<td><code>NSOrderedDescending</code></td>
	<td><code>obj1</code> comes after <code>obj2</code></td>
</tr>
</tbody>
</table>

<p>The following example sorts a list of car manufacturers based on how long
their name is, from shortest to longest.</p>

<div class="highlight"><pre><span class="nb">NSArray</span> <span class="o">*</span><span class="n">germanMakes</span> <span class="o">=</span> <span class="p">@[</span><span class="s">@&quot;Mercedes-Benz&quot;</span><span class="p">,</span> <span class="s">@&quot;BMW&quot;</span><span class="p">,</span> <span class="s">@&quot;Porsche&quot;</span><span class="p">,</span>
                         <span class="s">@&quot;Opel&quot;</span><span class="p">,</span> <span class="s">@&quot;Volkswagen&quot;</span><span class="p">,</span> <span class="s">@&quot;Audi&quot;</span><span class="p">];</span>
<span class="nb">NSArray</span> <span class="o">*</span><span class="n">sortedMakes</span> <span class="o">=</span> <span class="p">[</span><span class="n">germanMakes</span> <span class="nf">sortedArrayUsingComparator:</span>
    <span class="o">^</span><span class="nb">NSComparisonResult</span><span class="p">(</span><span class="kt">id</span> <span class="n">obj1</span><span class="p">,</span> <span class="kt">id</span> <span class="n">obj2</span><span class="p">)</span> <span class="p">{</span>
        <span class="k">if</span> <span class="p">([</span><span class="n">obj1</span> <span class="nf">length</span><span class="p">]</span> <span class="o">&lt;</span> <span class="p">[</span><span class="n">obj2</span> <span class="nf">length</span><span class="p">])</span> <span class="p">{</span>
            <span class="k">return</span> <span class="nb">NSOrderedAscending</span><span class="p">;</span>
        <span class="p">}</span> <span class="k">else</span> <span class="k">if</span> <span class="p">([</span><span class="n">obj1</span> <span class="nf">length</span><span class="p">]</span> <span class="o">&gt;</span> <span class="p">[</span><span class="n">obj2</span> <span class="nf">length</span><span class="p">])</span> <span class="p">{</span>
            <span class="k">return</span> <span class="nb">NSOrderedDescending</span><span class="p">;</span>
        <span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
            <span class="k">return</span> <span class="nb">NSOrderedSame</span><span class="p">;</span>
        <span class="p">}</span>
<span class="p">}];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">sortedMakes</span><span class="p">)</span><span class="o">;</span>
</pre></div>


<p>Like <code>NSSet</code>, <code>NSArray</code> is immutable, so the sorted
array is actually a <em>new</em> array, though it still references the same
elements as the original array (this is the same behavior as
<code>NSSet</code>).</p>


<h2>Filtering Arrays</h2>

<p>You can filter an array with the <code>filteredArrayUsingPredicate:</code>
method. A short introduction to predicates can be found in the <a
href='nsset.html#filtering-with-predicates'>NSSet</a> module, and a minimal
example is included below. Just as with the sort method discussed above, this
generates a brand new array.</p>


<div class="highlight"><pre><span class="nb">NSArray</span> <span class="o">*</span><span class="n">germanMakes</span> <span class="o">=</span> <span class="p">@[</span><span class="s">@&quot;Mercedes-Benz&quot;</span><span class="p">,</span> <span class="s">@&quot;BMW&quot;</span><span class="p">,</span> <span class="s">@&quot;Porsche&quot;</span><span class="p">,</span>
                         <span class="s">@&quot;Opel&quot;</span><span class="p">,</span> <span class="s">@&quot;Volkswagen&quot;</span><span class="p">,</span> <span class="s">@&quot;Audi&quot;</span><span class="p">];</span>

<span class="nb">NSPredicate</span> <span class="o">*</span><span class="n">beforeL</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSPredicate</span> <span class="nf">predicateWithBlock:</span>
    <span class="o">^</span><span class="kt">BOOL</span><span class="p">(</span><span class="kt">id</span> <span class="n">evaluatedObject</span><span class="p">,</span> <span class="nb">NSDictionary</span> <span class="o">*</span><span class="n">bindings</span><span class="p">)</span> <span class="p">{</span>
        <span class="nb">NSComparisonResult</span> <span class="n">result</span> <span class="o">=</span> <span class="p">[</span><span class="s">@&quot;L&quot;</span> <span class="nf">compare:</span><span class="n">evaluatedObject</span><span class="p">];</span>
        <span class="k">if</span> <span class="p">(</span><span class="n">result</span> <span class="o">==</span> <span class="nb">NSOrderedDescending</span><span class="p">)</span> <span class="p">{</span>
            <span class="k">return</span> <span class="kc">YES</span><span class="p">;</span>
        <span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
            <span class="k">return</span> <span class="kc">NO</span><span class="p">;</span>
        <span class="p">}</span>
    <span class="p">}];</span>
<span class="nb">NSArray</span> <span class="o">*</span><span class="n">makesBeforeL</span> <span class="o">=</span> <span class="p">[</span><span class="n">germanMakes</span>
                         <span class="nf">filteredArrayUsingPredicate:</span><span class="n">beforeL</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">makesBeforeL</span><span class="p">)</span><span class="o">;</span>    <span class="c1">// BMW, Audi</span>
</pre></div>



<h2>Subdividing Arrays</h2>

<p>Subdividing an array is essentially the same as extracting substrings from
an <code>NSString</code>, but instead of <code>substringWithRange:</code>, you
use <code>subarrayWithRange:</code>, as shown below.</p>

<div class="highlight"><pre><span class="nb">NSArray</span> <span class="o">*</span><span class="n">germanMakes</span> <span class="o">=</span> <span class="p">@[</span><span class="s">@&quot;Mercedes-Benz&quot;</span><span class="p">,</span> <span class="s">@&quot;BMW&quot;</span><span class="p">,</span> <span class="s">@&quot;Porsche&quot;</span><span class="p">,</span>
                         <span class="s">@&quot;Opel&quot;</span><span class="p">,</span> <span class="s">@&quot;Volkswagen&quot;</span><span class="p">,</span> <span class="s">@&quot;Audi&quot;</span><span class="p">];</span>

<span class="nb">NSArray</span> <span class="o">*</span><span class="n">lastTwo</span> <span class="o">=</span> <span class="p">[</span><span class="n">germanMakes</span> <span class="nf">subarrayWithRange:</span><span class="nb">NSMakeRange</span><span class="p">(</span><span class="mi">4</span><span class="p">,</span> <span class="mi">2</span><span class="p">)];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">lastTwo</span><span class="p">)</span><span class="o">;</span>    <span class="c1">// Volkswagen, Audi</span>
</pre></div>



<h2>Combining Arrays</h2>

<p>Arrays can be combined via <code>arrayByAddingObjectsFromArray:</code>. As
with all of the other immutable methods discussed above, this returns a
<em>new</em> array containing all of the elements in the original array, along
with the contents of the parameter.</p>

<div class="highlight"><pre><span class="nb">NSArray</span> <span class="o">*</span><span class="n">germanMakes</span> <span class="o">=</span> <span class="p">@[</span><span class="s">@&quot;Mercedes-Benz&quot;</span><span class="p">,</span> <span class="s">@&quot;BMW&quot;</span><span class="p">,</span> <span class="s">@&quot;Porsche&quot;</span><span class="p">,</span>
                         <span class="s">@&quot;Opel&quot;</span><span class="p">,</span> <span class="s">@&quot;Volkswagen&quot;</span><span class="p">,</span> <span class="s">@&quot;Audi&quot;</span><span class="p">];</span>
<span class="nb">NSArray</span> <span class="o">*</span><span class="n">ukMakes</span> <span class="o">=</span> <span class="p">@[</span><span class="s">@&quot;Aston Martin&quot;</span><span class="p">,</span> <span class="s">@&quot;Lotus&quot;</span><span class="p">,</span> <span class="s">@&quot;Jaguar&quot;</span><span class="p">,</span> <span class="s">@&quot;Bentley&quot;</span><span class="p">];</span>

<span class="nb">NSArray</span> <span class="o">*</span><span class="n">allMakes</span> <span class="o">=</span> <span class="p">[</span><span class="n">germanMakes</span> <span class="nf">arrayByAddingObjectsFromArray:</span><span class="n">ukMakes</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">allMakes</span><span class="p">)</span><span class="o">;</span>
</pre></div>



<h2>String Conversion</h2>

<p>The <code>componentsJoinedByString:</code> method concatenates each element
of the array into a string, separating them by the specified symbol(s).</p>

<div class="highlight"><pre><span class="nb">NSArray</span> <span class="o">*</span><span class="n">ukMakes</span> <span class="o">=</span> <span class="p">@[</span><span class="s">@&quot;Aston Martin&quot;</span><span class="p">,</span> <span class="s">@&quot;Lotus&quot;</span><span class="p">,</span> <span class="s">@&quot;Jaguar&quot;</span><span class="p">,</span> <span class="s">@&quot;Bentley&quot;</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">ukMakes</span> <span class="nf">componentsJoinedByString:</span><span class="s">@&quot;, &quot;</span><span class="p">])</span><span class="o">;</span>
</pre></div>


<p>This can be useful for regular expression generation, file path
manipulation, and rudimentary CSV processing; however, if you&rsquo;re doing
serious work with file paths/data, you&rsquo;ll probably want to look for a
dedicated library.</p>


<h1 id='nsmutablearray'>NSMutableArray</h1>

<p>The <code>NSMutableArray</code> class lets you dynamically add or remove
items from arbitrary locations in the collection. Keep in mind that it&rsquo;s
slower to insert or delete elements from a mutable array than a set or a
dictionary.</p>

<p>Like mutable sets, mutable arrays are often used to represent the state of a
system, but the fact that <code>NSMutableArray</code> records the order of its
elements opens up new modeling opportunities. For instance, consider the auto
repair shop we talked about in the <a href='nsset.html#nsmutableset'>NSSet</a>
module. Whereas a set can only represent the status of a collection of
automobiles, an array can record the order in which they should be fixed.</p>


<h2>Creating Mutable Arrays</h2>

<p>Literal arrays are always immutable, so the easiest way to create mutable
arrays is still through the <code>arrayWithObjects:</code> method. Be careful
to use <code>NSMutableArray</code>&rsquo;s version of the method, not
<code>NSArray</code>&rsquo;s. For example:</p>

<div class="highlight"><pre><span class="nb">NSMutableArray</span> <span class="o">*</span><span class="n">brokenCars</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableArray</span> <span class="nf">arrayWithObjects:</span>
                              <span class="s">@&quot;Audi A6&quot;</span><span class="p">,</span> <span class="s">@&quot;BMW Z3&quot;</span><span class="p">,</span>
                              <span class="s">@&quot;Audi Quattro&quot;</span><span class="p">,</span> <span class="s">@&quot;Audi TT&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
</pre></div>


<p>You can create empty mutable arrays using the <code>array</code> or
<code>arrayWithCapacity:</code> class methods. Or, if you already have an
immutable array that you want to convert to a mutable one, you can pass it to
the <code>arrayWithArray:</code> class method.</p>


<h2>Adding and Removing Objects</h2>

<p>The two basic methods for manipulating the contents of an array are the
<code>addObject:</code> and <code>removeLastObject</code> methods. The former
adds an object to the end of the array, and the latter is pretty
self-documenting. Note that these are also useful methods for treating an
<code>NSArray</code> as a stack.</p>

<div class="highlight"><pre><span class="nb">NSMutableArray</span> <span class="o">*</span><span class="n">brokenCars</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableArray</span> <span class="nf">arrayWithObjects:</span>
                              <span class="s">@&quot;Audi A6&quot;</span><span class="p">,</span> <span class="s">@&quot;BMW Z3&quot;</span><span class="p">,</span>
                              <span class="s">@&quot;Audi Quattro&quot;</span><span class="p">,</span> <span class="s">@&quot;Audi TT&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="p">[</span><span class="n">brokenCars</span> <span class="nf">addObject:</span><span class="s">@&quot;BMW F25&quot;</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">brokenCars</span><span class="p">)</span><span class="o">;</span>       <span class="c1">// BMW F25 added to end</span>
<span class="p">[</span><span class="n">brokenCars</span> <span class="nf">removeLastObject</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">brokenCars</span><span class="p">)</span><span class="o">;</span>       <span class="c1">// BMW F25 removed from end</span>
</pre></div>


<p>It&rsquo;s most efficient to add or remove items at the <em>end</em> of an
array, but you can also insert or delete objects at arbitrary locations using
<code>insertObject:atIndex:</code> and <code>removeObjectAtIndex:</code>. Or,
if you don&rsquo;t know the index of a particular object, you can use the
<code>removeObject:</code> method, which is really just a convenience method
for <code>indexOfObject:</code> followed by <code>removeObjectAtIndex:</code>.

<div class="highlight"><pre><span class="c1">// Add BMW F25 to front</span>
<span class="p">[</span><span class="n">brokenCars</span> <span class="nf">insertObject:</span><span class="s">@&quot;BMW F25&quot;</span> <span class="nf">atIndex:</span><span class="mi">0</span><span class="p">];</span>
<span class="c1">// Remove BMW F25 from front</span>
<span class="p">[</span><span class="n">brokenCars</span> <span class="nf">removeObjectAtIndex:</span><span class="mi">0</span><span class="p">];</span>
<span class="c1">// Remove Audi Quattro</span>
<span class="p">[</span><span class="n">brokenCars</span> <span class="nf">removeObject:</span><span class="s">@&quot;Audi Quattro&quot;</span><span class="p">];</span>
</pre></div>


<p>It&rsquo;s also possible to replace the contents of an index with the
<code>replaceObjectAtIndex:withObject:</code> method, as shown below.</p>

<div class="highlight"><pre><span class="c1">// Change second item to Audi Q5</span>
<span class="p">[</span><span class="n">brokenCars</span> <span class="nf">replaceObjectAtIndex:</span><span class="mi">1</span> <span class="nf">withObject:</span><span class="s">@&quot;Audi Q5&quot;</span><span class="p">];</span>
</pre></div>


<p>These are the basic methods for manipulating mutable arrays, but be sure to
check out the <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Classes/NSMutableArray_Class/Reference/Reference.html'>official
documentation</a> if you need more advanced functionality.</p>


<h2>Sorting With Descriptors</h2>

<p>Inline sorts can be accomplished through <code>sortUsingComparator:</code>,
which works just like the immutable version discussed in <a
href='nsarray.html#sorting-arrays'>Sorting Arrays</a>. However, this section discusses an
alternative method for sorting arrays called <code>NSSortDescriptor</code>.
Sort descriptors typically result in code that is more semantic and less
redundant than block-based sorts.</p>

<p>The <code>NSSortDescriptor</code> class encapsulates all of the information
required to sort an array of <a href='nsdictionary.html'>dictionaries</a> or
custom objects. This includes the property to be compared, the comparison
method, and whether the sort is ascending or descending. Once you configure a
descriptor(s), you can sort an array by passing it to the
<code>sortUsingDescriptors:</code> method. For example, the following snippet
sorts an array of cars by price and then by model.</p>

<div class="highlight"><pre><span class="nb">NSDictionary</span> <span class="o">*</span><span class="n">car1</span> <span class="o">=</span> <span class="p">@{</span>
    <span class="s">@&quot;make&quot;</span><span class="o">:</span> <span class="s">@&quot;Volkswagen&quot;</span><span class="p">,</span>
    <span class="s">@&quot;model&quot;</span><span class="o">:</span> <span class="s">@&quot;Golf&quot;</span><span class="p">,</span>
    <span class="s">@&quot;price&quot;</span><span class="o">:</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;18750.00&quot;</span><span class="p">]</span>
<span class="p">};</span>
<span class="nb">NSDictionary</span> <span class="o">*</span><span class="n">car2</span> <span class="o">=</span> <span class="p">@{</span>
    <span class="s">@&quot;make&quot;</span><span class="o">:</span> <span class="s">@&quot;Volkswagen&quot;</span><span class="p">,</span>
    <span class="s">@&quot;model&quot;</span><span class="o">:</span> <span class="s">@&quot;Eos&quot;</span><span class="p">,</span>
    <span class="s">@&quot;price&quot;</span><span class="o">:</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;35820.00&quot;</span><span class="p">]</span>
<span class="p">};</span>
<span class="nb">NSDictionary</span> <span class="o">*</span><span class="n">car3</span> <span class="o">=</span> <span class="p">@{</span>
    <span class="s">@&quot;make&quot;</span><span class="o">:</span> <span class="s">@&quot;Volkswagen&quot;</span><span class="p">,</span>
    <span class="s">@&quot;model&quot;</span><span class="o">:</span> <span class="s">@&quot;Jetta A5&quot;</span><span class="p">,</span>
    <span class="s">@&quot;price&quot;</span><span class="o">:</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;16675.00&quot;</span><span class="p">]</span>
<span class="p">};</span>
<span class="nb">NSDictionary</span> <span class="o">*</span><span class="n">car4</span> <span class="o">=</span> <span class="p">@{</span>
    <span class="s">@&quot;make&quot;</span><span class="o">:</span> <span class="s">@&quot;Volkswagen&quot;</span><span class="p">,</span>
    <span class="s">@&quot;model&quot;</span><span class="o">:</span> <span class="s">@&quot;Jetta A4&quot;</span><span class="p">,</span>
    <span class="s">@&quot;price&quot;</span><span class="o">:</span> <span class="p">[</span><span class="nb">NSDecimalNumber</span> <span class="nf">decimalNumberWithString:</span><span class="s">@&quot;16675.00&quot;</span><span class="p">]</span>
<span class="p">};</span>
<span class="nb">NSMutableArray</span> <span class="o">*</span><span class="n">cars</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableArray</span> <span class="nf">arrayWithObjects:</span>
                        <span class="n">car1</span><span class="p">,</span> <span class="n">car2</span><span class="p">,</span> <span class="n">car3</span><span class="p">,</span> <span class="n">car4</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>

<span class="nb">NSSortDescriptor</span> <span class="o">*</span><span class="n">priceDescriptor</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSortDescriptor</span>
                                     <span class="nf">sortDescriptorWithKey:</span><span class="s">@&quot;price&quot;</span>
                                                 <span class="nf">ascending:</span><span class="kc">YES</span>
                                                  <span class="nf">selector:</span><span class="k">@selector</span><span class="p">(</span><span class="nf">compare:</span><span class="p">)];</span>
<span class="nb">NSSortDescriptor</span> <span class="o">*</span><span class="n">modelDescriptor</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSortDescriptor</span>
                                     <span class="nf">sortDescriptorWithKey:</span><span class="s">@&quot;model&quot;</span>
                                     <span class="nf">ascending:</span><span class="kc">YES</span>
                                     <span class="nf">selector:</span><span class="k">@selector</span><span class="p">(</span><span class="nf">caseInsensitiveCompare:</span><span class="p">)];</span>

<span class="nb">NSArray</span> <span class="o">*</span><span class="n">descriptors</span> <span class="o">=</span> <span class="p">@[</span><span class="n">priceDescriptor</span><span class="p">,</span> <span class="n">modelDescriptor</span><span class="p">];</span>
<span class="p">[</span><span class="n">cars</span> <span class="nf">sortUsingDescriptors:</span><span class="n">descriptors</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">cars</span><span class="p">)</span><span class="o">;</span>    <span class="c1">// car4, car3, car1, car2</span>
</pre></div>


<p>The descriptor&rsquo;s <code>selector</code> is called on each key&rsquo;s
value, so in the above code, we&rsquo;re calling <code>compare:</code> on
<code>item[@"price"]</code> and <code>caseInsensitiveCompare:</code> on
<code>item[@"model"]</code> for each pair of items in the array.</p>


<h2>Filtering Mutable Arrays</h2>

<p>Filtering works the same as it does with immutable arrays, except items are
removed from the existing array instead of generating a new one, and you use
the <code>filterUsingPredicate:</code> method.</p>


<h2>Enumeration Considerations</h2>

<p>As with all mutable collections, you&rsquo;re not allowed to alter it in the
middle of an enumeration. This is covered in the <a
href='nsset.html#enumeration-considerations'>Enumeration Considerations</a>
section of the <code>NSSet</code> module, but instead of using
<code>allObjects</code> for the snapshot, you can create a temporary copy of
the original array by passing it to the <code>arrayWithArray:</code> class
method.</p>

<p class='sequential-nav'>
	<a href='nsdictionary.html'>Continue to <em>NSDictionary</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>