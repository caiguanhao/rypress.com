<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Objective-C Data Types | NSSet | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../../media/style.css" />
  <link rel="icon" type="image/png" href="../../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../../media/single.css" />
  <link rel="stylesheet" href="../../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../../index.html'>RyPress</a></li><li><a href='../../../tutorials.html'>Tutorials</a></li><li><a href='../../../sponsors.html'>Sponsors</a></li><li><a href='../../../about.html'>About</a></li><li><a href='../../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='../data-types.html'><span>&lsaquo;</span> Back to <em>Objective-C Data Types</em></a>
</p>

<h1 class='back-heading'>NSSet</h1>

<p><code>NSSet</code>, <code>NSArray</code>, and <code>NSDictionary</code> are
the three core collection classes defined by the Foundation Framework. An
<code>NSSet</code> object represents a static, unordered collection of distinct
objects. Sets are optimized for membership checking, so if you&rsquo;re asking
a lot of &ldquo;is this object part of this group?&rdquo; kind of questions,
you should be using a set&mdash;not an <a href='nsarray.html'>array</a>.</p>

<figure>
	<img style='max-width: 500px' src='../media/data-types/nsset.png' />
	<figcaption>The basic collection classes of the
Foundation Framework</figcaption>
</figure>

<p>Collections can only interact with Objective-C objects. As a result,
primitive C types like <code>int</code> need to be wrapped in an <a
href='nsnumber.html'><code>NSNumber</code></a> before you can store them in a
set, array, or dictionary.</p>

<p><code>NSSet</code> is immutable, so you cannot add or remove elements from a
set after it&rsquo;s been created. You can, however, alter mutable objects that
are contained in the set. For example, if you stored an
<code>NSMutableString</code>, you&rsquo;re free to call
<code>setString:</code>, <code>appendFormat:</code>, and the other manipulation
methods on that object. This module also covers <a
href='nsset.html#nsmutableset'><code>NSMutableSet</code></a> and <a
href='nsset.html#nscountedset'><code>NSCountedSet</code></a>.</p>


<h2>Creating Sets</h2>

<p>An <code>NSSet</code> can be created through the
<code>setWithObjects:</code> class method, which accepts a
<code>nil</code>-terminated list of objects. Most of the examples in this
module utilize strings, but an <code>NSSet</code> instance can record
<em>any</em> kind of Objective-C object, and it does not have to be
homogeneous.</p>

<div class="highlight"><pre><span class="nb">NSSet</span> <span class="o">*</span><span class="n">americanMakes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Chrysler&quot;</span><span class="p">,</span> <span class="s">@&quot;Ford&quot;</span><span class="p">,</span>
                                             <span class="s">@&quot;General Motors&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">americanMakes</span><span class="p">)</span><span class="o">;</span>
</pre></div>


<p><code>NSSet</code> also includes a <code>setWithArray:</code> method, which
turns an <code>NSArray</code> into an <code>NSSet</code>. Remember that sets
are composed of <em>unique</em> elements, so this serves as a convenient way to
remove all duplicates in an array. For example:</p>

<div class="highlight"><pre><span class="nb">NSArray</span> <span class="o">*</span><span class="n">japaneseMakes</span> <span class="o">=</span> <span class="p">@[</span><span class="s">@&quot;Honda&quot;</span><span class="p">,</span> <span class="s">@&quot;Mazda&quot;</span><span class="p">,</span>
                           <span class="s">@&quot;Mitsubishi&quot;</span><span class="p">,</span> <span class="s">@&quot;Honda&quot;</span><span class="p">];</span>
<span class="nb">NSSet</span> <span class="o">*</span><span class="n">uniqueMakes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSet</span> <span class="nf">setWithArray:</span><span class="n">japaneseMakes</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">uniqueMakes</span><span class="p">)</span><span class="o">;</span>    <span class="c1">// Honda, Mazda, Mitsubishi</span>
</pre></div>


<p>Sets maintain a <a href='../properties.html#the-strong-attribute'>strong
relationship</a> with their elements. That is to say, a set owns each item that
it contains. You should be careful to avoid retain cycles when creating sets of
custom objects by ensuring that an element in the set never has a strong
reference to the set itself.</p>


<h2 id='enumerating-sets'>Enumerating Sets</h2>

<p>Fast-enumeration is the preferred method of iterating through the contents
of a set, and the <code>count</code> method can be used to calculate the total
number of items. For example:</p>

<div class="highlight"><pre><span class="nb">NSSet</span> <span class="o">*</span><span class="n">models</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Civic&quot;</span><span class="p">,</span> <span class="s">@&quot;Accord&quot;</span><span class="p">,</span>
                                      <span class="s">@&quot;Odyssey&quot;</span><span class="p">,</span> <span class="s">@&quot;Pilot&quot;</span><span class="p">,</span> <span class="s">@&quot;Fit&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The set has %li elements&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">models</span> <span class="nf">count</span><span class="p">])</span><span class="o">;</span>
<span class="k">for</span> <span class="p">(</span><span class="kt">id</span> <span class="n">item</span> <span class="k">in</span> <span class="n">models</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">item</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>


<p>If you&rsquo;re interested in a <a href='../blocks.html'>block</a>-based
solution, you can also use the <code>enumerateObjectsUsingBlock:</code> method
to process the contents of a set. The method&rsquo;s only parameter is a
<code>^(id obj, BOOL *stop)</code> block. <code>obj</code> is the current
object, and the <code>*stop</code> pointer lets you prematurely exit the
iteration by setting its value to <code>YES</code>, as demonstrated below.</p>

<div class="highlight"><pre><span class="p">[</span><span class="n">models</span> <span class="nf">enumerateObjectsUsingBlock:</span><span class="o">^</span><span class="p">(</span><span class="kt">id</span> <span class="n">obj</span><span class="p">,</span> <span class="kt">BOOL</span> <span class="o">*</span><span class="n">stop</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Current item: %@&quot;</span><span class="p">,</span> <span class="n">obj</span><span class="p">);</span>
    <span class="k">if</span> <span class="p">([</span><span class="n">obj</span> <span class="nf">isEqualToString:</span><span class="s">@&quot;Fit&quot;</span><span class="p">])</span> <span class="p">{</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;I was looking for a Honda Fit, and I found it!&quot;</span><span class="p">);</span>
        <span class="o">*</span><span class="n">stop</span> <span class="o">=</span> <span class="kc">YES</span><span class="p">;</span>    <span class="c1">// Stop enumerating items</span>
    <span class="p">}</span>
<span class="p">}];</span>
</pre></div>


<p>The <code>*stop = YES</code> line tells the set to stop enumerating once it
reaches the <code>@"Fit"</code> element. This the block equivalent of the
<code>break</code> statement.</p>

<p>Note that since sets are unordered, it usually doesn&rsquo;t make sense to
access an element outside of an enumeration. Accordingly, <code>NSSet</code>
does <em>not</em> support subscripting syntax for accessing individual elements
(e.g., <code>models[i]</code>). This is one of the primary differences between
sets and <a href='nsarray.html'>arrays</a>/<a
href='nsdictionary.html'>dictionaries</a>.</p>


<h2 id='comparing-sets'>Comparing Sets</h2>

<p>In addition to equality, two <code>NSSet</code> objects can be checked for
subset and intersection status. All three of these comparisons are demonstrated
in the following example.</p>

<div class="highlight"><pre><span class="nb">NSSet</span> <span class="o">*</span><span class="n">japaneseMakes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Honda&quot;</span><span class="p">,</span> <span class="s">@&quot;Nissan&quot;</span><span class="p">,</span>
                              <span class="s">@&quot;Mitsubishi&quot;</span><span class="p">,</span> <span class="s">@&quot;Toyota&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="nb">NSSet</span> <span class="o">*</span><span class="n">johnsFavoriteMakes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Honda&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="nb">NSSet</span> <span class="o">*</span><span class="n">marysFavoriteMakes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Toyota&quot;</span><span class="p">,</span>
                                                  <span class="s">@&quot;Alfa Romeo&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>

<span class="k">if</span> <span class="p">([</span><span class="n">johnsFavoriteMakes</span> <span class="nf">isEqualToSet:</span><span class="n">japaneseMakes</span><span class="p">])</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;John likes all the Japanese auto makers and no others&quot;</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">if</span> <span class="p">([</span><span class="n">johnsFavoriteMakes</span> <span class="nf">intersectsSet:</span><span class="n">japaneseMakes</span><span class="p">])</span> <span class="p">{</span>
    <span class="c1">// You&#39;ll see this message</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;John likes at least one Japanese auto maker&quot;</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">if</span> <span class="p">([</span><span class="n">johnsFavoriteMakes</span> <span class="nf">isSubsetOfSet:</span><span class="n">japaneseMakes</span><span class="p">])</span> <span class="p">{</span>
    <span class="c1">// And this one, too</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;All of the auto makers that John likes are Japanese&quot;</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">if</span> <span class="p">([</span><span class="n">marysFavoriteMakes</span> <span class="nf">isSubsetOfSet:</span><span class="n">japaneseMakes</span><span class="p">])</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;All of the auto makers that Mary likes are Japanese&quot;</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>



<h2>Membership Checking</h2>

<p>Like all Foundation Framework collections, it&rsquo;s possible to check if
an object is in a particular <code>NSSet</code>. The
<code>containsObject:</code> method returns a <code>BOOL</code> indicating the
membership status of the argument. As an alternative, the <code>member:</code>
returns a reference to the object if it&rsquo;s in the set, otherwise
<code>nil</code>. This can be convenient depending on how you&rsquo;re using
the set.</p>

<div class="highlight"><pre><span class="nb">NSSet</span> <span class="o">*</span><span class="n">selectedMakes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Maserati&quot;</span><span class="p">,</span>
                                             <span class="s">@&quot;Porsche&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="c1">// BOOL checking</span>
<span class="k">if</span> <span class="p">([</span><span class="n">selectedMakes</span> <span class="nf">containsObject:</span><span class="s">@&quot;Maserati&quot;</span><span class="p">])</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The user seems to like expensive cars&quot;</span><span class="p">);</span>
<span class="p">}</span>
<span class="c1">// nil checking</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="n">result</span> <span class="o">=</span> <span class="p">[</span><span class="n">selectedMakes</span> <span class="nf">member:</span><span class="s">@&quot;Maserati&quot;</span><span class="p">];</span>
<span class="k">if</span> <span class="p">(</span><span class="n">result</span> <span class="o">!=</span> <span class="kc">nil</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@ is one of the selected makes&quot;</span><span class="p">,</span> <span class="n">result</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>


<p>Again, this is one of the strong suits of sets, so if you&rsquo;re doing a
lot of membership checking, you should be using <code>NSSet</code> instead of
<code>NSArray</code> (unless you have a compelling reason not to).</p>


<h2 id='filtering-sets'>Filtering Sets</h2>

<p>You can filter the contents of a set using the
<code>objectsPassingTest:</code> method, which accepts a block that is called
using each item in the set. The block should return <code>YES</code> if the
current object should be included in the new set, and <code>NO</code> if it
shouldn&rsquo;t. The following example finds all items that begin with an
uppercase letter <code>C</code>.</p>

<div class="highlight"><pre><span class="nb">NSSet</span> <span class="o">*</span><span class="n">toyotaModels</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Corolla&quot;</span><span class="p">,</span> <span class="s">@&quot;Sienna&quot;</span><span class="p">,</span>
                       <span class="s">@&quot;Camry&quot;</span><span class="p">,</span> <span class="s">@&quot;Prius&quot;</span><span class="p">,</span>
                       <span class="s">@&quot;Highlander&quot;</span><span class="p">,</span> <span class="s">@&quot;Sequoia&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="nb">NSSet</span> <span class="o">*</span><span class="n">cModels</span> <span class="o">=</span> <span class="p">[</span><span class="n">toyotaModels</span> <span class="nf">objectsPassingTest:</span><span class="o">^</span><span class="kt">BOOL</span><span class="p">(</span><span class="kt">id</span> <span class="n">obj</span><span class="p">,</span> <span class="kt">BOOL</span> <span class="o">*</span><span class="n">stop</span><span class="p">)</span> <span class="p">{</span>
    <span class="k">if</span> <span class="p">([</span><span class="n">obj</span> <span class="nf">hasPrefix:</span><span class="s">@&quot;C&quot;</span><span class="p">])</span> <span class="p">{</span>
        <span class="k">return</span> <span class="kc">YES</span><span class="p">;</span>
    <span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
        <span class="k">return</span> <span class="kc">NO</span><span class="p">;</span>
    <span class="p">}</span>
<span class="p">}];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">cModels</span><span class="p">)</span><span class="o">;</span>    <span class="c1">// Corolla, Camry</span>
</pre></div>


<p>Because <code>NSSet</code> is immutable, the
<code>objectsPassingTest:</code> method returns a <em>new</em> set instead of
altering the existing one. This is the same behavior as many of the
<code>NSString</code> manipulation operations. But, while the <em>set</em> is a
new instance, it still refers to the same <em>elements</em> as the original
set. That is to say, filtered elements are not copied&mdash;they are
referenced.</p>


<h2>Combining Sets</h2>

<p>Sets can be combined using the <code>setByAddingObjectsFromSet:</code>
method. Since sets are unique, duplicates will be ignored if both sets contain
the same object.</p>

<div class="highlight"><pre><span class="nb">NSSet</span> <span class="o">*</span><span class="n">affordableMakes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Ford&quot;</span><span class="p">,</span> <span class="s">@&quot;Honda&quot;</span><span class="p">,</span>
                                     <span class="s">@&quot;Nissan&quot;</span><span class="p">,</span> <span class="s">@&quot;Toyota&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="nb">NSSet</span> <span class="o">*</span><span class="n">fancyMakes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Ferrari&quot;</span><span class="p">,</span> <span class="s">@&quot;Maserati&quot;</span><span class="p">,</span>
                                          <span class="s">@&quot;Porsche&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="nb">NSSet</span> <span class="o">*</span><span class="n">allMakes</span> <span class="o">=</span> <span class="p">[</span><span class="n">affordableMakes</span> <span class="nf">setByAddingObjectsFromSet:</span><span class="n">fancyMakes</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">allMakes</span><span class="p">)</span><span class="o">;</span>
</pre></div>



<h1 id='nsmutableset'>NSMutableSet</h1>

<p>Mutable sets allow you to add or delete objects dynamically, which affords a
whole lot more flexibility than the static <code>NSSet</code>. In addition to
membership checking, mutable sets are also more efficient at inserting and
removing elements than <code>NSMutableArray</code>.</p>

<p><code>NSMutableSet</code> can be very useful for recording the state of a
system. For example, if you were writing an application to manage an auto
repair shop, you might maintain a mutable set called <code>repairedCars</code>
and add/remove cars to reflect whether or not they have been fixed yet.</p>


<h2>Creating Mutable Sets</h2>

<p>Mutable sets can be created with the exact same methods as
<code>NSSet</code>. Or, you can create an empty set with the
<code>setWithCapacity:</code> class method. The argument defines the initial
amount of space allocated for the set, but it in no way limits the number of
items it can hold.</p>

<div class="highlight"><pre><span class="nb">NSMutableSet</span> <span class="o">*</span><span class="n">brokenCars</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableSet</span> <span class="nf">setWithObjects:</span>
                            <span class="s">@&quot;Honda Civic&quot;</span><span class="p">,</span> <span class="s">@&quot;Nissan Versa&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="nb">NSMutableSet</span> <span class="o">*</span><span class="n">repairedCars</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableSet</span> <span class="nf">setWithCapacity:</span><span class="mi">5</span><span class="p">];</span>
</pre></div>



<h2>Adding and Removing Objects</h2>

<p>The big additions provided by <code>NSMutableSet</code> are the
<code>addObject:</code> and <code>removeObject:</code> methods. Note that
<code>addObject:</code> won&rsquo;t actually do anything if the object is
already a member of the collection because sets are composed of unique
items.</p>

<div class="highlight"><pre><span class="nb">NSMutableSet</span> <span class="o">*</span><span class="n">brokenCars</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableSet</span> <span class="nf">setWithObjects:</span>
                            <span class="s">@&quot;Honda Civic&quot;</span><span class="p">,</span> <span class="s">@&quot;Nissan Versa&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="nb">NSMutableSet</span> <span class="o">*</span><span class="n">repairedCars</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableSet</span> <span class="nf">setWithCapacity:</span><span class="mi">5</span><span class="p">];</span>
<span class="c1">// &quot;Fix&quot; the Honda Civic</span>
<span class="p">[</span><span class="n">brokenCars</span> <span class="nf">removeObject:</span><span class="s">@&quot;Honda Civic&quot;</span><span class="p">];</span>
<span class="p">[</span><span class="n">repairedCars</span> <span class="nf">addObject:</span><span class="s">@&quot;Honda Civic&quot;</span><span class="p">];</span>

<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Broken cars: %@&quot;</span><span class="p">,</span> <span class="n">brokenCars</span><span class="p">)</span><span class="o">;</span>     <span class="c1">// Nissan Versa</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Repaired cars: %@&quot;</span><span class="p">,</span> <span class="n">repairedCars</span><span class="p">)</span><span class="o">;</span> <span class="c1">// Honda Civic</span>
</pre></div>


<p>Just like mutable strings, <code>NSMutableSet</code> has a different
workflow than the static <code>NSSet</code>. Instead of generating a new set
and re-assigning it to the variable, you can operate directly on the existing
set.</p>

<p>You may also find the <code>removeAllObjects</code> method useful for
completely clearing a set.</p>


<h2 id='filtering-with-predicates'>Filtering With Predicates</h2>

<p>There is no mutable version of the <code>objectsPassingTest:</code> method,
but you can still filter items with <code>filterUsingPredicate:</code>.
Predicates are somewhat outside the scope of this tutorial, but suffice it to
say that they are designed to make it easier to define search/filter rules.
Fortunately, the <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Classes/NSPredicate_Class/Reference/NSPredicate.html'><code>NSPredicate</code></a>
class can be initialized with a block, so we don&rsquo;t need to learn an
entirely new format syntax.</p>

<p>The following code snippet is the mutable, predicate-based version of the
example from the <a href='nsset.html#filtering-sets'>Filtering Sets</a> section
above. Again, this operates directly on the existing set.</p>

<div class="highlight"><pre><span class="nb">NSMutableSet</span> <span class="o">*</span><span class="n">toyotaModels</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Corolla&quot;</span><span class="p">,</span> <span class="s">@&quot;Sienna&quot;</span><span class="p">,</span>
                              <span class="s">@&quot;Camry&quot;</span><span class="p">,</span> <span class="s">@&quot;Prius&quot;</span><span class="p">,</span>
                              <span class="s">@&quot;Highlander&quot;</span><span class="p">,</span> <span class="s">@&quot;Sequoia&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="nb">NSPredicate</span> <span class="o">*</span><span class="n">startsWithC</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSPredicate</span> <span class="nf">predicateWithBlock:</span>
                            <span class="o">^</span><span class="kt">BOOL</span><span class="p">(</span><span class="kt">id</span> <span class="n">evaluatedObject</span><span class="p">,</span> <span class="nb">NSDictionary</span> <span class="o">*</span><span class="n">bindings</span><span class="p">)</span> <span class="p">{</span>
                                <span class="k">if</span> <span class="p">([</span><span class="n">evaluatedObject</span> <span class="nf">hasPrefix:</span><span class="s">@&quot;C&quot;</span><span class="p">])</span> <span class="p">{</span>
                                    <span class="k">return</span> <span class="kc">YES</span><span class="p">;</span>
                                <span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
                                    <span class="k">return</span> <span class="kc">NO</span><span class="p">;</span>
                                <span class="p">}</span>
                            <span class="p">}];</span>
<span class="p">[</span><span class="n">toyotaModels</span> <span class="nf">filterUsingPredicate:</span><span class="n">startsWithC</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">toyotaModels</span><span class="p">)</span><span class="o">;</span>    <span class="c1">// Corolla, Camry</span>
</pre></div>


<p>For more information about predicates, please visit the official <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Conceptual/Predicates/predicates.html%23/apple_ref/doc/uid/TP40001798-SW1'>Predicate
Programming Guide</a>.</p>


<h2>Set Theory Operations</h2>

<p><code>NSMutableSet</code> also provides an API for the basic operations in
set theory. These methods let you take the union, intersection, and relative
complement of two sets. In addition, the <code>setSet:</code> method is also
useful for creating a shallow copy of a different set. All of these are
included in the following example.</p>

<div class="highlight"><pre><span class="nb">NSSet</span> <span class="o">*</span><span class="n">japaneseMakes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Honda&quot;</span><span class="p">,</span> <span class="s">@&quot;Nissan&quot;</span><span class="p">,</span>
                        <span class="s">@&quot;Mitsubishi&quot;</span><span class="p">,</span> <span class="s">@&quot;Toyota&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="nb">NSSet</span> <span class="o">*</span><span class="n">johnsFavoriteMakes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Honda&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="nb">NSSet</span> <span class="o">*</span><span class="n">marysFavoriteMakes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Toyota&quot;</span><span class="p">,</span>
                             <span class="s">@&quot;Alfa Romeo&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>

<span class="nb">NSMutableSet</span> <span class="o">*</span><span class="n">result</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableSet</span> <span class="nf">setWithCapacity:</span><span class="mi">5</span><span class="p">];</span>
<span class="c1">// Union</span>
<span class="p">[</span><span class="n">result</span> <span class="nf">setSet:</span><span class="n">johnsFavoriteMakes</span><span class="p">];</span>
<span class="p">[</span><span class="n">result</span> <span class="nf">unionSet:</span><span class="n">marysFavoriteMakes</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Either John&#39;s or Mary&#39;s favorites: %@&quot;</span><span class="p">,</span> <span class="n">result</span><span class="p">)</span><span class="o">;</span>
<span class="c1">// Intersection</span>
<span class="p">[</span><span class="n">result</span> <span class="nf">setSet:</span><span class="n">johnsFavoriteMakes</span><span class="p">];</span>
<span class="p">[</span><span class="n">result</span> <span class="nf">intersectSet:</span><span class="n">japaneseMakes</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;John&#39;s favorite Japanese makes: %@&quot;</span><span class="p">,</span> <span class="n">result</span><span class="p">)</span><span class="o">;</span>
<span class="c1">// Relative Complement</span>
<span class="p">[</span><span class="n">result</span> <span class="nf">setSet:</span><span class="n">japaneseMakes</span><span class="p">];</span>
<span class="p">[</span><span class="n">result</span> <span class="nf">minusSet:</span><span class="n">johnsFavoriteMakes</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Japanese makes that are not John&#39;s favorites: %@&quot;</span><span class="p">,</span>
      <span class="n">result</span><span class="p">);</span>
</pre></div>



<h2 id='enumeration-considerations'>Enumeration Considerations</h2>

<p>Iterating over a mutable set works the same as a static set, with one very
important caveat: you aren&rsquo;t allowed to change the set while you&rsquo;re
enumerating it. This is a general rule for any collection class.<p>

<p>The following example demonstrates the <em>wrong</em> way to mutate a set in
the middle of a for-in loop. We&rsquo;ll be using the rather contrived scenario
of removing <code>@"Toyota"</code> if any element in the set begins with the
letter <code>T</code>.</p>

<div class="highlight"><pre><span class="c1">// DO NOT DO THIS. EVER.</span>
<span class="nb">NSMutableSet</span> <span class="o">*</span><span class="n">makes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Ford&quot;</span><span class="p">,</span> <span class="s">@&quot;Honda&quot;</span><span class="p">,</span>
                       <span class="s">@&quot;Nissan&quot;</span><span class="p">,</span> <span class="s">@&quot;Toyota&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="k">for</span> <span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="n">make</span> <span class="k">in</span> <span class="n">makes</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">make</span><span class="p">);</span>
    <span class="k">if</span> <span class="p">([</span><span class="n">make</span> <span class="nf">hasPrefix:</span><span class="s">@&quot;T&quot;</span><span class="p">])</span> <span class="p">{</span>
        <span class="c1">// Throws an NSGenericException:</span>
        <span class="c1">// &quot;Collection was mutated while being enumerated&quot;</span>
        <span class="p">[</span><span class="n">makes</span> <span class="nf">removeObject:</span><span class="s">@&quot;Toyota&quot;</span><span class="p">];</span>
    <span class="p">}</span>
<span class="p">}</span>
</pre></div>


<p>The proper way to do this is shown below. Instead of iterating over the set
directly, you should create a temporary copy of it with the
<code>allObjects</code> method and iterate over that. This frees you to alter
the original set without any unintended consequences:</p>

<div class="highlight"><pre><span class="nb">NSMutableSet</span> <span class="o">*</span><span class="n">makes</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSMutableSet</span> <span class="nf">setWithObjects:</span><span class="s">@&quot;Ford&quot;</span><span class="p">,</span> <span class="s">@&quot;Honda&quot;</span><span class="p">,</span>
                       <span class="s">@&quot;Nissan&quot;</span><span class="p">,</span> <span class="s">@&quot;Toyota&quot;</span><span class="p">,</span> <span class="kc">nil</span><span class="p">];</span>
<span class="nb">NSArray</span> <span class="o">*</span><span class="n">snapshot</span> <span class="o">=</span> <span class="p">[</span><span class="n">makes</span> <span class="nf">allObjects</span><span class="p">];</span>
<span class="k">for</span> <span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="n">make</span> <span class="k">in</span> <span class="n">snapshot</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">make</span><span class="p">);</span>
    <span class="k">if</span> <span class="p">([</span><span class="n">make</span> <span class="nf">hasPrefix:</span><span class="s">@&quot;T&quot;</span><span class="p">])</span> <span class="p">{</span>
        <span class="p">[</span><span class="n">makes</span> <span class="nf">removeObject:</span><span class="s">@&quot;Toyota&quot;</span><span class="p">];</span>
    <span class="p">}</span>
<span class="p">}</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">makes</span><span class="p">);</span>
</pre></div>



<h1 id='nscountedset'>NSCountedSet</h1>

<p>The <code>NSCountedSet</code> class (also called a &ldquo;bag&rdquo;) is
worth a brief mention. It&rsquo;s a subclass of <code>NSMutableSet</code>, but
instead of being limited to <em>unique</em> values, it counts the number of
times an object has been added to the collection. This is a very efficient way
to keep object tallies, as it requires only one instance of an object
regardless of how many times it&rsquo;s been added to the bag.</p>

<p>The main difference between a mutable set and <code>NSCountedSet</code> is
the <code>countForObject:</code> method. This will often be used in place of
<code>containsObject:</code> (which still works as expected). For example:</p>

<div class="highlight"><pre><span class="nb">NSCountedSet</span> <span class="o">*</span><span class="n">inventory</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSCountedSet</span> <span class="nf">setWithCapacity:</span><span class="mi">5</span><span class="p">];</span>
<span class="p">[</span><span class="n">inventory</span> <span class="nf">addObject:</span><span class="s">@&quot;Honda Accord&quot;</span><span class="p">];</span>
<span class="p">[</span><span class="n">inventory</span> <span class="nf">addObject:</span><span class="s">@&quot;Honda Accord&quot;</span><span class="p">];</span>
<span class="p">[</span><span class="n">inventory</span> <span class="nf">addObject:</span><span class="s">@&quot;Nissan Altima&quot;</span><span class="p">];</span>
<span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;There are %li Accords in stock and %li Altima&quot;</span><span class="p">,</span>
      <span class="p">[</span><span class="n">inventory</span> <span class="nf">countForObject:</span><span class="s">@&quot;Honda Accord&quot;</span><span class="p">],</span>    <span class="c1">// 2</span>
      <span class="p">[</span><span class="n">inventory</span> <span class="nf">countForObject:</span><span class="s">@&quot;Nissan Altima&quot;</span><span class="p">]);</span>  <span class="c1">// 1</span>
</pre></div>


<p>Please see the <a
href='http://developer.apple.com/library/ios/#documentation/cocoa/reference/foundation/Classes/NSCountedSet_Class/Reference/Reference.html'>official
documentation</a> for more details.</p>

<p class='sequential-nav'>
	<a href='nsarray.html'>Continue to <em>NSArray</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>