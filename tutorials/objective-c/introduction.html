<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Introduction | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Objective-C
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Introduction</h1>

<p>Objective-C is the native programming language for Apple&rsquo;s iOS and
OS&nbsp;X operating systems. It&rsquo;s a compiled, general-purpose language
capable of building everything from command line utilities to animated GUIs to
domain-specific libraries. It also provides many tools for maintaining large,
scalable frameworks.</p>

<figure>
	<img style='max-width: 370px' src='media/introduction/obj-c-overview.png' />
	<figcaption>Types of programs written
in Objective-C</figcaption>
</figure>

<p>Like C++, Objective-C was designed to add object-oriented features
to&nbsp;C, but the two languages accomplished this using fundamentally distinct
philosophies. Objective-C is decidedly more dynamic, deferring most of its
decisions to run-time rather than compile-time. This is reflected in many of
the design patterns underlying iOS and OS&nbsp;X development.</p>

<p>Objective-C is also known for its verbose naming conventions. The resulting
code is so descriptive that it&rsquo;s virtually impossible to misunderstand or
misuse it. For example, the following snippet shows a C++ method call with its
Objective-C equivalent.</p>

<div class="highlight"><pre><span class="c1">// C++</span>
<span class="n">john</span><span class="o">-&gt;</span><span class="n">drive</span><span class="p">(</span><span class="s">&quot;Corvette&quot;</span><span class="p">,</span> <span class="s">&quot;Mary&#39;s House&quot;</span><span class="p">)</span>
<span class="c1">// Objective-C</span>
<span class="p">[</span><span class="n">john</span> <span class="nf">driveCar:</span><span class="s">@&quot;Corvette&quot;</span> <span class="nf">toDestination:</span><span class="s">@&quot;Mary&#39;s House&quot;</span><span class="p">]</span>
</pre></div>


<p>As you can see, Objective-C methods read more like a human language than a
computer one. Once you get used to this, it becomes very easy to orient
yourself in new projects and to work with third-party code. If you&rsquo;re a
little bit disarmed by the square-brackets, don&rsquo;t worry. You&rsquo;ll be
quite comfortable with them by the end of the tutorial.</p>


<h2>Frameworks</h2>

<p>As with most programming languages, Objective-C is a relatively simple
syntax backed by an extensive standard library. This tutorial focuses mostly on
the language itself, but it helps to have at least some idea of the tools that
you&rsquo;ll be interacting with in the real world.</p>

<p>There are a few different &ldquo;standard libraries&rdquo; out there, but
Apple&rsquo;s <a
href='https://developer.apple.com/technologies/mac/cocoa.html'>Cocoa</a> and <a
href='https://developer.apple.com/technologies/ios/cocoa-touch.html'>Cocoa
Touch</a> frameworks are by far the most popular. These define the API for
building OS&nbsp;X and iOS apps, respectively. The table below highlights some
of the key frameworks in Cocoa and Cocoa Touch. For a more detailed discussion,
please visit the <a
href='https://developer.apple.com/library/mac/#documentation/MacOSX/Conceptual/OSX_Technology_Overview/About/About.html'>Mac
Technology Overview</a> or <a
href='http://developer.apple.com/library/ios/#documentation/miscellaneous/conceptual/iphoneostechoverview/Introduction/Introduction.html'>iOS
Technology Overview</a>.</p>

<table class='multiline'>
<thead>
	<tr>
		<th>Framework</th>
		<th>Description</th>
	</tr>
</thead>
<tbody>

	<tr>
		<td><a href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/ObjC_classic/_index.html'>Foundation</a></td>
		<td>Defines core object-oriented data types like strings, arrays,
		dictionaries, etc. We&rsquo;ll explore the essential aspects of this
		framework in the <a href='data-types.html'>Data Types</a> module.</td>
	</tr>

	<tr>
		<td><a href='http://developer.apple.com/library/ios/#documentation/uikit/reference/UIKit_Framework/_index.html'>UIKit</a></td>
		<td>Provides dozens of classes for creating and controlling the user
		interface on iOS devices.</td> 
	</tr>
	<tr>
		<td><a
		href='http://developer.apple.com/library/ios/#documentation/uikit/reference/UIKit_Framework/_index.html'>AppKit</a></td>
		<td>Same as UIKit, but for OS&nbsp;X devices.</td> 
	</tr>
	<tr>
		<td><a
		href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/CoreData_ObjC/_index.html'>CoreData</a></td>
		<td>Provides a convenient API for managing object relationships,
		supporting undo/redo functionality, and interacting with persistent
		storage.</td>
	</tr>
	<tr>
		<td><a href='http://developer.apple.com/library/ios/#documentation/MediaPlayer/Reference/MediaPlayer_Framework/_index.html%23/apple_ref/doc/uid/TP40006952'>MediaPlayer</a></td>
		<td>Defines a high-level API for playing music, presenting videos, and
		accessing the user&rsquo;s iTunes library.</td>
	</tr>
	<tr>
		<td><a href='https://developer.apple.com/library/mac/documentation/AVFoundation/Reference/AVFoundationFramework/_index.html'>AVFoundation</a></td>
		<td>Provides lower-level support for playing, recording, and
		integrating audio/video into custom applications.</td>
	</tr>
	<tr>
		<td><a href='http://developer.apple.com/library/mac/#documentation/graphicsimaging/reference/QuartzCoreRefCollection/_index.html'>QuartzCore</a></td>
		<td>Contains two sub-frameworks for manipulating images. The
		<code>CoreAnimation</code> framework lets you animate UI components,
		and <code>CoreImage</code> provides image and video
		processing capabilities (e.g., filters).</td>
	</tr>
	<tr>
		<td><a href='http://developer.apple.com/library/ios/#documentation/coregraphics/reference/coregraphics_framework/_index.html'>CoreGraphics</a></td>
		<td>Provides low-level 2D drawing support. Handles path-based drawing,
		transformations, image creation, etc.</td>
	</tr>
</tbody>
</table>

<p>After you&rsquo;re comfortable with Objective-C, these are some of the tools
that you&rsquo;ll be leveraging to build iOS and OS&nbsp;X applications. But
again, this tutorial is not meant to be a comprehensive guide to app
development&mdash;it&rsquo;s designed to <em>prepare you</em> to use the above
frameworks. With the exception of the Foundation Framework, we won&rsquo;t
actually be working with any of these libraries.</p>


<h2>Xcode</h2>

<p><a href='https://developer.apple.com/xcode/'>Xcode</a> is Apple&rsquo;s
integrated development environment (IDE) for Mac, iPhone, and iPad app
development. It includes not only a source code editor, but also an interface
builder, a device simulator, a comprehensive testing and debugging suite, the
frameworks discussed in the previous section, and everything else you need to
make apps.</p>

<p>While there are other ways to compile Objective-C code, Xcode is definitely
the easiest. We strongly recommended that you install Xcode now so you can
follow along with the examples in this tutorial. It is freely available through
the <a href='https://itunes.apple.com/us/app/xcode/id497799835?ls=1&mt=12'>Mac
App Store</a>.</p>


<h3>Creating an Application</h3>

<p>Xcode provides several templates for various types of iOS and OS&nbsp;X
applications. All of them can be found by navigating to <em>File &gt; New &gt;
Project...</em> or using the <em>Cmd+Shift+N</em> shortcut. This will open a
dialog window asking you to select a template:</p>

<figure>
	<img style='max-width: 543px' src='media/introduction/creating-cmd-line-app.png' />
	<figcaption>Creating a command
line application</figcaption>
</figure>

<p>For this tutorial, we&rsquo;ll be using the <em>Command Line Tool</em>
template found under <em>OS&nbsp;X &gt; Application</em>, highlighted in the
above screenshot. This lets us strip away all of the elements specific to
iOS/OS&nbsp;X and focus on Objective-C as a language. Go ahead and create a new
<em>Command Line Tool</em> now. This opens another dialog asking you to
configure the project:</p>

<figure>
	<img style='max-width: 522px' src='media/introduction/configuring-cmd-line-app.png' />
	<figcaption>Configuring a
command line application</figcaption>
</figure>

<p>You can use whatever you like for the <em>Product Name</em> and
<em>Organization Name</em> fields. For the <em>Company Identifier</em> use
<code>edu.self</code>, which is the canonical private use identifier. For
production applications, you&rsquo;ll need to get a real company ID from Apple
by <a href='https://developer.apple.com/programs/register/'>registering as a
developer</a>.</p>

<p>This tutorial utilizes several classes defined in the Foundation Framework,
so be sure to select <em>Foundation</em> for the <em>Type</em> field. Finally,
the <em>Use Automatic Reference Counting</em> checkbox should always be
selected for new projects.</p>

<p>Clicking <em>Next</em> prompts you for a file path to store the project
(save it anywhere you like), and you should now have a brand new Xcode project
to play with. In the left-hand column of the Xcode IDE, you&rsquo;ll find a
file called <code>main.m</code> (along with some other files and folders). At
the moment, this file contains the entirety of your application. Note that the
<code>.m</code> extension is used for Objective-C source files.</p>

<figure>
	<img style='max-width: 258px' src='media/introduction/xcode-project-navigator.png' />
	<figcaption><code>main.m</code> in the Project Navigator</figcaption>
</figure>

<p>To compile the project, click the <em>Run</em> button in the upper-left
corner of the IDE or use the <em>Cmd+R</em> shortcut. This should display
<code>Hello, World!</code> in the <em>Output Panel</em> located at the bottom
of the IDE:</p>

<figure>
	<img style='max-width: 485px'
		 src='media/introduction/xcode-output-panel.png'
		 alt="Xcode's Output Panel displaying 'Hello, World!'" />
	<figcaption>Xcode&rsquo;s Output Panel</figcaption>
</figure>


<h3>The main() Function</h3>

<p>As with plain old C programs, the <code>main()</code> function serves as the
root of an Objective-C application. Most of the built-in Xcode templates create
a file called <code>main.m</code> that defines a default <code>main()</code>
function. Selecting our <code>main.m</code> in Xcode&rsquo;s <em>Project
Navigator</em> panel should open the editor window and display the
following.</p>

<div class="highlight"><pre><span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="c1">// insert code here...</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Hello, World!&quot;</span><span class="p">);</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>Inside of the <code>@autoreleasepool</code> block is where you can write
code and experiment with the snippets from this tutorial. The above
<code>main()</code> function simply calls the global <code>NSLog()</code>
function defined by the Foundation Framework. This is Objective-C&rsquo;s
general-purpose tool for outputting messages to the console. Also note that
Objective-C strings are always prefixed with an at (<code>@</code>) symbol.</p>

<p>Throughout this tutorial, we&rsquo;ll directly edit the above
<code>main.m</code> to see how new language features work, but in the real
world, you&rsquo;ll probably never have to alter the <code>main()</code>
function provided by the template. For most applications, the only thing
<code>main()</code> needs to do is pass control of the program to the
&ldquo;application delegate.&rdquo; For example, the default
<code>main()</code> function for an iOS project looks like the following.</p>

<div class="highlight"><pre><span class="cp">#import</span> <span class="l">&lt;UIKit/UIKit.h&gt;</span><span class="cp"></span>

<span class="cp">#import</span> <span class="l">&quot;AppDelegate.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">char</span> <span class="o">*</span><span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="k">return</span> <span class="n">UIApplicationMain</span><span class="p">(</span><span class="n">argc</span><span class="p">,</span> <span class="n">argv</span><span class="p">,</span> <span class="kc">nil</span><span class="p">,</span> <span class="nb">NSStringFromClass</span><span class="p">([</span><span class="n">AppDelegate</span> <span class="nf">class</span><span class="p">]));</span>
    <span class="p">}</span>
<span class="p">}</span>
</pre></div>


<p>But, since we&rsquo;ll be sticking with command line tools, this is somewhat
outside the scope of this tutorial.</p>


<h2>Get Ready!</h2>

<p>The next two modules explore the basic C syntax. After that, we&rsquo;ll be
ready to dive into classes, methods, protocols, and other object-oriented
constructs. This tutorial is chock-full of hands-on examples, and we encourage
you to paste them into the template project we just created, mess with some
parameters, and see what happens.</p>

<p class='sequential-nav'>
	<a href='c-basics.html'>Continue to <em>C&nbsp;Basics</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>