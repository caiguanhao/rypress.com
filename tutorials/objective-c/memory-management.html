<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Memory Management | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Objective-C
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Memory Management</h1>

<p>As discussed in the <a
href='properties.html#memory-management'>Properties</a> module, the goal of any
memory management system is to reduce the memory footprint of a program by
controlling the lifetime of all its objects. iOS and OS&nbsp;X applications
accomplish this through <strong>object ownership</strong>, which makes sure
objects exist as long as they have to, but no longer.</p>

<p>This object-ownership scheme is implemented through a
<strong>reference-counting system</strong> that internally tracks how many
owners each object has. When you claim ownership of an object, you increase
it&rsquo;s reference count, and when you&rsquo;re done with the object, you
decrease its reference count. While its reference count is greater than zero,
an object is guaranteed to exist, but as soon as the count reaches zero, the
operating system is allowed to destroy it.</p>

<figure>
	<img style='max-width: 400px' src='media/memory-management/reference-counting.png' />
	<figcaption>Destroying an
object with zero references</figcaption>
</figure>

<p>In the past, developers manually controlled an object&rsquo;s reference
count by calling special memory-management methods defined by the <a
href='http://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Protocols/NSObject_Protocol/Reference/NSObject.html%23/apple_ref/occ/intfm/NSObject/isKindOfClass:'><code>NSObject</code>
protocol</a>. This is called <strong>Manual Retain Release (MRR)</strong>.
However, Xcode 4.2 introduced <strong>Automatic Reference Counting
(ARC)</strong>, which automatically inserts all of these method calls for you.
Modern applications should always use ARC, since it&rsquo;s more reliable and
lets you focus on your app&rsquo;s features instead of its memory
management.</p>

<p>This module explains core reference-counting concepts in the context of MRR,
then discusses some of the practical considerations of ARC.</p>


<h1 id='manual-retain-release'>Manual Retain Release</h1>

<p>In a Manual Retain Release environment, it&rsquo;s your job to claim and
relinquish ownership of every object in your program. You do this by calling
special memory-related methods, which are described below.</p>

<table class='multiline'>
<thead>
<tr>
	<th>Method</th>
	<th>Behavior</th>
</tr>
</thead>
<tbody>
<tr>
	<td><code>alloc</code></td>
	<td>Create an object and claim ownership of it.</td>
</tr>
<tr>
	<td><code>retain</code></td>
	<td>Claim ownership of an existing object.</td>
</tr>
<tr>
	<td><code>copy</code></td>
	<td>Copy an object and claim ownership of it.</td>
</tr>
<tr>
	<td><code>release</code></td>
	<td>Relinquish ownership of an object and destroy it immediately.</td>
</tr>
<tr>
	<td><code>autorelease</code></td>
	<td>Relinquish ownership of an object but defer its destruction.</td>
</tr>
</tbody>
</table>

<p>Manually controlling object ownership might seem like a daunting task, but
it&rsquo;s actually very easy. All you have to do is claim ownership of any
object you need and remember to relinquish ownership when you&rsquo;re
done with it. From a practical standpoint, this means that you have to balance
every <code>alloc</code>, <code>retain</code>, and <code>copy</code> call with
a <code>release</code> or <code>autorelease</code> on the same object.</p>

<p>When you forget to balance these calls, one of two things can happen. If you
forget to release an object, its underlying memory is never freed, resulting in
a <strong>memory leak</strong>. Small leaks won&rsquo;t have a visible effect
on your program, but if you eat up enough memory, your program will eventually
crash. On the other hand, if you try to release an object too many times,
you&rsquo;ll have what&rsquo;s called a <strong>dangling pointer</strong>. When
you try to access the dangling pointer, you&rsquo;ll be requesting an invalid
memory address, and your program will most likely crash.</p>

<figure>
	<img style='max-width: 390px' src='media/memory-management/balance.png' />
	<figcaption>Balancing ownership claims
with reliquishes</figcaption>
</figure>

<p>The rest of this section explains how to avoid memory leaks and dangling
pointers through the proper usage of the above methods.</p>


<h2>Enabling MRR</h2>

<p>Before we can experiment with manual memory management, we need to turn off
Automatic Reference Counting. Click the project icon in the <em>Project
Navigator</em>, make sure the <em>Build Settings</em> tab is selected, and
start typing <code>automatic reference counting</code> in the search bar. The
<em>Objective-C Automatic Reference Counting</em> compiler option should
appear. Change it from <em>Yes</em> to <em>No</em>.</p>

<figure>
	<img style='max-width: 459px' src='media/memory-management/turning-off-arc.png' />
	<figcaption>Turning off Automatic
Reference Counting</figcaption>
</figure>

<p>Remember, we&rsquo;re only doing this for instructional purposes&mdash;you
should never use Manual Retain Release for new projects.</p>


<h2>The alloc Method</h2>

<p>We&rsquo;ve been using the <code>alloc</code> method to create objects
throughout this tutorial. But, it&rsquo;s not just allocating memory for the
object, it&rsquo;s also setting its reference count to <code>1</code>. This
makes a lot of sense, since we wouldn&rsquo;t be creating the object if we
didn&rsquo;t want to keep it around for at least a little while.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="nb">NSMutableArray</span> <span class="o">*</span><span class="n">inventory</span> <span class="o">=</span> <span class="p">[[</span><span class="n">NSMutableArray</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="p">[</span><span class="n">inventory</span> <span class="nf">addObject:</span><span class="s">@&quot;Honda Civic&quot;</span><span class="p">];</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">inventory</span><span class="p">);</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>The above code should look familiar. All we&rsquo;re doing is instantiating
a <a href='data-types/nsarray.html#nsmutablearray'>mutable array</a>, adding a
value, and displaying its contents. From a memory-management perspective, we
now own the <code>inventory</code> object, which means it&rsquo;s our
responsibility to release it somewhere down the road.</p>

<p>But, since we <em>haven&rsquo;t</em> released it, our program currently has
a memory leak. We can examine this in Xcode by running our project through the
static analyzer tool. Navigate to <em>Product&nbsp;&gt;&nbsp;Analyze</em> in
the menu bar or use the <em>Shift+Cmd+B</em> keyboard shortcut. This looks for
predictable problems in your code, and it should uncover the following in
<code>main.m</code>.</p>

<figure>
	<img style='max-width: 515px' src='media/memory-management/analyzer-memory-leak.png' />
	<figcaption>A memory leak!
Oh no!</figcaption>
</figure>

<p>This is a small object, so the leak isn&rsquo;t fatal. However, if it
happened over and over (e.g., in a long loop or every time the user clicked a
button), the program would eventually run out of memory and crash.</p>


<h2>The release Method</h2>

<p>The <code>release</code> method relinquishes ownership of an object by
decrementing its reference count. So, we can get rid of our memory leak by
adding the following line after the <code>NSLog()</code> call in
<code>main.m</code>.</p>

<div class="highlight"><pre><span class="p">[</span><span class="n">inventory</span> <span class="nf">release</span><span class="p">];</span>
</pre></div>


<p>Now that our <code>alloc</code> is balanced with a <code>release</code>, the
static analyzer shouldn&rsquo;t output any problems. Typically, you&rsquo;ll
want to relinquish ownership of an object at the end of the same method in
which it was created, as we did here.</p>

<p>Releasing an object too soon creates a dangling pointer. For example, try
moving the above line to <em>before</em> the <code>NSLog()</code> call. Since
<code>release</code> immediately frees the underlying memory, the
<code>inventory</code> variable in <code>NSLog()</code> now points to an
invalid address, and your program will crash with a <code>EXC_BAD_ACCESS</code>
error code when you try to run it:</p>

<figure>
	<img style='max-width: 516px' src='media/memory-management/crash-invalid-address.png' />
	<figcaption>Trying to access an
invalid memory address</figcaption>
</figure>

<p>The point is, don&rsquo;t relinquish ownership of an object before
you&rsquo;re done using it.</p>


<h2>The retain Method</h2>

<p>The <code>retain</code> method claims ownership of an existing object.
It&rsquo;s like telling the operating system, &ldquo;Hey! I need that object
too, so don&rsquo;t get rid of it!&rdquo; This is a necessary ability when
other objects need to make sure their properties refer to a valid instance.</p>

<p>As an example, we&rsquo;ll use <code>retain</code> to create a <a
href='properties.html#the-strong-attribute'>strong reference</a> to our
<code>inventory</code> array. Create a new class called <code>CarStore</code>
and change its header to the following.</p>

<div class="highlight"><pre><span class="c1">// CarStore.h</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">CarStore</span> : <span class="nc">NSObject</span>

<span class="k">-</span> <span class="p">(</span><span class="nb">NSMutableArray</span> <span class="o">*</span><span class="p">)</span><span class="nf">inventory</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">setInventory:</span><span class="p">(</span><span class="nb">NSMutableArray</span> <span class="o">*</span><span class="p">)</span><span class="nv">newInventory</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<p>This manually declares the accessors for a property called
<code>inventory</code>. Our first iteration of <code>CarStore.m</code> provides
a straightforward implementation of the getter and setter, along with an
instance variable to record the object:</p>

<div class="highlight"><pre><span class="c1">// CarStore.m</span>
<span class="cp">#import</span> <span class="l">&quot;CarStore.h&quot;</span><span class="cp"></span>

<span class="k">@implementation</span> <span class="nc">CarStore</span> <span class="p">{</span>
    <span class="nb">NSMutableArray</span> <span class="o">*</span><span class="n">_inventory</span><span class="p">;</span>
<span class="p">}</span>

<span class="o">-</span> <span class="p">(</span><span class="nb">NSMutableArray</span> <span class="o">*</span><span class="p">)</span><span class="n">inventory</span> <span class="p">{</span>
    <span class="k">return</span> <span class="n">_inventory</span><span class="p">;</span>
<span class="p">}</span>

<span class="o">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">setInventory:</span><span class="p">(</span><span class="nb">NSMutableArray</span> <span class="o">*</span><span class="p">)</span><span class="n">newInventory</span> <span class="p">{</span>
    <span class="n">_inventory</span> <span class="o">=</span> <span class="n">newInventory</span><span class="p">;</span>
<span class="p">}</span>

<span class="k">@end</span>
</pre></div>


<p>Back in <code>main.m</code>, let&rsquo;s assign our <code>inventory</code>
variable to <code>CarStore</code>&rsquo;s <code>inventory</code> property:</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;CarStore.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="nb">NSMutableArray</span> <span class="o">*</span><span class="n">inventory</span> <span class="o">=</span> <span class="p">[[</span><span class="n">NSMutableArray</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="p">[</span><span class="n">inventory</span> <span class="nf">addObject:</span><span class="s">@&quot;Honda Civic&quot;</span><span class="p">];</span>
        
        <span class="n">CarStore</span> <span class="o">*</span><span class="n">superstore</span> <span class="o">=</span> <span class="p">[[</span><span class="n">CarStore</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="p">[</span><span class="n">superstore</span> <span class="nf">setInventory:</span><span class="n">inventory</span><span class="p">];</span>
        <span class="p">[</span><span class="n">inventory</span> <span class="nf">release</span><span class="p">];</span>

        <span class="c1">// Do some other stuff...</span>

        <span class="c1">// Try to access the property later on (error!)</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">superstore</span> <span class="nf">inventory</span><span class="p">]);</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>The <code>inventory</code> property in the last line is a dangling pointer
because the object was already released earlier in <code>main.m</code>. Right
now, the <code>superstore</code> object has a <a
href='properties.html#the-weak-attribute'>weak reference</a> to the array. To
turn it into a strong reference, <code>CarStore</code> needs to claim ownership
of the array in its <code>setInventory:</code> accessor:</p>

<div class="highlight"><pre><span class="c1">// CarStore.m</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">setInventory:</span><span class="p">(</span><span class="nb">NSMutableArray</span> <span class="o">*</span><span class="p">)</span><span class="nv">newInventory</span> <span class="p">{</span>
    <span class="n">_inventory</span> <span class="o">=</span> <span class="p">[</span><span class="n">newInventory</span> <span class="nf">retain</span><span class="p">];</span>
<span class="p">}</span>
</pre></div>


<p>This ensures the <code>inventory</code> object won&rsquo;t be released while
<code>superstore</code> is using it. Notice that the <code>retain</code> method
returns the object itself, which lets us perform the retain and assignment in a
single line.</p>

<p>Unfortunately, this code creates another problem: the <code>retain</code>
call isn&rsquo;t balanced with a <code>release</code>, so we have another
memory leak. As soon as we pass another value to <code>setInventory:</code>, we
can&rsquo;t access the old value, which means we can never free it. To fix
this, <code>setInventory:</code> needs to call <code>release</code> on the old
value:</p>

<div class="highlight"><pre><span class="c1">// CarStore.m</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">setInventory:</span><span class="p">(</span><span class="nb">NSMutableArray</span> <span class="o">*</span><span class="p">)</span><span class="nv">newInventory</span> <span class="p">{</span>
    <span class="k">if</span> <span class="p">(</span><span class="n">_inventory</span> <span class="o">==</span> <span class="n">newInventory</span><span class="p">)</span> <span class="p">{</span>
        <span class="k">return</span><span class="p">;</span>
    <span class="p">}</span>
    <span class="nb">NSMutableArray</span> <span class="o">*</span><span class="n">oldValue</span> <span class="o">=</span> <span class="n">_inventory</span><span class="p">;</span>
    <span class="n">_inventory</span> <span class="o">=</span> <span class="p">[</span><span class="n">newInventory</span> <span class="nf">retain</span><span class="p">];</span>
    <span class="p">[</span><span class="n">oldValue</span> <span class="nf">release</span><span class="p">];</span>
<span class="p">}</span>
</pre></div>


<p>This is basically what the <a
href='properties.html#the-retain-attribute'><code>retain</code></a> and the <a
href='properties.html#the-strong-attribute'><code>strong</code></a> property
attributes do. Obviously, using <code>@property</code> is much more convenient
than creating these accessors on our own.</p>

<figure>
	<img style='max-width: 520px' src='media/memory-management/retain-release-calls.png' />
	<figcaption>Memory
management calls on the <code>inventory</code> object</figcaption>
</figure>

<p>The above diagram visualizes all of the memory management calls on the
<code>inventory</code> array that we created in <code>main.m</code>, along with
their respective locations. As you can see, all of the
<code>alloc</code>&rsquo;s and <code>retain</code>&rsquo;s are balanced with a
<code>release</code> somewhere down the line, ensuring that the underlying
memory will eventually be freed up.</p>


<h2>The copy Method</h2>

<p>An alternative to <code>retain</code> is the <code>copy</code> method, which
creates a brand new instance of the object and increments the reference count
on that, leaving the original unaffected. So, if you want to copy the
<code>inventory</code> array instead of referring to the mutable one, you can
change <code>setInventory:</code> to the following.</p>

<div class="highlight"><pre><span class="c1">// CarStore.m</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">setInventory:</span><span class="p">(</span><span class="nb">NSMutableArray</span> <span class="o">*</span><span class="p">)</span><span class="nv">newInventory</span> <span class="p">{</span>
    <span class="k">if</span> <span class="p">(</span><span class="n">_inventory</span> <span class="o">==</span> <span class="n">newInventory</span><span class="p">)</span> <span class="p">{</span>
        <span class="k">return</span><span class="p">;</span>
    <span class="p">}</span>
    <span class="nb">NSMutableArray</span> <span class="o">*</span><span class="n">oldValue</span> <span class="o">=</span> <span class="n">_inventory</span><span class="p">;</span>
    <span class="n">_inventory</span> <span class="o">=</span> <span class="p">[</span><span class="n">newInventory</span> <span class="nf">copy</span><span class="p">];</span>
    <span class="p">[</span><span class="n">oldValue</span> <span class="nf">release</span><span class="p">];</span>
<span class="p">}</span>
</pre></div>


<p>You may also recall from <a href='properties.html#the-copy-attribute'>The
<code>copy</code> Attribute</a> that this has the added perk of freezing
mutable collections at the time of assignment. Some classes provide multiple
<code>copy</code> methods (much like multiple <code>init</code> methods), and
it&rsquo;s safe to assume that any method starting with <code>copy</code> has
the same behavior.</p>


<h2 id='the-autorelease-method'>The autorelease method</h2>

<p>Like <code>release</code>, the <code>autorelease</code> method relinquishes
ownership of an object, but instead of destroying the object immediately, it
defers the actual freeing of memory until later on in the program. This allows
you to release objects when you are &ldquo;supposed&rdquo; to, while still
keeping them around for others to use.</p>

<p>For example, consider a simple factory method that creates and returns a
<code>CarStore</code> object:</p>

<div class="highlight"><pre><span class="c1">// CarStore.h</span>
<span class="k">+</span> <span class="p">(</span><span class="n">CarStore</span> <span class="o">*</span><span class="p">)</span><span class="nf">carStore</span><span class="p">;</span>
</pre></div>


<p>Technically speaking, it&rsquo;s the <code>carStore</code> method&rsquo;s
responsibility to release the object because the caller has no way of knowing
that he owns the returned object. So, its implementation should return an
autoreleased object, like so:</p>

<div class="highlight"><pre><span class="c1">// CarStore.m</span>
<span class="k">+</span> <span class="p">(</span><span class="n">CarStore</span> <span class="o">*</span><span class="p">)</span><span class="nf">carStore</span> <span class="p">{</span>
    <span class="n">CarStore</span> <span class="o">*</span><span class="n">newStore</span> <span class="o">=</span> <span class="p">[[</span><span class="n">CarStore</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
    <span class="k">return</span> <span class="p">[</span><span class="n">newStore</span> <span class="nf">autorelease</span><span class="p">];</span>
<span class="p">}</span>
</pre></div>


<p>This relinquishes ownership of the object immediately after creating it, but
keeps it in memory long enough for the caller to interact with it.
Specifically, it waits until the end of the nearest
<code>@autoreleasepool{}</code> block, after which it calls a normal
<code>release</code> method. This is why there&rsquo;s always an
<code>@autoreleasepool{}</code> surrounding the entire <code>main()</code>
function&mdash;it makes sure all of the autoreleased objects are destroyed
after the program is done executing.</p>

<p>All of those built-in factory methods like <a
href='data-types/nsstring.html'><code>NSString</code></a>&rsquo;s
<code>stringWithFormat:</code> and <code>stringWithContentsOfFile:</code> work
the exact same way as our <code>carStore</code> method. Before ARC, this was a
convenient convention, since it let you create objects without worrying about
calling <code>release</code> somewhere down the road.</p>

<p>If you change the <code>superstore</code> constructor from
<code>alloc</code>/<code>init</code> to the following, you won&rsquo;t have to
release it at the end of <code>main()</code>.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="n">CarStore</span> <span class="o">*</span><span class="n">superstore</span> <span class="o">=</span> <span class="p">[</span><span class="n">CarStore</span> <span class="nf">carStore</span><span class="p">];</span>
</pre></div>


<p>In fact, you aren&rsquo;t <em>allowed</em> to release the
<code>superstore</code> instance now because you no longer own it&mdash;the
<code>carStore</code> factory method does. It&rsquo;s very important to avoid
explicitly releasing autoreleased objects (otherwise, you&rsquo;ll have a
dangling pointer and a crashed program).</p>


<h2 id='the-dealloc-method'>The dealloc Method</h2>

<p>An object&rsquo;s <code>dealloc</code> method is the opposite of its
<code>init</code> method. It&rsquo;s called right before the object is
destroyed, giving you a chance to clean up any internal objects. This method is
called automatically by the runtime&mdash;you should never try to call
<code>dealloc</code> yourself.</p>

<p>In an MRR environment, the most common thing you need to do in a
<code>dealloc</code> method is release objects stored in instance variables.
Think about what happens to our current <code>CarStore</code> when an instance
is deallocated: its <code>_inventory</code> instance variable, which has been
retained by the setter, never has the chance to be released. This is another
form of memory leak. To fix this, all we have to do is add a custom
<code>dealloc</code> to <code>CarStore.m</code>:

<div class="highlight"><pre><span class="c1">// CarStore.m</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">dealloc</span> <span class="p">{</span>
    <span class="p">[</span><span class="n">_inventory</span> <span class="nf">release</span><span class="p">];</span>
    <span class="p">[</span><span class="n">super</span> <span class="nf">dealloc</span><span class="p">];</span>
<span class="p">}</span>
</pre></div>


<p>Note that you should always call the superclass&rsquo;s <code>dealloc</code>
to make sure that all of the instance variables in parent classes are properly
released. As a general rule, you want to keep custom <code>dealloc</code>
methods as simple as possible, so you shouldn&rsquo;t try to use them for logic
that can be handled elsewhere.</p>


<h2>Summary</h2>

<p>And that&rsquo;s manual memory management in a nutshell. The key is to
balance every <code>alloc</code>, <code>retain</code>, and <code>copy</code>
with a <code>release</code> or <code>autorelease</code>, otherwise you&rsquo;ll
encounter either a dangling pointer or a memory leak at some point in your
application.</p>

<p>Remember that this section only used Manual Retain Release to understand the
internal workings of iOS and OS&nbsp;X memory management. In the real world,
much of the above code is actually obsolete, though you might encounter it in
older documentation. It&rsquo;s very important to understand that explicitly
claiming and relinquishing ownership like this has been completely superseded
by Automatic Reference Counting.</p>


<h1 id='automatic-reference-counting'>Automatic Reference Counting</h1>

<p>Now that you&rsquo;ve got your head wrapped around manual memory management,
you can forget all about it. Automatic Reference Counting works the exact same
way as MRR, but it automatically inserts the appropriate memory-management
methods for you. This is a big deal for Objective-C developers, as it lets them
focus entirely on <em>what</em> their application needs to do rather than
<em>how</em> it does it.</p>

<p>ARC takes the human error out of memory management with virtually no
downside, so the only reason <em>not</em> to use it is when you&rsquo;re
interfacing with a legacy code base (however, ARC is, for the most part,
backward compatible with MRR programs). The rest of this module explains the
major changes between MRR and ARC.</p>


<h2>Enabling ARC</h2>

<p>First, let&rsquo;s go ahead and turn ARC back on in the project&rsquo;s
<em>Build Settings</em> tab. Change the <em>Automatic Reference Counting</em>
compiler option to <em>Yes</em>. Again, this is the default for all Xcode
templates, and it&rsquo;s what you should be using for all of your
projects.</p>

<figure>
	<img style='max-width: 477px' src='media/memory-management/turning-on-arc.png' />
	<figcaption>Turning on Automatic
Reference Counting</figcaption>
</figure>


<h2>No More Memory Methods</h2>

<p>ARC works by analyzing your code to figure how what the ideal lifetime of
each object should be, then inserts the necessary <code>retain</code> and
<code>release</code> calls automatically. The algorithm requires complete
control over the object-ownership in your entire program, which means
you&rsquo;re <em>not allowed</em> to manually call <code>retain</code>,
<code>release</code>, or <code>autorelease</code>.

<p>The only memory-related methods you should ever find in an ARC program are
<code>alloc</code> and <code>copy</code>. You can think of these as plain old
constructors and ignore the whole object-ownership thing.</p>


<h2>New Property Attributes</h2>

<p>ARC introduces new <code>@property</code> attributes. You should use the
<code>strong</code> attribute in place of <code>retain</code>, and
<code>weak</code> in place of <code>assign</code>. All of these attributes are
discussed in the <a href='properties.html'>Properties</a> module.</p>


<h2>The dealloc Method in ARC</h2>

<p>Deallocation in ARC is also a little bit different. You don&rsquo;t need to
release instance variables as we did in <a href='memory-management.html#the-dealloc-method'>The
<code>dealloc</code> Method</a>&mdash;ARC does this for you. In addition, the
superclass&rsquo;s <code>dealloc</code> is automatically called, so you
don&rsquo;t have to do that either.</p>

<p>For the most part, this means you&rsquo;ll never need to include a custom
<code>dealloc</code> method. One of the few exceptions is when you&rsquo;re
using low-level memory allocation functions like <a
href='https://developer.apple.com/library/mac/#documentation/Darwin/Reference/ManPages/man3/malloc.3.html'><code>malloc()</code></a>.
In this case, you&rsquo;d still have to call <code>free()</code> in
<code>dealloc</code> to avoid a memory leak.</p>


<h1>Summary</h1>

<p>For the most part, Automatic Reference Counting lets you completely forget
about memory management. The idea is to focus on high-level functionality
instead of the underlying memory management. The only thing you need to worry
about are retain cycles, which was covered in the <a
href='properties.html'>Properties</a> module.</p>

<p>If you need a more detailed discussion about the nuances of ARC, please
visit the <a
href='http://developer.apple.com/library/mac/#releasenotes/ObjectiveC/RN-TransitioningToARC/Introduction/Introduction.html%23/apple_ref/doc/uid/TP40011226'>Transitioning
to ARC Release Notes</a>.

<p>By now, you should know almost everything you need to know about
Objective-C. The only thing we haven&rsquo;t covered are the basic data types
provided by both C and the Foundation Framework. The next module introduces all
of the standard types, from numbers to strings, arrays, dictionaries, and even
dates.</p>

<p class='sequential-nav'>
	<a href='data-types.html'>Continue to <em>Data Types</em>&nbsp;&rsaquo;</a>
</p>





</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>