<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Functions | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Objective-C
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Functions</h1>

<p>Along with variables, conditionals, and loops, functions are one of the
fundamental components of any modern programming language. They let you reuse
an arbitrary block of code throughout your application, which is necessary for
organizing and maintaining all but the most trivial code bases. You&rsquo;ll
find <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Miscellaneous/Foundation_Functions/Reference/reference.html%23/apple_ref/doc/uid/TP40003774'>many
examples</a> of functions throughout the iOS and OS&nbsp;X frameworks.</p>

<p>Just like its other basic constructs, Objective-C relies entirely on the C
programming language for functions. This module introduces the most important
aspects of C functions, including basic syntax, the separation of declaration
and implementation, common scope issues, and function library considerations.</p>


<h2>Basic Syntax</h2>

<p>There are four components to a C function: its return value, name,
parameters, and associated code block. After you&rsquo;ve defined these, you
can <strong>call</strong> the function to execute its associated code by
passing any necessary parameters between parentheses.</p>

<p>For example, the following snippet defines a function called
<code>getRandomInteger()</code> that accepts two <code>int</code> values as
parameters and returns another <code>int</code> value. Inside of the function,
we access the inputs through the <code>minimum</code> and <code>maximum</code>
parameters, and we return a calculated value via the <code>return</code>
keyword. Then in <code>main()</code>, we call this new function and pass
<code>-10</code> and <code>10</code> as arguments.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">getRandomInteger</span><span class="p">(</span><span class="kt">int</span> <span class="n">minimum</span><span class="p">,</span> <span class="kt">int</span> <span class="n">maximum</span><span class="p">)</span> <span class="p">{</span>
    <span class="k">return</span> <span class="nb">arc4random_uniform</span><span class="p">((</span><span class="n">maximum</span> <span class="o">-</span> <span class="n">minimum</span><span class="p">)</span> <span class="o">+</span> <span class="mi">1</span><span class="p">)</span> <span class="o">+</span> <span class="n">minimum</span><span class="p">;</span>
<span class="p">}</span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="kt">int</span> <span class="n">randomNumber</span> <span class="o">=</span> <span class="n">getRandomInteger</span><span class="p">(</span><span class="o">-</span><span class="mi">10</span><span class="p">,</span> <span class="mi">10</span><span class="p">);</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Selected a random number between -10 and 10: %d&quot;</span><span class="p">,</span>
              <span class="n">randomNumber</span><span class="p">);</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>The built-in <a
href='https://developer.apple.com/library/mac/#documentation/Darwin/Reference/ManPages/man3/arc4random.3.html'><code>arc4random_uniform()</code></a>
function returns a random number between 0 and whatever argument you pass. (This
is preferred over the older <code>rand()</code> and <code>random()</code>
algorithms.)</p>

<p>Functions let you use pointer references as return values or parameters,
which means that they can be seamlessly integrated with Objective-C objects
(remember that all objects are represented as pointers). For example, try
changing <code>main.m</code> to the following.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="nb">NSString</span> <span class="o">*</span><span class="nf">getRandomMake</span><span class="p">(</span><span class="nb">NSArray</span> <span class="o">*</span><span class="n">makes</span><span class="p">)</span> <span class="p">{</span>
    <span class="kt">int</span> <span class="n">maximum</span> <span class="o">=</span> <span class="p">(</span><span class="kt">int</span><span class="p">)[</span><span class="n">makes</span> <span class="nf">count</span><span class="p">];</span>
    <span class="kt">int</span> <span class="n">randomIndex</span> <span class="o">=</span> <span class="nb">arc4random_uniform</span><span class="p">(</span><span class="n">maximum</span><span class="p">);</span>
    <span class="k">return</span> <span class="n">makes</span><span class="p">[</span><span class="n">randomIndex</span><span class="p">];</span>
<span class="p">}</span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="nb">NSArray</span> <span class="o">*</span><span class="n">makes</span> <span class="o">=</span> <span class="p">@[</span><span class="s">@&quot;Honda&quot;</span><span class="p">,</span> <span class="s">@&quot;Ford&quot;</span><span class="p">,</span> <span class="s">@&quot;Nissan&quot;</span><span class="p">,</span> <span class="s">@&quot;Porsche&quot;</span><span class="p">];</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Selected a %@&quot;</span><span class="p">,</span> <span class="n">getRandomMake</span><span class="p">(</span><span class="n">makes</span><span class="p">));</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>This <code>getRandomMake()</code> function accepts an <code>NSArray</code>
object as an argument and returns an <code>NSString</code> object. Note that it
uses the same asterisk syntax as <a href='c-basics.html#pointers'>pointer
variable declarations</a>.</p>


<h2 id='declarations-vs-implementations'>Declarations vs. Implementations</h2>

<p>Functions need to be defined <em>before</em> they are used. If you were to
define the above <code>getRandomMake()</code> function <em>after</em>
<code>main()</code>, the compiler wouldn&rsquo;t be able to find it when you
try to call it in <code>main()</code>. This imposes a rather strict structure
on developers and can make it hard to organize larger applications. To solve
this problem, C lets you separate the declaration of a function from its
implementation.</p>

<figure>
	<img style='max-width: 460px' src='media/functions/declarations-vs-implementations.png' />
	<figcaption>Function
declarations vs. implementations</figcaption>
</figure>

<p>A function <strong>declaration</strong> tells the compiler what the
function&rsquo;s inputs and outputs look like. By providing the data types for
the return value and parameters of a function, the compiler can make sure that
you&rsquo;re using it properly without knowing what it actually does. The
corresponding <strong>implementation</strong> attaches a code block to the
declared function. Together, these give you a complete function
<strong>definition</strong>.</p>

<p>The following example declares the <code>getRandomMake()</code> function so
that it can be used in <code>main()</code> before it gets implemented. Notice
that the declaration only needs the data types of the parameters&mdash;their
names can be omitted (if desired).</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="c1">// Declaration</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="nf">getRandomMake</span><span class="p">(</span><span class="nb">NSArray</span> <span class="o">*</span><span class="p">)</span><span class="o">;</span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="nb">NSArray</span> <span class="o">*</span><span class="n">makes</span> <span class="o">=</span> <span class="p">@[</span><span class="s">@&quot;Honda&quot;</span><span class="p">,</span> <span class="s">@&quot;Ford&quot;</span><span class="p">,</span> <span class="s">@&quot;Nissan&quot;</span><span class="p">,</span> <span class="s">@&quot;Porsche&quot;</span><span class="p">];</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Selected a %@&quot;</span><span class="p">,</span> <span class="n">getRandomMake</span><span class="p">(</span><span class="n">makes</span><span class="p">));</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>

<span class="c1">// Implementation</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="n">getRandomMake</span><span class="p">(</span><span class="nb">NSArray</span> <span class="o">*</span><span class="n">makes</span><span class="p">)</span> <span class="p">{</span>
    <span class="kt">int</span> <span class="n">maximum</span> <span class="o">=</span> <span class="p">(</span><span class="kt">int</span><span class="p">)[</span><span class="n">makes</span> <span class="nf">count</span><span class="p">];</span>
    <span class="kt">int</span> <span class="n">randomIndex</span> <span class="o">=</span> <span class="nb">arc4random_uniform</span><span class="p">(</span><span class="n">maximum</span><span class="p">);</span>
    <span class="k">return</span> <span class="n">makes</span><span class="p">[</span><span class="n">randomIndex</span><span class="p">];</span>
<span class="p">}</span>
</pre></div>


<p>As we&rsquo;ll see in <a href='functions.html#function-libraries'>Function Libraries</a>,
separating function declarations from implementations is really more useful for
organizing large frameworks.</p>


<h2 id='the-static-keyword'>The Static Keyword</h2>

<p>The <code>static</code> keyword lets you alter the availability of a
function or variable. Unfortunately, it has different effects depending on
where you use it. This section explains two common use cases for the
<code>static</code> keyword.</p>


<h3>Static Functions</h3>

<p>By default, all functions have a global scope. This means that as soon as
you define a function in one file, it&rsquo;s immediately available everywhere
else. The <code>static</code> specifier lets you limit the function&rsquo;s
scope to the current file, which is useful for creating &ldquo;private&rdquo;
functions and avoiding naming conflicts.</p>

<figure>
	<img style='max-width: 290px' src='media/functions/static-functions.png' />
	<figcaption>Globally-scoped functions
vs. statically-scoped functions</figcaption>
</figure>

<p>The following example shows you how to create a static function. If you were
to add this code to another file (e.g., a dedicated function library), you
would not be able to access <code>getRandomInteger()</code> from
<code>main.m</code>. Note that the <code>static</code> keyword should be used
on both the function declaration and implementation.</p>

<div class="highlight"><pre><span class="c1">// Static function declaration</span>
<span class="kt">static</span> <span class="kt">int</span> <span class="nf">getRandomInteger</span><span class="p">(</span><span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">)</span><span class="o">;</span>

<span class="c1">// Static function implementation</span>
<span class="kt">static</span> <span class="kt">int</span> <span class="nf">getRandomInteger</span><span class="p">(</span><span class="kt">int</span> <span class="n">minimum</span><span class="p">,</span> <span class="kt">int</span> <span class="n">maximum</span><span class="p">)</span> <span class="p">{</span>
    <span class="k">return</span> <span class="nb">arc4random_uniform</span><span class="p">((</span><span class="n">maximum</span> <span class="o">-</span> <span class="n">minimum</span><span class="p">)</span> <span class="o">+</span> <span class="mi">1</span><span class="p">)</span> <span class="o">+</span> <span class="n">minimum</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>



<h3 id='static-local-variables'>Static Local Variables</h3>

<p>Variables declared inside of a function (also called <strong>automatic local
variables</strong>) are reset each time the function is called. This is an
intuitive default behavior, as the function behaves consistently regardless of
how many times you call it. However, when you use the <code>static</code>
modifier on a local variable, the function &ldquo;remembers&rdquo; its value
across invocations.</p>

<figure>
	<img style='max-width: 520px' src='media/functions/static-local-variables.png' />
	<figcaption>Independent automatic
variables vs. shared static variables</figcaption>
</figure>

<p>For example, the <code>currentCount</code> variable in the following snippet
never gets reset, so instead of storing the count in a variable inside of
<code>main()</code>, we can let <code>countByTwo()</code> do the recording for
us.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">countByTwo</span><span class="p">()</span> <span class="p">{</span>
    <span class="kt">static</span> <span class="kt">int</span> <span class="n">currentCount</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span>
    <span class="n">currentCount</span> <span class="o">+=</span> <span class="mi">2</span><span class="p">;</span>
    <span class="k">return</span> <span class="n">currentCount</span><span class="p">;</span>
<span class="p">}</span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%d&quot;</span><span class="p">,</span> <span class="n">countByTwo</span><span class="p">());</span>    <span class="c1">// 2</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%d&quot;</span><span class="p">,</span> <span class="n">countByTwo</span><span class="p">());</span>    <span class="c1">// 4</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%d&quot;</span><span class="p">,</span> <span class="n">countByTwo</span><span class="p">());</span>    <span class="c1">// 6</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>But, unlike the static functions discussed in the previous section, this use
of the <code>static</code> keyword does <em>not</em> affect the scope of local
variables. That is to say, local variables are still only accessible inside of
the function itself.</p>


<h2 id='function-libraries'>Function Libraries</h2>

<p>Objective-C doesn&rsquo;t support namespaces, so to prevent naming
collisions with other global functions, large frameworks need to prefix their
functions (and classes) with a unique identifier. This is why you see built-in
functions like <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Miscellaneous/Foundation_Functions/Reference/reference.html%23/apple_ref/doc/uid/TP40003774'><code>NSMakeRange()</code></a>
and <a
href='http://developer.apple.com/library/ios/#documentation/GraphicsImaging/Reference/CGImage/Reference/reference.html%23/apple_ref/doc/uid/TP30000956'><code>CGImageCreate()</code></a>
instead of just <code>makeRange()</code> and <code>imageCreate()</code>.

<p>When creating your own function libraries, you should declare functions in a
dedicated header file and implement them in a separate implementation file
(just like <a href='classes.html'>Objective-C classes</a>). This lets files
that use the library import the header without worrying about how its functions
are implemented. For example, the header for a <code>CarUtilities</code>
library might look something like the following:</p>

<div class="highlight"><pre><span class="c1">// CarUtilities.h</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="nb">NSString</span> <span class="o">*</span><span class="nf">CUGetRandomMake</span><span class="p">(</span><span class="nb">NSArray</span> <span class="o">*</span><span class="n">makes</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="nf">CUGetRandomModel</span><span class="p">(</span><span class="nb">NSArray</span> <span class="o">*</span><span class="n">models</span><span class="p">)</span><span class="o">;</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="nf">CUGetRandomMakeAndModel</span><span class="p">(</span><span class="nb">NSDictionary</span> <span class="o">*</span><span class="n">makesAndModels</span><span class="p">)</span><span class="o">;</span>
</pre></div>


<p>The corresponding implementation file defines what these functions actually
do.  Since other files are not supposed to import the implementation, you can
use the <code>static</code> specifier to create &ldquo;private&rdquo; functions
for internal use by the library.</p>

<div class="highlight"><pre><span class="c1">// CarUtilities.m</span>
<span class="cp">#import</span> <span class="l">&quot;CarUtilities.h&quot;</span><span class="cp"></span>

<span class="c1">// Private function declaration</span>
<span class="kt">static</span> <span class="kt">id</span> <span class="nf">getRandomItemFromArray</span><span class="p">(</span><span class="nb">NSArray</span> <span class="o">*</span><span class="n">anArray</span><span class="p">)</span><span class="o">;</span>

<span class="c1">// Public function implementations</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="nf">CUGetRandomMake</span><span class="p">(</span><span class="nb">NSArray</span> <span class="o">*</span><span class="n">makes</span><span class="p">)</span> <span class="p">{</span>
    <span class="k">return</span> <span class="n">getRandomItemFromArray</span><span class="p">(</span><span class="n">makes</span><span class="p">);</span>
<span class="p">}</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="nf">CUGetRandomModel</span><span class="p">(</span><span class="nb">NSArray</span> <span class="o">*</span><span class="n">models</span><span class="p">)</span> <span class="p">{</span>
    <span class="k">return</span> <span class="n">getRandomItemFromArray</span><span class="p">(</span><span class="n">models</span><span class="p">);</span>
<span class="p">}</span>
<span class="nb">NSString</span> <span class="o">*</span><span class="nf">CUGetRandomMakeAndModel</span><span class="p">(</span><span class="nb">NSDictionary</span> <span class="o">*</span><span class="n">makesAndModels</span><span class="p">)</span> <span class="p">{</span>
    <span class="nb">NSArray</span> <span class="o">*</span><span class="n">makes</span> <span class="o">=</span> <span class="p">[</span><span class="n">makesAndModels</span> <span class="nf">allKeys</span><span class="p">];</span>
    <span class="nb">NSString</span> <span class="o">*</span><span class="n">randomMake</span> <span class="o">=</span> <span class="n">CUGetRandomMake</span><span class="p">(</span><span class="n">makes</span><span class="p">);</span>
    <span class="nb">NSArray</span> <span class="o">*</span><span class="n">models</span> <span class="o">=</span> <span class="n">makesAndModels</span><span class="p">[</span><span class="n">randomMake</span><span class="p">];</span>
    <span class="nb">NSString</span> <span class="o">*</span><span class="n">randomModel</span> <span class="o">=</span> <span class="n">CUGetRandomModel</span><span class="p">(</span><span class="n">models</span><span class="p">);</span>
    <span class="k">return</span> <span class="p">[</span><span class="n">randomMake</span> <span class="nf">stringByAppendingFormat:</span><span class="s">@&quot; %@&quot;</span><span class="p">,</span> <span class="n">randomModel</span><span class="p">];</span>
<span class="p">}</span>

<span class="c1">// Private function implementation</span>
<span class="kt">static</span> <span class="kt">id</span> <span class="nf">getRandomItemFromArray</span><span class="p">(</span><span class="nb">NSArray</span> <span class="o">*</span><span class="n">anArray</span><span class="p">)</span> <span class="p">{</span>
    <span class="kt">int</span> <span class="n">maximum</span> <span class="o">=</span> <span class="p">(</span><span class="kt">int</span><span class="p">)[</span><span class="n">anArray</span> <span class="nf">count</span><span class="p">];</span>
    <span class="kt">int</span> <span class="n">randomIndex</span> <span class="o">=</span> <span class="nb">arc4random_uniform</span><span class="p">(</span><span class="n">maximum</span><span class="p">);</span>
    <span class="k">return</span> <span class="n">anArray</span><span class="p">[</span><span class="n">randomIndex</span><span class="p">];</span>
<span class="p">}</span>
</pre></div>


<p>Now, <code>main.m</code> can import the header and call the functions as if
they were defined in the same file. Also notice that trying to call the static
<code>getRandomItemFromArray()</code> function from <code>main.m</code> results
in a compiler error.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;CarUtilities.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="nb">NSDictionary</span> <span class="o">*</span><span class="n">makesAndModels</span> <span class="o">=</span> <span class="p">@{</span>
            <span class="s">@&quot;Ford&quot;</span><span class="o">:</span> <span class="p">@[</span><span class="s">@&quot;Explorer&quot;</span><span class="p">,</span> <span class="s">@&quot;F-150&quot;</span><span class="p">],</span>
            <span class="s">@&quot;Honda&quot;</span><span class="o">:</span> <span class="p">@[</span><span class="s">@&quot;Accord&quot;</span><span class="p">,</span> <span class="s">@&quot;Civic&quot;</span><span class="p">,</span> <span class="s">@&quot;Pilot&quot;</span><span class="p">],</span>
            <span class="s">@&quot;Nissan&quot;</span><span class="o">:</span> <span class="p">@[</span><span class="s">@&quot;370Z&quot;</span><span class="p">,</span> <span class="s">@&quot;Altima&quot;</span><span class="p">,</span> <span class="s">@&quot;Versa&quot;</span><span class="p">],</span>
            <span class="s">@&quot;Porsche&quot;</span><span class="o">:</span> <span class="p">@[</span><span class="s">@&quot;911 Turbo&quot;</span><span class="p">,</span> <span class="s">@&quot;Boxster&quot;</span><span class="p">,</span> <span class="s">@&quot;Cayman S&quot;</span><span class="p">]</span>
        <span class="p">};</span>
        <span class="nb">NSString</span> <span class="o">*</span><span class="n">randomCar</span> <span class="o">=</span> <span class="n">CUGetRandomMakeAndModel</span><span class="p">(</span><span class="n">makesAndModels</span><span class="p">);</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Selected a %@&quot;</span><span class="p">,</span> <span class="n">randomCar</span><span class="p">)</span><span class="o">;</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>



<h2>Summary</h2>

<p>This module finished up our introduction to the C programming language with
an in-depth look at functions. We learned how to declare and implement
functions, change their scope, make them remember local variables, and organize
large function libraries.</p>

<p>While most of the functionality behind the Cocoa and Cocoa Touch frameworks
is encapsulated as Objective-C classes, there&rsquo;s no shortage of built-in
functions. The ones you&rsquo;re most likely to encounter in the real world are
utility functions like <code>NSLog()</code> and convenience functions like
<code>NSMakeRect()</code> that create and configure complex objects using a
friendlier API.</p>

<p>We&rsquo;re now ready to start tackling the object-oriented aspects of
Objective-C. In the next module, we&rsquo;ll learn how to define classes,
instantiate objects, set properties, and call methods.</p>


<p class='sequential-nav'>
	<a href='classes.html'>Continue to <em>Classes</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>