<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Exceptions and Errors | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Objective-C
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Exceptions &amp; Errors</h1>

<p>Two distinct types of problems can arise while an iOS or OS&nbsp;X
application is running. <strong>Exceptions</strong> represent programmer-level
bugs like trying to access an array element that doesn&rsquo;t exist. They are
designed to inform the developer that an <em>unexpected</em> condition
occurred. Since they usually result in the program crashing, exceptions should
rarely occur in your production code.</p>

<p>On the other hand, <strong>errors</strong> are user-level issues like trying
load a file that doesn&rsquo;t exist. Because errors are <em>expected</em>
during the normal execution of a program, you should manually check for these
kinds of conditions and inform the user when they occur. Most of the time, they
should not cause your application to crash.</p>

<figure>
	<img style='max-width: 500px' src='media/exceptions/exceptions-vs-errors.png' />
	<figcaption>Exceptions vs. errors</figcaption>
</figure>

<p>This module provides a thorough introduction to exceptions and errors.
Conceptually, working with exceptions is very similar to working with errors.
First you need to detect the problem, and then you need to handle it. But, as
we&rsquo;re about to find out, the underlying mechanics are slightly
different</p>


<h1>Exceptions</h1>

<p>Exceptions are represented by the <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Classes/NSException_Class/Reference/Reference.html'><code>NSException</code></a>
class. It&rsquo;s designed to be a universal way to encapsulate exception data,
so you should rarely need to subclass it or otherwise define a custom exception
object. <code>NSException</code>&rsquo;s three main properties are listed
below.</p>

<table class='multiline'>
<thead>
<tr>
	<th>Property</th>
	<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
	<td><code>name</code></td>
	<td>An <code>NSString</code> that uniquely identifies the exception.</td>
</tr>
<tr>
	<td><code>reason</code></td>
	<td>An <code>NSString</code> that contains a human-readable description of
	the exception.</td>
</tr>
<tr>
	<td><code>userInfo</code></td>
	<td>An <code>NSDictionary</code> whose key-value pairs contain extra
	information about the exception. This varies based on the type of
	exception.</td>
</tr>
</tbody>
</table>

<p>It&rsquo;s important to understand that exceptions are only used for serious
programming errors. The idea is to let you know that something has gone wrong
early in the development cycle, after which you&rsquo;re expected to fix it so
it never occurs again. If you&rsquo;re trying to handle a problem that&rsquo;s
<em>supposed</em> to occur, you should be using an <a href='exceptions.html#errors'>error
object</a>, not an exception.</p>


<h2>Handling Exceptions</h2>

<p>Exceptions can be handled using the standard try-catch-finally pattern found
in most other high-level programming languages. First, you need to place any
code that <em>might</em> result in an exception in an <code>@try</code> block.
Then, if an exception is thrown, the corresponding <code>@catch()</code> block
is executed to handle the problem. The <code>@finally</code> block is called
afterwards, regardless of whether or not an exception occurred.</p>

<p>The following <code>main.m</code> file raises an exception by trying to
access an array element that doesn&rsquo;t exist. In the <code>@catch()</code>
block, we simply display the exception details. The <code>NSException
*theException</code> in the parentheses defines the name of the variable
containing the exception object.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="nb">NSArray</span> <span class="o">*</span><span class="n">inventory</span> <span class="o">=</span> <span class="p">@[</span><span class="s">@&quot;Honda Civic&quot;</span><span class="p">,</span>
                               <span class="s">@&quot;Nissan Versa&quot;</span><span class="p">,</span>
                               <span class="s">@&quot;Ford F-150&quot;</span><span class="p">];</span>
        <span class="kt">int</span> <span class="n">selectedIndex</span> <span class="o">=</span> <span class="mi">3</span><span class="p">;</span>
        <span class="k">@try</span> <span class="p">{</span>
            <span class="nb">NSString</span> <span class="o">*</span><span class="n">car</span> <span class="o">=</span> <span class="n">inventory</span><span class="p">[</span><span class="n">selectedIndex</span><span class="p">];</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The selected car is: %@&quot;</span><span class="p">,</span> <span class="n">car</span><span class="p">);</span>
        <span class="p">}</span> <span class="k">@catch</span><span class="p">(</span><span class="nb">NSException</span> <span class="o">*</span><span class="n">theException</span><span class="p">)</span> <span class="p">{</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;An exception occurred: %@&quot;</span><span class="p">,</span> <span class="n">theException</span><span class="p">.</span><span class="n">name</span><span class="p">);</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Here are some details: %@&quot;</span><span class="p">,</span> <span class="n">theException</span><span class="p">.</span><span class="n">reason</span><span class="p">)</span><span class="o">;</span>
        <span class="p">}</span> <span class="k">@finally</span> <span class="p">{</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Executing finally block&quot;</span><span class="p">);</span>
        <span class="p">}</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>In the real world, you&rsquo;ll want your <code>@catch()</code> block to
actually handle the exception by logging the problem, correcting it, or
possibly even promoting the exception to an error object so it can be displayed
to the user. The default behavior for uncaught exceptions is to output a
message to the console and exit the program.</p>

<p>Objective-C&rsquo;s exception-handling capabilities are not the most
efficient, so you should only use <code>@try</code>/<code>@catch()</code>
blocks to test for truly exceptional circumstances. Do <em>not</em> use it in
place of ordinary control flow tools. Instead, check for predictable conditions
using standard <code>if</code> statements.</p>

<p>This means that the above snippet is actually a very poor use of exceptions.
A much better route would have been to make sure that the
<code>selectedIndex</code> was smaller than the
<code>[inventory&nbsp;count]</code> using a traditional comparison:</p>

<div class="highlight"><pre><span class="k">if</span> <span class="p">(</span><span class="n">selectedIndex</span> <span class="o">&lt;</span> <span class="p">[</span><span class="n">inventory</span> <span class="nf">count</span><span class="p">])</span> <span class="p">{</span>
    <span class="nb">NSString</span> <span class="o">*</span><span class="n">car</span> <span class="o">=</span> <span class="n">inventory</span><span class="p">[</span><span class="n">selectedIndex</span><span class="p">];</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The selected car is: %@&quot;</span><span class="p">,</span> <span class="n">car</span><span class="p">)</span><span class="o">;</span>
<span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
    <span class="c1">// Handle the error</span>
<span class="p">}</span>
</pre></div>



<h2>Built-In Exceptions</h2>

<p>The standard iOS and OS&nbsp;X frameworks define several built-in
exceptions. The complete list can be found <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Conceptual/Exceptions/Concepts/PredefinedExceptions.html%23/apple_ref/doc/uid/20000057-BCIGHECA'>here</a>,
but the most common ones are described below.</p>

<table class='multiline'>
<thead>
<tr>
	<th>Exception Name</th>
	<th>Description</th>
</tr>
</thead>
</tbody>
<tr>
<td><code>NSRangeException</code></td>
<td>Occurs when you try to access an element that&rsquo;s outside the bounds of
a collection.</td>
</tr>

<tr>
<td><code>NSInvalidArgumentException</code></td>
<td>Occurs when you pass an invalid argument to a method.</td>
</tr>

<tr>
<td><code>NSInternalInconsistencyException</code></td>
<td>Occurs when an unexpected condition arises internally.</td>
</tr>
<tr>
<td><code>NSGenericException</code></td>
<td>Occurs when you don&rsquo;t know what else to call the exception.</td>
</tr>
</tbody>
</table>

<p>Note that these values are <em>strings</em>, not <code>NSException</code>
subclasses. So, when you&rsquo;re looking for a specific type of exception,
you need to check the <code>name</code> property, like so:</p>

<div class="highlight"><pre><span class="p">...</span>
<span class="p">}</span> <span class="k">@catch</span><span class="p">(</span><span class="nb">NSException</span> <span class="o">*</span><span class="n">theException</span><span class="p">)</span> <span class="p">{</span>
    <span class="k">if</span> <span class="p">(</span><span class="n">theException</span><span class="p">.</span><span class="n">name</span> <span class="o">==</span> <span class="nb">NSRangeException</span><span class="p">)</span> <span class="p">{</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Caught an NSRangeException&quot;</span><span class="p">);</span>
    <span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Ignored a %@ exception&quot;</span><span class="p">,</span> <span class="n">theException</span><span class="p">);</span>
        <span class="k">@throw</span><span class="p">;</span>
    <span class="p">}</span>
<span class="p">}</span> <span class="p">...</span>
</pre></div>


<p>In an <code>@catch()</code> block, the <code>@throw</code> directive
re-raises the caught exception. We used this in the above snippet to ignore all
of the exceptions we didn&rsquo;t want by throwing it up to the next highest
<code>@try</code> block. But again, a simple <code>if</code>-statement would be
preferred.</p>


<h2 id='custom-exceptions'>Custom Exceptions</h2>

<p>You can also use <code>@throw</code> to raise <code>NSException</code>
objects that contain custom data. The easiest way to create an
<code>NSException</code> instance is through the
<code>exceptionWithName:reason:userInfo:</code> factory method. The following
example throws an exception inside of a top-level function and catches it in
the <code>main()</code> function.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="nb">NSString</span> <span class="o">*</span><span class="nf">getRandomCarFromInventory</span><span class="p">(</span><span class="nb">NSArray</span> <span class="o">*</span><span class="n">inventory</span><span class="p">)</span> <span class="p">{</span>
    <span class="kt">int</span> <span class="n">maximum</span> <span class="o">=</span> <span class="p">(</span><span class="kt">int</span><span class="p">)[</span><span class="n">inventory</span> <span class="nf">count</span><span class="p">];</span>
    <span class="k">if</span> <span class="p">(</span><span class="n">maximum</span> <span class="o">==</span> <span class="mi">0</span><span class="p">)</span> <span class="p">{</span>
        <span class="nb">NSException</span> <span class="o">*</span><span class="n">e</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSException</span>
                          <span class="nf">exceptionWithName:</span><span class="s">@&quot;EmptyInventoryException&quot;</span>
                          <span class="nf">reason:</span><span class="s">@&quot;*** The inventory has no cars!&quot;</span>
                          <span class="nf">userInfo:</span><span class="kc">nil</span><span class="p">];</span>
        <span class="k">@throw</span> <span class="n">e</span><span class="p">;</span>
    <span class="p">}</span>
    <span class="kt">int</span> <span class="n">randomIndex</span> <span class="o">=</span> <span class="nb">arc4random_uniform</span><span class="p">(</span><span class="n">maximum</span><span class="p">);</span>
    <span class="k">return</span> <span class="n">inventory</span><span class="p">[</span><span class="n">randomIndex</span><span class="p">];</span>
<span class="p">}</span>

<span class="kt">int</span> <span class="n">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="k">@try</span> <span class="p">{</span>
            <span class="nb">NSString</span> <span class="o">*</span><span class="n">car</span> <span class="o">=</span> <span class="n">getRandomCarFromInventory</span><span class="p">(@[]);</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The selected car is: %@&quot;</span><span class="p">,</span> <span class="n">car</span><span class="p">)</span><span class="o">;</span>
        <span class="p">}</span> <span class="k">@catch</span><span class="p">(</span><span class="nb">NSException</span> <span class="o">*</span><span class="n">theException</span><span class="p">)</span> <span class="p">{</span>
            <span class="k">if</span> <span class="p">(</span><span class="n">theException</span><span class="p">.</span><span class="n">name</span> <span class="o">==</span> <span class="s">@&quot;EmptyInventoryException&quot;</span><span class="p">)</span> <span class="p">{</span>
                <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Caught an EmptyInventoryException&quot;</span><span class="p">);</span>
            <span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
                <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Ignored a %@ exception&quot;</span><span class="p">,</span> <span class="n">theException</span><span class="p">);</span>
                <span class="k">@throw</span><span class="p">;</span>
            <span class="p">}</span>
        <span class="p">}</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>While occasionally necessary, you shouldn&rsquo;t really need to throw
custom exceptions like this in normal applications. For one, exceptions
represent programmer errors, and there are very few times when you should be
planning for serious coding mistakes. Second, <code>@throw</code> is an
expensive operation, so it&rsquo;s always better to use errors if possible.</p>


<h1 id='errors'>Errors</h1>

<p>Since errors represent <em>expected</em> problems, and there are several
types of operations that can fail without causing the program to crash, they
are much more common than exceptions. Unlike exceptions, this kind of error
checking is a normal aspect of production-quality code.</p>

<p>The <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Classes/NSError_Class/Reference/Reference.html'><code>NSError</code></a>
class encapsulates the details surrounding a failed operation. It&rsquo;s main
properties are similar to <code>NSException</code>.</p>

<table class='multiline'>
<thead>
<tr>
	<th>Property</th>
	<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
	<td><code>domain</code></td>
	<td>An <code>NSString</code> containing the error&rsquo;s domain. This is
	used to organize errors into a hierarchy and ensure that error codes
	don&rsquo;t conflict.</td>
</tr>
<tr>
	<td><code>code</code></td>
	<td>An <code>NSInteger</code> representing the ID of the error. Each error
	in the same domain must have a unique value.</td>
</tr>
<tr>
	<td><code>userInfo</code></td>
	<td>An <code>NSDictionary</code> whose key-value pairs contain extra
	information about the error. This varies based on the type of
	error.</td>
</tr>
</tbody>
</table>

<p>The <code>userInfo</code> dictionary for <code>NSError</code> objects
typically contains more information than <code>NSException</code>&rsquo;s
version. Some of the pre-defined keys, which are defined as named constants,
are listed below.</p>

<table class='multiline'>
<thead>
<tr>
	<th>Key</th>
	<th>Value</th>
</tr>
</thead>
<tbody>
<tr>
	<td><code>NSLocalizedDescriptionKey</code></td>
	<td>An <code>NSString</code> representing the full description of the
	error. This usually includes the failure reason, too.</td>
</tr>
<tr>
	<td><code>NSLocalizedFailureReasonErrorKey</code></td>
	<td>A brief <code>NSString</code> isolating the reason for the
	failure.</td>
</tr>
<tr>
	<td><code>NSUnderlyingErrorKey</code></td>
	<td>A reference to another <code>NSError</code> object that represents the
	error in the next-highest domain.</td>
</tr>
</tbody>
</table>

<p>Depending on the error, this dictionary will also contain other
domain-specific information. For example, file-loading errors will have a key
called <code>NSFilePathErrorKey</code> that contains the path to the requested
file.</p>

<p>Note that the <code>localizedDescription</code> and
<code>localizedFailureReason</code> methods are an alternative way to access
the first two keys, respectively. These are used in the next section&rsquo;s
example.</p>


<h2>Handling Errors</h2>

<p>Errors don&rsquo;t require any dedicated language constructs like
<code>@try</code> and <code>@catch()</code>. Instead, functions or methods that
<em>may</em> fail accept an additional argument (typically called
<code>error</code>) that is a reference to an <code>NSError</code> object. If
the operation fails, it returns <code>NO</code> or <code>nil</code> to indicate
failure and populates this argument with the error details. If it succeeds, it
simply returns the requested value as normal.</p>

<p>Many methods are configured to accept an <strong>indirect reference</strong>
to an <code>NSError</code> object. An indirect reference is a pointer to a
pointer, and it allows the method to point the argument to a brand new
<code>NSError</code> instance. You can determine if a method&rsquo;s
<code>error</code> argument accepts a indirect reference by its double-pointer
notation: <code>(NSError **)error</code>.</p>

<p>The following snippet demonstrates this error-handling pattern by trying to
load a file that doesn&rsquo;t exist via <code>NSString</code>&rsquo;s
<code>stringWithContentsOfFile:encoding:error:</code> method. When the file
loads successfully, the method returns the contents of the file as an
<code>NSString</code>, but when it fails, it directly returns <code>nil</code>
and <em>indirectly</em> returns the error by populating the <code>error</code>
argument with a new <code>NSError</code> object.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="nb">NSString</span> <span class="o">*</span><span class="n">fileToLoad</span> <span class="o">=</span> <span class="s">@&quot;/path/to/non-existent-file.txt&quot;</span><span class="p">;</span>
        
        <span class="nb">NSError</span> <span class="o">*</span><span class="n">error</span><span class="p">;</span>
        <span class="nb">NSString</span> <span class="o">*</span><span class="n">content</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSString</span> <span class="nf">stringWithContentsOfFile:</span><span class="n">fileToLoad</span>
                                                      <span class="nf">encoding:</span><span class="nb">NSUTF8StringEncoding</span>
                                                         <span class="nf">error:</span><span class="o">&amp;</span><span class="n">error</span><span class="p">];</span>
        
        <span class="k">if</span> <span class="p">(</span><span class="n">content</span> <span class="o">==</span> <span class="kc">nil</span><span class="p">)</span> <span class="p">{</span>
            <span class="c1">// Method failed</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Error loading file %@!&quot;</span><span class="p">,</span> <span class="n">fileToLoad</span><span class="p">);</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Domain: %@&quot;</span><span class="p">,</span> <span class="n">error</span><span class="p">.</span><span class="n">domain</span><span class="p">);</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Error Code: %ld&quot;</span><span class="p">,</span> <span class="n">error</span><span class="p">.</span><span class="n">code</span><span class="p">);</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Description: %@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">error</span> <span class="nf">localizedDescription</span><span class="p">]);</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Reason: %@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">error</span> <span class="nf">localizedFailureReason</span><span class="p">]);</span>
        <span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
            <span class="c1">// Method succeeded</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Content loaded!&quot;</span><span class="p">);</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">content</span><span class="p">)</span><span class="o">;</span>
        <span class="p">}</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>Notice how the we had to pass the <code>error</code> variable to the method
using the <a href='c-basics.html#pointers'>reference operator</a>. This is
because the <code>error</code> argument accepts a double-pointer. Also notice
how we checked the return value of the method for success with an ordinary
<code>if</code> statement. You should only try to access the
<code>NSError</code> reference if the method directly returns <code>nil</code>,
and you should never use the presence of an <code>NSError</code> object to
indicate success or failure.</p>

<p>Of course, if you only care about the success of the operation and
aren&rsquo;t concern with <em>why</em> it failed, you can just pass
<code>NULL</code> for the <code>error</code> argument and it will be
ignored.</p>


<h2>Built-In Errors</h2>

<p>Like <code>NSException</code>, <code>NSError</code> is designed to be a
universal object for representing errors. Instead of subclassing it, the
various iOS and OS&nbsp;X frameworks define their own constants for the
<code>domain</code> and <code>code</code> fields. There are several built-in
error domains, but the main four are as follows:</p>

<ul style='list-style-type: none'>
	<li><code>NSMachErrorDomain</code></li>
	<li><code>NSPOSIXErrorDomain</code></li>
	<li><code>NSOSStatusErrorDomain</code></li>
	<li><code>NSCocoaErrorDomain</code></li>
</ul>

<p>Most of the errors you&rsquo;ll be working with are in the
<code>NSCocoaErrorDomain</code>, but if you drill down to the lower-level
domains, you&rsquo;ll see some of the other ones. For example, if you add the
following line to <code>main.m</code>, you&rsquo;ll find an error with
<code>NSPOSIXErrorDomain</code> for its domain.</p>

<div class="highlight"><pre><span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Underlying Error: %@&quot;</span><span class="p">,</span> <span class="n">error</span><span class="p">.</span><span class="n">userInfo</span><span class="p">[</span><span class="nb">NSUnderlyingErrorKey</span><span class="p">])</span><span class="o">;</span>
</pre></div>


<p>For most applications, you shouldn&rsquo;t need to do this, but it can come
in handy when you need to get at the root cause of an error.</p>

<p>After you&rsquo;ve determined your error domain, you can check for a
specific error code. The <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Miscellaneous/Foundation_Constants/Reference/reference.html%23/apple_ref/doc/uid/TP40003793'>Foundation
Constants Reference</a> describes several <code>enum</code>&rsquo;s that define
most of the error codes in the <code>NSCocoaErrorDomain</code>. For example,
the following code searches for a <code>NSFileReadNoSuchFileError</code>
error.</p>

<div class="highlight"><pre><span class="p">...</span>
<span class="k">if</span> <span class="p">(</span><span class="n">content</span> <span class="o">==</span> <span class="kc">nil</span><span class="p">)</span> <span class="p">{</span>
    <span class="k">if</span> <span class="p">([</span><span class="n">error</span><span class="p">.</span><span class="n">domain</span> <span class="nf">isEqualToString:</span><span class="s">@&quot;NSCocoaErrorDomain&quot;</span><span class="p">]</span> <span class="o">&amp;&amp;</span>
        <span class="n">error</span><span class="p">.</span><span class="n">code</span> <span class="o">==</span> <span class="nb">NSFileReadNoSuchFileError</span><span class="p">)</span> <span class="p">{</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;That file doesn&#39;t exist!&quot;</span><span class="p">);</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Path: %@&quot;</span><span class="p">,</span> <span class="p">[[</span><span class="n">error</span> <span class="nf">userInfo</span><span class="p">]</span> <span class="nf">objectForKey:</span><span class="nb">NSFilePathErrorKey</span><span class="p">])</span><span class="o">;</span>
    <span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Some other kind of read occurred&quot;</span><span class="p">);</span>
    <span class="p">}</span>
<span class="p">}</span> <span class="p">...</span>
</pre></div>


<p>Other frameworks should include any custom domains and error codes in their
documentation.</p>


<h2>Custom Errors</h2>

<p>If you&rsquo;re working on a large project, you&rsquo;ll probably have at
least a few functions or methods that can result in an error. This section
explains how to configure them to use the canonical error-handling pattern
discussed above.</p>

<p>As a best practice, you should define all of your errors in a dedicated
header. For instance, a file called <code>InventoryErrors.h</code> might define
a domain containing various error codes related to fetching items from an
inventory.</p>

<div class="highlight"><pre><span class="c1">// InventoryErrors.h</span>

<span class="nb">NSString</span> <span class="o">*</span><span class="n">InventoryErrorDomain</span> <span class="o">=</span> <span class="s">@&quot;com.RyPress.Inventory.ErrorDomain&quot;</span><span class="p">;</span>

<span class="k">enum</span> <span class="p">{</span>
    <span class="n">InventoryNotLoadedError</span><span class="p">,</span>
    <span class="n">InventoryEmptyError</span><span class="p">,</span>
    <span class="n">InventoryInternalError</span>
<span class="p">};</span>
</pre></div>


<p>Technically, custom error domains can be anything you want, but the
recommended form is
<code>com.&lt;Company&gt;.&lt;Framework-or-project&gt;.ErrorDomain</code>, as
shown in <code>InventoryErrorDomain</code>. The <code>enum</code> defines the
error code constants.</p>

<p>The only thing that&rsquo;s different about a function or method that is
error-enabled is the additional <code>error</code> argument. It should specify
<code>NSError **</code> as its type, as shown in the following iteration of
<code>getRandomCarFromInventory()</code>. When an error occurs, you point this
argument to a new <code>NSError</code> object. Also note how we defined
<code>localizedDescription</code> by manually adding it to the
<code>userInfo</code> dictionary with
<code>NSLocalizedDescriptionKey</code>.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;InventoryErrors.h&quot;</span><span class="cp"></span>

<span class="nb">NSString</span> <span class="o">*</span><span class="nf">getRandomCarFromInventory</span><span class="p">(</span><span class="nb">NSArray</span> <span class="o">*</span><span class="n">inventory</span><span class="p">,</span> <span class="nb">NSError</span> <span class="o">**</span><span class="n">error</span><span class="p">)</span> <span class="p">{</span>
    <span class="kt">int</span> <span class="n">maximum</span> <span class="o">=</span> <span class="p">(</span><span class="kt">int</span><span class="p">)[</span><span class="n">inventory</span> <span class="nf">count</span><span class="p">];</span>
    <span class="k">if</span> <span class="p">(</span><span class="n">maximum</span> <span class="o">==</span> <span class="mi">0</span><span class="p">)</span> <span class="p">{</span>
        <span class="k">if</span> <span class="p">(</span><span class="n">error</span> <span class="o">!=</span> <span class="kc">NULL</span><span class="p">)</span> <span class="p">{</span>
            <span class="nb">NSDictionary</span> <span class="o">*</span><span class="n">userInfo</span> <span class="o">=</span> <span class="p">@{</span><span class="nf">NSLocalizedDescriptionKey:</span> <span class="s">@&quot;Could not&quot;</span>
            <span class="s">&quot; select a car because there are no cars in the inventory.&quot;</span><span class="p">};</span>
            
            <span class="o">*</span><span class="n">error</span> <span class="o">=</span> <span class="p">[</span><span class="nb">NSError</span> <span class="nf">errorWithDomain:</span><span class="n">InventoryErrorDomain</span>
                                         <span class="nf">code:</span><span class="n">InventoryEmptyError</span>
                                     <span class="nf">userInfo:</span><span class="n">userInfo</span><span class="p">];</span>
        <span class="p">}</span>
        <span class="k">return</span> <span class="kc">nil</span><span class="p">;</span>
    <span class="p">}</span>
    <span class="kt">int</span> <span class="n">randomIndex</span> <span class="o">=</span> <span class="nb">arc4random_uniform</span><span class="p">(</span><span class="n">maximum</span><span class="p">);</span>
    <span class="k">return</span> <span class="n">inventory</span><span class="p">[</span><span class="n">randomIndex</span><span class="p">];</span>
<span class="p">}</span>

<span class="kt">int</span> <span class="n">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="nb">NSArray</span> <span class="o">*</span><span class="n">inventory</span> <span class="o">=</span> <span class="p">@[];</span>
        <span class="nb">NSError</span> <span class="o">*</span><span class="n">error</span><span class="p">;</span>
        <span class="nb">NSString</span> <span class="o">*</span><span class="n">car</span> <span class="o">=</span> <span class="n">getRandomCarFromInventory</span><span class="p">(</span><span class="n">inventory</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">error</span><span class="p">);</span>
        
        <span class="k">if</span> <span class="p">(</span><span class="n">car</span> <span class="o">==</span> <span class="kc">nil</span><span class="p">)</span> <span class="p">{</span>
            <span class="c1">// Failed</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Car could not be selected&quot;</span><span class="p">);</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Domain: %@&quot;</span><span class="p">,</span> <span class="n">error</span><span class="p">.</span><span class="n">domain</span><span class="p">)</span><span class="o">;</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Error Code: %ld&quot;</span><span class="p">,</span> <span class="n">error</span><span class="p">.</span><span class="n">code</span><span class="p">)</span><span class="o">;</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Description: %@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">error</span> <span class="nf">localizedDescription</span><span class="p">])</span><span class="o">;</span>
            
        <span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
            <span class="c1">// Succeeded</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Car selected!&quot;</span><span class="p">);</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@&quot;</span><span class="p">,</span> <span class="n">car</span><span class="p">)</span><span class="o">;</span>
        <span class="p">}</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>Since it techinically is an error and not an exception, this version of
<code>getRandomCarFromInventory()</code> is the &ldquo;proper&rdquo; way to
handle it (opposed to <a href='exceptions.html#custom-exceptions'>Custom Exceptions</a>).</p>


<h1>Summary</h1>

<p>Errors represent a failed operation in an iOS or OS&nbsp;X application.
It&rsquo;s a standardized way to record the relevant information at the point
of detection and pass it off to the handling code. Exceptions are similar, but
are designed as more of a development aid. They generally should not be used in
your production-ready programs.</p>

<p>How you handle an error or exception is largely dependent on the type of
problem, as well as your application. But, most of the time you&rsquo;ll want
to inform the user with something like <a
href='http://developer.apple.com/library/ios/#documentation/UIKit/Reference/UIAlertView_Class/UIAlertView/UIAlertView.html%23/apple_ref/occ/cl/UIAlertView'><code>UIAlertView</code></a>
(iOS).  or <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/ApplicationKit/Classes/NSAlert_Class/Reference/Reference.html'><code>NSAlert</code></a>
(OS&nbsp;X). After that, you&rsquo;ll probably want to figure out what went
wrong by inspecting the <code>NSError</code> or <code>NSException</code> object
so that you can try to recover from it.</p>

<p>The next module explores some of the more conceptual aspects of the
Objective-C runtime. We&rsquo;ll learn about how the memory behind our objects
is managed by experimenting with the (now obsolete) Manual Retain Release
system, as well as the practical implications of the newer
Automatic Reference Counting system.</p>

<p class='sequential-nav'>
	<a href='memory-management.html'>Continue to <em>Memory Management</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>