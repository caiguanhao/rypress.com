<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Classes | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Objective-C
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Classes</h1>

<p>As in any other object-oriented programming language, Objective-C classes
provide the blueprint for creating objects. First, you define a reusable set of
properties and behaviors inside of a class. Then, you instantiate objects from
that class to interact with those properties and behaviors.</p>

<p>Objective-C is similar to C++ in that it abstracts a class&rsquo;s interface
from its implementation. An <strong>interface</strong> declares the public
properties and methods of a class, and the corresponding
<strong>implementation</strong> defines the code that actually makes these
properties and methods work. This is the same separation of concerns that we
saw with <a
href='functions.html#declarations-vs-implementations'>functions</a>.</p>

<figure>
	<img style='max-width: 500px' src='media/classes/interface-vs-implementation.png' />
	<figcaption>A class&rsquo;s
interface and implementation</figcaption>
</figure>

<p>In this module, we&rsquo;ll explore the basic syntax for class interfaces,
implementations, properties, and methods, as well as the canonical way to
instantiate objects. We&rsquo;ll also introduce some of Objective-C&rsquo;s
introspection and reflection capabilities.</p>


<h2>Creating Classes</h2>

<p>We&rsquo;ll be working with a class called <code>Car</code> whose interface
resides in a file named <code>Car.h</code> (also called a &ldquo;header&rdquo;)
and whose implementation resides in <code>Car.m</code>. These are the standard
file extensions for Objective-C classes. The class&rsquo;s header file is what
other classes use when they need to interact with it, and its implementation
file is used only by the compiler.</p>

<p>Xcode provides a convenient template for creating new classes. To create our
<code>Car</code> class, navigate to <em>File &gt; New &gt; File&hellip;</em> or
use the <em>Cmd+N</em> shortcut. For the template, choose <em>Objective-C
class</em> under the <em>iOS&nbsp;&gt; Cocoa Touch</em> category. After that,
you&rsquo;ll be presented with some configuration options:</p>

<figure>
	<img style='max-width: 357px' src='media/classes/class-creation.png' />
	<figcaption>Creating the <code>Car</code>
class</figcaption>
</figure>

<p>Use <code>Car</code> for the <em>Class</em> field and <code>NSObject</code>
for the <em>Subclass of</em> field. <a
href='https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/Foundation/Classes/NSObject_Class/Reference/Reference.html'><code>NSObject</code></a>
is Objective-C&rsquo;s top-level class from which all others inherit. Clicking
<em>Next</em> will prompt you to select a location for the file. Save it in the
top-level of the project directory. At the bottom of that dialog, make sure
that your project is checked in the <em>Targets</em> section. This is what
actually adds the class to the list of compiled sources.</p>

<figure>
	<img style='max-width: 334px' src='media/classes/adding-class-to-target.png' />
	<figcaption>Adding the class to the
build target</figcaption>
</figure>

<p>After clicking <em>Next</em>, you should see new <code>Car.h</code> and
<code>Car.m</code> files in Xcode&rsquo;s <em>Project Navigator</em>. If you
select the project name in the navigator, you&rsquo;ll also find
<code>Car.m</code> in the <em>Build Phases</em> tab under the <em>Compile
Sources</em> section. Any files that you want the compiler to see must be in
this list (if you didn&rsquo;t create your source files through Xcode, this is
where you can manually add them).</p>

<figure>
	<img style='max-width: 600px' src='media/classes/compile-sources.png' />
	<figcaption>The list of compiled source
files</figcaption>
</figure>


<h2>Interfaces</h2>

<p><code>Car.h</code> contains some template code, but let&rsquo;s go ahead and
change it to the following. This declares a property called <code>model</code>
and a method called <code>drive</code>.</p>

<div class="highlight"><pre><span class="c1">// Car.h</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">Car</span> : <span class="nc">NSObject</span> <span class="p">{</span>
    <span class="c1">// Protected instance variables (not recommended)</span>
<span class="p">}</span>

<span class="k">@property</span> <span class="p">(</span><span class="k">copy</span><span class="p">)</span> <span class="nb">NSString</span> <span class="o">*</span><span class="n">model</span><span class="p">;</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">drive</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<p>An interface is created with the <code>@interface</code> directive, after
which come the class and the superclass name, separated by a colon. Protected
variables can be defined inside of the curly braces, but most developers treat
instance variables as implementation details and prefer to store them in the
<code>.m</code> file instead of the interface.</p>

<p>The <code>@property</code> directive declares a public property, and the
<code>(copy)</code> attribute defines its memory management behavior. In this
case, the value assigned to <code>model</code> will be stored as a copy instead
of a direct pointer. The <a href='properties.html'>Properties</a> module
discusses this in more detail. Next come the property&rsquo;s data type and
name, just like a normal variable declaration.</p>

<p>The <code>-(void)drive</code> line declares a method called
<code>drive</code> that takes no parameters, and the <code>(void)</code>
portion defines its return type. The minus sign prepended to the method marks
it as an <em>instance</em> method (opposed to a <a
href='classes.html#class-methods-and-variables'><em>class</em> method</a>).</p>


<h2>Implementations</h2>

<p>The first thing any class implementation needs to do is import its
corresponding interface. The <code>@implementation</code> directive is similar
to <code>@interface</code>, except you don&rsquo;t need to include the super
class. Private instance variables can be stored between curly braces after the
class name:</p>

<div class="highlight"><pre><span class="c1">// Car.m</span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="k">@implementation</span> <span class="nc">Car</span> <span class="p">{</span>
    <span class="c1">// Private instance variables</span>
    <span class="kt">double</span> <span class="n">_odometer</span><span class="p">;</span>
<span class="p">}</span>

<span class="k">@synthesize</span> <span class="n">model</span> <span class="o">=</span> <span class="n">_model</span><span class="p">;</span>    <span class="c1">// Optional for Xcode 4.4+</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">drive</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Driving a %@. Vrooooom!&quot;</span><span class="p">,</span> <span class="kc">self</span><span class="p">.</span><span class="n">model</span><span class="p">);</span>
<span class="p">}</span>

<span class="k">@end</span>
</pre></div>


<p><code>@synthesize</code> is a convenience directive that automatically
generates accessor methods for the property. By default, the getter is simply
the property name (<code>model</code>), and the setter is the capitalized name
with the <code>set</code> prefix (<code>setModel</code>). This is much easier
than manually creating accessors for every property. The <code>_model</code>
portion of the synthesize statement defines the private instance variable name
to use for the property.</p>

<p>As of Xcode 4.4, properties declared with <code>@property</code> will be
automatically synthesized, so it&rsquo;s safe to omit the
<code>@synthesize</code> line if you&rsquo;re ok with the default instance
variable naming conventions.</p>

<p>The <code>drive</code> implementation has the same signature as the
interface, but it&rsquo;s followed by whatever code should be executed when the
method is called. Note how we accessed the value via <code>self.model</code>
instead of the <code>_model</code> instance variable. This is a best practice
step because it utilizes the property&rsquo;s <a
href='properties.html'>accessor methods</a>. Typically, the only place
you&rsquo;ll need to directly access instance variables is in <a
href='classes.html#constructor-methods'><code>init</code> methods</a> and the <a
href='memory-management.html#the-dealloc-method'><code>dealloc</code>
method</a>.</p>

<p>The <code>self</code> keyword refers to the instance calling the method
(like <code>this</code> in C++ and Java). In addition to accessing properties,
this can be used to call other methods defined on the same class (e.g.,
<code>[self&nbsp;anotherMethod]</code>). We&rsquo;ll see many examples of this
throughout the tutorial.</p>


<h2 id='instantiation-and-usage'>Instantiation and Usage</h2>

<p>Any files that need access to a class must import its header file
(<code>Car.h</code>)&mdash;they should never, ever try to access the
implementation file directly. That would defeat the goal of separating the
public API from its underlying implementation. So, to see our <code>Car</code>
class in action, change <code>main.m</code> to the following.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="n">Car</span> <span class="o">*</span><span class="n">toyota</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        
        <span class="p">[</span><span class="n">toyota</span> <span class="nf">setModel:</span><span class="s">@&quot;Toyota Corolla&quot;</span><span class="p">];</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Created a %@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">toyota</span> <span class="nf">model</span><span class="p">]);</span>
        
        <span class="n">toyota</span><span class="p">.</span><span class="n">model</span> <span class="o">=</span> <span class="s">@&quot;Toyota Camry&quot;</span><span class="p">;</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Changed the car to a %@&quot;</span><span class="p">,</span> <span class="n">toyota</span><span class="p">.</span><span class="n">model</span><span class="p">);</span>
        
        <span class="p">[</span><span class="n">toyota</span> <span class="nf">drive</span><span class="p">];</span>
        
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>After the interface has been imported with the <code>#import</code>
directive, you can instantiate objects with the
<code>alloc</code>/<code>init</code> pattern shown above. As you can see,
instantiation is a two-step process: first you must allocate some memory for
the object by calling the <code>alloc</code> method, then you need to
initialize it so it&rsquo;s ready to use. You should never use an uninitialized
object.</p>

<p>It&rsquo;s worth <a
href='c-basics.html#pointers-in-objective-c'>repeating</a> that <em>all</em>
objects must be stored as pointers. This is why we used <code>Car
*toyota</code> instead of <code>Car toyota</code> to declare the variable.</p>

<p>To call a method on an Objective-C object, you place the instance and the
method in square brackets, separated by a space. Arguments are passed after the
method name, preceded by a colon. So, if you&rsquo;re coming from a C++, Java,
or Python background, the <code>[toyota&nbsp;setModel:@"Toyota Corolla"]</code>
call would translate to:</p>

<div class="highlight"><pre><span class="n">toyota</span><span class="o">.</span><span class="na">setModel</span><span class="o">(</span><span class="s">&quot;Toyota Corolla&quot;</span><span class="o">);</span>
</pre></div>


<p>This square-bracket syntax can be unsettling for newcomers to the language,
but rest assured, you&rsquo;ll be more than comfortable with
Objective-C&rsquo;s method conventions after reading through the <a
href='methods.html'>Methods</a> module.</p>

<p>This example also shows you both ways to work with an object&rsquo;s
properties. You can either use the synthesized <code>model</code> and
<code>setModel</code> accessor methods, or you can use the convenient
dot-syntax, which should be more familiar to developers who have been using
Simula-style languages.</p>


<h2 id='class-methods-and-variables'>Class Methods and Variables</h2>

<p>The above snippets define instance-level properties and methods, but
it&rsquo;s also possible to define class-level ones. These are commonly called
&ldquo;static&rdquo; methods/properties in other programming languages (not to
be confused with the <a
href='functions.html#the-static-keyword'><code>static</code> keyword</a>).</p>

<p>Class method declarations look just like instance methods, except they are
prefixed with a plus sign instead of a minus sign. For example, let&rsquo;s add
the following class-level method to <code>Car.h</code>:

<div class="highlight"><pre><span class="c1">// Car.h</span>
<span class="k">+</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">setDefaultModel:</span><span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="p">)</span><span class="nv">aModel</span><span class="p">;</span>
</pre></div>


<p>Similarly, a class method <em>implementation</em> is also preceded by a plus
sign. While there is technically no such thing as a class-level variable in
Objective-C, you can emulate one by declaring a <code>static</code> variable
before defining the implementation:</p>

<div class="highlight"><pre><span class="c1">// Car.m</span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="kt">static</span> <span class="nb">NSString</span> <span class="o">*</span><span class="n">_defaultModel</span><span class="p">;</span>

<span class="k">@implementation</span> <span class="nc">Car</span> <span class="p">{</span>
<span class="p">...</span>

<span class="o">+</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">setDefaultModel:</span><span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="p">)</span><span class="n">aModel</span> <span class="p">{</span>
    <span class="n">_defaultModel</span> <span class="o">=</span> <span class="p">[</span><span class="n">aModel</span> <span class="nf">copy</span><span class="p">];</span>
<span class="p">}</span>

<span class="k">@end</span>
</pre></div>


<p>The <code>[aModel copy]</code> call creates a copy of the parameter instead
of assigning it directly. This is what&rsquo;s going on under the hood when we
used the <code>(copy)</code> attribute on the <code>model</code> property.</p>

<p>Class methods use the same square-bracket syntax as instance methods, but
they must be called directly on the class, as shown below. They <em>cannot</em>
be called on an instance of that class (<code>[toyota&nbsp;setDefaultModel:@"Model
T"]</code> will throw an error).</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="p">[</span><span class="n">Car</span> <span class="nf">setDefaultModel:</span><span class="s">@&quot;Nissan Versa&quot;</span><span class="p">];</span>
</pre></div>



<h2 id='constructor-methods'>&ldquo;Constructor&rdquo; Methods</h2>

<p>There are no constructor methods in Objective-C. Instead, an object is
<strong>initialized</strong> by calling the <code>init</code> method
immediately after it&rsquo;s allocated. This is why instantiation is always a
two-step process: allocate, then initialize. There is also a class-level
initialization method that will be discussed in a moment.</p>

<p><code>init</code> is the default initialization method, but you can also
define your own versions to accept configuration parameters. There&rsquo;s
nothing special about custom initialization methods&mdash;they&rsquo;re just
normal instance methods, except the method name should always begin with
<code>init</code>. An exemplary &ldquo;constructor&rdquo; method is shown
below.</p>

<div class="highlight"><pre><span class="c1">// Car.h</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">id</span><span class="p">)</span><span class="nf">initWithModel:</span><span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="p">)</span><span class="nv">aModel</span><span class="p">;</span>
</pre></div>


<p>To implement this method, you should follow the canonical initialization
pattern shown in <code>initWithModel:</code> below. The <code>super</code>
keyword refers to the parent class, and again, the <code>self</code> keyword
refers to the instance calling the method. Go ahead and add the following
methods to <code>Car.m</code>.</p>

<div class="highlight"><pre><span class="c1">// Car.m</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">id</span><span class="p">)</span><span class="nf">initWithModel:</span><span class="p">(</span><span class="nb">NSString</span> <span class="o">*</span><span class="p">)</span><span class="nv">aModel</span> <span class="p">{</span>
    <span class="kc">self</span> <span class="o">=</span> <span class="p">[</span><span class="n">super</span> <span class="nf">init</span><span class="p">];</span>
    <span class="k">if</span> <span class="p">(</span><span class="kc">self</span><span class="p">)</span> <span class="p">{</span>
        <span class="c1">// Any custom setup work goes here</span>
        <span class="n">_model</span> <span class="o">=</span> <span class="p">[</span><span class="n">aModel</span> <span class="nf">copy</span><span class="p">];</span>
        <span class="n">_odometer</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="kc">self</span><span class="p">;</span>
<span class="p">}</span>

<span class="o">-</span> <span class="p">(</span><span class="kt">id</span><span class="p">)</span><span class="n">init</span> <span class="p">{</span>
    <span class="c1">// Forward to the &quot;designated&quot; initialization method</span>
    <span class="k">return</span> <span class="p">[</span><span class="kc">self</span> <span class="nf">initWithModel:</span><span class="n">_defaultModel</span><span class="p">];</span>
<span class="p">}</span>
</pre></div>


<p>Initialization methods should always return a reference to the object
itself, and if it cannot be initialized, it should return <code>nil</code>.
This is why we need to check if <code>self</code> exists before trying to use
it. There should typically only be one initialization method that needs to do
this, and the rest should forward calls to this <strong>designated
initializer</strong>. This eliminates boilerplate code when you have several
custom <code>init</code> methods.</p>

<p>Also notice how we directly assigned values to the <code>_model</code> and
<code>_odometer</code> instance variables in <code>initWithModel:</code>.
Remember that this is one of the only places you should do this&mdash;in the
rest of your methods you should be using <code>self.model</code> and
<code>self.odometer</code>.</p>


<h3>Class-Level Initialization</h3>

<p>The <code>initialize</code> method is the class-level equivalent of
<code>init</code>. It gives you a chance to set up the class before anyone uses
it. For example, we can use this to populate the <code>_defaultModel</code>
variable with a valid value, like so:</p>

<div class="highlight"><pre><span class="c1">// Car.m</span>
<span class="k">+</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">initialize</span> <span class="p">{</span>
    <span class="k">if</span> <span class="p">(</span><span class="kc">self</span> <span class="o">==</span> <span class="p">[</span><span class="n">Car</span> <span class="nf">class</span><span class="p">])</span> <span class="p">{</span>
        <span class="c1">// Makes sure this isn&#39;t executed more than once</span>
        <span class="n">_defaultModel</span> <span class="o">=</span> <span class="s">@&quot;Nissan Versa&quot;</span><span class="p">;</span>
    <span class="p">}</span>
<span class="p">}</span>
</pre></div>


<p>The <code>initialize</code> class method is called once for every class
before the class is used. This includes all subclasses of <code>Car</code>,
which means that <code>Car</code> will get two <code>initialize</code> calls if
one of its subclasses didn&rsquo;t re-implement it. As a result, it&rsquo;s
good practice to use the <code>self&nbsp;==&nbsp;[Car&nbsp;class]</code>
conditional to ensure that the initialization code is only run once. Also note
that in class methods, the <code>self</code> keyword refers to the <em>class
itself</em>, not an instance.</p>

<p>Objective-C doesn&rsquo;t force you to mark methods as overrides. Even
though <code>init</code> and <code>initialize</code> are both defined by its
superclass, <code>NSObject</code>, the compiler won&rsquo;t complain when you
redefine them in <code>Car.m</code>.</p>

<p>The next iteration of <code>main.m</code> shows our custom initialization
methods in action. Before the first time the class is used, <code>[Car
initialize]</code> is called automatically, setting <code>_defaultModel</code>
to <code>@"Nissan Versa"</code>. This can be seen in the first
<code>NSLog()</code>. You can also see the result of the custom initialization
method (<code>initWithModel:</code>) in the second log output.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        
        <span class="c1">// Instantiating objects</span>
        <span class="n">Car</span> <span class="o">*</span><span class="n">nissan</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Created a %@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">nissan</span> <span class="nf">model</span><span class="p">]);</span>
        
        <span class="n">Car</span> <span class="o">*</span><span class="n">chevy</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">initWithModel:</span><span class="s">@&quot;Chevy Corvette&quot;</span><span class="p">];</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Created a %@, too.&quot;</span><span class="p">,</span> <span class="n">chevy</span><span class="p">.</span><span class="n">model</span><span class="p">);</span>
        
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>



<h2>Dynamic Typing</h2>

<p>Classes themselves are represented as objects, which makes it possible to
query their properties (introspection), or even change their behavior
on-the-fly (reflection). These are very powerful dynamic typing capabilities,
as they let you call methods and set properties on objects even when you
don&rsquo;t know what type of object they are.</p>

<p>The easiest way to get a class object is via the <code>class</code>
class-level method (apologies for the redundant terminology). For example,
<code>[Car class]</code> returns an object representing the <code>Car</code>
class. You can pass this object around to methods like
<code>isMemberOfClass:</code> and <code>isKindOfClass:</code> to get
information about other instances. A comprehensive example is included
below.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="n">Car</span> <span class="o">*</span><span class="n">delorean</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">initWithModel:</span><span class="s">@&quot;DeLorean&quot;</span><span class="p">];</span>
        
        <span class="c1">// Get the class of an object</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@ is an instance of the %@ class&quot;</span><span class="p">,</span>
              <span class="p">[</span><span class="n">delorean</span> <span class="nf">model</span><span class="p">],</span> <span class="p">[</span><span class="n">delorean</span> <span class="nf">class</span><span class="p">]);</span>
        
        <span class="c1">// Check an object against a class and all subclasses</span>
        <span class="k">if</span> <span class="p">([</span><span class="n">delorean</span> <span class="nf">isKindOfClass:</span><span class="p">[</span><span class="n">NSObject</span> <span class="nf">class</span><span class="p">]])</span> <span class="p">{</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@ is an instance of NSObject or one &quot;</span>
                  <span class="s">&quot;of its subclasses&quot;</span><span class="p">,</span>
                  <span class="p">[</span><span class="n">delorean</span> <span class="nf">model</span><span class="p">]);</span>
        <span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@ is not an instance of NSObject or &quot;</span>
                  <span class="s">&quot;one of its subclasses&quot;</span><span class="p">,</span>
                  <span class="p">[</span><span class="n">delorean</span> <span class="nf">model</span><span class="p">]);</span>
        <span class="p">}</span>
        
        <span class="c1">// Check an object against a class, but not its subclasses</span>
        <span class="k">if</span> <span class="p">([</span><span class="n">delorean</span> <span class="nf">isMemberOfClass:</span><span class="p">[</span><span class="n">NSObject</span> <span class="nf">class</span><span class="p">]])</span> <span class="p">{</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@ is a instance of NSObject&quot;</span><span class="p">,</span>
                  <span class="p">[</span><span class="n">delorean</span> <span class="nf">model</span><span class="p">]);</span>
        <span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;%@ is not an instance of NSObject&quot;</span><span class="p">,</span>
                  <span class="p">[</span><span class="n">delorean</span> <span class="nf">model</span><span class="p">]);</span>
        <span class="p">}</span>
        
        <span class="c1">// Convert between strings and classes</span>
        <span class="k">if</span> <span class="p">(</span><span class="nb">NSClassFromString</span><span class="p">(</span><span class="s">@&quot;Car&quot;</span><span class="p">)</span> <span class="o">==</span> <span class="p">[</span><span class="n">Car</span> <span class="nf">class</span><span class="p">])</span> <span class="p">{</span>
            <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;I can convert between strings and classes!&quot;</span><span class="p">);</span>
        <span class="p">}</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>The <code>NSClassFromString()</code> function is an alternative way to get
your hands on a class object. This is very flexible, as it lets you dynamically
request class objects at runtime; however, it&rsquo;s also rather inefficient.
For this reason, you should opt for the <code>class</code> method whenever
possible.</p>

<p>If you&rsquo;re interested in dynamic typing, be sure to check out <a
href='methods.html#selectors'>Selectors</a> and <a
href='data-types/primitives.html#id'>The <code>id</code> Type</a>.</p>


<h2>Summary</h2>

<p>In this module, we learned how to create classes, instantiate objects,
define initialization methods, and work with class-level methods and variables.
We also took a brief look at dynamic typing.</p>

<p>The <a href='functions.html#function-libraries'>previous module</a>
mentioned that Objective-C doesn&rsquo;t support namespaces, which is why the
Cocoa functions require prefixes like <code>NS</code>, <code>CA</code>,
<code>AV</code>, etc to avoid naming collisions. This applies to classes, too.
The recommended convention is to use a three-letter prefix for your
application-specific classes (e.g., <code>XYZCar</code>).</p>

<p>While this is pretty much everything you need to know to start writing your
own classes, we did skim over some important details, so don&rsquo;t worry if
you&rsquo;re feeling not entirely comfortable with properties or methods. The
next module will begin filling in these holes with a closer look at the
<code>@property</code> directive and all of the attributes that affect its
behavior.</p>

<p class='sequential-nav'>
	<a href='properties.html'>Continue to <em>Properties</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>