<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Objective-C Tutorial | Categories | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="RyPress.com creates
  high-quality software tutorials and publishes them completely free of
  charge." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>
<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Objective-C
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Categories</h1>

<p>Categories are a way to split a single class definition into multiple files.
Their goal is to ease the burden of maintaining large code bases by
modularizing a class. This prevents your source code from becoming monolithic
10000+ line files that are impossible to navigate and makes it easy to assign
specific, well-defined portions of a class to individual developers.</p>

<figure>
	<img style='max-width: 250px' src='media/categories/category-overview.png' />
	<figcaption>Using multiple files to
implement a class</figcaption>
</figure>

<p>In this module, we&rsquo;ll use a category to extend an existing class
without touching its original source file. Then, we&rsquo;ll learn how this
functionality can be used to emulate protected methods. Extensions are a close
relative to categories, so we&rsquo;ll be taking a brief look at those,
too.</p>


<h2>Setting Up</h2>

<p>Before we can start experimenting with categories, we need a base class to
work off of. Create or change your existing <code>Car</code> interface to the
following:</p>

<div class="highlight"><pre><span class="c1">// Car.h</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">Car</span> : <span class="nc">NSObject</span>

<span class="k">@property</span> <span class="p">(</span><span class="k">copy</span><span class="p">)</span> <span class="nb">NSString</span> <span class="o">*</span><span class="n">model</span><span class="p">;</span>
<span class="k">@property</span> <span class="p">(</span><span class="k">readonly</span><span class="p">)</span> <span class="kt">double</span> <span class="n">odometer</span><span class="p">;</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">startEngine</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">drive</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">turnLeft</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">turnRight</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<p>The corresponding implementation just outputs some descriptive messages so
we can see when different methods get called:</p>

<div class="highlight"><pre><span class="c1">// Car.m</span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="k">@implementation</span> <span class="nc">Car</span>

<span class="k">@synthesize</span> <span class="n">model</span> <span class="o">=</span> <span class="n">_model</span><span class="p">;</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">startEngine</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Starting the %@&#39;s engine&quot;</span><span class="p">,</span> <span class="n">_model</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">drive</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The %@ is now driving&quot;</span><span class="p">,</span> <span class="n">_model</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">turnLeft</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The %@ is turning left&quot;</span><span class="p">,</span> <span class="n">_model</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">turnRight</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The %@ is turning right&quot;</span><span class="p">,</span> <span class="n">_model</span><span class="p">);</span>
<span class="p">}</span>

<span class="k">@end</span>
</pre></div>


<p>Now, let&rsquo;s say you want to add another set of methods related to car
maintenance. Instead of cluttering up these <code>Car.h</code> and
<code>Car.m</code> files, you can place the new methods in a dedicated
category.</p>


<h2>Creating Categories</h2>

<p>Categories work just like normal class definitions in that they are composed
of an interface and an implementation. To add a new category to your Xcode
project, create a new file and choose the <em>Objective-C category</em>
template under <em>iOS&nbsp;&gt;&nbsp;Cocoa&nbsp;Touch</em>. Use
<code>Maintenance</code> for the <em>Category</em> field and <code>Car</code>
for <em>Category on</em>.</p>

<figure>
	<img style='max-width: 357px' src='media/categories/creating-a-category.png' />
	<figcaption>Creating the <code>Maintenance</code> category</figcaption>
</figure>

<p>The only restriction on category names is that they don&rsquo;t conflict
with other categories on the same class. The canonical file-naming convention
is to use the class name and the category name separated by a plus sign, so you
should find a <code>Car+Maintenance.h</code> and a
<code>Car+Maintenance.m</code> in Xcode&rsquo;s <em>Project Navigator</em>
after saving the above category.</p>

<p>As you can see in <code>Car+Maintenance.h</code>, a category interface looks
exactly like a normal interface, except the class name is followed by the
category name in parentheses. Let&rsquo;s go ahead and add a few methods to the
category:</p>

<div class="highlight"><pre><span class="c1">// Car+Maintenance.h</span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">Car</span> <span class="nl">(Maintenance)</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">BOOL</span><span class="p">)</span><span class="nf">needsOilChange</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">changeOil</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">rotateTires</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">jumpBatteryUsingCar:</span><span class="p">(</span><span class="n">Car</span> <span class="o">*</span><span class="p">)</span><span class="nv">anotherCar</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<p>At runtime, these methods become part of the <code>Car</code> class. Even
though they&rsquo;re declared in a different file, you will be able to access
them as if they were defined in the original <code>Car.h</code>.</p>

<p>Of course, you have to implement the category interface for the above
methods to actually <em>do</em> anything. Again, a category implementation
looks almost exactly like a standard implementation, except the category name
appears in parentheses after the class name:</p>

<div class="highlight"><pre><span class="c1">// Car+Maintenance.m</span>
<span class="cp">#import</span> <span class="l">&quot;Car+Maintenance.h&quot;</span><span class="cp"></span>

<span class="k">@implementation</span> <span class="nc">Car</span> <span class="nl">(Maintenance)</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">BOOL</span><span class="p">)</span><span class="nf">needsOilChange</span> <span class="p">{</span>
    <span class="k">return</span> <span class="kc">YES</span><span class="p">;</span>
<span class="p">}</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">changeOil</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Changing oil for the %@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">self</span> <span class="nf">model</span><span class="p">]);</span>
<span class="p">}</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">rotateTires</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Rotating tires for the %@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">self</span> <span class="nf">model</span><span class="p">]);</span>
<span class="p">}</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">jumpBatteryUsingCar:</span><span class="p">(</span><span class="n">Car</span> <span class="o">*</span><span class="p">)</span><span class="nv">anotherCar</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Jumped the %@ with a %@&quot;</span><span class="p">,</span> <span class="p">[</span><span class="n">self</span> <span class="nf">model</span><span class="p">],</span> <span class="p">[</span><span class="n">anotherCar</span> <span class="nf">model</span><span class="p">]);</span>
<span class="p">}</span>

<span class="k">@end</span>
</pre></div>


<p>It&rsquo;s important to note that a category can also be used to override
existing methods in the base class (e.g., the <code>Car</code> class&rsquo;s
<code>drive</code> method), but <strong>you should never do this</strong>. The
problem is that categories are a flat organizational structure. If you override
an existing method in <code>Car+Maintenance.m</code>, and then decide you want
to change its behavior again with another category, there is no way for
Objective-C to know which implementation to use. Subclassing is almost always a
better option in such a situation.</p>


<h2>Using Categories</h2>

<p>Any files that use an API defined in a category need to import that category
header just like a normal class interface. After importing
<code>Car+Maintenance.h</code>, all of its methods will be available directly
through the <code>Car</code> class:</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Car+Maintenance.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="n">Car</span> <span class="o">*</span><span class="n">porsche</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="n">porsche</span><span class="p">.</span><span class="n">model</span> <span class="o">=</span> <span class="s">@&quot;Porsche 911 Turbo&quot;</span><span class="p">;</span>
        <span class="n">Car</span> <span class="o">*</span><span class="n">ford</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="n">ford</span><span class="p">.</span><span class="n">model</span> <span class="o">=</span> <span class="s">@&quot;Ford F-150&quot;</span><span class="p">;</span>
        
        <span class="c1">// &quot;Standard&quot; functionality from Car.h</span>
        <span class="p">[</span><span class="n">porsche</span> <span class="nf">startEngine</span><span class="p">];</span>
        <span class="p">[</span><span class="n">porsche</span> <span class="nf">drive</span><span class="p">];</span>
        <span class="p">[</span><span class="n">porsche</span> <span class="nf">turnLeft</span><span class="p">];</span>
        <span class="p">[</span><span class="n">porsche</span> <span class="nf">turnRight</span><span class="p">];</span>
        
        <span class="c1">// Additional methods from Car+Maintenance.h</span>
        <span class="k">if</span> <span class="p">([</span><span class="n">porsche</span> <span class="nf">needsOilChange</span><span class="p">])</span> <span class="p">{</span>
            <span class="p">[</span><span class="n">porsche</span> <span class="nf">changeOil</span><span class="p">];</span>
        <span class="p">}</span>
        <span class="p">[</span><span class="n">porsche</span> <span class="nf">rotateTires</span><span class="p">];</span>
        <span class="p">[</span><span class="n">porsche</span> <span class="nf">jumpBatteryUsingCar:</span><span class="n">ford</span><span class="p">];</span>
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>If you remove the import statement for <code>Car+Maintenance.h</code>, the
<code>Car</code> class will revert to its original state, and the compiler will
complain that <code>needsOilChange</code>, <code>changeOil</code>, and the rest
of the methods from the <code>Maintenance</code> category don&rsquo;t exist.</p>


<h2 id='protected-methods'>&ldquo;Protected&rdquo; Methods</h2>

<p>But, categories aren&rsquo;t just for spreading a class definition over
several files. They are a powerful organizational tool that allow arbitrary
files to &ldquo;opt-in&rdquo; to a portion of an API by simply importing the
category. To everybody else, that API remains hidden.</p>

<p>Recall from the <a href='methods.html#protected-private'>Methods</a> module
that protected methods don&rsquo;t actually exist in Objective-C; however, the
opt-in behavior of categories can be used to <em>emulate</em> protected access
modifiers. The idea is to define a &ldquo;protected&rdquo; API in a dedicated
category, and only import it into subclass implementations. This makes the
protected methods available to subclasses, but keeps them hidden from other
aspects of the application. For example:</p>

<div class="highlight"><pre><span class="c1">// Car+Protected.h</span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">Car</span> <span class="nl">(Protected)</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">prepareToDrive</span><span class="p">;</span>

<span class="k">@end</span>
</pre></div>


<div class="highlight"><pre><span class="c1">// Car+Protected.m</span>
<span class="cp">#import</span> <span class="l">&quot;Car+Protected.h&quot;</span><span class="cp"></span>

<span class="k">@implementation</span> <span class="nc">Car</span> <span class="nl">(Protected)</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">prepareToDrive</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Doing some internal work to get the %@ ready to drive&quot;</span><span class="p">,</span>
          <span class="p">[</span><span class="n">self</span> <span class="nf">model</span><span class="p">]);</span>
<span class="p">}</span>

<span class="k">@end</span>
</pre></div>


<p>The <code>Protected</code> category shown above defines a single method for
internal use by <code>Car</code> and its subclasses. To see this in action,
let&rsquo;s modify <code>Car.m</code>&rsquo;s <code>drive</code> method to use
the protected <code>prepareToDrive</code> method:</p>

<div class="highlight"><pre><span class="c1">// Car.m</span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Car+Protected.h&quot;</span><span class="cp"></span>

<span class="k">@implementation</span> <span class="nc">Car</span>
<span class="p">...</span>
<span class="o">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="n">drive</span> <span class="p">{</span>
    <span class="p">[</span><span class="n">self</span> <span class="nf">prepareToDrive</span><span class="p">];</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;The %@ is now driving&quot;</span><span class="p">,</span> <span class="n">_model</span><span class="p">)</span><span class="o">;</span>
<span class="p">}</span>
<span class="p">...</span>
</pre></div>


<p>Next, let&rsquo;s take a look at how this protected method works by creating
a subclass called <code>Coupe</code>. There&rsquo;s nothing special about the
interface, but notice how the implementation opts-in to the protected API by
importing <code>Car+Protected.h</code>. This makes it possible to use the
protected <code>prepareToDrive</code> method in the subclass. If desired, you
can also override the protected method by simply re-defining it in
<code>Coupe.m</code>.</p>

<div class="highlight"><pre><span class="c1">// Coupe.h</span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">Coupe</span> : <span class="nc">Car</span>
<span class="c1">// Extra methods defined by the Coupe subclass</span>
<span class="k">@end</span>
</pre></div>


<div class="highlight"><pre><span class="c1">// Coupe.m</span>
<span class="cp">#import</span> <span class="l">&quot;Coupe.h&quot;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Car+Protected.h&quot;</span><span class="cp"></span>

<span class="k">@implementation</span> <span class="nc">Coupe</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">startEngine</span> <span class="p">{</span>
    <span class="p">[</span><span class="n">super</span> <span class="nf">startEngine</span><span class="p">];</span>
    <span class="c1">// Call the protected method here instead of in `drive`</span>
    <span class="p">[</span><span class="n">self</span> <span class="nf">prepareToDrive</span><span class="p">];</span>
<span class="p">}</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">drive</span> <span class="p">{</span>
    <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;VROOOOOOM!&quot;</span><span class="p">);</span>
<span class="p">}</span>

<span class="k">@end</span>
</pre></div>


<p>To enforce the protected status of the methods in
<code>Car+Protected.h</code>, it should only be made available to subclass
implementations&mdash;do <em>not</em> import it into other files. In the
following <code>main.m</code>, you can see the protected
<code>prepareToDrive</code> method called by <code>[ford drive]</code> and
<code>[porsche startEngine]</code>, but the compiler will complain if you try
to call it directly.</p>

<div class="highlight"><pre><span class="c1">// main.m</span>
<span class="cp">#import</span> <span class="l">&lt;Foundation/Foundation.h&gt;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>
<span class="cp">#import</span> <span class="l">&quot;Coupe.h&quot;</span><span class="cp"></span>

<span class="kt">int</span> <span class="nf">main</span><span class="p">(</span><span class="kt">int</span> <span class="n">argc</span><span class="p">,</span> <span class="kt">const</span> <span class="kt">char</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span> <span class="p">{</span>
    <span class="k">@autoreleasepool</span> <span class="p">{</span>
        <span class="n">Car</span> <span class="o">*</span><span class="n">ford</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Car</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="n">ford</span><span class="p">.</span><span class="n">model</span> <span class="o">=</span> <span class="s">@&quot;Ford F-150&quot;</span><span class="p">;</span>
        <span class="p">[</span><span class="n">ford</span> <span class="nf">startEngine</span><span class="p">];</span>
        <span class="p">[</span><span class="n">ford</span> <span class="nf">drive</span><span class="p">];</span> <span class="c1">// Calls the protected method</span>
        
        <span class="n">Car</span> <span class="o">*</span><span class="n">porsche</span> <span class="o">=</span> <span class="p">[[</span><span class="n">Coupe</span> <span class="nf">alloc</span><span class="p">]</span> <span class="nf">init</span><span class="p">];</span>
        <span class="n">porsche</span><span class="p">.</span><span class="n">model</span> <span class="o">=</span> <span class="s">@&quot;Porsche 911 Turbo&quot;</span><span class="p">;</span>
        <span class="p">[</span><span class="n">porsche</span> <span class="nf">startEngine</span><span class="p">];</span> <span class="c1">// Calls the protected method</span>
        <span class="p">[</span><span class="n">porsche</span> <span class="nf">drive</span><span class="p">];</span>
        
        <span class="c1">// &quot;Protected&quot; methods have not been imported,</span>
        <span class="c1">// so this will *not* work</span>
        <span class="c1">// [porsche prepareToDrive];</span>
        
        <span class="kt">SEL</span> <span class="n">protectedMethod</span> <span class="o">=</span> <span class="k">@selector</span><span class="p">(</span><span class="n">prepareToDrive</span><span class="p">);</span>
        <span class="k">if</span> <span class="p">([</span><span class="n">porsche</span> <span class="nf">respondsToSelector:</span><span class="n">protectedMethod</span><span class="p">])</span> <span class="p">{</span>
            <span class="c1">// This *will* work</span>
            <span class="p">[</span><span class="n">porsche</span> <span class="nf">performSelector:</span><span class="n">protectedMethod</span><span class="p">];</span>
        <span class="p">}</span>
        
        
    <span class="p">}</span>
    <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>Notice that you <em>can</em> access <code>prepareToDrive</code> dynamically
through <code>performSelector:</code>. Once again, <em>all</em> methods in
Objective-C are public, and there is no way to truly hide them from client
code. Categories are merely a convention-based way to control which parts of an
API are visible to which files.</p>


<h2 id='extensions'>Extensions</h2>

<p>Extensions are similar to categories in that they let you add methods to a
class outside of the main interface file. But, in contrast to categories, an
extension&rsquo;s API must be implemented in the <em>main</em> implementation
file&mdash;it <em>cannot</em> be implemented in a category.</p>

<p>Remember that private methods can be emulated by adding them to the
implementation but not the interface. This works when you have only a few
private methods, but can become unwieldy for larger classes. Extensions solve
this problem by letting you declare a <em>formal</em> private API.</p>

<p>For example, if you wanted to formally add a private
<code>engineIsWorking</code> method to the <code>Car</code> class defined
above, you could include an extension in <code>Car.m</code>. The compiler
complains if the method isn&rsquo;t defined in the main
<code>@implementation</code> block, but since it&rsquo;s declared in
<code>Car.m</code> instead of <code>Car.h</code>, it remains a <em>private</em>
method. The extension syntax looks like an empty category:</p>

<div class="highlight"><pre><span class="c1">// Car.m</span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="c1">// The class extension</span>
<span class="k">@interface</span> <span class="nc">Car</span> <span class="nl">()</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">BOOL</span><span class="p">)</span><span class="nf">engineIsWorking</span><span class="p">;</span>
<span class="k">@end</span>

<span class="c1">// The main implementation</span>
<span class="k">@implementation</span> <span class="nc">Car</span>

<span class="k">@synthesize</span> <span class="n">model</span> <span class="o">=</span> <span class="n">_model</span><span class="p">;</span>

<span class="k">-</span> <span class="p">(</span><span class="kt">BOOL</span><span class="p">)</span><span class="nf">engineIsWorking</span> <span class="p">{</span>
    <span class="c1">// In the real world, this would probably return a useful value</span>
    <span class="k">return</span> <span class="kc">YES</span><span class="p">;</span>
<span class="p">}</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span><span class="nf">startEngine</span> <span class="p">{</span>
    <span class="k">if</span> <span class="p">([</span><span class="n">self</span> <span class="nf">engineIsWorking</span><span class="p">])</span> <span class="p">{</span>
        <span class="nb">NSLog</span><span class="p">(</span><span class="s">@&quot;Starting the %@&#39;s engine&quot;</span><span class="p">,</span> <span class="n">_model</span><span class="p">);</span>
    <span class="p">}</span>
<span class="p">}</span>
<span class="p">...</span>
<span class="k">@end</span>
</pre></div>


<p>In addition to declaring formal private API&rsquo;s, extensions can be used
to re-declare properties from the public interface. This is often used to make
properties internally behave as read-write properties while remaining read-only
to other objects. For instance, if we change the above class extension to:</p>

<div class="highlight"><pre><span class="c1">// Car.m</span>
<span class="cp">#import</span> <span class="l">&quot;Car.h&quot;</span><span class="cp"></span>

<span class="k">@interface</span> <span class="nc">Car</span> <span class="nl">()</span>
<span class="k">@property</span> <span class="p">(</span><span class="k">readwrite</span><span class="p">)</span> <span class="kt">double</span> <span class="n">odometer</span><span class="p">;</span>
<span class="k">-</span> <span class="p">(</span><span class="kt">BOOL</span><span class="p">)</span><span class="nf">engineIsWorking</span><span class="p">;</span>
<span class="k">@end</span>
<span class="p">...</span>
</pre></div>


<p>We can then assign values to <code>self.odometer</code> inside of the
implementation, but trying to do so outside of <code>Car.m</code> will result
in a compiler error.</p>

<p>Again, re-declaring properties as read-write and creating formal private
API&rsquo;s isn&rsquo;t all that useful for small classes. Their real utility
comes into play when you need to organize larger frameworks.</p>

<p>Extensions used to see much more action before Xcode 4.3, back when private
methods had to be declared <em>before</em> they were used. This was
inconvenient for many developers, and extensions provided a workaround by
acting as forward-declarations of private methods. So, even if you don&rsquo;t
use the above pattern in your own projects, you&rsquo;re likely to encounter it
at some point in your Objective-C career.</p>


<h2>Summary</h2>

<p>This module covered Objective-C categories and extensions. Categories are a
way to modularize a class by spreading its implementation over many files.
Extensions provide similar functionality, except its API must be declared in
the <em>main</em> implementation file.</p>

<p>Outside of organizing large code libraries, one of the most common uses of
categories is to add methods to built-in data types like <code>NSString</code>
or <code>NSArray</code>. The advantage of this is that you don&rsquo;t have to
update existing code to use a new subclass, but you need to be very careful not
to override existing functionality. For small personal projects, categories
really aren&rsquo;t worth the trouble, and sticking with standard tools like
subclassing and protocols will save you some debugging headaches down the
road.</p>

<p>In the next module, we&rsquo;ll explore another organizational tool called
blocks. Blocks are a way to represent and pass around arbitrary statements.
This opens the door to a whole new world of programming paradigms.</p>

<p class='sequential-nav'>
	<a href='blocks.html'>Continue to <em>Blocks</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>