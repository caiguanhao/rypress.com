<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Git Tutorial | Branches, Part II | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="Instead of introducing new commands, this module
discusses real-world branch workflows and some of problems that arise in a
branched environment." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Git
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Branches, Part II</h1>

<p>Now that we&rsquo;ve covered the mechanics behind Git branches, we can
discuss the practical impact that they have on the software development
process. Instead of introducing new commands, this module covers how the
typical Git user applies this workflow to real projects, as well as some of the
problems that arise in a branched environment.</p>

<p>To Git, a branch is a branch, but it&rsquo;s often useful to assign special
meaning to different branches. For example, we&rsquo;ve been using
<code>master</code> as the stable branch for our example project, and
we&rsquo;ve also used a temporary branch to add some CSS formatting. Temporary
branches like the latter are called <strong>topic branches</strong> because
they exist to develop a certain topic, then they are deleted. We&rsquo;ll work
with two types of topic branches later in this module.</p>

<p>Amid our exploration of Git branches, we&rsquo;ll also discover that some
merges cannot be &ldquo;fast-forwarded.&rdquo; When the history of two branches
diverges, a dedicated commit is required to combine the branches. This
situation may also give rise to a merge conflict, which must be manually
resolved before anything can be committed to the repository.</p>

<div class='icon-text download-icon-text'>
	<img src='../../media/icons/download.png' style='max-width: 45px'/>

	<p><a href='media/repo-zips/branches-2.zip'>Download the repository
	for this module</a></p>

</div>

<div style='clear: both'></div>

<p>If you&rsquo;ve been following along from the previous module, you already
have everything you need. Otherwise, download the zipped Git repository from
the above link, uncompress it, and you&rsquo;re good to go.</p>


<h2>Continue the Crazy Experiment</h2>

<p>Let&rsquo;s start by checking out the <code>crazy</code> branch.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">branch</span>
<span class="k">git</span> <span class="k">checkout</span> <span class="n">crazy</span>
<span class="k">git</span> <span class="k">log</span> <span class="na">--oneline</span>
</pre></div>


<p>The <code>crazy</code> branch is a longer-running type of topic branch
called a <strong>feature branch</strong>. This is fitting, as it was created
with the intention of developing a specific <em>feature</em>. It&rsquo;s also a
term that makes Git&rsquo;s contribution to the development workflow readily
apparent: branches enable you to focus on developing one clearly defined
feature at a time.</p>

<p>This brings us to my rule-of-thumb for using Git branches:</p>

<ul>
	<li>Create a new branch for each major addition to your project.</li>
	<li><em>Don&rsquo;t</em> create a branch if you can&rsquo;t give it a
	specific name.</li>
</ul>

<p>Following these simple guidelines will have a dramatic impact on your
programming efficiency.</p>


<h2>Merge the CSS Updates</h2>

<p>Note that the CSS formatting we merged into <code>master</code> is nowhere
to be found.  This presents a bit of a problem if we want our experiment to
reflect these updates. Conveniently, Git lets us merge changes into
<em>any</em> branch (not just the <code>master</code> branch). So, we can pull
the updates in with the familiar <code>git merge</code> command. Remember that
merging only affects the checked-out branch.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">merge</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">log</span> <span class="na">--oneline</span>
</pre></div>


<p>As of Git 1.7.10, this will open your editor and prompt you for a message
explaining why the commit was necessary. You can use the default <code>Merge
branch 'master' into crazy</code>. When you save and close the file,
you&rsquo;ll notice an extra commit in your project history. Recall that our
first merge didn&rsquo;t add any new commits; it just
&ldquo;fast-forwarded&rdquo; the tip of the <code>master</code> branch. This
was not the case for our new merge, which is shown below.</p>

<figure>
	<img style='max-width: 490px' src='media/4-1.png' />
	<figcaption>Merging <code>master</code> into the <code>crazy</code> branch</figcaption>
</figure>

<p>Take a moment to examine why the current merge couldn&rsquo;t be a
fast-forward one.  How could Git have walked the <code>crazy</code> pointer
over to the tip of the <code>master</code> branch? It&rsquo;s not possible
without backtracking, which kind of defeats the idea of
&ldquo;fast-forwarding.&rdquo; We&rsquo;re left with a new way to combine
branches: the <strong>3-way merge</strong>.</p>

<p>A 3-way merge occurs when you try to merge two branches whose history has
diverged. It creates an extra <strong>merge commit</strong> to function as a
link between the two branches. As a result, it has <em>two</em> parent commits.
The above figure visualizes this with two arrows originating from the tip of
<code>crazy</code>.  It&rsquo;s like saying &ldquo;this commit comes from both
the <code>crazy</code> branch <em>and</em> from <code>master</code>.&rdquo;
After the merge, the <code>crazy</code> branch has access to both its history
and the <code>master</code> history.</p>

<p>The name comes from the internal method used to create the merge commit. Git
looks at <em>three</em> commits (numbered in the above figure) to generate the
final state of the merge.</p>

<p>This kind of branch interaction is a big part of what makes Git such a
powerful development tool. We can not only create independent lines of
development, but we can also share information between them by tying together
their histories with a 3-way merge.</p>


<h2>Style the Rainbow Page</h2>

<p>Now that we have access to the CSS updates from <code>master</code>, we can
continue developing our crazy experiment. Link the CSS stylesheet to
<code>rainbow.html</code> by adding the following HTML on the line after the
<code>&lt;title&gt;</code> element.</p>

<div class="highlight"><pre><span class="nt">&lt;link</span> <span class="na">rel=</span><span class="s">&quot;stylesheet&quot;</span> <span class="na">href=</span><span class="s">&quot;style.css&quot;</span> <span class="nt">/&gt;</span>
</pre></div>


<p>Stage and commit the update, then check that it&rsquo;s reflected in the
history.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">status</span>
<span class="k">git</span> <span class="k">commit</span> <span class="na">-a</span> <span class="na">-m</span> <span class="s">&quot;Add CSS stylesheet to rainbow.html&quot;</span>
<span class="k">git</span> <span class="k">log</span> <span class="na">--oneline</span>
</pre></div>


<p>Notice that we skipped the staging step this time around. Instead of using
<code>git add</code>, we passed the <code>-a</code> flag to <code>git
commit</code>. This convenient parameter tells Git to automatically include
<em>all</em> tracked files in the staged snapshot.  Combined with the
<code>-m</code> flag, we can stage and commit snapshots with a single command.
However, be careful not to include unintended files when using the
<code>-a</code> flag.</p>


<h2>Link to the Rainbow Page</h2>

<p>We still need to add a navigation link to the home page. Change the
&ldquo;Navigation&rdquo; section of <code>index.html</code> to the
following.</p>

<div class="highlight"><pre><span class="nt">&lt;h2&gt;</span>Navigation<span class="nt">&lt;/h2&gt;</span>
<span class="nt">&lt;ul&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: #F90&quot;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">&quot;orange.html&quot;</span><span class="nt">&gt;</span>The Orange Page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: #00F&quot;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">&quot;blue.html&quot;</span><span class="nt">&gt;</span>The Blue Page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">&quot;rainbow.html&quot;</span><span class="nt">&gt;</span>The Rainbow Page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span>
</pre></div>


<p>As usual, stage and commit the snapshot.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">commit</span> <span class="na">-a</span> <span class="na">-m</span> <span class="s">&quot;Link index.html to rainbow.html&quot;</span>
<span class="k">git</span> <span class="k">log</span> <span class="na">--oneline</span>
</pre></div>



<h2>Fork an Alternative Rainbow</h2>

<p>Next, we&rsquo;re going to brainstorm an alternative to the current
<code>rainbow.html</code> page. This is a perfect time to create another topic
branch:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">branch</span> <span class="n">crazy-alt</span>
<span class="k">git</span> <span class="k">checkout</span> <span class="n">crazy-alt</span>
</pre></div>


<p>Remember, we can do whatever we want here without worrying about either
<code>crazy</code> or <code>master</code>. When <code>git branch</code> creates
a branch, it uses the current <code>HEAD</code> as the starting point for the
new branch. This means that we begin with the same files as <code>crazy</code>
(if we called <code>git branch</code> from <code>master</code>, we would have
had to re-create <code>rainbow.html</code>). After creating the new branch, our
repository&rsquo;s history looks like:</p>

<figure>
	<img style='max-width: 520px' src='media/4-2.png' />
	<figcaption>Creating the <code>crazy-alt</code> branch</figcaption>
</figure>


<h2>Change the Rainbow</h2>

<p>Change the colorful list in <code>rainbow.html</code> from:</p>

<div class="highlight"><pre><span class="nt">&lt;ul&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: red&quot;</span><span class="nt">&gt;</span>Red<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: orange&quot;</span><span class="nt">&gt;</span>Orange<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: yellow&quot;</span><span class="nt">&gt;</span>Yellow<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: green&quot;</span><span class="nt">&gt;</span>Green<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: blue&quot;</span><span class="nt">&gt;</span>Blue<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: indigo&quot;</span><span class="nt">&gt;</span>Indigo<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: violet&quot;</span><span class="nt">&gt;</span>Violet<span class="nt">&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span>
</pre></div>


<p>to the following:</p>

<div class="highlight"><pre><span class="nt">&lt;div</span> <span class="na">style=</span><span class="s">&quot;background-color: red&quot;</span><span class="nt">&gt;&lt;/div&gt;</span>
<span class="nt">&lt;div</span> <span class="na">style=</span><span class="s">&quot;background-color: orange&quot;</span><span class="nt">&gt;&lt;/div&gt;</span>
<span class="nt">&lt;div</span> <span class="na">style=</span><span class="s">&quot;background-color: yellow&quot;</span><span class="nt">&gt;&lt;/div&gt;</span>
<span class="nt">&lt;div</span> <span class="na">style=</span><span class="s">&quot;background-color: green&quot;</span><span class="nt">&gt;&lt;/div&gt;</span>
<span class="nt">&lt;div</span> <span class="na">style=</span><span class="s">&quot;background-color: blue&quot;</span><span class="nt">&gt;&lt;/div&gt;</span>
<span class="nt">&lt;div</span> <span class="na">style=</span><span class="s">&quot;background-color: indigo&quot;</span><span class="nt">&gt;&lt;/div&gt;</span>
<span class="nt">&lt;div</span> <span class="na">style=</span><span class="s">&quot;background-color: violet&quot;</span><span class="nt">&gt;&lt;/div&gt;</span>
</pre></div>


<p>Then, add some CSS formatting to <code>&lt;head&gt;</code> on the line after the
<code>&lt;meta&gt;</code> element:</p>
	
<div class="highlight"><pre><span class="nt">&lt;style&gt;</span>
  <span class="nt">div</span> <span class="p">{</span>
    <span class="k">width</span><span class="o">:</span> <span class="m">300px</span><span class="p">;</span>
    <span class="k">height</span><span class="o">:</span> <span class="m">50px</span><span class="p">;</span>
  <span class="p">}</span>
<span class="nt">&lt;/style&gt;</span>
</pre></div>


<p>If you open <code>rainbow.html</code> in a browser, you should now see
colorful blocks in place of the colorful text. Don&rsquo;t forget to commit the
changes:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">commit</span> <span class="na">-a</span> <span class="na">-m</span> <span class="s">&quot;Make a REAL rainbow&quot;</span>
</pre></div>


<p>The resulting project history is show below, with the first four commits omitted
for the sake of presentation.</p>

<figure>
	<img style='max-width: 470px' src='media/4-3.png' />
	<figcaption>Committing on the <code>crazy-alt</code> branch</figcaption>
</figure>


<h2>Emergency Update!</h2>

<p><em>Our boss called in with some breaking news!</em> He needs us to update
the site immediately, but what do we do with our <code>rainbow.html</code>
developments? Well, the beauty of Git branches is that we can just leave them
where they are and add the breaking news to <code>master</code>.</p>

<p>We&rsquo;ll use what&rsquo;s called a <strong>hotfix branch</strong> to
create and test the news updates.  In contrast to our relatively long-running
feature branch (<code>crazy</code>), hotfix branches are used to quickly patch
a production release. For example, you&rsquo;d use a hotfix branch to fix a
time-sensitive bug in a public software project. This distinction is useful for
demonstrating when it&rsquo;s appropriate to create a new branch, but it is
purely conceptual&mdash;a branch is a branch according to Git.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">branch</span> <span class="n">news-hotfix</span>
<span class="k">git</span> <span class="k">checkout</span> <span class="n">news-hotfix</span>
</pre></div>


<p>Change the &ldquo;News&rdquo; list in <code>index.html</code> to match the
following.</p>
	
<div class="highlight"><pre><span class="nt">&lt;h2</span> <span class="na">style=</span><span class="s">&quot;color: #C00&quot;</span><span class="nt">&gt;</span>News<span class="nt">&lt;/h2&gt;</span>
<span class="nt">&lt;ul&gt;</span>
  <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;news-1.html&quot;</span><span class="nt">&gt;</span>Blue Is The New Hue<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span>
</pre></div>


<p>And, create a new HTML page called <code>news-1.html</code> with the following
content.</p>
	
<div class="highlight"><pre><span class="cp">&lt;!DOCTYPE html&gt;</span>
<span class="nt">&lt;html</span> <span class="na">lang=</span><span class="s">&quot;en&quot;</span><span class="nt">&gt;</span>
<span class="nt">&lt;head&gt;</span>
  <span class="nt">&lt;title&gt;</span>Blue Is The New Hue<span class="nt">&lt;/title&gt;</span>
  <span class="nt">&lt;link</span> <span class="na">rel=</span><span class="s">&quot;stylesheet&quot;</span> <span class="na">href=</span><span class="s">&quot;style.css&quot;</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;meta</span> <span class="na">charset=</span><span class="s">&quot;utf-8&quot;</span> <span class="nt">/&gt;</span>
<span class="nt">&lt;/head&gt;</span>
<span class="nt">&lt;body&gt;</span>
  <span class="nt">&lt;h1</span> <span class="na">style=</span><span class="s">&quot;color: #079&quot;</span><span class="nt">&gt;</span>Blue Is The New Hue<span class="nt">&lt;/h1&gt;</span>
  <span class="nt">&lt;p&gt;</span>European designers have just announced that
  <span class="nt">&lt;span</span> <span class="na">style=</span><span class="s">&quot;color: #079&quot;</span><span class="nt">&gt;</span>Blue<span class="nt">&lt;/span&gt;</span> will be this year&#39;s
  hot color.<span class="nt">&lt;/p&gt;</span>
    
  <span class="nt">&lt;p&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;index.html&quot;</span><span class="nt">&gt;</span>Return to home page<span class="nt">&lt;/a&gt;&lt;/p&gt;</span>
<span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span>
</pre></div>

	
<p>We can&rsquo;t use <code>git commit -a</code> to automatically stage
<code>news-1.html</code> because it&rsquo;s an <em>untracked</em> file (as
shown in <code>git status</code>). So, let&rsquo;s use an explicit <code>git
add</code>:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">add</span> <span class="n">index.html</span> <span class="n">news-1.html</span>
<span class="k">git</span> <span class="k">status</span>
<span class="k">git</span> <span class="k">commit</span> <span class="na">-m</span> <span class="s">&quot;Add 1st news item&quot;</span>
</pre></div>


<p>Test these additions in a browser to make sure that the links work,
it&rsquo;s typo free, etc. If everything looks good, we can
&ldquo;publish&rdquo; the changes by merging them into the stable
<code>master</code> branch. Isolating this in a separate branch isn&rsquo;t
really necessary for our trivial example, but in the real world, this would
give you the opportunity to run build tests without touching your stable
project.</p>


<h2>Publish the News Hotfix</h2>

<p>Remember that to merge into the <code>master</code> branch, we first need to
check it out.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">merge</span> <span class="n">news-hotfix</span>
</pre></div>


<p>Since <code>master</code> now contains the news update, we can delete the hotfix
branch:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">branch</span> <span class="na">-d</span> <span class="n">news-hotfix</span>
<span class="k">git</span> <span class="k">branch</span>
</pre></div>


<p>The following diagram reflects our repository&rsquo;s history before and after the
merge. Can you figure out why was this a fast-forward merge instead of a 3-way
merge?</p>

<figure>
	<img style='max-width: 480px' src='media/4-4.png' />
	<figcaption>Fast-forwarding <code>master</code> to the
<code>news-hotfix</code> branch</figcaption>
</figure>

<p>Also notice that we have another fork in our history (the commit before
<code>master</code>
branches in two directions), which means we should expect to see another merge
commit in the near future.</p>


<h2>Complete the Crazy Experiment</h2>

<p>Ok, let&rsquo;s finish up our crazy experiment with one more commit.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">crazy</span>
</pre></div>


<p>Note that the news article is nowhere to be found, as should be expected
(this branch is a completely isolated development environment).</p>

<p>We&rsquo;ll finish up our crazy experiment by adding a news item for it on the home
page. Change the news list in <code>index.html</code> to the following:</p>

<div class="highlight"><pre><span class="nt">&lt;h2</span> <span class="na">style=</span><span class="s">&quot;color: #C00&quot;</span><span class="nt">&gt;</span>News<span class="nt">&lt;/h2&gt;</span>
<span class="nt">&lt;ul&gt;</span>
  <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;rainbow.html&quot;</span><span class="nt">&gt;</span>Our New Rainbow<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span>
</pre></div>


<p>Astute readers have probably observed that this directly conflicts with what
we changed in the <code>news-hotfix</code> branch. We should <em>not</em>
manually add in the other news item because it has no relationship with the
current branch. In addition, there would be no way to make sure the link works
because <code>news-1.html</code> doesn&rsquo;t exist in this branch. This may
seem trivial, but imagine the errors that could be introduced had
<code>news-hotfix</code> made dozens of different changes.</p>

<p>We&rsquo;ll simply stage and commit the snapshot as if there were no
conflicts:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">commit</span> <span class="na">-a</span> <span class="na">-m</span> <span class="s">&quot;Add news item for rainbow&quot;</span>
<span class="k">git</span> <span class="k">log</span> <span class="na">--oneline</span>
</pre></div>


<p>Look at all those experimental commits (marked with asterisks below)!</p>

<div class="highlight"><pre><span class="s">*42fa173</span> Add news item for rainbow
<span class="s">*7147cc5</span> Link index.html to rainbow.html
<span class="s">*6aa4b3b</span> Add CSS stylesheet to rainbow.html
<span class="s">b9ae1bc</span> Merge branch &#39;master&#39; into crazy
<span class="s">ae4e756</span> Link HTML pages to stylesheet
<span class="s">98cd46d</span> Add CSS stylesheet
<span class="s">*33e25c9</span> Rename crazy.html to rainbow.html
<span class="s">*677e0e0</span> Add a rainbow to crazy.html
<span class="s">506bb9b</span> Revert &quot;Add a crazzzy experiment&quot;
<span class="s">*514fbe7</span> Add a crazzzy experiment
<span class="s">1c310d2</span> Add navigation links
<span class="s">54650a3</span> Create blue and orange pages
<span class="s">b650e4b</span> Create index page
</pre></div>



<h2>Publish the Crazy Experiment</h2>

<p>We&rsquo;re finally ready to merge our <code>crazy</code> branch back into
<code>master</code>.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">merge</span> <span class="n">crazy</span>
</pre></div>


<p>You should get a message that reads:</p>

<div class="highlight"><pre>Auto-merging index.html
CONFLICT (content): Merge conflict in index.html
Automatic merge failed; fix conflicts and then commit the result.
</pre></div>


<p>This is our first <strong>merge conflict</strong>. Conflicts occur when we
try to merge branches that have edited the same content. Git doesn&rsquo;t know
how to combine the two changes, so it stops to ask us what to do. We can see
exactly what went wrong with the familiar <code>git status</code> command:</p>

<div class="highlight"><pre><span class="c">#</span> <span class="c">On branch </span><span class="cs">master</span>
<span class="c"># </span><span class="cs">Changes to be committed:</span>
<span class="c">#</span>
<span class="c">#       </span><span class="cs">new file:   rainbow.html</span>
<span class="c">#</span>
<span class="c"># </span><span class="cs">Unmerged paths:</span>
<span class="c">#   (use &quot;git add/rm &lt;file&gt;...&quot; as appropriate to mark resolution)</span>
<span class="c">#</span>
<span class="c">#       </span><span class="cs">both modified:      index.html</span>
<span class="c">#</span>
</pre></div>


<p>We&rsquo;re looking at the staged snapshot of a merge commit. We never saw
this with the first 3-way merge because we didn&rsquo;t have any conflicts to
resolve. But now, Git stopped to let us modify files and resolve the conflict
before committing the snapshot. The &ldquo;Unmerged paths&rdquo; section
contains files that have a conflict.</p>

<p>Open up <code>index.html</code> and find the section that looks like:</p>

<div class="highlight"><pre>&lt;&lt;&lt;&lt;&lt;&lt;&lt; HEAD
    &lt;li&gt;&lt;a href=&quot;news-1.html&quot;&gt;Blue Is The New Hue&lt;/a&gt;&lt;/li&gt;
=======
    &lt;li&gt;&lt;a href=&quot;rainbow.html&quot;&gt;Our New Rainbow&lt;/a&gt;&lt;/li&gt;
&gt;&gt;&gt;&gt;&gt;&gt;&gt; crazy
</pre></div>


<p>Git went ahead and modified the conflicted file to show us exactly which
lines are afflicted. The format of the above text shows us the difference
between the two versions of the file. The section labeled
<code>&lt;&lt;&lt;&lt;&lt;&lt;&lt; HEAD</code> shows us the version in the
current branch, while the part after the <code>=======</code> shows the version
in the <code>crazy</code> branch.</p>


<h2>Resolve the Merge Conflicts</h2>

<p>We can change the affected lines to whatever we want in order to resolve the
conflict. Edit the news section of <code>index.html</code> to keep changes from
both versions:</p>

<div class="highlight"><pre><span class="nt">&lt;h2</span> <span class="na">style=</span><span class="s">&quot;color: #C00&quot;</span><span class="nt">&gt;</span>News<span class="nt">&lt;/h2&gt;</span>
<span class="nt">&lt;ul&gt;</span>
  <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;news-1.html&quot;</span><span class="nt">&gt;</span>Blue Is The New Hue<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;rainbow.html&quot;</span><span class="nt">&gt;</span>Our New Rainbow<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span>
</pre></div>


<p>The <code>&lt;&lt;&lt;&lt;&lt;&lt;&lt;</code>, <code>=======</code>, and
<code>&gt;&gt;&gt;&gt;&gt;&gt;&gt;</code> markers are only used to show us the
conflict and should be deleted. Next, we need to tell Git that we&rsquo;re done
resolving the conflict:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">add</span> <span class="n">index.html</span>
<span class="k">git</span> <span class="k">status</span>
</pre></div>


<p>That&rsquo;s right, all you have to do is add <code>index.html</code> to the
staged snapshot to mark it as resolved. Finally, complete the 3-way merge:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">commit</span>
</pre></div>


<p>We didn&rsquo;t use the <code>-m</code> flag to specify a message because
Git already gives us a default message for merge commits. It also gives us a
&ldquo;Conflicts&rdquo; list, which can be particularly handy when trying to
figure out where something went wrong in a project. Save and close the file to
create the merge commit.</p>

<p>The final state of our project looks like the following.</p>

<figure>
	<img style='max-width: 520px' src='media/4-5.png' />
	<figcaption>Merging the <code>crazy</code> branch into
<code>master</code></figcaption>
</figure>


<h2>Cleanup the Feature Branches</h2>

<p>Since our crazy experiment has been successfully merged, we can get rid of our
feature branches.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">branch</span> <span class="na">-d</span> <span class="n">crazy</span>
<span class="k">git</span> <span class="k">branch</span> <span class="na">-d</span> <span class="n">crazy-alt</span>
</pre></div>


<p>As noted in the last module, the <code>git branch -d</code> command
won&rsquo;t let you delete a branch that contains unmerged changes. But, we
really do want to scrap the alternative experiment, so we&rsquo;ll follow the
error message&rsquo;s instructions for overriding this behavior:</p>
	
<div class="highlight"><pre><span class="k">git</span> <span class="k">branch</span> <span class="na">-D</span> <span class="n">crazy-alt</span>
</pre></div>


<p>Because we never merged <code>crazy-alt</code> into <code>master</code>, it
is lost forever. However, the <code>crazy</code> branch is still accessible
through its commits, which are now reachable via the <code>master</code>
branch. That is to say, it is still part of the structure of the
repository&rsquo;s history, even though we deleted our reference to it.</p>

<figure>
	<img style='max-width: 520px' src='media/4-6.png' />
	<figcaption>Deleting the feature branches</figcaption>
</figure>

<p>Fast-forward merges are <em>not</em> reflected in the project history. This
is the tangible distinction between fast-forward merges and 3-way merges. The
next module will discuss the appropriate usage of both and the potential
complications of a non-linear history.</p>


<h2>Conclusion</h2>

<p>This module demonstrated the three most common uses of Git branches:</p>

<ul>
	<li>To develop long-running features (<code>crazy</code>)</li>
	<li>To apply quick updates (<code>news-hotfix</code>)</li>
	<li>To record the evolution of a project (<code>master</code>)</li>
</ul>

<p>In the first two cases, we needed an <em>isolated</em> environment to test
some changes before integrating them with our stable code. As you get more
comfortable with Git, you should find yourself doing virtually everything in an
isolated topic branch, then merging it into a stable branch once you&rsquo;re
done. Practically, this means you&rsquo;ll never have a broken version of your
project.</p>

<p>We used the permanent <code>master</code> branch as the foundation for all
of these temporary branches, effectively making it the historian for our entire
project.  In addition to <code>master</code>, many programmers often add a
second permanent branch called <code>develop</code>. This lets them use
<code>master</code> to record <em>really</em> stable snapshots (e.g., public
releases) and use <code>develop</code> as more of a preparation area for
<code>master</code>.</p>

<p>This module also introduced the 3-way merge, which combines two branches using
a dedicated commit. The 3-way merge and the fast-forward merge are actually what
makes branching so powerful: they let developers share and integrate independent
updates with reliable results.</p>

<p>Next, we&rsquo;ll learn how to clean up our repository&rsquo;s history.
Using a new Git command, we&rsquo;ll be able to better manage merge commits and
make sure our history is easy to navigate.</p>


<h2>Quick Reference</h2>

<dl>
	<dt><code><span class="k">git</span> <span class="k">commit</span> <span class="na">-a</span> <span class="na">-m</span> <span class="s">&quot;&lt;message&gt;&quot;</span>
</code></dt>
	<dd>Stage all tracked files and commit the snapshot using the specified
	message.</dd>

	<dt><code><span class="k">git</span> <span class="k">branch</span> <span class="na">-D</span> <span class="n">&lt;branch-name&gt;</span>
</code></dt>
	<dd>Force the removal of an unmerged branch (<em>be careful</em>: it will be lost
    forever).</dd>
</dl>

<p class='sequential-nav'>
	<a href='rebasing.html'>Continue to <em>Rebasing</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>