<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Git Tutorial | Introduction | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="Git is a version control system (VCS) created for a
single task: managing changes to your files." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Git
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Introduction</h1>

<p>Git is a version control system (VCS) created for a single task: managing
changes to your files. It lets you track every change a software project goes
through, as well as where those changes came from. This makes Git an essential
tool for managing large projects, but it can also open up a vast array of
possibilities for your personal workflow.</p>


<h2>A Brief History of Revision Control</h2>

<p>We&rsquo;ll talk more about the core philosophy behind Git in a moment, but
first, let&rsquo;s step through the evolution of version control systems in
general.</p>

<h3>Files and Folders</h3>

<p>Before the advent of revision control software, there were only files and
folders. The only way to track revisions of a project was to copy the entire
project and give it a new name. Just think about how many times you&rsquo;ve
saved a &ldquo;backup&rdquo; called <code>my-term-paper-2.doc</code>. This is
the simplest form of version control.</p>

<figure>
	<img style='max-width: 310px' src='media/0-1.png' />
	<figcaption>Revision control with files and folders</figcaption>
</figure>

<p>But, it&rsquo;s easy to see how copying files from folder to folder could
prove disastrous for software developers. What happens if you mis-label a
folder? Or if you overwrite the wrong file? How would you even know that you
lost an important piece of code? It didn&rsquo;t take long for software
developers to realize they needed something more reliable.</p>

<h3>Local VCS</h3>

<p>So, developers began writing utility programs dedicated to managing file
revisions. Instead of keeping old versions as independent files, these new VCSs
stored them in a database. When you needed to look at an old version, you used
the VCS instead of accessing the file directly. That way, you would only have a
single &ldquo;checked out&rdquo; copy of the project at any given time,
eliminating the possibility of mixing up or losing revisions.</p>

<figure>
	<img style='max-width: 450px' src='media/0-2.png' />
	<figcaption>Local version control</figcaption>
</figure>

<p>At this point, versioning only took place on the developer&rsquo;s
<em>local</em> computer&mdash;there was no way to efficiently share code
amongst several programmers.</p>

<h3>Centralized VCS</h3>

<p>Enter the centralized version control system (CVCS). Instead of storing
project history on the developer&rsquo;s hard disk, these new CVCS programs
stored everything on a server. Developers checked out files and saved them back
into the project over a network. This setup let several programmers collaborate
on a project by giving them a single point of entry.</p>

<figure>
	<img style='max-width: 420px' src='media/0-3.png' />
	<figcaption>Centralized version control</figcaption>
</figure>

<p>While a big improvement on local VCS, centralized systems presented a new
set of problems: how do multiple users work on the same files at the same time?
Just imagine a scenario where two people fix the same bug and try to commit
their updates to the central server. Whose changes should be accepted?</p>

<p>CVCSs addressed this issue by preventing users from overriding others&rsquo;
work. If two changes conflicted, someone had to manually go in and merge the
differences. This solution worked for projects with relatively few updates
(which meant relatively few conflicts), but proved cumbersome for projects with
dozens of active contributors submitting several updates everyday: development
couldn&rsquo;t continue until all merge conflicts were resolved and made
available to the entire development team.</p>

<h3>Distributed VCS</h3>

<p>The next generation of revision control programs shifted away from the idea
of a single centralized repository, opting instead to give every developer
their own <em>local</em> copy of the entire project. The resulting
<em>distributed</em> network of repositories let each developer work in
isolation, much like a local VCS&mdash;but now the conflict resolution problem
of CVCS had a much more elegant solution.</p>

<figure>
	<img style='max-width: 360px' src='media/0-4.png' />
	<figcaption>Distributed version control</figcaption>
</figure>

<p>Since there was no longer a central repository, everyone could develop at
their own pace, store the updates locally, and put off merging conflicts until
their convenience. In addition, distributed version control systems (DVCS)
focused on efficient management for separate branches of development, which
made it much easier to share code, merge conflicts, and experiment with new
ideas.</p>

<p>The local nature of DVCSs also made development much faster, since you no
longer had to perform actions over a network. And, since each user had a
complete copy of the project, the risk of a server crash, a corrupted
repository, or any other type of data loss was much lower than that of their
CVCS predecessors.</p>


<h2>The Birth of Git</h2>

<p>And so, we arrive at Git, a distributed version control system created to
manage the Linux kernel. In 2005, the Linux community lost their free license
to the BitKeeper software, a commercial DVCS that they had been using since
2002. In response, Linus Torvalds advocated the development of a new
open-source DVCS as a replacement. This was the birth of Git.</p>

<p>As a source code manager for the entire Linux kernel, Git had several unique
constraints, including:</p>

<ul>
	<li>Reliability</li>
	<li>Efficient management of large projects</li>
	<li>Support for distributed development</li>
	<li>Support for non-linear development</li>
</ul>

<p>While other DVCSs did exist at the time (e.g., GNU&rsquo;s Arch or David
Roundy&rsquo;s Darcs), none of them could satisfy this combination of features.
Driven by these goals, Git has been under active development for several years
and now enjoys a great deal of stability, popularity, and community
involvement.</p>

<p>Git originated as a command-line program, but a variety of visual interfaces
have been released over the years. Graphical tools mask some of the complexity
behind Git and often make it easier to visualize the state of a repository, but
they still require a solid foundation in distributed version control. With this
in mind, we&rsquo;ll be sticking to the command-line interface, which is still
the most common way to interact with Git.</p>


<h2>Installation</h2>

<p>The upcoming modules will explore Git&rsquo;s features by applying commands
to real-world scenarios. But first, you&rsquo;ll need a working Git
installation to experiment with. Downloads for all supported platforms are
available via the <a href='http://git-scm.com'>official Git website</a>.</p>

<p>For Windows users, this will install a special command shell called <em>Git
Bash</em>. You should be using this shell instead of the native command prompt
to run Git commands. OS&nbsp;X and Linux users can access Git from a normal
shell. To test your installation, open a new command prompt and run <code>git
--version</code>. It should output something like <code>git version 1.7.10.2
(Apple Git-33)</code>.</p>


<h2>Get Ready!</h2>

<p>Remember that <em>Ry&rsquo;s Git Tutorial</em> is designed to
<em>demonstrate</em> Git&rsquo;s feature set, not just give you a superficial
overview of the most common commands. To get the most out of this tutorial,
it&rsquo;s important to actually execute the commands you&rsquo;re reading
about. So, make sure you&rsquo;re sitting in front of a computer, and
let&rsquo;s get to it!</p>

<p class='sequential-nav'>
	<a href='the-basics.html'>Continue to <em>The Basics</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>