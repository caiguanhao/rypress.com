<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Git Tutorial | The Basics | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="Using Git as a VCS is a lot like working with a normal
software project. In this module, we'll explore the fundamental
edit/stage/commit workflow." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Git
	Tutorial</em></a>
</p>

<h1 class='back-heading'>The Basics</h1>

<p>Now that you have a basic understanding of version control systems in
general, we can start experimenting with Git. Using Git as a VCS is a lot like
working with a normal software project. You&rsquo;re still writing code in
files and storing those files in folders, only now you have access to a
plethora of Git commands to manipulate those files.</p>

<p>For example, if you want to revert to a previous version of your project,
all you have to do is run a simple Git command. This command will dive into
Git&rsquo;s internal database, figure out what your project looked like at the
desired state, and update all the existing files in your project folder (also
known as the <strong>working directory</strong>). From an external standpoint,
it will look like your project magically went back in time.</p>

<p>This module explores the fundamental Git workflow: creating a repository,
staging and committing snapshots, configuring options, and viewing the state of
a repository. It also introduces the HTML website that serves as the running
example for this entire tutorial. A very basic knowledge of HTML and CSS will
give you a deeper understanding of the purpose underlying various Git commands
but is not strictly required.</p>


<h2>Create the Example Site</h2>

<p>Before we can execute any Git commands, we need to create the example
project. Create a new folder called <code>my-git-repo</code> to store the
project, then add a file called <code>index.html</code> to it. Open
<code>index.html</code> in your favorite text editor and add the following
HTML.</p>

<div class="highlight"><pre><span class="cp">&lt;!DOCTYPE html&gt;</span>
<span class="nt">&lt;html</span> <span class="na">lang=</span><span class="s">&quot;en&quot;</span><span class="nt">&gt;</span>
<span class="nt">&lt;head&gt;</span>
  <span class="nt">&lt;title&gt;</span>A Colorful Website<span class="nt">&lt;/title&gt;</span>
  <span class="nt">&lt;meta</span> <span class="na">charset=</span><span class="s">&quot;utf-8&quot;</span> <span class="nt">/&gt;</span>
<span class="nt">&lt;/head&gt;</span>
<span class="nt">&lt;body&gt;</span>
  <span class="nt">&lt;h1</span> <span class="na">style=</span><span class="s">&quot;color: #07F&quot;</span><span class="nt">&gt;</span>A Colorful Website<span class="nt">&lt;/h1&gt;</span>
  <span class="nt">&lt;p&gt;</span>This is a website about color!<span class="nt">&lt;/p&gt;</span>    
  
  <span class="nt">&lt;h2</span> <span class="na">style=</span><span class="s">&quot;color: #C00&quot;</span><span class="nt">&gt;</span>News<span class="nt">&lt;/h2&gt;</span>
  <span class="nt">&lt;ul&gt;</span>
    <span class="nt">&lt;li&gt;</span>Nothing going on (yet)<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;/ul&gt;</span>
<span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span>
</pre></div>


<p>Save the file when you&rsquo;re done. This serves as the foundation of our
example project. Feel free to open the <code>index.html</code> in a web browser
to see what kind of website it translates to. It&rsquo;s not exactly pretty,
but it serves our purposes.</p>


<h2>Initialize the Git Repository</h2>

<p>Now, we&rsquo;re ready to create our first Git repository. Open a command
prompt (or Git Bash for Windows users) and navigate to the project directory by
executing:</p> 

<div class="highlight"><pre><span class="k">cd</span> <span class="n">/path/to/my-git-repo</span>
</pre></div>


<p>where <code>/path/to/my-git-repo</code> is a path to the folder created in
the previous step. For example, if you created <code>my-git-repo</code> on your
desktop, you would execute:</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">~/Desktop/my-git-repo</span>
</pre></div>


<p>Next, run the following command to turn the directory into a Git
repository.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">init</span>
</pre></div>


<p>This initializes the repository, which enables the rest of Git&rsquo;s
powerful features. Notice that there is now a <code>.git</code> directory in
<code>my-git-repo</code> that stores all the tracking data for our repository
(you may need to enable hidden files to view this folder). The
<code>.git</code> folder is the only difference between a Git repository and an
ordinary folder, so deleting it will turn your project back into an unversioned
collection of files.</p>


<h2>View the Repository Status</h2>

<p>Before we try to start creating revisions, it would be helpful to view the
status of our new repository. Execute the following in your command prompt.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">status</span>
</pre></div>


<p>This should output something like:</p>

<div class="highlight"><pre><span class="c">#</span> <span class="c">On branch </span><span class="cs">master</span>
<span class="c">#</span>
<span class="c"># Initial commit</span>
<span class="c">#</span>
<span class="c"># </span><span class="cs">Untracked files:</span>
<span class="c">#   (use &quot;git add &lt;file&gt;...&quot; to include in what will be committed)</span>
<span class="c">#</span>
<span class="c">#       </span><span class="cs">index.html</span>
nothing added to commit but untracked files present (use &quot;git add&quot; to track)
</pre></div>



<p>Ignoring the <code>On branch master</code> portion for the time being, this
status message tells us that we&rsquo;re on our initial commit and that we have
nothing to commit but &ldquo;untracked files.&rdquo;</p>

<p>An <strong>untracked file</strong> is one that is not under version control.
Git doesn&rsquo;t automatically track files because there are often project
files that we don&rsquo;t want to keep under revision control. These include
binaries created by a C program, compiled Python modules (<code>.pyc</code>
files), and any other content that would unnecessarily bloat the repository. To
keep a project small and efficient, you should only track <em>source</em> files
and omit anything that can be <em>generated</em> from those files. This latter
content is part of the build process&mdash;not revision control.</p>


<h2>Stage a Snapshot</h2>

<p>So, we need to explicitly tell Git to add <code>index.html</code> to the
repository. The aptly named <code>git add</code> command tells Git to start
tracking <code>index.html</code>:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">add</span> <span class="n">index.html</span>
<span class="k">git</span> <span class="k">status</span>
</pre></div>


<p>In place of the &ldquo;Untracked files&rdquo; list, you should see the
following status.</p>

<div class="highlight"><pre><span class="c">#</span> <span class="cs">Changes to be committed:</span>
<span class="c">#   (use &quot;git rm --cached &lt;file&gt;...&quot; to unstage)</span>
<span class="c">#</span>
<span class="c">#       </span><span class="cs">new file:   index.html</span>
</pre></div>


<p>We&rsquo;ve just added <code>index.html</code> to the
<strong>snapshot</strong> for the next commit. A snapshot represents the state
of your project at a given point in time. In this case, we created a snapshot
with one file: <code>index.html</code>. If we ever told Git to revert to this
snapshot, it would replace the entire project folder with this one file,
containing the exact same HTML as it does right now.</p>

<p>Git&rsquo;s term for creating a snapshot is called <strong>staging</strong>
because we can add or remove multiple files before actually committing it to
the project history. Staging gives us the opportunity to group related changes
into distinct snapshots&mdash;a practice that makes it possible to track the
<em>meaningful</em> progression of a software project (instead of just
arbitrary lines of code).</p>


<h2>Commit the Snapshot</h2>

<p>We have staged a snapshot, but we still need to <strong>commit</strong> it
to the project history. The next command will open a text editor and prompt you
to enter a message for the commit.</p>
	
<div class="highlight"><pre><span class="k">git</span> <span class="k">commit</span>
</pre></div>


<p>Type <code>Create index page</code> for the message, leave the remaining
text, save the file, and exit the editor. You should see the message <code>1
files changed</code> among a mess of rather ambiguous output. This changed file
is our <code>index.html</code>.</p>

<p>As we just demonstrated, saving a version of your project is a two step
process:</p>

<ol>
	<li><strong>Staging.</strong> Telling Git what files to include in the next
	commit.</li>
	<li><strong>Committing.</strong> Recording the staged snapshot with a
	descriptive message.</li>
</ol>

<p>Staging files with the <code>git add</code> command doesn&rsquo;t actually
affect the repository in any significant way&mdash;it just lets us get our
files in order for the next commit. Only after executing <code>git
commit</code> will our snapshot be recorded in the repository. Committed
snapshots can be seen as &ldquo;safe&rdquo; versions of the project. Git will
never change them, which means you can do almost anything you want to your
project without losing those &ldquo;safe&rdquo; revisions. This is the
principal goal of any version control system.</p>

<figure>
	<img style='max-width: 520px' src='media/1-1.png' />
	<figcaption>The stage/commit process</figcaption>
</figure>


<h2>View the Repository History</h2>

<p>Note that <code>git status</code> now tells us that there is <code>nothing
to commit</code>, which means our current state matches what is stored in the
repository. The <code>git status</code> command will <em>only</em> show us
uncommitted changes&mdash;to view our project history, we need a new
command:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">log</span>
</pre></div>


<p>When you execute this command, Git will output information about our one and
only commit, which should look something like this:</p>

<div class="highlight"><pre><span class="k">commit</span> <span class="s">b650e4bd831aba05fa62d6f6d064e7ca02b5ee1b</span>
<span class="nl">Author:</span> <span class="s">unknown &lt;user@computer.(none)&gt;</span>
<span class="nl">Date:</span>   <span class="s">Wed Jan 11 00:45:10 2012 -0600</span>

    Create index page
</pre></div>


<p>Let&rsquo;s break this down. First, the commit is identified with a very
large, very random-looking string (<code>b650e4b...</code>). This is an SHA-1
checksum of the commit&rsquo;s contents, which ensures that the commit will
never be corrupted without Git knowing about it. All of your SHA-1 checksums
will be different than those displayed in this tutorial due to the different
dates and authors in your commits. In the next module, we&rsquo;ll see how a
checksum also serves as a unique ID for a commit.</p>

<p>Next, Git displays the author of the commit. But since we haven&rsquo;t told
Git our name yet, it just displays <code>unknown</code> with a generated
username. Git also outputs the date, time, and timezone (<code>-0600</code>) of
when the commit took place. Finally, we see the commit message that was entered
in the previous step.</p>


<h2>Configure Git</h2>

<p>Before committing any more snapshots, we should probably tell Git who we
are.  We can do this with the <code>git config</code> command:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">config</span> <span class="na">--global</span> <span class="n">user.name</span> <span class="s">&quot;Your Name&quot;</span>
<span class="k">git</span> <span class="k">config</span> <span class="na">--global</span> <span class="n">user.email</span> <span class="n">your.email@example.com</span>
</pre></div>


<p>Be sure to replace <code>Your Name</code> and
<code>your.email@example.com</code> with your actual name and email. The
<code>--global</code> flag tells Git to use this configuration as a default for
all of your repositories. Omitting it lets you specify different user
information for individual repositories, which will come in handy later on.</p>


<h2>Create New HTML Files</h2>

<p>Let&rsquo;s continue developing our website a bit. Start by creating a file
called <code>orange.html</code> with the following content.</p>

<div class="highlight"><pre><span class="cp">&lt;!DOCTYPE html&gt;</span>
<span class="nt">&lt;html</span> <span class="na">lang=</span><span class="s">&quot;en&quot;</span><span class="nt">&gt;</span>
<span class="nt">&lt;head&gt;</span>
  <span class="nt">&lt;title&gt;</span>The Orange Page<span class="nt">&lt;/title&gt;</span>
  <span class="nt">&lt;meta</span> <span class="na">charset=</span><span class="s">&quot;utf-8&quot;</span> <span class="nt">/&gt;</span>
<span class="nt">&lt;/head&gt;</span>
<span class="nt">&lt;body&gt;</span>
  <span class="nt">&lt;h1</span> <span class="na">style=</span><span class="s">&quot;color: #F90&quot;</span><span class="nt">&gt;</span>The Orange Page<span class="nt">&lt;/h1&gt;</span>
  <span class="nt">&lt;p&gt;</span>Orange is so great it has a
  <span class="nt">&lt;span</span> <span class="na">style=</span><span class="s">&quot;color: #F90&quot;</span><span class="nt">&gt;</span>fruit<span class="nt">&lt;/span&gt;</span> named after it.<span class="nt">&lt;/p&gt;</span>
<span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span>
</pre></div>


<p>Then, add a <code>blue.html</code> page:</p>

<div class="highlight"><pre><span class="cp">&lt;!DOCTYPE html&gt;</span>
<span class="nt">&lt;html</span> <span class="na">lang=</span><span class="s">&quot;en&quot;</span><span class="nt">&gt;</span>
<span class="nt">&lt;head&gt;</span>
  <span class="nt">&lt;title&gt;</span>The Blue Page<span class="nt">&lt;/title&gt;</span>
  <span class="nt">&lt;meta</span> <span class="na">charset=</span><span class="s">&quot;utf-8&quot;</span> <span class="nt">/&gt;</span>
<span class="nt">&lt;/head&gt;</span>
<span class="nt">&lt;body&gt;</span>
  <span class="nt">&lt;h1</span> <span class="na">style=</span><span class="s">&quot;color: #00F&quot;</span><span class="nt">&gt;</span>The Blue Page<span class="nt">&lt;/h1&gt;</span>
  <span class="nt">&lt;p&gt;</span>Blue is the color of the sky.<span class="nt">&lt;/p&gt;</span>
<span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span>
</pre></div>



<h2>Stage the New Files</h2>

<p>Next, we can stage the files the same way we created the first snapshot.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">add</span> <span class="n">orange.html</span> <span class="n">blue.html</span>
<span class="k">git</span> <span class="k">status</span>
</pre></div>


<p>Notice that we can pass more than one file to <code>git add</code>. After
adding the files, your status output should look like the following:</p>

<div class="highlight"><pre><span class="c">#</span> <span class="c">On branch </span><span class="cs">master</span>
<span class="c"># </span><span class="cs">Changes to be committed:</span>
<span class="c">#   (use &quot;git reset HEAD &lt;file&gt;...&quot; to unstage)</span>
<span class="c">#</span>
<span class="c">#       </span><span class="cs">new file:   blue.html</span>
<span class="c">#       </span><span class="cs">new file:   orange.html</span>
</pre></div>


<p>Try running <code>git log</code>. It only outputs the first commit, which
tells us that <code>blue.html</code> and <code>orange.html</code> have not yet
been added to the repository&rsquo;s history. Remember, we can see
<em>staged</em> changes with <code>git status</code>, but not with <code>git
log</code>. The latter is used only for <em>committed</em> changes.</p>

<figure>
	<img style='max-width: 440px' src='media/1-2.png' />
	<figcaption>Status output vs. Log output</figcaption>
</figure>

<h2>Commit the New Files</h2>

<p>Let&rsquo;s commit our staged snapshot:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">commit</span>
</pre></div>


<p>Use <code>Create blue and orange pages</code> as the commit message, then
save and close the file. Now, <code>git log</code> should output two commits,
the second of which reflects our name/email configuration. This project history
can be visualized as:</p>

<figure>
	<img style='max-width: 150px' src='media/1-3.png' />
	<figcaption>Current project history</figcaption>
</figure>

<p>Each circle represents a commit, the red circle designates the commit
we&rsquo;re currently viewing, and the arrow points to the preceding commit.
This last part may seem counterintuitive, but it reflects the underlying
relationship between commits (that is, a new commit refers to its parent
commit). You&rsquo;ll see this type of diagram many, many times throughout this
tutorial.</p>


<h2>Modify the HTML Pages</h2>

<p>The <code>git add</code> command we&rsquo;ve been using to stage
<em>new</em> files can also be used to stage <em>modified</em> files. Add the
following to the bottom of <code>index.html</code>, right before the closing
<code>&lt;/body&gt;</code> tag:</p>

<div class="highlight"><pre><span class="nt">&lt;h2&gt;</span>Navigation<span class="nt">&lt;/h2&gt;</span>
<span class="nt">&lt;ul&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: #F90&quot;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">&quot;orange.html&quot;</span><span class="nt">&gt;</span>The Orange Page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: #00F&quot;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">&quot;blue.html&quot;</span><span class="nt">&gt;</span>The Blue Page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span>
</pre></div>


<p>Next, add a home page link to the bottom of <code>orange.html</code> and
<code>blue.html</code> (again, before the <code>&lt;/body&gt;</code> line):</p>

<div class="highlight"><pre><span class="nt">&lt;p&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;index.html&quot;</span><span class="nt">&gt;</span>Return to home page<span class="nt">&lt;/a&gt;&lt;/p&gt;</span>
</pre></div>


<p>You can now navigate between pages when viewing them in a web browser.</p>


<h2>Stage and Commit the Snapshot</h2>

<p>Once again, we&rsquo;ll stage the modifications, then commit the
snapshot.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">status</span>
<span class="k">git</span> <span class="k">add</span> <span class="n">index.html</span> <span class="n">orange.html</span> <span class="n">blue.html</span>
<span class="k">git</span> <span class="k">status</span>
<span class="k">git</span> <span class="k">commit</span> <span class="na">-m</span> <span class="s">&quot;Add navigation links&quot;</span>
</pre></div>


<p>The <code>-m</code> option lets you specify a commit message on the command
line instead of opening a text editor. This is just a convenient shortcut (it
has the exact same effect as our previous commits).</p>

<p>Our history can now be represented as the following. Note that the red
circle, which represents the current commit, automatically moves forward every
time we commit a new snapshot.</p>

<figure>
	<img style='max-width: 200px' src='media/1-4.png' />
	<figcaption>Current project history</figcaption>
</figure>


<h2>Explore the Repository<span class='apo'>&rsquo;</span>s History</h2>

<p>The <code>git log</code> command comes with a lot of formatting options, a
few of which will be introduced throughout this tutorial. For now, we&rsquo;ll
just use the convenient <code>--oneline</code> flag:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">log</span> <span class="na">--oneline</span>
</pre></div>


<p>Condensing output to a single line is a great way to get a high-level
overview of a repository. Another useful configuration is to pass a filename to
<code>git log</code>:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">log</span> <span class="na">--oneline</span> <span class="n">blue.html</span>
</pre></div>


<p>This displays only the <code>blue.html</code> history. Notice that the
initial <code>Create index page</code> commit is missing, since
<code>blue.html</code> didn&rsquo;t exist in that snapshot.</p>


<h2>Conclusion</h2>

<p>In this module, we introduced the fundamental Git workflow: edit files,
stage a snapshot, and commit the snapshot. We also had some hands-on experience
with the components involved in this process:</p>

<figure>
	<img style='max-width: 520px' src='media/1-5.png' />
	<figcaption>The fundamental Git workflow</figcaption>
</figure>

<p>The distinction between the working directory, the staged snapshot, and
committed snapshots is at the very core of Git version control. Nearly all
other Git commands manipulate one of these components in some way, so
understanding the interplay between them is a fantastic foundation for
mastering Git.</p>

<p>The next module puts our existing project history to work by reverting to
previous snapshots. This is all you need to start using Git as a simple
versioning tool for your own projects.</p>


<h2>Quick Reference</h2>

<dl>
	<dt><code><span class="k">git</span> <span class="k">init</span>
</code></dt>
	<dd>Create a Git repository in the current folder.</dd>

	<dt><code><span class="k">git</span> <span class="k">status</span>
</code></dt>
	<dd>View the status of each file in a repository.</dd>

	<dt><code><span class="k">git</span> <span class="k">add</span> <span class="n">&lt;file&gt;</span>
</code></dt>
	<dd>Stage a file for the next commit.</dd>

	<dt><code><span class="k">git</span> <span class="k">commit</span>
</code></dt>
	<dd>Commit the staged files with a descriptive message.</dd>

	<dt><code><span class="k">git</span> <span class="k">log</span>
</code></dt>
	<dd>View a repository&rsquo;s commit history.</dd>

	<dt><code><span class="k">git</span> <span class="k">config</span> <span class="na">--global</span> <span class="n">user.name</span> <span class="s">&quot;&lt;name&gt;&quot;</span>
</code></dt>
	<dd>Define the author name to be used in all repositories.</dd>

	<dt><code><span class="k">git</span> <span class="k">config</span> <span class="na">--global</span> <span class="n">user.email</span> <span class="n">&lt;email&gt;</span>
</code></dt>
	<dd>Define the author email to be used in all repositories.</dd>
</dl>

<p class='sequential-nav'>
	<a href='undoing-changes.html'>Continue to <em>Undoing Changes</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>