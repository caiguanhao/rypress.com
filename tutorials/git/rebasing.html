<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Git Tutorial | Rebasing | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="Rebasing lets us move branches around by changing the
commit that they are based on. This helps keep the project history clean and
understandable." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Git
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Rebasing</h1>

<p>Let&rsquo;s start this module by taking an in-depth look at our history.
The six commits asterisked below are part of the same train of thought. We even
developed them in their own feature branch. However, they show up interspersed
with commits from other branches, along with a superfluous merge commit
(<code>b9ae1bc</code>). In other words, our repository&rsquo;s history is kind
of messy:</p>

<div class="highlight"><pre><span class="s">ec1b8cb</span> Merge branch &#39;crazy&#39;
<span class="s">*42fa173</span> Add news item for rainbow
<span class="s">3db88e1</span> Add 1st news item
<span class="s">*7147cc5</span> Link index.html to rainbow.html
<span class="s">*6aa4b3b</span> Add CSS stylesheet to rainbow.html
<span class="s">b9ae1bc</span> Merge branch &#39;master&#39; into crazy
<span class="s">ae4e756</span> Link HTML pages to stylesheet
<span class="s">98cd46d</span> Add CSS stylesheet
<span class="s">*33e25c9</span> Rename crazy.html to rainbow.html
<span class="s">*677e0e0</span> Add a rainbow to crazy.html
<span class="s">506bb9b</span> Revert &quot;Add a crazzzy experiment&quot;
<span class="s">*514fbe7</span> Add a crazzzy experiment
<span class="s">1c310d2</span> Add navigation links
<span class="s">54650a3</span> Create blue and orange pages
<span class="s">b650e4b</span> Create index page
</pre></div>


<p>Fortunately, Git includes a tool to help us clean up our commits: <code>git
rebase</code>. Rebasing lets us move branches around by changing the commit
that they are <em>based</em> on. Conceptually, this is what it allows us to
do:</p>

<figure>
	<img style='max-width: 440px' src='media/5-1.png' />
	<figcaption>Rebasing a feature branch onto <code>master</code></figcaption>
</figure>

<p>After rebasing, the <code>feature</code> branch has a new parent commit,
which is the same commit pointed to by <code>master</code>. Instead of joining
the branches with a merge commit, rebasing integrates the <code>feature</code>
branch by building <em>on top of</em> <code>master</code>. The result is a
perfectly linear history that reads more like a story than the hodgepodge of
unrelated edits shown above.</p>

<p>To explore Git&rsquo;s rebasing capabilities, we&rsquo;ll need to build up
our example project so that we have something to work with. Then, we&rsquo;ll
go back and rewrite history using <code>git rebase</code>.</p>

<div class='icon-text download-icon-text'>
	<img src='../../media/icons/download.png' style='max-width: 45px'/>

	<p><a href='media/repo-zips/rebasing.zip'>Download the repository
	for this module</a></p>

</div>

<div style='clear: both'></div>

<p>If you&rsquo;ve been following along from the previous module, you already
have everything you need. Otherwise, download the zipped Git repository from
the above link, uncompress it, and you&rsquo;re good to go.</p>


<h2>Create an About Section</h2>

<p>We&rsquo;ll begin by creating an about page for the website. Remember, we
should be doing all of our work in isolated branches so that we don&rsquo;t
cause any unintended changes to the stable version of the project.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">branch</span> <span class="n">about</span>
<span class="k">git</span> <span class="k">checkout</span> <span class="n">about</span>
</pre></div>


<p>The next few steps break this feature into several unnecessarily small
commits so that we can see the effects of a rebase. First, make a new directory
in <code>my-git-repo</code> called <code>about</code>. Then, create the empty
file <code>about/index.html</code>.  Stage and commit a snapshot.</p>
	
<div class="highlight"><pre><span class="k">git</span> <span class="k">add</span> <span class="n">about</span>
<span class="k">git</span> <span class="k">status</span>
<span class="k">git</span> <span class="k">commit</span> <span class="na">-m</span> <span class="s">&quot;Add empty page in about section&quot;</span>
</pre></div>


<p>Note that <code>git add</code> can also add entire directories to the
staging area.</p>


<h2>Add an About Page</h2>

<p>Next, we&rsquo;ll add some HTML to <code>about/index.html</code>:</p>

<div class="highlight"><pre><span class="cp">&lt;!DOCTYPE html&gt;</span>
<span class="nt">&lt;html</span> <span class="na">lang=</span><span class="s">&quot;en&quot;</span><span class="nt">&gt;</span>
<span class="nt">&lt;head&gt;</span>
  <span class="nt">&lt;title&gt;</span>About Us<span class="nt">&lt;/title&gt;</span>
  <span class="nt">&lt;link</span> <span class="na">rel=</span><span class="s">&quot;stylesheet&quot;</span> <span class="na">href=</span><span class="s">&quot;../style.css&quot;</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;meta</span> <span class="na">charset=</span><span class="s">&quot;utf-8&quot;</span> <span class="nt">/&gt;</span>
<span class="nt">&lt;/head&gt;</span>
<span class="nt">&lt;body&gt;</span>
  <span class="nt">&lt;h1&gt;</span>About Us<span class="nt">&lt;/h1&gt;</span>
  <span class="nt">&lt;p&gt;</span>We&#39;re a small, colorful website with just two employees:<span class="nt">&lt;/p&gt;</span>

  <span class="nt">&lt;ul&gt;</span>
    <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;me.html&quot;</span><span class="nt">&gt;</span>Me: The Developer<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;mary.html&quot;</span><span class="nt">&gt;</span>Mary: The Graphic Designer<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
  <span class="nt">&lt;/ul&gt;</span>
    
  <span class="nt">&lt;p&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;../index.html&quot;</span><span class="nt">&gt;</span>Return to home page<span class="nt">&lt;/a&gt;&lt;/p&gt;</span>
<span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span>
</pre></div>

	
<p>Stage and commit the snapshot.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">status</span>
<span class="k">git</span> <span class="k">commit</span> <span class="na">-a</span> <span class="na">-m</span> <span class="s">&quot;Add contents to about page&quot;</span>
</pre></div>


<p>After a few commits on this branch, our history looks like the
following.</p>

<figure>
	<img style='max-width: 360px' src='media/5-2.png' />
	<figcaption>Adding the <code>about</code> branch</figcaption>
</figure>


<h2>Another Emergency Update!</h2>

<p><em>Our boss just gave us some more breaking news!</em> Again, we&rsquo;ll
use a hotfix branch to update the site without affecting our about page
developments.  Make sure to base the updates on <code>master</code>, not the
<code>about</code> branch:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">branch</span> <span class="n">news-hotfix</span>
<span class="k">git</span> <span class="k">checkout</span> <span class="n">news-hotfix</span>
<span class="k">git</span> <span class="k">branch</span>
</pre></div>


<p>Change the &ldquo;News&rdquo; section in <code>index.html</code> to:</p>

<div class="highlight"><pre><span class="nt">&lt;h2</span> <span class="na">style=</span><span class="s">&quot;color: #C00&quot;</span><span class="nt">&gt;</span>News<span class="nt">&lt;/h2&gt;</span>
<span class="nt">&lt;ul&gt;</span>
  <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;news-1.html&quot;</span><span class="nt">&gt;</span>Blue Is The New Hue<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;rainbow.html&quot;</span><span class="nt">&gt;</span>Our New Rainbow<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;news-2.html&quot;</span><span class="nt">&gt;</span>A Red Rebellion<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span>
</pre></div>


<p>Commit a snapshot:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">status</span>
<span class="k">git</span> <span class="k">commit</span> <span class="na">-a</span> <span class="na">-m</span> <span class="s">&quot;Add 2nd news item to index page&quot;</span>
</pre></div>


<p>Then, create a new page called <code>news-2.html</code>:</p>

<div class="highlight"><pre><span class="cp">&lt;!DOCTYPE html&gt;</span>
<span class="nt">&lt;html</span> <span class="na">lang=</span><span class="s">&quot;en&quot;</span><span class="nt">&gt;</span>
<span class="nt">&lt;head&gt;</span>
  <span class="nt">&lt;title&gt;</span>A Red Rebellion<span class="nt">&lt;/title&gt;</span>
  <span class="nt">&lt;link</span> <span class="na">rel=</span><span class="s">&quot;stylesheet&quot;</span> <span class="na">href=</span><span class="s">&quot;style.css&quot;</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;meta</span> <span class="na">charset=</span><span class="s">&quot;utf-8&quot;</span> <span class="nt">/&gt;</span>
<span class="nt">&lt;/head&gt;</span>
<span class="nt">&lt;body&gt;</span>
  <span class="nt">&lt;h1</span> <span class="na">style=</span><span class="s">&quot;color: #C03&quot;</span><span class="nt">&gt;</span>A Red Rebellion<span class="nt">&lt;/h1&gt;</span>
    
  <span class="nt">&lt;p&gt;</span>Earlier today, several American design firms
  announced that they have completely rejected the use
  of blue in any commercial ventures. They have
  opted instead for <span class="nt">&lt;span</span> <span class="na">style=</span><span class="s">&quot;color: #C03&quot;</span><span class="nt">&gt;</span>Red<span class="nt">&lt;/span&gt;</span>.<span class="nt">&lt;/p&gt;</span>
    
  <span class="nt">&lt;p&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;index.html&quot;</span><span class="nt">&gt;</span>Return to home page<span class="nt">&lt;/a&gt;&lt;/p&gt;</span>
<span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span>
</pre></div>


<p>Stage and commit another snapshot:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">add</span> <span class="n">news-2.html</span>
<span class="k">git</span> <span class="k">status</span>
<span class="k">git</span> <span class="k">commit</span> <span class="na">-m</span> <span class="s">&quot;Add article for 2nd news item&quot;</span>
</pre></div>



<h2>Publish News Hotfix</h2>

<p>We&rsquo;re ready to merge the news update back into
<code>master</code>.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">merge</span> <span class="n">news-hotfix</span>
<span class="k">git</span> <span class="k">branch</span> <span class="na">-d</span> <span class="n">news-hotfix</span>
</pre></div>


<p>The <code>master</code> branch hasn&rsquo;t been altered since we created
<code>news-hotfix</code>, so Git can perform a fast-forward merge. Our
repository now looks like the following.</p>

<figure>
	<img style='max-width: 360px' src='media/5-3.png' />
	<figcaption>Fast-forwarding <code>master</code> to the
<code>news-hotfix</code></figcaption>
</figure>


<h2>Rebase the About Branch</h2>

<p>This puts us in the exact same position as we were in before our first 3-way
merge. We want to pull changes from <code>master</code> into a feature branch,
only this time we&rsquo;ll do it with a rebase instead of a merge.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">about</span>
<span class="k">git</span> <span class="k">rebase</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">log</span> <span class="na">--oneline</span>
</pre></div>


<p>Originally, the <code>about</code> branch was based on the <code>Merge
branch 'crazy-experiment'</code> commit. The rebase took the entire
<code>about</code> branch and plopped it onto the <em>tip</em> of the
<code>master</code> branch, which is visualized in the following diagram. Also
notice that, like the <code>git merge</code> command, <code>git rebase</code>
requires you to be on the branch that you want to move.</p>

<figure>
	<img style='max-width: 460px' src='media/5-4.png' />
	<figcaption>Rebasing the <code>about</code> branch onto
<code>master</code></figcaption>
</figure>

<p>After the rebase, <code>about</code> is a linear extension of the
<code>master</code> branch, enabling us to do a fast-forward merge later on.
Rebasing also allowed us to integrate the most up-to-date version of
<code>master</code> <em>without a merge commit</em>.</p>


<h2>Add a Personal Bio</h2>

<p>With our news hotfix out of the way, we can now continue work on our about
section. Create the file <code>about/me.html</code> with the following
contents:</p>

<div class="highlight"><pre><span class="cp">&lt;!DOCTYPE html&gt;</span>
<span class="nt">&lt;html</span> <span class="na">lang=</span><span class="s">&quot;en&quot;</span><span class="nt">&gt;</span>
<span class="nt">&lt;head&gt;</span>
  <span class="nt">&lt;title&gt;</span>About Me<span class="nt">&lt;/title&gt;</span>
  <span class="nt">&lt;link</span> <span class="na">rel=</span><span class="s">&quot;stylesheet&quot;</span> <span class="na">href=</span><span class="s">&quot;../style.css&quot;</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;meta</span> <span class="na">charset=</span><span class="s">&quot;utf-8&quot;</span> <span class="nt">/&gt;</span>
<span class="nt">&lt;/head&gt;</span>
<span class="nt">&lt;body&gt;</span>
  <span class="nt">&lt;h1&gt;</span>About Me<span class="nt">&lt;/h1&gt;</span>
  <span class="nt">&lt;p&gt;</span>I&#39;m a big nerd.<span class="nt">&lt;/p&gt;</span>

  <span class="nt">&lt;h2&gt;</span>Interests<span class="nt">&lt;/h2&gt;</span>
  <span class="nt">&lt;ul&gt;</span>
    <span class="nt">&lt;li&gt;</span>Computers<span class="nt">&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;</span>Mathematics<span class="nt">&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;</span>Typography<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;/ul&gt;</span>

  <span class="nt">&lt;p&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;index.html&quot;</span><span class="nt">&gt;</span>Return to about page<span class="nt">&lt;/a&gt;&lt;/p&gt;</span>
<span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span>
</pre></div>


<p>Then, commit the changes to the repository.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">add</span> <span class="n">about/me.html</span>
<span class="k">git</span> <span class="k">commit</span> <span class="na">-m</span> <span class="s">&quot;Add HTML page for personal bio&quot;</span>
<span class="k">git</span> <span class="k">log</span> <span class="na">--oneline</span>
</pre></div>


<p>Remember that thanks to the rebase, <code>about</code> rests on top of
<code>master</code>. So, all of our about section commits are grouped together,
which would not be the case had we merged instead of rebased. This also
eliminates an unnecessary fork in our project history.</p>


<h2>Add Dummy Page for Mary</h2>

<p>Once again, the next two snapshots are unnecessarily trivial. However,
we&rsquo;ll use an <em>interactive</em> rebase to combine them into a single commit
later on. That&rsquo;s right, <code>git rebase</code> not only lets you move
branches around, it enables you to manipulate individual commits as you do
so.</p>

<p>Create a new empty file in the about section:
<code>about/mary.html</code>.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">add</span> <span class="n">about</span>
<span class="k">git</span> <span class="k">status</span>
<span class="k">git</span> <span class="k">commit</span> <span class="na">-m</span> <span class="s">&quot;Add empty HTML page for Mary&#39;s bio&quot;</span>
</pre></div>



<h2>Link to the About Section</h2>

<p>Then, add a link to the about page in <code>index.html</code> so that its
&ldquo;Navigation&rdquo; section looks like the following.</p>
	
<div class="highlight"><pre><span class="nt">&lt;h2&gt;</span>Navigation<span class="nt">&lt;/h2&gt;</span>
<span class="nt">&lt;ul&gt;</span>
  <span class="nt">&lt;li&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">&quot;about/index.html&quot;</span><span class="nt">&gt;</span>About Us<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: #F90&quot;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">&quot;orange.html&quot;</span><span class="nt">&gt;</span>The Orange Page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: #00F&quot;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">&quot;blue.html&quot;</span><span class="nt">&gt;</span>The Blue Page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">&quot;rainbow.html&quot;</span><span class="nt">&gt;</span>The Rainbow Page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span>
</pre></div>


<p>Don&rsquo;t forget to commit the change:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">commit</span> <span class="na">-a</span> <span class="na">-m</span> <span class="s">&quot;Add link to about section in home page&quot;</span>
</pre></div>



<h2>Clean Up the Commit History</h2>

<p>Before we merge into the <code>master</code> branch, we should make sure we
have a clean, meaningful history in our feature branch. By rebasing
interactively, we can choose <em>how</em> each commit is transferred to the new
base. Specify an interactive rebase by passing the <code>-i</code> flag to the
rebase command:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">rebase</span> <span class="na">-i</span> <span class="n">master</span>
</pre></div>


<p>This should open up a text editor populated with all of the commits
introduced in the <code>about</code> branch, listed from oldest to newest. The
listing defines exactly how Git will transfer the commits to the new base.
Leaving it as is will do a normal <code>git rebase</code>, but if we move the
lines around, we can change the order in which commits are applied.</p>

<p>In addition, we can replace the <code>pick</code> command before each line
to edit it or combine it with other commits. All of the available commands are
shown in the comment section of the rebase listing, but right now, we only need
the <code>squash</code> command. This will condense our unnecessarily small
commits into a single, meaningful snapshot. Change your listing to match the
following:</p>

<div class="highlight"><pre><span class="k">pick</span> <span class="s">5cf316e</span> Add empty page in about section
<span class="k">squash</span> <span class="s">964e013</span> Add contents to about page
<span class="k">pick</span> <span class="s">89db9ab</span> Add HTML page for personal bio
<span class="k">squash</span> <span class="s">2bda8e5</span> Add empty HTML page for Mary&#39;s bio
<span class="k">pick</span> <span class="s">915466f</span> Add link to about section in home page
</pre></div>


<p>Then, begin the rebase by saving and closing the editor. The following list
describes the rebasing process in-depth and tells you what you need to change
along the way.</p>

<ol>

	<li>Git moves the <code>5cf316e</code> commit to the tip of
	<code>master</code>.</li>

	<li>Git combines the snapshots of <code>964e013</code> and
	<code>5cf316e</code>.</li>

	<li>Git stops to ask you what commit message to use for the combined
	snapshot. It automatically includes the messages of both commits, but you
	can delete that and simplify it to just <code>Create the about page</code>.
	Save and exit the text editor to continue.</li>

	<li>Git repeats this process for commits <code>89db9ab</code> and
	<code>2bda8e5</code>. Use <code>Begin creating bio pages</code> for the
	message.</li>

	<li>Git adds the final commit (<code>915466f</code>) on top of the commits
	created in the previous steps.</li>

</ol>

<p>You can see the result of all this activity with <code>git log
--oneline</code>, as well as in the diagram below. The five commits originally
in <code>about</code> have been condensed to three, and two of them have new
messages. Also notice that they all have different commit ID&rsquo;s. These new
ID&rsquo;s tell us that we didn&rsquo;t just <em>move</em> a couple of
commits&mdash;we&rsquo;ve literally rewritten our repository history with brand
new commits.</p>

<figure>
	<img style='max-width: 520px' src='media/5-5.png' />
	<figcaption>Results of the interactive rebase</figcaption>
</figure>

<p>Interactive rebasing gives you complete control over your project history,
but this can also be very dangerous. For example, if you were to delete a line
from the rebase listing, the associated commit wouldn&rsquo;t be transferred to
the new base, and its content would be lost forever. In a future module,
we&rsquo;ll also see how rewriting history can get you in trouble with public
Git repositories.</p>


<h2>Stop to Amend a Commit</h2>

<p>The previous rebase only stopped us to edit the <em>messages</em> of each
commit. We can take this one step further and alter a <em>snapshot</em> during
the rebase. Start by running another interactive rebasing session. Note that
we&rsquo;ve still been using <code>master</code> as the new base because it
selects the desired commits from the <code>about</code> branch.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">rebase</span> <span class="na">-i</span> <span class="n">master</span>
</pre></div>


<p>Specify the <code>edit</code> command for the second commit, as shown
below.</p>

<div class="highlight"><pre><span class="k">pick</span> <span class="s">58dec2a</span> Create the about page
<span class="k">edit</span> <span class="s">6ac8a9f</span> Begin creating bio pages
<span class="k">pick</span> <span class="s">51c958c</span> Add link to about section in home page
</pre></div>


<p>When Git starts to move the second commit to the new base, it will stop to
do some &ldquo;amending.&rdquo; This gives you the opportunity to alter the
staged snapshot before committing it.</p>

<figure>
	<img style='max-width: 430px' src='media/5-6.png' />
	<figcaption>Stopping to amend a commit</figcaption>
</figure>

<p>We&rsquo;ll leave a helpful note for Mary, whom we&rsquo;ll meet in the <a
href='remotes.html'>Remotes</a> module. Open up <code>about/mary.html</code>
and add the following.</p>

<div class="highlight"><pre>[Mary, please update your bio!]
</pre></div>


<p>We&rsquo;re currently between commits in a rebase, but we can alter the
staged snapshot in the exact same way as we have been throughout this entire
tutorial:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">add</span> <span class="n">about/mary.html</span>
<span class="k">git</span> <span class="k">status</span>
<span class="k">git</span> <span class="k">commit</span> <span class="na">--amend</span>
</pre></div>


<p>You can use the default message created by <code>git commit</code>. The new
<code>&#8209;&#8209;amend</code> flag tells Git to <em>replace</em> the existing commit
with the staged snapshot instead of creating a new one. This is also very
useful for fixing premature commits that often occur during normal
development.</p>


<h2>Continue the Interactive Rebase</h2>

<p>Remember that we&rsquo;re in the middle of a rebase, and Git still has one
more commit that it needs to re-apply. Tell Git that we&rsquo;re ready to move
on with the <code>--continue</code> flag:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">rebase</span> <span class="na">--continue</span>
<span class="k">git</span> <span class="k">log</span> <span class="na">--oneline</span>
</pre></div>


<p>Note that our history still appears to be the same (because we used the
default commit message above), but the <code>Begin creating bio pages</code>
commit contains different content than it did before the rebase, along with a
new ID.</p>

<p>If you ever find yourself lost in the middle of a rebase and you&rsquo;re
afraid to continue, you can use the <code>&#8209;&#8209;abort</code> flag to
abandon it and start over from scratch.</p>


<h2>Publish the About Section</h2>

<p>The point of all this interactive rebasing is to generate a
<em>meaningful</em> history that we can merge back into <code>master</code>.
And, since we&rsquo;ve rebased <code>about</code> onto the tip of
<code>master</code>, Git will be able to perform a fast-forward merge instead
of using a merge commit to join the two branches.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">log</span> <span class="na">--oneline</span>
<span class="k">git</span> <span class="k">merge</span> <span class="n">about</span>
<span class="k">git</span> <span class="k">log</span> <span class="na">--oneline</span>
</pre></div>


<p>Don&rsquo;t forget to delete the obsolete <code>about</code> branch.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">branch</span> <span class="na">-d</span> <span class="n">about</span>
</pre></div>


<p>Our final history is shown in the figure below. As you can see, a linear
history is much easier to comprehend than the back-and-forth merging of the
previous module. But on the other hand, we don&rsquo;t have the slightest
notion of <em>how</em> we got to our current state.</p>

<figure>
	<img style='max-width: 420px' src='media/5-7.png' />
	<figcaption>Merging and deleting the <code>about</code> branch</figcaption>
</figure>


<h2>Conclusion</h2>

<p>Rebasing enables fast-forward merges by moving a branch to the tip of
another branch. It effectively eliminates the need for merge commits, resulting
in a completely linear history. To an outside observer, it will seem as though
you created every part of your project in a neatly planned sequence, even
though you may have explored various alternatives or developed unrelated
features in parallel. Rebasing gives you the power to choose exactly what gets
stored in your repositories.</p>

<p>This can actually be a bit of a controversial topic within the Git
community. Some believe that the benefits discussed in this module
aren&rsquo;t worth the hassle of rewriting history. They take a more
&ldquo;pure&rdquo; approach to Git by saying that your history should reflect
<em>exactly</em> what you&rsquo;ve done, ensuring that no information is ever
lost. Furthermore, an advanced configuration of <code>git log</code> can
display a linear history from a highly-branched repository.</p>

<p>But, others contend that merge commits should be <em>meaningful</em>.
Instead of merging at arbitrary points just to access updates, they claim that
merge commits should represent a symbolic joining of two branches. In
particular, large software projects (such as the Linux kernel) typically
advocate interactive rebasing to keep the repository as clean and
straightforward as possible.</p>

<p>The use of <code>git rebase</code> is entirely up to you. Customizing the
evolution of your project can be very beneficial, but it might not be worth the
trouble when you can accomplish close to the same functionality using merges
exclusively. As a related note, you can use the following command to force a
merge commit when Git would normally do a fast-forward merge.</p>
	
<div class="highlight"><pre><span class="k">git</span> <span class="k">merge</span> <span class="na">--no-ff</span> <span class="n">&lt;branch-name&gt;</span>
</pre></div>


<p>The next module will get a little bit more involved in our project history.
We&rsquo;ll try fixing mistakes via complex rebases and even learn how to
recover deleted commits.</p>


<h2>Quick Reference</h2>

<dl>
	<dt><code><span class="k">git</span> <span class="k">rebase</span> <span class="n">&lt;new-base&gt;</span>
</code></dt>

	<dd>Move the current branch&rsquo;s commits to the tip of
	<code>&lt;new-base&gt;</code>, which can be either a branch name or a
	commit ID.</dd>

	<dt><code><span class="k">git</span> <span class="k">rebase</span> <span class="na">-i</span> <span class="n">&lt;new-base&gt;</span>
</code></dt>
	<dd>Perform an interactive rebase and select actions for each commit.</dd>

	<dt><code><span class="k">git</span> <span class="k">commit</span> <span class="na">--amend</span>
</code></dt>
	<dd>Add staged changes to the most recent commit instead of creating a new
	one.</dd>

	<dt><code><span class="k">git</span> <span class="k">rebase</span> <span class="na">--continue</span>
</code></dt>
	<dd>Continue a rebase after amending a commit.</dd>

	<dt><code><span class="k">git</span> <span class="k">rebase</span> <span class="na">--abort</span>
</code></dt>
	<dd>Abandon the current interactive rebase and return the repository to
	its former state.</dd>

	<dt><code><span class="k">git</span> <span class="k">merge</span> <span class="na">--no-ff</span> <span class="n">&lt;branch-name&gt;</span>
</code></dt>
	<dd>Force a merge commit even if Git could do a fast-forward merge.</dd>
</dl>

<p class='sequential-nav'>
	<a href='rewriting-history.html'>Continue to <em>Rewriting History</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>