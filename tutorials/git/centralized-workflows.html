<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Git Tutorial | Centralized Workflows | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="Larger software projects call for a more structured
environment. This module introduces one such environment: the centralized
workflow." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Git
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Centralized Workflows</h1>

<p>In the previous module, we shared information directly between two
developers&rsquo; repositories: <code>my-git-repo</code> and
<code>marys-repo</code>. This works for very small teams developing simple
programs, but larger projects call for a more structured environment. This
module introduces one such environment: the <strong>centralized
workflow</strong>.</p>

<p>We&rsquo;ll use a third Git repository to act as a central communication hub
between us and Mary. Instead of pulling changes into <code>my-git-repo</code>
from <code>marys-repo</code> and vice versa, we&rsquo;ll push to and fetch from
a dedicated storage repository. After this module, our workflow will look like
the following.</p>

<figure>
	<img style='max-width: 360px' src='media/8-1.png' />
	<figcaption>The centralized workflow</figcaption>
</figure>

<p>Typically, you would store the central repository on a server to allow
internet-based collaboration. Unfortunately, server configuration can vary among
hosting providers, making it hard to write universal step-by-step instructions.
So, we&rsquo;ll continue exploring remote repositories using our local filesystem,
just like in the previous module.</p>

<p>If you have access to a server, feel free to use it to host the central
repository that we&rsquo;re about to create. You&rsquo;ll have to provide SSH
paths to your server-based repository in place of the paths provided below, but
other than that, you can follow this module&rsquo;s instructions as you find
them. For everyone else, our network-based Git experience will begin in the
next module.</p>


<div class='icon-text download-icon-text'>
	<img src='../../media/icons/download.png' style='max-width: 45px'/>

	<p><a href='media/repo-zips/centralized-workflows.zip'>Download the
	repositories for this module</a></p>

</div>

<div style='clear: both'></div>

<p>If you&rsquo;ve been following along from the previous module, you already
have everything you need. Otherwise, download the zipped Git repositories from
the above link, uncompress them, and you&rsquo;re good to go.</p>


<h2>Create a Bare Repository (Central)</h2>

<p>First, let&rsquo;s create our central &ldquo;communication hub.&rdquo;
Again, make sure to change <code>/path/to/my-git-repo</code> to the actual path
to your repository. If you&rsquo;ve decided to host the central repository on
your server, you should SSH into it and run the <code>git init</code> command
wherever you&rsquo;d like to store the repository.</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">/path/to/my-git-repo</span>
<span class="k">cd</span> <span class="n">..</span>
<span class="k">git</span> <span class="k">init</span> <span class="na">--bare</span> <span class="n">central-repo.git</span>
</pre></div>


<p>As in the very first module, <code>git init</code> creates a new repository.
But this time, we used the <code>--bare</code> flag to tell Git that we
don&rsquo;t want a working directory. This will prevent us from developing in
the central repository, which eliminates the possibility of messing up another
user&rsquo;s environment with <code>git push</code>. A central repository is
only supposed to act as a <em>storage facility</em>&mdash;not a development
environment.</p>

<p>If you examine the contents of the resulting <code>central-repo.git</code>
folder, you&rsquo;ll notice that it contains the exact same files as the
<code>.git</code> folder in our <code>my-git-repo</code> project. Git has
<em>literally</em> gotten rid of our working directory. The conventional
<code>.git</code> extension in the directory name is a way to convey this
property.</p>


<h2>Update Remotes (Mary and You)</h2>

<p>We&rsquo;ve successfully set up a central repository that can be used to
share updates between us, Mary, and any other developers. Next, we should add
it as a remote to both <code>marys-repo</code> and
<code>my-git-repo</code>.</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">marys-repo</span>
<span class="k">git</span> <span class="k">remote</span> <span class="k">rm</span> <span class="n">origin</span>
<span class="k">git</span> <span class="k">remote</span> <span class="k">add</span> <span class="n">origin</span> <span class="n">../central-repo.git</span>
</pre></div>


<p>Now for our repository:</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">../my-git-repo</span>
<span class="k">git</span> <span class="k">remote</span> <span class="k">add</span> <span class="n">origin</span> <span class="n">../central-repo.git</span>
<span class="k">git</span> <span class="k">remote</span> <span class="k">rm</span> <span class="n">mary</span>
</pre></div>


<p>Note that we deleted the remote connections between Mary and our
<code>my-git-repo</code> folder with <code>git remote rm</code>. For the rest
of this module, we&rsquo;ll only use the central repository to share
updates.</p>

<p>If you decided to host the central repository on a server, you&rsquo;ll need
to change the <code>../central-repo.git</code> path to:
<code>ssh://user@example.com/path/to/central-repo.git</code>, substituting your
SSH username and server location for <code>user@example.com</code> and the
central repository&rsquo;s location for
<code>path/to/central-repo.git</code>.</p>


<h2>Push the Master Branch (You)</h2>

<p>We didn&rsquo;t <em>clone</em> the central repository&mdash;we just
initialized it as a bare repository. This means it doesn&rsquo;t have any of
our project history yet. We can fix that using the <code>git push</code>
command introduced in the last module.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">push</span> <span class="n">origin</span> <span class="n">master</span>
</pre></div>


<p>Our central repository now contains our entire <code>master</code> branch,
which we can double-check with the following.</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">../central-repo.git</span>
<span class="k">git</span> <span class="k">log</span>
</pre></div>


<p>This should output the familiar history listing of the <code>master</code>
branch.</p>

<p>Recall that <code>git push</code> creates <em>local</em> branches in the
destination repository.  We said it was dangerous to push to a friend&rsquo;s
repository, as they probably wouldn&rsquo;t appreciate new branches appearing
at random. However, it&rsquo;s safe to create local branches in
<code>central-repo.git</code> because it has no working directory, which means
it&rsquo;s impossible to disturb anyone&rsquo;s development.</p>


<h2>Add News Update (You)</h2>

<p>Let&rsquo;s see our new centralized collaboration workflow in action by
committing a few more snapshots.</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">../my-git-repo</span>
<span class="k">git</span> <span class="k">checkout</span> <span class="na">-b</span> <span class="n">news-item</span>
</pre></div>


<p>Create a file called <code>news-3.html</code> in <code>my-git-repo</code>
and add the following HTML.</p>

<div class="highlight"><pre><span class="cp">&lt;!DOCTYPE html&gt;</span>
<span class="nt">&lt;html</span> <span class="na">lang=</span><span class="s">&quot;en&quot;</span><span class="nt">&gt;</span>
<span class="nt">&lt;head&gt;</span>
  <span class="nt">&lt;title&gt;</span>Middle East&#39;s Silent Beast<span class="nt">&lt;/title&gt;</span>
  <span class="nt">&lt;link</span> <span class="na">rel=</span><span class="s">&quot;stylesheet&quot;</span> <span class="na">href=</span><span class="s">&quot;style.css&quot;</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;meta</span> <span class="na">charset=</span><span class="s">&quot;utf-8&quot;</span> <span class="nt">/&gt;</span>
<span class="nt">&lt;/head&gt;</span>
<span class="nt">&lt;body&gt;</span>
  <span class="nt">&lt;h1</span> <span class="na">style=</span><span class="s">&quot;color: #D90&quot;</span><span class="nt">&gt;</span>Middle East&#39;s Silent Beast<span class="nt">&lt;/h1&gt;</span>
  <span class="nt">&lt;p&gt;</span>Late yesterday evening, the Middle East&#39;s largest
  design house<span class="ni">&amp;mdash;</span>until now, silent on the West&#39;s colorful
  disagreement<span class="ni">&amp;mdash;</span>announced the adoption of
  <span class="nt">&lt;span</span> <span class="na">style=</span><span class="s">&quot;color: #D90&quot;</span><span class="nt">&gt;</span>Yellow<span class="nt">&lt;/span&gt;</span> as this year&#39;s
  color of choice.<span class="nt">&lt;/p&gt;</span>
    
  <span class="nt">&lt;p&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;index.html&quot;</span><span class="nt">&gt;</span>Return to home page<span class="nt">&lt;/a&gt;&lt;/p&gt;</span>
<span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span>
</pre></div>


<p>Next, add a link to the &ldquo;News&rdquo; section of
<code>index.html</code> so that it looks like:</p>

<div class="highlight"><pre><span class="nt">&lt;h2</span> <span class="na">style=</span><span class="s">&quot;color: #C00&quot;</span><span class="nt">&gt;</span>News<span class="nt">&lt;/h2&gt;</span>
<span class="nt">&lt;ul&gt;</span>
  <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;news-1.html&quot;</span><span class="nt">&gt;</span>Blue Is The New Hue<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;rainbow.html&quot;</span><span class="nt">&gt;</span>Our New Rainbow<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;news-2.html&quot;</span><span class="nt">&gt;</span>A Red Rebellion<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;news-3.html&quot;</span><span class="nt">&gt;</span>Middle East&#39;s Silent Beast<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span>
</pre></div>


<p>Stage and commit a snapshot.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">add</span> <span class="n">news-3.html</span> <span class="n">index.html</span>
<span class="k">git</span> <span class="k">status</span>
<span class="k">git</span> <span class="k">commit</span> <span class="na">-m</span> <span class="s">&quot;Add 3rd news item&quot;</span>
</pre></div>



<h2>Publish the News Item (You)</h2>

<p>Previously, &ldquo;publishing&rdquo; meant merging with the local
<code>master</code> branch. But since we&rsquo;re <em>only</em> interacting
with the central repository, our <code>master</code> branch is private again.
There&rsquo;s no chance of Mary pulling content directly from our
repository.</p>

<p>Instead, everyone accesses updates through the <em>public</em>
<code>master</code> branch, so &ldquo;publishing&rdquo; means pushing to the
central repository.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">merge</span> <span class="n">news-item</span>
<span class="k">git</span> <span class="k">branch</span> <span class="na">-d</span> <span class="n">news-item</span>
<span class="k">git</span> <span class="k">push</span> <span class="n">origin</span> <span class="n">master</span>
</pre></div>


<p>After merging into <code>master</code> as we normally would, <code>git
push</code> updates the central repository&rsquo;s <code>master</code> branch
to reflect our local <code>master</code>. From our perspective, the push can be
visualized as the following:</p>

<figure>
	<img style='max-width: 360px' src='media/8-2.png' />
	<figcaption>Pushing <code>master</code> to the central repository</figcaption>
</figure>

<p>Note that this accomplishes the exact same thing as going into the central
repository and doing a fetch/fast-forward merge, except <code>git push</code>
allows us to do everything from inside <code>my-git-repo</code>. We&rsquo;ll
see some other convenient features of this command later in the module.</p>


<h2>Update CSS Styles (Mary)</h2>

<p>Next, let&rsquo;s pretend to be Mary again and add some CSS formatting (she
is our graphic designer, after all).</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">../marys-repo</span>
<span class="k">git</span> <span class="k">checkout</span> <span class="na">-b</span> <span class="n">css-edits</span>
</pre></div>


<p>Add the following to the end of <code>style.css</code>:</p>

<div class="highlight"><pre><span class="nt">h1</span> <span class="p">{</span>
  <span class="k">font-size</span><span class="o">:</span> <span class="m">32px</span><span class="p">;</span>
<span class="p">}</span>

<span class="nt">h2</span> <span class="p">{</span>
  <span class="k">font-size</span><span class="o">:</span> <span class="m">24px</span><span class="p">;</span>
<span class="p">}</span>

<span class="nt">a</span><span class="nd">:link</span><span class="o">,</span> <span class="nt">a</span><span class="nd">:visited</span> <span class="p">{</span>
  <span class="k">color</span><span class="o">:</span> <span class="m">#03C</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>And, stage and commit a snapshot.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">commit</span> <span class="na">-a</span> <span class="na">-m</span> <span class="s">&quot;Add CSS styles for headings and links&quot;</span>
</pre></div>



<h2>Update Another CSS Style (Mary)</h2>

<p>Oops, Mary forgot to add some formatting. Append the <code>h3</code> styling
to <code>style.css</code>:</p>

<div class="highlight"><pre><span class="nt">h3</span> <span class="p">{</span>
  <span class="k">font-size</span><span class="o">:</span> <span class="m">18px</span><span class="p">;</span>
  <span class="k">margin-left</span><span class="o">:</span> <span class="m">20px</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>


<p>And of course, stage and commit the updates.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">commit</span> <span class="na">-a</span> <span class="na">-m</span> <span class="s">&quot;Add CSS styles for 3rd level headings&quot;</span>
</pre></div>



<h2>Clean Up Before Publishing (Mary)</h2>

<p>Before Mary considers pushing her updates to the central repository, she
needs to make sure she has a clean history. This <em>must</em> be done by Mary,
because it&rsquo;s near-impossible to change history after it has been made
public.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">rebase</span> <span class="na">-i</span> <span class="n">master</span>
</pre></div>


<p>This highlights another benefit of using isolated branches to develop
independent features. Mary doesn&rsquo;t need to go back and figure out what changes
need to be rebased, since they all reside in her current branch. Change the
rebase configuration to:</p>

<div class="highlight"><pre><span class="k">pick</span> <span class="s">681bd1c</span> Add CSS styles for headings and links
<span class="k">squash</span> <span class="s">eabac68</span> Add CSS styles for 3rd level headings
</pre></div>


<p>When Git stops to ask for the combined commit message, just use the first
commit&rsquo;s message:</p>
	
<div class="highlight"><pre>Add CSS styles for headings and links
</pre></div>


<p>Consider what would have happened had Mary rebased <em>after</em> pushing to
the central repository. She would be re-writing commits that other developers
may have already pulled into their project. To Git, Mary&rsquo;s re-written
commits look like entirely new commits (since they have different ID&rsquo;s).
This situation is shown below.</p>

<figure>
	<img style='max-width: 440px' src='media/8-3.png' />
	<figcaption>Squashing a public commit</figcaption>
</figure>

<p>The commits labeled <em>1</em> and <em>2</em> are the public commits that
Mary would be rebasing. Afterwards, the public history is still the exact same
as Mary&rsquo;s original history, but now her local <code>master</code> branch
has diverged from <code>origin/master</code>&mdash;even though they represent
the same snapshot.</p>

<p>So, to publish her rebased <code>master</code> branch to the central
repository, Mary would have to merge with <code>origin/master</code>. This
cannot be a fast-forward merge, and the resulting merge commit is likely to
confuse her collaborators and disrupt their workflow.</p>

<p>This brings us to the most important rule to remember while rebasing:
<strong>Never, ever rebase commits that have been pushed to a shared
repository</strong>.</p>

<p>If you need to change a public commit, use the <code>git revert</code>
command that we discussed in <a href='undoing-changes.html'>Undoing
Changes</a>. This creates a new commit with the required modifications instead
of re-writing old snapshots.</p>


<h2>Publish CSS Changes (Mary)</h2>

<p>Now that her history is cleaned up, Mary can publish the changes.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">merge</span> <span class="n">css-edits</span>
<span class="k">git</span> <span class="k">branch</span> <span class="na">-d</span> <span class="n">css-edits</span>
</pre></div>


<p>She shouldn&rsquo;t push the <code>css-edits</code> branch to the server,
since it&rsquo;s no longer under development, and other collaborators
wouldn&rsquo;t know what it contains.  However, if we had all decided to
develop the CSS edits together and wanted an isolated environment to do so, it
would make sense to publish it as an independent branch.</p>

<p>Mary still needs to push the changes to the central repository. But first,
let&rsquo;s take a look at the state of everyone&rsquo;s project.</p>

<figure>
	<img style='max-width: 460px' src='media/8-4.png' />
	<figcaption>Before publishing Mary&rsquo;s CSS changes</figcaption>
</figure>

<p>You might be wondering how Mary can push her local <code>master</code> up to
the central repository, since it has progressed since Mary last fetched from
it. This is a common situation when many developers are working on a project
simultaneously. Let&rsquo;s see how Git handles it:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">push</span> <span class="n">origin</span> <span class="n">master</span>
</pre></div>


<p>This will output a verbose rejection message. It seems that Git won&rsquo;t
let anyone push to a remote server if it doesn&rsquo;t result in a fast-forward
merge. This prevents us from losing the <code>Add 3rd news item</code> commit
that would need to be overwritten for <code>origin/master</code> to match
<code>mary/master</code>.</p>


<h2>Pull in Changes (Mary)</h2>

<p>Mary can solve this problem by pulling in the central changes before trying
to push her CSS changes. First, she needs the most up-to-date version of the
<code>origin/master</code> branch.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">fetch</span> <span class="n">origin</span>
</pre></div>


<p>Remember that Mary can see what&rsquo;s in <code>origin/master</code> and
not in the local <code>master</code> using the <code>..</code> syntax:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">log</span> <span class="n">master..origin/master</span>
</pre></div>


<p>And she can also see what&rsquo;s in her <code>master</code> that&rsquo;s
not in <code>origin/master</code>:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">log</span> <span class="n">origin/master..master</span>
</pre></div>


<p>Since both of these output a commit, we can tell that Mary&rsquo;s history
diverged. This should also be clear from the diagram below, which shows the
updated <code>origin/master</code> branch.</p>

<figure>
	<img style='max-width: 460px' src='media/8-5.png' />
	<figcaption>Fetching the central repository&rsquo;s
<code>master</code> branch</figcaption>
</figure>

<p>Mary is now in the familiar position of having to pull in changes from
another branch. She can either merge, which <em>cannot</em> be fast-forwarded,
or she can rebase for a linear history.</p>

<p>Typically, you&rsquo;ll want to rebase your changes on top of those found in your
central repository. This is the equivalent of saying, &ldquo;I want to add my changes
to what everyone else has already done.&rdquo; As previously discussed, rebasing also
eliminates superfluous merge commits. For these reasons, Mary will opt for a
rebase.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">rebase</span> <span class="n">origin/master</span>
<span class="k">git</span> <span class="k">push</span> <span class="n">origin</span> <span class="n">master</span>
</pre></div>


<p>After the rebase, Mary&rsquo;s <code>master</code> branch contains
everything from the central repository, so she can do a fast-forward push to
publish her changes.</p>

<figure>
	<img style='max-width: 400px' src='media/8-6.png' />
	<figcaption>Updating the central repository&rsquo;s
<code>master</code></figcaption>
</figure>


<h2>Pull in Changes (You)</h2>

<p>Finally, we&rsquo;ll switch back to our repository and pull in Mary&rsquo;s
CSS formatting.</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">../my-git-repo</span>
<span class="k">git</span> <span class="k">fetch</span> <span class="n">origin</span>
<span class="k">git</span> <span class="k">log</span> <span class="n">master..origin/master</span> <span class="na">--stat</span>
<span class="k">git</span> <span class="k">log</span> <span class="n">origin/master..master</span> <span class="na">--stat</span>
</pre></div>


<p>Of course, the second log command won&rsquo;t output anything, since we
haven&rsquo;t added any new commits while Mary was adding her CSS edits.
It&rsquo;s usually a good idea to check this before trying to merge in a remote
branch. Otherwise, you might end up with some extra merge commits when you
thought you were fast-forwarding your branch.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">merge</span> <span class="n">origin/master</span>
</pre></div>


<p>Our repository is now synchronized with the central repository. Note that
Mary may have moved on and added some new content that we don&rsquo;t know
about, but it doesn&rsquo;t matter. The only changes we need to know about are
those in <code>central-repo.git</code>. While this doesn&rsquo;t make a huge
difference when we&rsquo;re working with just one other developer, imagine
having to keep track of a dozen different developers&rsquo; repositories in
real-time. This kind of chaos is precisely the problem a centralized
collaboration workflow is designed to solve:</p>

<figure>
	<img style='max-width: 520px' src='media/8-7.png' />
	<figcaption>The centralized workflow with many developers</figcaption>
</figure>

<p>The presence of a central communication hub condenses all this development
into a single repository and ensures that no one overwrites another&rsquo;s
content, as we discovered while trying to push Mary&rsquo;s CSS updates.</p>


<h2>Conclusion</h2>

<p>In this module, we introduced another remote repository to serve as the
central storage facility for our project. We also discovered bare repositories,
which are just like ordinary repositories&mdash;minus the working directory.
Bare repositories provide a &ldquo;safe&rdquo; location to push branches to, as
long as you remember not to rebase the commits that it already contains.</p>

<p>We hosted the central repository on our local filesystem, right next to both
ours and Mary&rsquo;s projects. However, most real-world central repositories reside
on a remote server with internet access. This lets any developer fetch from or
push to the repository over the internet, making Git a very powerful multi-user
development platform. Having the central repository on a remote server is also
an affordable, convenient way to back up a project.</p>

<p>Next up, we&rsquo;ll configure a network-based repository using a service called
GitHub. In addition to introducing network access for Git repositories, this
will open the door for another collaboration standard: the integrator
workflow.</p>


<h2>Quick Reference</h2>

<dl>
	<dt><code><span class="k">git</span> <span class="k">init</span> <span class="na">--bare</span> <span class="n">&lt;repository-name&gt;</span>
</code></dt>
	<dd>Create a Git repository, but omit the working directory.</dd>

	<dt><code><span class="k">git</span> <span class="k">remote</span> <span class="k">rm</span> <span class="n">&lt;remote-name&gt;</span>
</code></dt>
	<dd>Remove the specified remote from your bookmarked connections.</dd>
</dl>

<p class='sequential-nav'>
	<a href='distributed-workflows.html'>Continue to <em>Distributed Workflows</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>