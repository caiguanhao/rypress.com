<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Git Tutorial | Patch Workflows | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="Git's patch workflow lets you communicate directly on
the commit level by representing commits as plaintext files." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Git
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Patch Workflows</h1>

<p>Thus far, all of the collaboration workflows we&rsquo;ve seen rely heavily
on branches. For example, in the last module, a contributor had to publish an
entire <em>branch</em> for us to merge into our project. However, it&rsquo;s
also possible to communicate directly on the <em>commit</em> level using a
<strong><em>patch</em></strong>.</p>

<p>A patch file represents a single set of changes (i.e., a commit) that can be
applied to any branch, in any order. In this sense, the patch workflow is akin
to interactive rebasing, except you can easily share patches with other
developers. This kind of communication lessens the importance of a
project&rsquo;s branch structure and gives complete control to the project
maintainer (at least with regards to incorporating contributions).</p>

<p>Integrating on the commit level will also give us a deeper understanding of
how a Git repository records project history.</p>

<div class='icon-text download-icon-text'>
	<img src='../../media/icons/download.png' style='max-width: 45px'/>

	<p><a href='media/repo-zips/patch-workflows.zip'>Download the
	repositories for this module</a></p>

</div>

<div style='clear: both'></div>

<p>If you&rsquo;ve been following along from the previous module, you already
have everything you need. Otherwise, download the zipped Git repositories from
the above link, uncompress them, and you&rsquo;re good to go. If you&rsquo;ve
set up a Bitbucket account, you should also run the following commands to
configure the downloaded repositories:</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">/path/to/my-git-repo</span>
<span class="k">git</span> <span class="k">remote</span> <span class="k">add</span> <span class="n">origin</span> <span class="n">https://&lt;username&gt;@bitbucket.org/&lt;username&gt;/my-git-repo.git</span>
<span class="k">cd</span> <span class="n">../marys-repo</span>
<span class="k">git</span> <span class="k">remote</span> <span class="k">add</span> <span class="n">origin</span> <span class="n">http://bitbucket.org/&lt;username&gt;/my-git-repo.git</span>
</pre></div>



<h2>Change the Pink Page (Mary)</h2>

<p>We&rsquo;ll begin by pretending to be Mary again. Mary didn&rsquo;t like the
pink page that John contributed and wants to change it.</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">/path/to/marys-repo</span>
<span class="k">git</span> <span class="k">checkout</span> <span class="na">-b</span> <span class="n">pink-page</span>
</pre></div>


<p>Developing in a new branch not only gives Mary an isolated environment, it
also makes it easier to create a series of patches once she&rsquo;s done
editing the pink page. Find these lines in <code>pink.html</code>:</p>

<div class="highlight"><pre><span class="nt">&lt;p&gt;</span>Pink is <span class="nt">&lt;span</span> <span class="na">style=</span><span class="s">&quot;color: #F0F&quot;</span><span class="nt">&gt;</span>girly,
flirty and fun<span class="nt">&lt;/span&gt;</span>!<span class="nt">&lt;/p&gt;</span>
</pre></div>


<p>and change them to the following.</p>

<div class="highlight"><pre><span class="nt">&lt;p&gt;</span>Only <span class="nt">&lt;span</span> <span class="na">style=</span><span class="s">&quot;color: #F0F&quot;</span><span class="nt">&gt;</span>real men<span class="nt">&lt;/span&gt;</span> wear pink!<span class="nt">&lt;/p&gt;</span>
</pre></div>


<p>Stage and commit the update as normal.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">commit</span> <span class="na">-a</span> <span class="na">-m</span> <span class="s">&quot;Change pink to a manly color&quot;</span>
</pre></div>


<p>Note that Mary&rsquo;s local development process doesn&rsquo;t change at
all. Patches&mdash;like the centralized and integrator workflows&mdash;are
merely a way to share changes amongst developers. It has little effect on the
core Git concepts introduced in the first portion of this tutorial.</p>


<h2>Create a Patch (Mary)</h2>

<p>Mary can create a patch from the new commit using the
<code>git&nbsp;format&#8209;patch</code> command.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">format-patch</span> <span class="n">master</span>
</pre></div>


<p>This creates a file called
<code>0001-Change-pink-to-a-manly-color.patch</code> that contains enough
information to re-create the commit from the last step. The <code>master</code>
parameter tells Git to generate patches for every commit in the current branch
that&rsquo;s missing from <code>master</code>.</p>

<p>Open up the patch file in a text editor. As shown by the addresses in the top of
the file, it&rsquo;s actually a complete email. This makes it incredibly easy to send
patches to other developer. Further down, you should see the following.</p>

<div class="highlight"><pre><span class="gh">index 98e10a1..828dd1a 100644</span>
<span class="gd">--- a/pink.html</span>
<span class="gi">+++ b/pink.html</span>
<span class="gu">@@ -7,8 +7,7 @@</span>
 &lt;/head&gt;
 &lt;body&gt;
   &lt;h1 style=&quot;color: #F0F&quot;&gt;The Pink Page&lt;/h1&gt;
<span class="gd">-  &lt;p&gt;Pink is &lt;span style=&quot;color: #F0F&quot;&gt;girly,</span>
<span class="gd">-  flirty and fun&lt;/span&gt;!&lt;/p&gt;</span>
<span class="gi">+  &lt;p&gt;Only &lt;span style=&quot;color: #F0F&quot;&gt;real men&lt;/span&gt; wear pink!&lt;/p&gt;</span>
 
   &lt;p&gt;&lt;a href=&quot;index.html&quot;&gt;Return to home page&lt;/a&gt;&lt;/p&gt;
 &lt;/body&gt;
</pre></div>


<p>This unique formatting is called a <strong><em>diff</em></strong>, because
it shows the <em>diff</em>erence between two versions of a file. In our case,
it tells us what happened to the <code>pink.html</code> file between the
<code>98e10a1</code> and <code>828dd1a</code> commits (your patch will contain
different commit ID&rsquo;s). The <code>-7,8 +7,7</code> portion describes the
lines affected in the respective versions of the file, and the rest of the text
shows us the content that has been changed. The lines beginning with
<code>-</code> have been deleted in the new version, and the line starting with
<code>+</code> has been added.</p>

<p>While you don&rsquo;t have to know the ins-and-outs of diffs to make use of
patches, you do need to understand that a single patch file represents a
complete commit. And, since it&rsquo;s a normal file (and also an email),
it&rsquo;s much easier to pass around than a Git branch.</p>

<p>Delete the patch file for now (we&rsquo;ll re-create it later).</p>


<h2>Add a Pink Block (Mary)</h2>

<p>Before learning how to turn patches back into commits, Mary will add one
more snapshot.</p>

<p>In <code>pink.html</code>, add the following on the line after the
<code>&lt;meta&gt;</code> tag.</p>
	
<div class="highlight"><pre><span class="nt">&lt;style&gt;</span>
  <span class="nt">div</span> <span class="p">{</span>
    <span class="k">width</span><span class="o">:</span> <span class="m">300px</span><span class="p">;</span>
    <span class="k">height</span><span class="o">:</span> <span class="m">50px</span><span class="p">;</span>
  <span class="p">}</span>
<span class="nt">&lt;/style&gt;</span>
</pre></div>


<p>And, add the next line of HTML after <code>Only real men wear
pink!</code>:</p>

<div class="highlight"><pre><span class="nt">&lt;div</span> <span class="na">style=</span><span class="s">&quot;background-color: #F0F&quot;</span><span class="nt">&gt;&lt;/div&gt;</span>
</pre></div>


<p>Stage and commit the snapshot.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">commit</span> <span class="na">-a</span> <span class="na">-m</span> <span class="s">&quot;Add a pink block of color&quot;</span>
</pre></div>


<p>Mary&rsquo;s repository now contains two commits after the tip of
<code>master</code>:</p>

<figure>
	<img style='max-width: 390px' src='media/10-1.png' />
	<figcaption>Adding two commits on the <code>pink-page</code> branch</figcaption>
</figure>


<h2>Create Patch of Entire Branch (Mary)</h2>

<p>Mary can use the same command as before to generate patches for all the
commits in her <code>pink-page</code> branch.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">format-patch</span> <span class="n">master</span>
</pre></div>


<p>The first patch is the exact same as we previously examined, but we also
have a new one called <code>0002-Add-a-pink-block-of-color.patch</code>. Note
that the first line of the commit message will always be used to make a
descriptive filename for the patch. You should find the following diff in the
second patch.</p>

<div class="highlight"><pre><span class="gh">index 828dd1a..2713b10 100644</span>
<span class="gd">--- a/pink.html</span>
<span class="gi">+++ b/pink.html</span>
<span class="gu">@@ -4,10 +4,17 @@</span>
   &lt;title&gt;The Pink Page&lt;/title&gt;
   &lt;link rel=&quot;stylesheet&quot; href=&quot;style.css&quot; /&gt;
   &lt;meta charset=&quot;utf-8&quot; /&gt;
<span class="gi">+  &lt;style&gt;</span>
<span class="gi">+    div {</span>
<span class="gi">+      width: 300px;</span>
<span class="gi">+      height: 50px;</span>
<span class="gi">+    }</span>
<span class="gi">+  &lt;/style&gt;</span>
 &lt;/head&gt;
 &lt;body&gt;
   &lt;h1 style=&quot;color: #F0F&quot;&gt;The Pink Page&lt;/h1&gt;
   &lt;p&gt;Only &lt;span style=&quot;color: #F0F&quot;&gt;real men&lt;/span&gt; wear pink!&lt;/p&gt;
<span class="gi">+  &lt;div style=&quot;background-color: #F0F&quot;&gt;&lt;/div&gt;</span>
 
   &lt;p&gt;&lt;a href=&quot;index.html&quot;&gt;Return to home page&lt;/a&gt;&lt;/p&gt;
 &lt;/body&gt;
</pre></div>


<p>This is the same formatting as the first patch, except its lack of
<code>-</code> lines indicate that we only added HTML during the second commit.
As you can see, this patch is really just a machine-readable summary of our
actions from the previous section.</p>


<h2>Mail the Patches (Mary)</h2>

<p>Now that Mary has prepared a series of patches, she can send them to the
project maintainer (us). In the typical patch workflow, she would send them via
email using one of the following methods:</p>

<ul>

	<li>Copying and pasting the contents of the patch files into an email
	client. If she uses this method, Mary would have to make sure that her
	email application doesn&rsquo;t change the whitespace in the patch upon
	sending it.</li>

	<li>Sending the patch file as an attachment to a normal email.</li>

	<li>Using the convenient <code>git send-email</code> command and specifying
	a file or a directory of files to send. For example, <code>git send-email
	.</code> will send all the patches in the current directory. Git also
	requires some special configurations for this command. Please consult the
	<a
	href='http://www.kernel.org/pub/software/scm/git/docs/git-send-email.html'>official
	Git documentation</a> for details.</li>

</ul>
 
<p>The point is, the <code>.patch</code> files need to find their way into the
Git repository of whoever wants to add it to their project. For our example,
all we need to do is copy the patches into the <code>my-git-repo</code>
directory that represents our local version of the project.</p>


<h2>Apply the Patches (You)</h2>

<p>Copy the two patch files from <code>marys-repo</code> into
<code>my-git-repo</code>. Using the new <code>git am</code> command, we can use
these patches to add Mary&rsquo;s commits to our repository.</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">../my-git-repo</span>
<span class="k">git</span> <span class="k">checkout</span> <span class="na">-b</span> <span class="n">patch-integration</span>
<span class="k">git</span> <span class="k">am</span> <span class="n">&lt;</span> <span class="n">0001-Change-pink-to-a-manly-color.patch</span>
<span class="k">git</span> <span class="k">log</span> <span class="n">master..HEAD</span> <span class="na">--stat</span>
</pre></div>


<p>First, notice that we&rsquo;re doing our integrating in a new topic branch.
Again, this ensures that we won&rsquo;t destroy our existing functionality and
gives us a chance to approve changes. Second, the <code>git am</code> command
takes a patch file and creates a new commit from it. The log output shows us
that our integration branch contains Mary&rsquo;s update, along with her author
information.</p>

<p>Let&rsquo;s repeat the process for the second commit.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">am</span> <span class="n">&lt;</span> <span class="n">0002-Add-a-pink-block-of-color.patch</span>
<span class="k">git</span> <span class="k">log</span> <span class="n">master..HEAD</span> <span class="na">--stat</span>
</pre></div>


<p>The <code>git am</code> command is configured to read from something called
&ldquo;standard input,&rdquo; and the <code>&lt;</code> character is how we can
turn a file&rsquo;s contents into standard input. As it&rsquo;s really more of
an operating system topic, you can just think of this syntax as a quirk of the
<code>git am</code> command.</p>

<p>After applying this patch, our integration branch now looks exactly like
Mary&rsquo;s <code>pink-page</code> branch. We applied Mary&rsquo;s patches in
the same order she did, but that didn&rsquo;t necessarily have to be the case.
The whole idea behind patches is that they let you isolate a commit and move it
around as you please.</p>


<h2>Integrate the Patches (You)</h2>

<p>Once again, we&rsquo;re in the familiar situation of integrating a topic
branch into the stable <code>master</code> branch.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">merge</span> <span class="n">patch-integration</span>
<span class="k">git</span> <span class="k">branch</span> <span class="na">-d</span> <span class="n">patch-integration</span>
<span class="k">git</span> <span class="k">clean</span> <span class="na">-f</span>
<span class="k">git</span> <span class="k">push</span> <span class="n">origin</span> <span class="n">master</span>
</pre></div>


<p>Mary&rsquo;s updates are now completely integrated into our local
repository, so we can get rid of the patch files with <code>git clean</code>.
This was also an appropriate time to push changes to the public repository so
other developers can access the most up-to-date version of the project.</p>


<h2>Update Mary&rsquo;s Repository (Mary)</h2>

<p>Mary might be tempted to merge her <code>pink-page</code> branch directly
into her <code>master</code> branch, but this would be a mistake. Her
<code>master</code> branch <em>must</em> track the &ldquo;official&rdquo;
repository&rsquo;s <code>master</code>, as discussed in the previous
module.</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">../marys-repo</span>
<span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">fetch</span> <span class="n">origin</span>
<span class="k">git</span> <span class="k">rebase</span> <span class="n">origin/master</span>
<span class="k">git</span> <span class="k">branch</span> <span class="na">-D</span> <span class="n">pink-page</span>
<span class="k">git</span> <span class="k">clean</span> <span class="na">-f</span>
</pre></div>


<p>Patches are a convenient way to share commits amongst developers, but the
patch workflow still requires an &ldquo;official&rdquo; repository that
contains <em>everybody&rsquo;s</em> changes. What would have happened if Mary
wasn&rsquo;t the only one sending patches to us? We may very well have applied
several different patches or applied Mary&rsquo;s contributions in a different
order. Using Mary&rsquo;s <code>pink-page</code> to update her
<code>master</code> branch would completely ignore all these updates.</p>

<p>Taking this into consideration, our final patch workflow resembles the
following.</p>

<figure>
	<img style='max-width: 450px' src='media/10-2.png' />
	<figcaption>The patch workflow</figcaption>
</figure>


<h2>Conclusion</h2>

<p>Whereas remote repositories are a way to share entire <em>branches</em>,
patches are a way to send individual <em>commits</em> to another developer.
Keep in mind that patches are usually only sent to a project maintainer, who
then integrates them into the &ldquo;official&rdquo; project for all to see. It
would be impossible for everyone to communicate using only patches, as no one
would be applying them in the same order. Eventually, everyone&rsquo;s project
history would look entirely different.</p>

<p>In many ways, patches are a simpler way to accept contributions than the
integrator workflow from the previous module. Only the project maintainer
needs a public repository, and he&rsquo;ll never have to peek at anyone
else&rsquo;s repository. From the maintainer&rsquo;s perspective, patches also
provide the same security as the integrator workflow: he still won&rsquo;t have
to give anyone access to his &ldquo;official&rdquo; repository. But, now he
won&rsquo;t have to keep track of everybody&rsquo;s remote repositories,
either.</p>

<p>As a programmer, you&rsquo;re most likely to use patches when you want to
fix a bug in someone else&rsquo;s project. After fixing it, you can send them a
patch of the resulting commit. For this kind of one-time-fix, it&rsquo;s much
more convenient for you to generate a patch than to set up a public Git
repository.</p>

<p>This module concludes our discussion of the standard Git workflows.
Hopefully, you now have a good idea of how Git can better manage your personal
and professional software projects using a centralized, integrator, or patch
workflow. In the next module, we&rsquo;ll switch gears and introduce a variety
of practical Git commands.</p>


<h2>Quick Reference</h2>

<dl>
	<dt><code><span class="k">git</span> <span class="k">format-patch</span> <span class="n">&lt;branch-name&gt;</span>
</code></dt>
	<dd>Create a patch for each commit contained in the current branch but not
	in <code>&lt;branch-name&gt;</code>. You can also specify a commit ID
	instead of <code>&lt;branch-name&gt;</code>.</dd>

	<dt><code><span class="k">git</span> <span class="k">am</span> <span class="n">&lt;</span> <span class="n">&lt;patch-file&gt;</span>
</code></dt>
	<dd>Apply a patch to the current branch.</dd>
</dl>

<p class='sequential-nav'>
	<a href='tips-and-tricks.html'>Continue to <em>Tips &amp; Tricks</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>