<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Git Tutorial | Undoing Changes | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="Our next task is to learn how to view the previous
states of a project, revert back to them, and reset uncommitted changes." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Git
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Undoing Changes</h1>

<p>In the last module, we learned how to record versions of a project into a
Git repository. The whole point of maintaining these &ldquo;safe&rdquo; copies
is peace of mind: should our project suddenly break, we&rsquo;ll know that we
have easy access to a functional version, and we&rsquo;ll be able to pinpoint
precisely where the problem was introduced.</p>

<p>To this end, storing &ldquo;safe&rdquo; versions isn&rsquo;t much help
without the ability to restore them. Our next task is to learn how to view the
previous states of a project, revert back to them, and reset uncommitted
changes.</p>

<div class='icon-text download-icon-text'>
	<img src='../../media/icons/download.png' style='max-width: 45px'/>

	<p><a href='media/repo-zips/undoing-changes.zip'>Download the repository
	for this module</a></p>

</div>

<div style='clear: both'></div>

<p>If you&rsquo;ve been following along from the previous module, you already
have everything you need. Otherwise, download the zipped Git repository from
the above link, uncompress it, and you&rsquo;re good to go.</p>


<h2>Display Commit Checksums</h2>

<p>As a quick review, let&rsquo;s display our repository&rsquo;s history.
Navigate a command prompt (or Git Bash) to the <code>my-git-repo</code> folder
and run the following.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">log</span> <span class="na">--oneline</span>
</pre></div>


<p>The output for this should look similar to the following, but contain different
commit checksums.</p>

<div class="highlight"><pre><span class="s">1c310d2</span> Add navigation links
<span class="s">54650a3</span> Create blue and orange pages
<span class="s">b650e4b</span> Create index page
</pre></div>


<p>Git only outputs the first 7 characters of the checksum (remember that you
can see the full version with the default formatting of <code>git log</code>).
These first few characters effectively serve as a unique ID for each
commit.</p>


<h2>View an Old Revision</h2>

<p>Using the new <code>git checkout</code> command, we can view the contents of
a previous snapshot. Make sure to change <code>54650a3</code> in the following
command to the ID of your <em>second</em> commit.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">54650a3</span>
</pre></div>


<p>This will output a lot of information about a <code>detached HEAD</code>
state. You can ignore it for now. All you need to know is that the above
command turns your <code>my-git-repo</code> directory into an exact replica of
the second snapshot we committed in <a href='the-basics.html'>The
Basics</a>.</p>

<p>Open the HTML files in a text editor or web browser to verify that the
navigation links we added in the third commit have disappeared. Running
<code>git log</code> will also tell us that the third commit is no longer part
of the project. After checking out the second commit, our repository history
looks like the following (the red circle represents the current commit).</p>

<figure>
	<img style='max-width: 200px' src='media/2-1.png' />
	<figcaption>Checking out the 2nd commit</figcaption>
</figure>


<h2>View an Older Revision</h2>

<p>Let&rsquo;s go even farther back in our history. Be sure to change
<code>b650e4b</code> to the ID of your <em>first</em> commit.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">b650e4b</span>
</pre></div>


<p>Now, the <code>blue.html</code> and <code>orange.html</code> files are gone,
as is the rest of the <code>git log</code> history.</p>

<figure>
	<img style='max-width: 200px' src='media/2-2.png' />
	<figcaption>Checking out the 1st commit</figcaption>
</figure>

<p>In the last module, we said that Git was designed to never lose a committed
snapshot. So, where did our second and third snapshots go? A simple <code>git
status</code> will answer that question for us. It should return the following
message:</p>

<div class="highlight"><pre><span class="c">#</span> <span class="c">Not currently on any branch.</span>
nothing to commit (working directory clean)
</pre></div>


<p>Compare this with the status output from the previous module:</p>

<div class="highlight"><pre><span class="c">#</span> <span class="c">On branch </span><span class="cs">master</span>
nothing to commit (working directory clean)
</pre></div>


<p>All of our actions in <em>The Basics</em> took place on the
<code>master</code> branch, which is where our second and third commits still
reside. To retrieve our complete history, we just have to check out this
branch. This is a very brief introduction to branches, but it&rsquo;s all we
need to know to navigate between commits. The next module will discuss branches
in full detail.</p>
 

<h2>Return to Current Version</h2>

<p>We can use the same <code>git checkout</code> command to return to the
<code>master</code> branch.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
</pre></div>


<p>This makes Git update our working directory to reflect the state of the
<code>master</code> branch&rsquo;s snapshot. It re-creates the
<code>blue.html</code> and <code>orange.html</code> files for us, and the
content of <code>index.html</code> is updated as well. We&rsquo;re now back to
the current state of the project, and our history looks like:</p>

<figure>
	<img style='max-width: 200px' src='media/2-3.png' />
	<figcaption>Current project history</figcaption>
</figure>


<h2>Tag a Release</h2>

<p>Let&rsquo;s call this a stable version of the example website. We can make
it official by <strong>tagging</strong> the most recent commit with a version
number.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">tag</span> <span class="na">-a</span> <span class="n">v1.0</span> <span class="na">-m</span> <span class="s">&quot;Stable version of the website&quot;</span>
</pre></div>


<p>Tags are convenient references to official releases and other significant
milestones in a software project. It lets developers easily browse and check
out important revisions. For example, we can now use the <code>v1.0</code> tag
to refer to the third commit instead of its random ID. To view a list of
existing tags, execute <code>git tag</code> without any arguments.</p>

<p>In the above snippet, the <code>-a</code> flag tells Git to create an
<strong>annotated tag</strong>, which lets us record our name, the date, and a
descriptive message (specified via the <code>-m</code> flag). We&rsquo;ll use
this tag to find the stable version after we try some crazy experiments.</p>


<h2>Try a Crazy Experiment</h2>

<p>We&rsquo;re now free to add experimental changes to the example site without
affecting any committed content. Create a new file called
<code>crazy.html</code> and add the following HTML.</p>

<div class="highlight"><pre><span class="cp">&lt;!DOCTYPE html&gt;</span>
<span class="nt">&lt;html</span> <span class="na">lang=</span><span class="s">&quot;en&quot;</span><span class="nt">&gt;</span>
<span class="nt">&lt;head&gt;</span>
  <span class="nt">&lt;title&gt;</span>A Crazy Experiment<span class="nt">&lt;/title&gt;</span>
  <span class="nt">&lt;meta</span> <span class="na">charset=</span><span class="s">&quot;utf-8&quot;</span> <span class="nt">/&gt;</span>
<span class="nt">&lt;/head&gt;</span>
<span class="nt">&lt;body&gt;</span>
  <span class="nt">&lt;h1&gt;</span>A Crazy Experiment<span class="nt">&lt;/h1&gt;</span>
  <span class="nt">&lt;p&gt;</span>We&#39;re trying out a <span class="nt">&lt;span</span> <span class="na">style=</span><span class="s">&quot;color: #F0F&quot;</span><span class="nt">&gt;</span>crazy<span class="nt">&lt;/span&gt;</span>
  <span class="nt">&lt;span</span> <span class="na">style=</span><span class="s">&quot;color: #06C&quot;</span><span class="nt">&gt;</span>experiment<span class="nt">&lt;/span&gt;</span>!
    
  <span class="nt">&lt;p&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;index.html&quot;</span><span class="nt">&gt;</span>Return to home page<span class="nt">&lt;/a&gt;&lt;/p&gt;</span>
<span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span>
</pre></div>



<h2>Stage and Commit the Snapshot</h2>

<p>Stage and commit the new file as usual.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">add</span> <span class="n">crazy.html</span>
<span class="k">git</span> <span class="k">status</span>
<span class="k">git</span> <span class="k">commit</span> <span class="na">-m</span> <span class="s">&quot;Add a crazzzy experiment&quot;</span>
<span class="k">git</span> <span class="k">log</span>
</pre></div>


<p>It&rsquo;s a good practice to run <code>git status</code> to see exactly
what you&rsquo;re committing before running <code>git commit -m</code>. This
will keep you from unintentionally committing a file that doesn&rsquo;t belong
in the current snapshot.</p>

<p>As expected, the new snapshot shows up in the repository&rsquo;s history. If
your log history takes up more than one screen, you can scroll down by pressing
<code>Space</code> and return to the command line by pressing the letter
<code>q</code>.</p>


<h2>View the Stable Commit</h2>

<p>Let&rsquo;s go back and take a look at our stable revision. Remember that
the <code>v1.0</code> tag now serves as a user-friendly shortcut to the third
commit&rsquo;s ID.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">v1.0</span>
</pre></div>


<p>After seeing the stable version of the site, we decide to scrap the crazy
experiment. But, before we undo the changes, we need to return to the
<code>master</code> branch. If we didn&rsquo;t, all of our updates would be on
some non-existent branch. As we&rsquo;ll discover next module, you should
never make changes directly to a previous revision.</p>
	
<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">log</span> <span class="na">--oneline</span>
</pre></div>


<p>At this point, our history should look like the following:</p>

<div class="highlight"><pre><span class="s">514fbe7</span> Add a crazzzy experiment
<span class="s">1c310d2</span> Add navigation links
<span class="s">54650a3</span> Create blue and orange pages
<span class="s">b650e4b</span> Create index page
</pre></div>



<h2>Undo Committed Changes</h2>

<p>We&rsquo;re ready to restore our stable tag by removing the most recent
commit.  Make sure to change <code>514fbe7</code> to the ID of the <em>crazy
experiment&rsquo;s commit</em> before running the next command.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">revert</span> <span class="n">514fbe7</span>
</pre></div>


<p>This will prompt you for a commit message with a default of <code>Revert
"Add a crazzzy experiment"...</code>. You can leave the default message and
close the file.  After verifying that <code>crazy.html</code> is gone, take a
look at your history with <code>git log --oneline</code>.</p>

<div class="highlight"><pre><span class="s">506bb9b</span> Revert &quot;Add a crazzzy experiment&quot;
<span class="s">514fbe7</span> Add a crazzzy experiment
<span class="s">1c310d2</span> Add navigation links
<span class="s">54650a3</span> Create blue and orange pages
<span class="s">b650e4b</span> Create index page
</pre></div>


<p>Notice that instead of deleting the &ldquo;crazzzy experiment&rdquo; commit,
Git figures out how to undo the changes it contains, then tacks on another
commit with the resulting content. So, our fifth commit and our third commit
represent the exact same snapshot, as shown below. Again, Git is designed to
<em>never</em> lose history: the fourth snapshot is still accessible, just in
case we want to continue developing it.</p>

<figure>
	<img style='max-width: 300px' src='media/2-4.png' />
	<figcaption>Current project history</figcaption>
</figure>

<p>When using <code>git revert</code>, remember to specify the commit that you
want to undo&mdash;not the stable commit that you want to return to. It helps
to think of this command as saying &ldquo;undo this commit&rdquo; rather than
&ldquo;restore this version.&rdquo;</p>


<h2>Start a Smaller Experiment</h2>

<p>Let&rsquo;s try a smaller experiment this time. Create
<code>dummy.html</code> and leave it as a blank file. Then, add a link in the
&ldquo;Navigation&rdquo; section of <code>index.html</code> so that it
resembles the following.</p>
	
<div class="highlight"><pre><span class="nt">&lt;h2&gt;</span>Navigation<span class="nt">&lt;/h2&gt;</span>
<span class="nt">&lt;ul&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: #F90&quot;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">&quot;orange.html&quot;</span><span class="nt">&gt;</span>The Orange Page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: #00F&quot;</span><span class="nt">&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">&quot;blue.html&quot;</span><span class="nt">&gt;</span>The Blue Page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">&quot;dummy.html&quot;</span><span class="nt">&gt;</span>The Dummy Page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span>
</pre></div>


<p>In the next section, we&rsquo;re going to abandon this uncommitted
experiment. But since the <code>git revert</code> command requires a commit ID
to undo, we can&rsquo;t use the method discussed above.</p>


<h2>Undo Uncommitted Changes</h2>

<p>Before we start undoing things, let&rsquo;s take a look at the status of our
repository.</p>
	
<div class="highlight"><pre><span class="k">git</span> <span class="k">status</span>
</pre></div>


<p>We have a tracked file and an untracked file that need to be changed. First,
we&rsquo;ll take care of <code>index.html</code>:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">reset</span> <span class="na">--hard</span>
</pre></div>


<p>This changes all <em>tracked</em> files to match the most recent commit. You
can also pass a filename to this command to reset only that file (e.g.,
<code>git reset --hard index.html</code>). The <code>--hard</code> flag is what
actually updates the file. Running <code>git reset index.html</code> without
any flags will simply unstage the file, leaving its contents as is. In either
case, <code>git reset</code> only operates on the working directory and the
staging area, so our <code>git log</code> history remains unchanged.</p>

<p>Next, let&rsquo;s remove the <code>dummy.html</code> file. Of course, we
could manually delete it, but using Git to reset changes eliminates human
errors when working with several files in large teams. Run the following
command.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">clean</span> <span class="na">-f</span>
</pre></div>


<p>This will remove all <em>untracked</em> files. With <code>dummy.html</code>
gone, <code>git status</code> should now tell us that we have a
&ldquo;clean&rdquo; working directory, meaning our project matches the most
recent commit.</p>

<p><strong><em>Be careful</em></strong> with <code>git reset</code> and
<code>git clean</code>. Both operate on the working directory, not on the
committed snapshots. Unlike <code>git revert</code>, they
<strong><em>permanently</em></strong> undo changes, so make sure you really
want to trash what you&rsquo;re working on before you use them.</p>


<h2>Conclusion</h2>

<p>As noted in the previous module, most Git commands operate on one of the
three main components of a Git repository: the working directory, the staged
snapshot, or the committed snapshots. The <code>git reset</code> command undoes
changes to the working directory and the staged snapshot, while <code>git
revert</code> undoes changes contained in committed snapshots. Not
surprisingly, <code>git status</code> and <code>git log</code> directly
parallel this behavior.</p>

<figure>
	<img style='max-width: 440px' src='media/2-5.png' />
	<figcaption>Resetting vs. Reverting</figcaption>
</figure>

<p>I mentioned that instead of completely removing a commit, <code>git
revert</code> saves the commit in case you want to come back to it later. This
is only one reason for preserving committed snapshots. When we start working
with remote repositories, we&rsquo;ll see that altering the history by removing
a commit has dramatic consequences for collaborating with other developers.</p>

<p>This module also introduces the concept of switching between various
commits and branches with <code>git checkout</code>. Branches round out our
discussion of the core Git components, and they offer an elegant option for
optimizing your development workflow. In the next module, we&rsquo;ll cover
the basic Git branch commands.</p>


<h2>Quick Reference</h2>

<dl>
	<dt><code><span class="k">git</span> <span class="k">checkout</span> <span class="n">&lt;commit-id&gt;</span>
</code></dt>
	<dd>View a previous commit.</dd>

	<dt><code><span class="k">git</span> <span class="k">tag</span> <span class="na">-a</span> <span class="n">&lt;tag-name&gt;</span> <span class="na">-m</span> <span class="s">&quot;&lt;description&gt;&quot;</span>
</code></dt>
	<dd>Create an annotated tag pointing to the most recent commit.</dd>

	<dt><code><span class="k">git</span> <span class="k">revert</span> <span class="n">&lt;commit-id&gt;</span>
</code></dt>
	<dd>Undo the specified commit by applying a new commit.</dd>

	<dt><code><span class="k">git</span> <span class="k">reset</span> <span class="na">--hard</span>
</code></dt>
	<dd>Reset <em>tracked</em> files to match the most recent commit.</dd>

	<dt><code><span class="k">git</span> <span class="k">clean</span> <span class="na">-f</span>
</code></dt>
	<dd>Remove <em>untracked</em> files.</dd>

	<dt><code><span class="k">git</span> <span class="k">reset</span> <span class="na">--hard</span>
</code> / <code><span class="k">git</span> <span class="k">clean</span> <span class="na">-f</span>
</code></dt>
	<dd>Permanently undo uncommitted changes.</dd>
</dl>


<p class='sequential-nav'>
	<a href='branches-1.html'>Continue to <em>Branches, Part I</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>