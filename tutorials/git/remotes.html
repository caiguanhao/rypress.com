<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Git Tutorial | Remote Repositories | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="We've already seen how branches can streamline an
individual workflow, but they also happen to be Git's mechanism for sharing
commits between repositories." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Git
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Remotes</h1>

<p>Simply put, a <strong>remote repository</strong> is one that is not your
own. It can be another Git repository that&rsquo;s on your company&rsquo;s
network, the internet, or even your local filesystem, but the point is that
it&rsquo;s a repository distinct from your <code>my-git-repo</code>
project.</p>

<p>We&rsquo;ve already seen how branches can streamline a workflow within a
single repository, but they also happen to be Git&rsquo;s mechanism for sharing
commits between repositories. <strong>Remote branches</strong> act just like
the local branches that we&rsquo;ve been using, only they represent a branch in
someone else&rsquo;s repository.</p>

<figure>
	<img style='max-width: 460px' src='media/7-1.png' />
	<figcaption>Accessing a feature branch from a remote repository</figcaption>
</figure>

<p>This means that we can adapt our merging and rebasing skills to make Git a
fantastic collaboration tool. Over the next few modules, we&rsquo;ll be exploring
various multi-user workflows by pretending to be different developers working on
our example website.</p>

<p>For several parts of this module, we&rsquo;re going to pretend to be Mary, the
graphic designer for our website. Mary&rsquo;s actions are clearly denoted by
including her name in the heading of each step.</p>

<div class='icon-text download-icon-text'>
	<img src='../../media/icons/download.png' style='max-width: 45px'/>

	<p><a href='media/repo-zips/remotes.zip'>Download the repository
	for this module</a></p>

</div>

<div style='clear: both'></div>

<p>If you&rsquo;ve been following along from the previous module, you already
have everything you need. Otherwise, download the zipped Git repository from
the above link, uncompress it, and you&rsquo;re good to go.</p>



<h2>Clone the Repository (Mary)</h2>

<p>First, Mary needs her own copy of the repository to work with. The <a
href='distributed-workflows.html'>Distributed Workflows</a> module will discuss
network-based remotes, but right now we&rsquo;re just going to store them on
the local filesystem.</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">/path/to/my-git-repo</span>
<span class="k">cd</span> <span class="n">..</span>
<span class="k">git</span> <span class="k">clone</span> <span class="n">my-git-repo</span> <span class="n">marys-repo</span>
<span class="k">cd</span> <span class="n">marys-repo</span>
</pre></div>


<p>The first two lines navigate the command shell to the directory
<em>above</em> <code>my-git-repo</code>. Make sure to change
<code>/path/to/my-git-repo</code> to the actual path to your repository. The
<code>git clone</code> command copies our repository into
<code>marys-repo</code>, which will reside in the same directory as
<code>my-git-repo</code>. Then, we navigate to Mary&rsquo;s repository so we
can start pretending to be Mary.</p>

<p>Run <code>git log</code> to verify that Mary&rsquo;s repository is in fact a
copy of our original repository.</p>


<h2>Configure The Repository (Mary)</h2>

<p>First off, Mary needs to configure her repository so that we know who
contributed what to the project.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">config</span> <span class="n">user.name</span> <span class="s">&quot;Mary&quot;</span>
<span class="k">git</span> <span class="k">config</span> <span class="n">user.email</span> <span class="n">mary.example@rypress.com</span>
</pre></div>


<p>You may recall from the first module that we used a <code>--global</code>
flag to set the configuration for the entire Git installation. But since
Mary&rsquo;s repository is on the local filesystem, she needs a <em>local</em>
configuration.</p>

<p>Use a text editor to open up the file called <code>config</code> in the
<code>.git</code> folder of Mary&rsquo;s project (you may need to enable hidden
files to see <code>.git</code>). This is where local configurations are stored,
and we see Mary&rsquo;s information at the bottom of the file. Note that this
overrides the global configuration that we set in <a href='the-basics.html'>The
Basics</a>.</p>


<h2>Start Mary&rsquo;s Day (Mary)</h2>

<p>Today, Mary is going to be working on her bio page, which she should develop
in a separate branch:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="na">-b</span> <span class="n">bio-page</span>
</pre></div>


<p>Mary can create and check out branches just like we did in our copy of the
project. Her repository is a completely isolated development environment, and
she can do whatever she wants in here without worrying about what&rsquo;s going
on in <code>my-git-repo</code>. Just as branches are an abstraction for the
working directory, the staged snapshot, and a commit history, a repository is
an abstraction for branches.</p>


<h2>Create Mary&rsquo;s Bio Page (Mary)</h2>

<p>Let&rsquo;s complete Mary&rsquo;s biography page. In
<code>marys-repo</code>, change <code>about/mary.html</code> to:</p>

<div class="highlight"><pre><span class="cp">&lt;!DOCTYPE html&gt;</span>
<span class="nt">&lt;html</span> <span class="na">lang=</span><span class="s">&quot;en&quot;</span><span class="nt">&gt;</span>
<span class="nt">&lt;head&gt;</span>
  <span class="nt">&lt;title&gt;</span>About Mary<span class="nt">&lt;/title&gt;</span>
  <span class="nt">&lt;link</span> <span class="na">rel=</span><span class="s">&quot;stylesheet&quot;</span> <span class="na">href=</span><span class="s">&quot;../style.css&quot;</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;meta</span> <span class="na">charset=</span><span class="s">&quot;utf-8&quot;</span> <span class="nt">/&gt;</span>
<span class="nt">&lt;/head&gt;</span>
<span class="nt">&lt;body&gt;</span>
  <span class="nt">&lt;h1&gt;</span>About Mary<span class="nt">&lt;/h1&gt;</span>
  <span class="nt">&lt;p&gt;</span>I&#39;m a graphic designer.<span class="nt">&lt;/p&gt;</span>

  <span class="nt">&lt;h2&gt;</span>Interests<span class="nt">&lt;/h2&gt;</span>
  <span class="nt">&lt;ul&gt;</span>
    <span class="nt">&lt;li&gt;</span>Oil Painting<span class="nt">&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;</span>Web Design<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;/ul&gt;</span>

  <span class="nt">&lt;p&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;index.html&quot;</span><span class="nt">&gt;</span>Return to about page<span class="nt">&lt;/a&gt;&lt;/p&gt;</span>
<span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span>
</pre></div>


<p>Again, we&rsquo;re developing this in a branch as a best-practice step: our
<code>master</code> branch is only for stable, tested code. Stage and commit
the snapshot, then take a look at the result.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">commit</span> <span class="na">-a</span> <span class="na">-m</span> <span class="s">&quot;Add bio page for Mary&quot;</span>
<span class="k">git</span> <span class="k">log</span> <span class="na">-n</span> <span class="n">1</span>
</pre></div>


<p>The <code>Author</code> field in the log output should reflect the local
configurations we made for Mary&rsquo;s name and email. Remember that the
<code>-n 1</code> flag limits history output to a single commit.</p>


<h2>Publish the Bio Page (Mary)</h2>

<p>Now, we can publish the bio page by merging into the <code>master</code>
branch.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">merge</span> <span class="n">bio-page</span>
</pre></div>


<p>Of course, this results in a fast-forward merge. We&rsquo;ll eventually pull
this update into <code>my-git-repo</code> once we stop pretending to be Mary.
Here&rsquo;s what Mary&rsquo;s repository looks like compared to ours:</p>

<figure>
	<img style='max-width: 240px' src='media/7-2.png' />
	<figcaption>Merging Mary&rsquo;s <code>bio-page</code> branch
with her <code>master</code></figcaption>
</figure>

<p>Notice that both repositories have normal, local branches&mdash;we
haven&rsquo;t had any interaction between the two repositories, so we
don&rsquo;t see any remote branches yet. Before we switch back to
<code>my-git-repo</code>, let&rsquo;s examine Mary&rsquo;s remote
connections.</p>


<h2>View Remote Repositories (Mary)</h2>

<p>Mary can list the connections she has to other repositories using the following
command.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">remote</span>
</pre></div>


<p>Apparently, she has a remote called <code>origin</code>. When you clone a
repository, Git automatically adds an <code>origin</code> remote pointing to
the original repository, under the assumption that you&rsquo;ll probably want
to interact with it down the road. We can request a little bit more information
with the <code>-v</code> (verbose) flag:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">remote</span> <span class="na">-v</span>
</pre></div>


<p>This shows the full path to our original repository, verifying that
<code>origin</code> is a remote connection to <code>my-git-repo</code>. The
same path is designated as a &ldquo;fetch&rdquo; and a &ldquo;push&rdquo;
location. We&rsquo;ll see what these mean in a moment.</p>


<h2>Return to Your Repository (You)</h2>

<p>Ok, we&rsquo;re done being Mary, and we can return to our own repository.</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">../my-git-repo</span>
</pre></div>


<p>Notice that Mary&rsquo;s bio page is still empty. It&rsquo;s very important
to understand that this repository and Mary&rsquo;s repository are completely
separate. While she was altering her bio page, we could have been doing all
sorts of other things in <code>my-git-repo</code>. We could have even changed
her bio page, which would result in a merge conflict when we try to pull her
changes in.</p>


<h2>Add Mary as a Remote (You)</h2>

<p>Before we can get ahold of Mary&rsquo;s bio page, we need access to her
repository. Let&rsquo;s look at our current list of remotes:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">remote</span>
</pre></div>


<p>We don&rsquo;t have any (<code>origin</code> was never created because we
didn&rsquo;t clone from anywhere). So, let&rsquo;s add Mary as a remote
repository.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">remote</span> <span class="k">add</span> <span class="n">mary</span> <span class="n">../marys-repo</span>
<span class="k">git</span> <span class="k">remote</span> <span class="na">-v</span>
</pre></div>


<p>We can now use <code>mary</code> to refer to Mary&rsquo;s repository, which
is located at <code>../marys-repo</code>. The <code>git remote add</code>
command is used to bookmark another Git repository for easy access, and our
connections can be seen in the figure below.</p>

<figure>
	<img style='max-width: 310px' src='media/7-3.png' />
	<figcaption>Connections to remote repositories</figcaption>
</figure>

<p>Now that our remote <em>repositories</em> are setup, we&rsquo;ll spend the
rest of the module discussing remote <em>branches</em>.</p>


<h2>Fetch Mary&rsquo;s Branches (You)</h2>

<p>As noted earlier, we can use remote branches to access snapshots from
another repository. Let&rsquo;s take a look at our current remote branches with
the <code>-r</code> flag:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">branch</span> <span class="na">-r</span>
</pre></div>


<p>Again, we don&rsquo;t have any. To populate our remote branch listing, we
need to <strong>fetch</strong> the branches from Mary&rsquo;s repository:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">fetch</span> <span class="n">mary</span>
<span class="k">git</span> <span class="k">branch</span> <span class="na">-r</span>
</pre></div>


<p>This will go to the &ldquo;fetch&rdquo; location shown in <code>git remote
-v</code> and download all of the branches it finds there into our repository.
The resulting branches are shown below.</p>

<div class="highlight"><pre>mary/bio-page
mary/master
</pre></div>


<p>Remote branches are always listed in the form
<code>&lt;remote&#8209;name&gt;/&lt;branch&#8209;name&gt;</code> so that they
will never be mistaken for local branches. The above listing reflects the state
of Mary&rsquo;s repository at the time of the fetch, but they will not be
automatically updated if Mary continues developing any of her branches.</p>

<p>That is to say, our remote branches are not <em>direct</em> links into
Mary&rsquo;s repository&mdash;they are read-only copies of her branches, stored
in our own repository. This means that we would have to do another fetch to
access new updates.</p>

<figure>
	<img style='max-width: 400px' src='media/7-4.png' />
	<figcaption>Mary&rsquo;s remote branches in our repository</figcaption>
</figure>

<p>The above figure shows the state of <em>our</em> repository. We have access
to Mary&rsquo;s snapshots (represented as squares) and her branches, even
though we don&rsquo;t have a real-time connection to Mary&rsquo;s
repository.</p>


<h2>Check Out a Remote Branch</h2>

<p>Let&rsquo;s check out a remote branch to review Mary&rsquo;s changes.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">mary/master</span>
</pre></div>


<p>This puts us in a <code>detached HEAD</code> state, just like we were in
when we checked out a dangling commit. This shouldn&rsquo;t be that surprising,
considering that our remote branches are <em>copies</em> of Mary&rsquo;s
branches. Checking out a remote branch takes our <code>HEAD</code> off the tip
of a local branch, illustrated by the following diagram.</p>

<figure>
	<img style='max-width: 350px' src='media/7-5.png' />
	<figcaption>Checking out Mary&rsquo;s <code>master</code>
branch</figcaption>
</figure>

<p>We can&rsquo;t continue developing if we&rsquo;re not on a local branch. To
build on <code>mary/master</code> we either need to merge it into our own local
<code>master</code> or create another branch. We did the latter in <a
href='branches-1.html'>Branches, Part I</a> to build on an old commit and in <a
href='rewriting-history.html'>the previous module</a> to revive a
&ldquo;lost&rdquo; commit, but right now we&rsquo;re just looking at what Mary
did, so the <code>detached HEAD</code> state doesn&rsquo;t really affect
us.</p>


<h2>Find Mary&rsquo;s Changes</h2>

<p>We can use the same log-filtering syntax from the previous module to view
Mary&rsquo;s changes.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">log</span> <span class="n">master..mary/master</span> <span class="na">--stat</span>
</pre></div>


<p>This shows us what Mary has added to her master branch, but it&rsquo;s also
a good idea to see if we&rsquo;ve added any new changes that aren&rsquo;t in
Mary&rsquo;s repository:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">log</span> <span class="n">mary/master..master</span> <span class="na">--stat</span>
</pre></div>


<p>This won&rsquo;t output anything, since we haven&rsquo;t altered our
database since Mary cloned it. In other words, our history hasn&rsquo;t
<em>diverged</em>&mdash;we&rsquo;re just <em>behind</em> by a commit.</p>


<h2>Merge Mary&rsquo;s Changes</h2>

<p>Let&rsquo;s approve Mary&rsquo;s changes and integrate them into our own
<code>master</code> branch.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">merge</span> <span class="n">mary/master</span>
</pre></div>


<p>Even though <code>mary/master</code> is a remote branch, this still results
in a fast-forward merge because there is a linear path from our
<code>master</code> to the tip of <code>mary/master</code>:</p>

<figure>
	<img style='max-width: 350px' src='media/7-6.png' />
	<figcaption>Before merging Mary&rsquo;s <code>master</code>
branch into our own</figcaption>
</figure>

<p>After the merge, the snapshots from Mary&rsquo;s remote branch become a part
of our local <code>master</code> branch. As a result, our <code>master</code>
is now synchronized with Mary&rsquo;s:</p>

<figure>
	<img style='max-width: 350px' src='media/7-7.png' />
	<figcaption>After merging Mary&rsquo;s master
<code>branch</code> into our own</figcaption>
</figure>

<p>Notice that we only interacted with Mary&rsquo;s <code>master</code> branch,
even though we had access to her <code>bio-page</code>. If we hadn&rsquo;t been
pretending to be Mary, we wouldn&rsquo;t have known what this feature branch
was for or if it was ready to be merged.  But, since we&rsquo;ve designated
<code>master</code> as a stable branch for the project, it was safe to
integrate those updates (assuming Mary was also aware of this convention).</p>


<h2>Push a Dummy Branch</h2>

<p>To complement our <code>git fetch</code> command, we&rsquo;ll take a brief
look at <strong>pushing</strong>. Fetching and pushing are <em>almost</em>
opposites, in that fetching imports branches, while pushing exports branches to
another repository. Let&rsquo;s take a look:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">branch</span> <span class="n">dummy</span>
<span class="k">git</span> <span class="k">push</span> <span class="n">mary</span> <span class="n">dummy</span>
</pre></div>


<p>This creates a new branch called <code>dummy</code> and sends it to Mary.
Switch into Mary&rsquo;s repository to see what we did:</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">../marys-repo</span>
<span class="k">git</span> <span class="k">branch</span>
</pre></div>


<p>You should find a new <code>dummy</code> branch in her <em>local</em> branch
listing. I said that <code>git fetch</code> and <code>git push</code> are
<em>almost</em> opposites because pushing creates a new <em>local</em> branch,
while fetching imports commits into <em>remote</em> branches.</p>

<p>Now, put yourself in Mary&rsquo;s shoes. She was developing in her own
repository when, all of a sudden, a new <code>dummy</code> branch appeared out
of nowhere. Obviously, pushing branches into other people&rsquo;s repositories
can make for a chaotic workflow. So, as a general rule, <strong>you should
never push into another developer&rsquo;s repository</strong>. But then, why
does <code>git push</code> even exist?</p>

<p>Over the next few modules, we&rsquo;ll see that pushing is a necessary tool
for maintaining public repositories. Until then, just remember to never, ever
push into one of your friend&rsquo;s repositories. Let&rsquo;s get rid of these
dummy branches and return to our repository.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">branch</span> <span class="na">-d</span> <span class="n">dummy</span>
<span class="k">cd</span> <span class="n">../my-git-repo</span>
<span class="k">git</span> <span class="k">branch</span> <span class="na">-d</span> <span class="n">dummy</span>
</pre></div>



<h2>Push a New Tag</h2>

<p>An important property of <code>git push</code> is that it does not
automatically push tags associated with a particular branch. Let&rsquo;s
examine this by creating a new tag.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">tag</span> <span class="na">-a</span> <span class="n">v2.0</span> <span class="na">-m</span> <span class="s">&quot;An even stabler version of the website&quot;</span>
</pre></div>


<p>We now have a <code>v2.0</code> tag in <code>my-git-repo</code>, which we
can see by running the <code>git tag</code> command. Now, let&rsquo;s try
pushing the branch to Mary&rsquo;s repository.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">push</span> <span class="n">mary</span> <span class="n">master</span>
</pre></div>


<p>Git will say her <code>master</code> branch is already up-to-date, and her
repository will remain unchanged. Instead of pushing the branch that contains
the tag, Git requires us to manually push the tag itself:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">push</span> <span class="n">mary</span> <span class="n">v2.0</span>
</pre></div>


<p>You should now be able to see the <code>v2.0</code> tag in Mary&rsquo;s
repository with a quick <code>git tag</code>. It&rsquo;s very easy to forget to
push new tags, so if it seems like your project has lost a tag or two,
it&rsquo;s most likely because you didn&rsquo;t to push them to the remote
repository.</p>


<h2>Conclusion</h2>

<p>In this module, we learned how remote branches can be used to access
content in someone else&rsquo;s repository. The remotes listed in <code>git
remote</code> are merely bookmarks for a full path to another repository. We
used a local path, but as we&rsquo;ll soon see, Git can use the SSH protocol to
access repositories on another computer.</p>

<p>The convention of <code>master</code> as a stable branch allowed us to pull
changes without consulting Mary, but this doesn&rsquo;t necessarily have to be
the case. When implementing your own workflow, Git offers you a lot of
flexibility about when and where you should pull from your team members. As
long as you clearly define your project conventions, you can designate special
uses or privileges to <em>any</em> branch.</p>

<p>That said, it&rsquo;s important to note that remotes are for
<em>people</em>, whereas branches are for <em>topics</em>. Do <em>not</em>
create separate branches for each of your developers&mdash;give them separate
repositories and bookmark them with <code>git remote add</code>. Branches
should always be for project development, not user management.</p>

<p>Now that we know how Git shares information between repositories, we can add
some more structure to our multi-user development environment. The next module
will show you how to set up and access a shared central repository.</p>


<h2>Quick Reference</h2>

<dl>
	<dt><code><span class="k">git</span> <span class="k">clone</span> <span class="n">&lt;remote-path&gt;</span>
</code></dt>
	<dd>Create a copy of a remote Git repository.</dd>

	<dt><code><span class="k">git</span> <span class="k">remote</span>
</code></dt>
	<dd>List remote repositories.</dd>

	<dt><code><span class="k">git</span> <span class="k">remote</span> <span class="k">add</span> <span class="n">&lt;remote-name&gt;</span> <span class="n">&lt;remote-path&gt;</span>
</code></dt>
	<dd>Add a remote repository.</dd>

	<dt><code><span class="k">git</span> <span class="k">fetch</span> <span class="n">&lt;remote-name&gt;</span>
</code></dt>
	<dd>Download remote branch information, but do not merge anything.</dd>

	<dt><code><span class="k">git</span> <span class="k">merge</span> <span class="n">&lt;remote-name&gt;/&lt;branch-name&gt;</span>
</code></dt>
	<dd>Merge a remote branch into the checked-out branch.</dd>

	<dt><code><span class="k">git</span> <span class="k">branch</span> <span class="na">-r</span>
</code></dt>
	<dd>List remote branches.</dd>

	<dt><code><span class="k">git</span> <span class="k">push</span> <span class="n">&lt;remote-name&gt;</span> <span class="n">&lt;branch-name&gt;</span>
</code></dt>
	<dd>Push a local branch to another repository.</dd>

	<dt><code><span class="k">git</span> <span class="k">push</span> <span class="n">&lt;remote-name&gt;</span> <span class="n">&lt;tag-name&gt;</span>
</code></dt>
	<dd>Push a tag to another repository.</dd>
</dl>

<p class='sequential-nav'>
	<a href='centralized-workflows.html'>Continue to <em>Centralized Workflows</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>