<!DOCTYPE html>
<html lang='en'>
<head>
  <title>Ry's Git Tutorial | Distributed Workflows | RyPress</title>
  <meta charset='UTF-8' />
  <meta name='description' content="In this module, we'll discover some of the drawbacks of
centralized workflows and explore the advantages of a distributed workflow." />
  <meta name='viewport'
  	    content='width=device-width; initial-scale=1.0; maximum-scale=1.0' />
  <link rel="stylesheet" href="../../media/style.css" />
  <link rel="icon" type="image/png" href="../../media/favicon.png" />
  
  <link rel="stylesheet" href="../../media/single.css" />
  <link rel="stylesheet" href="../../media/pygments.css" />

</head>
<body>

<div id='page'>

<nav id='main-nav'>
  <ul>
  	<li><a href='../../index.html'>RyPress</a></li><li><a href='../../tutorials.html'>Tutorials</a></li><li><a href='../../sponsors.html'>Sponsors</a></li><li><a href='../../about.html'>About</a></li><li><a href='../../contact.html'>Contact</a>
	</li>
  </ul>
</nav>

<div id='content'>


<p class='top-back-link'>
	<a href='index.html'><span>&lsaquo;</span> Back to <em>Ry&rsquo;s Git
	Tutorial</em></a>
</p>

<h1 class='back-heading'>Distributed Workflows</h1>

<p>Now that we know how to share information via a centralized workflow, we can
appreciate some of the drawbacks of this collaboration model. While it may be
convenient, allowing everyone to push to an &ldquo;official&rdquo; repository
raises some legitimate security concerns. It means that for anyone to
contribute content, they need access to the <em>entire project.</em></p>

<p>This is fine if you&rsquo;re only interacting with a small team, but imagine a
scenario where you&rsquo;re working on an open-source software project and a stranger
found a bug, fixed it, and wants to incorporate the update into the main
project. You probably don&rsquo;t want to give them push-access to your central
repository, since they could start pushing all sorts of random snapshots, and
you would effectively lose control of the project.</p>

<p>But, what you can do is tell the contributor to push the changes to
<em>their own</em> public repository. Then, you can pull their bug fix into
your private repository to ensure it doesn&rsquo;t contain any undeclared code.
If you approve their contributions, all you have to do is merge them into a local
branch and push it to the main repository, just like we did in the previous
module. You&rsquo;ve become an <em>integrator</em>, in addition to an ordinary
developer:</p>

<figure>
	<img style='max-width: 430px' src='media/9-1.png' />
	<figcaption>The integrator workflow</figcaption>
</figure>

<p>In this module, we&rsquo;ll experience all of this first-hand by creating a
free public repository on <a href='http://bitbucket.org'>Bitbucket.org</a> and
incorporating a contribution from an anonymous developer named John. Bitbucket
is a DVCS hosting provider that makes it very easy to set up a Git repository
and start collaborating with a team of developers.</p>

<div class='icon-text download-icon-text'>
	<img src='../../media/icons/download.png' style='max-width: 45px' />

	<p><a href='media/repo-zips/distributed-workflows.zip'>Download the
	repositories for this module</a></p>

</div>

<div style='clear: both'></div>

<p>If you&rsquo;ve been following along from the previous module, you already
have everything you need. Otherwise, download the zipped Git repositories from
the above link, uncompress them, and you&rsquo;re good to go.</p>



<h2>Create a Bitbucket Account</h2>

<p>The first part of this module will walk you through setting up a Bitbucket
account. Navigate your web browser to <a
href='http://bitbucket.org'>Bitbucket.org</a> and sign up for a free
account.</p>

<figure>
	<img style='max-width: 420px' src='media/bitbucket-logo.png' />
	<figcaption>The Bitbucket logo</figcaption>
</figure>

<p>You can choose any username for your account, but the email address should
match the one you assigned to your Git installation with <code>git
config</code> in <a href='the-basics.html'>The Basics</a>. If you need to
change your email, you can run another <code>git config --global user.email
you@example.com</code> command.</p>


<h2>Create a Public Repository (You)</h2>

<p>To create our first networked Git repository, log into your Bitbucket
account, and navigate to <em>Repositories&nbsp;&gt;&nbsp;Create
repository</em>. Use <code>my-git-repo</code> as the <em>Repository Name</em>,
and anything you like for the <em>Description</em> field. Since this is just an
example project, go ahead and uncheck the <em>This is a private repository</em>
field. Select <em>HTML/CSS</em> for the <em>Language</em> field, then go ahead
and click <em>Create repository</em>.</p>

<figure>
	<img style='max-width: 603px' src='media/9-4.png' />
	<figcaption>Bitbucket&rsquo;s new repository form</figcaption>
</figure>

<p>Essentially, we just ran <code>git init --bare</code> on a Bitbucket server.
We can now push to and fetch from this repository as we did with
<code>central-repo.git</code> in the previous module.</p>

<p>After initialization, Bitbucket offers some helpful instructions, but
don&rsquo;t follow them just yet&mdash;we&rsquo;ll walk through importing an
existing repository in the next section.</p>

<figure>
	<img style='max-width: 600px' src='media/9-5.png' />
	<figcaption>Bitbucket&rsquo;s setup instructions</figcaption>
</figure>


<h2>Push to the Public Repository (You)</h2>

<p>Before populating this new repository with our existing
<code>my-git-repo</code> project, we first need to point our
<code>origin</code> remote to the Bitbucket repository. Be sure to change the
<code>&lt;username&gt;</code> portion to your actual Bitbucket username.</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">/path/to/my-git-repo</span>
<span class="k">git</span> <span class="k">remote</span> <span class="k">rm</span> <span class="n">origin</span>
<span class="k">git</span> <span class="k">remote</span> <span class="k">add</span> <span class="n">origin</span> <span class="n">https://&lt;username&gt;@bitbucket.org/&lt;username&gt;/my-git-repo.git</span>
</pre></div>


<p>The utility of remotes should be more apparent than in previous modules, as
typing the full path to this repository every time we needed to interact with
it would be rather tedious.</p>

<p>To populate the remote repository with our existing code, we can use the
same push mechanism as with a centralized workflow. When prompted for a
password, use the one that you signed up with.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">push</span> <span class="n">origin</span> <span class="n">master</span>
</pre></div>



<h2>Browse the Public Repository (You)</h2>

<p>We should now be able to see our project on the Bitbucket site. The
<em>Source</em> tab displays all of the files in the project, and the
<em>Commits</em> tab contains the entire commit history. Note that the branch
structure of the repository is also visualized to the left of each commit.

<figure>
	<img style='max-width: 555px' src='media/9-6.png' />
	<figcaption>Our history in Bitbucket&rsquo;s Commit tab</figcaption>
</figure>

<p>This repository now serves as the &ldquo;official&rdquo; copy of our example
website. We&rsquo;ll tell everyone else to download from this repository, and
we&rsquo;ll push all the changes from our local <code>my-git-repo</code> to it.
However, it&rsquo;s important to note that this &ldquo;official&rdquo; status
is merely a convention. As the <code>master</code> branch is just another
branch, our Bitbucket repository is just another repository according to Git.</p>

<p>Having both a public and a private repository for each developer makes it
easy to incorporate contributions from third-parties, even if you&rsquo;ve
never met them before.</p>


<h2>Clone the Repository (John)</h2>

<p>Next, we&rsquo;re going to pretend to be John, a third-party contributor to
our website. John noticed that we didn&rsquo;t have a pink page and, being the
friendly developer that he is, wants to create one for us. We&rsquo;d like to
let him contribute, but we don&rsquo;t want to give him push-access to our
entire repository&mdash;this would allow him to re-write or even delete all of
our hard work.</p>

<p>Fortunately, John knows how to exploit Bitbucket&rsquo;s collaboration
potential. He&rsquo;ll start by cloning a copy of our public repository:</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">/path/to/my-git-repo</span>
<span class="k">cd</span> <span class="n">..</span>
<span class="k">git</span> <span class="k">clone</span> <span class="n">http://bitbucket.org/&lt;username&gt;/my-git-repo.git</span> <span class="n">johns-repo</span>
<span class="k">cd</span> <span class="n">johns-repo</span>
</pre></div>


<p>You should now have another copy of our repository called
<code>johns-repo</code> in the same folder as <code>my-git-repo</code>. This is
John&rsquo;s <em>private</em> repository&mdash;a completely isolated
environment where he can safely develop the pink page. Let&rsquo;s quickly
configure his name and email:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">config</span> <span class="n">user.name</span> <span class="s">&quot;John&quot;</span>
<span class="k">git</span> <span class="k">config</span> <span class="n">user.email</span> <span class="n">john.example@rypress.com</span>
</pre></div>



<h2>Add the Pink Page (John)</h2>

<p>Of course, John should be developing his contributions in a dedicated
feature branch.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="na">-b</span> <span class="n">pink-page</span>
</pre></div>


<p>In addition to being a best practice, this makes it easy for the integrator
to see what commits to include. When John&rsquo;s done, he&rsquo;ll tell us
where to find his repository and what branch the new feature resides in. Then,
we&rsquo;ll be able merge his content with minimal effort.</p>

<p>Create the file <code>pink.html</code> and add the following code:</p>

<div class="highlight"><pre><span class="cp">&lt;!DOCTYPE html&gt;</span>
<span class="nt">&lt;html</span> <span class="na">lang=</span><span class="s">&quot;en&quot;</span><span class="nt">&gt;</span>
<span class="nt">&lt;head&gt;</span>
  <span class="nt">&lt;title&gt;</span>The Pink Page<span class="nt">&lt;/title&gt;</span>
  <span class="nt">&lt;link</span> <span class="na">rel=</span><span class="s">&quot;stylesheet&quot;</span> <span class="na">href=</span><span class="s">&quot;style.css&quot;</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;meta</span> <span class="na">charset=</span><span class="s">&quot;utf-8&quot;</span> <span class="nt">/&gt;</span>
<span class="nt">&lt;/head&gt;</span>
<span class="nt">&lt;body&gt;</span>
  <span class="nt">&lt;h1</span> <span class="na">style=</span><span class="s">&quot;color: #F0F&quot;</span><span class="nt">&gt;</span>The Pink Page<span class="nt">&lt;/h1&gt;</span>
  <span class="nt">&lt;p&gt;</span>Pink is <span class="nt">&lt;span</span> <span class="na">style=</span><span class="s">&quot;color: #F0F&quot;</span><span class="nt">&gt;</span>girly,
  flirty and fun<span class="nt">&lt;/span&gt;</span>!<span class="nt">&lt;/p&gt;</span>

  <span class="nt">&lt;p&gt;&lt;a</span> <span class="na">href=</span><span class="s">&quot;index.html&quot;</span><span class="nt">&gt;</span>Return to home page<span class="nt">&lt;/a&gt;&lt;/p&gt;</span>
<span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span>
</pre></div>


<p>Add the pink page to the &ldquo;Navigation&rdquo; section in
<code>index.html</code>:</p>

<div class="highlight"><pre><span class="nt">&lt;li</span> <span class="na">style=</span><span class="s">&quot;color: #F0F&quot;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">&quot;pink.html&quot;</span><span class="nt">&gt;</span>The Pink Page<span class="nt">&lt;/a&gt;</span>
<span class="nt">&lt;/li&gt;</span>
</pre></div>


<p>Then, stage and commit the snapshot as normal.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">add</span> <span class="n">pink.html</span> <span class="n">index.html</span>
<span class="k">git</span> <span class="k">status</span>
<span class="k">git</span> <span class="k">commit</span> <span class="na">-m</span> <span class="s">&quot;Add pink page&quot;</span>
</pre></div>



<h2>Publish the Pink Page (John)</h2>

<p>Now, John needs to publish his contributions to a public repository.
Remember that we don&rsquo;t want him to push to <em>our</em> public
repository, which is stored in his <code>origin</code> remote. In fact, he
<em>can&rsquo;t</em> push to <code>origin</code> for reasons we&rsquo;ll
discuss in a moment.</p>

<p>Instead, he&rsquo;ll create his own Bitbucket repository that we can pull
contributions from. In the real world, John would have his own Bitbucket
account, but for convenience, we&rsquo;ll just store his public repository
under our existing account. Once again, navigate to your Bitbucket home page
and click <em>Repositories&nbsp;&gt;&nbsp;Create&nbsp;repository</em> to create
John&rsquo;s public repository. For the <em>Name</em> field, use
<code>johns-repo</code>.</p>

<figure>
	<img style='max-width: 606px' src='media/9-7.png' />
	<figcaption>John&rsquo;s new repository form</figcaption>
</figure>

<p>Back in John&rsquo;s private repository, we&rsquo;ll need to add this as a
remote:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">remote</span> <span class="k">add</span> <span class="n">john-public</span> <span class="n">https://&lt;username&gt;@bitbucket.org/&lt;username&gt;/johns-repo.git</span>
</pre></div>


<p>This is where John will publish the pink page for us to access. Since
he&rsquo;s pushing with HTTPS, he&rsquo;ll need to enter the password for his
Bitbucket account (which is actually the password for <em>your</em>
account).</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">push</span> <span class="n">john-public</span> <span class="n">pink-page</span>
</pre></div>


<p>All John needs to do now is tell us the name of the feature branch and send
us a link to his repository, which will be:</p>

<div class="highlight"><pre>http://bitbucket.org/&lt;username&gt;/johns-repo.git
</pre></div>


<p>Note that John used a different path for pushing to his public repository
than the one he gave us for fetching from it. The most important distinction is
the transport protocol: the former used <code>https://</code> while the latter
used <code>http://</code>. Accessing a repository over HTTPS (or SSH) lets you
fetch or push, but as we saw, requires a password. This prevents unknown
developers from overwriting commits.</p>

<p>On the other hand, fetching over HTTP requires no username or password, but
pushing is not possible. This lets anyone fetch from a repository without
compromising its security. In the integrator workflow, other developers access
your repository via HTTP, while you publish changes via HTTPS. This is also the
reason why John can&rsquo;t push to his <code>origin</code> remote.</p>

<p>Of course, if you&rsquo;re working on a private project, anonymous HTTP
access would be disabled for that repository.</p>


<h2>View John&rsquo;s Contributions (You)</h2>

<p>Ok, we&rsquo;re done being John and we&rsquo;re ready to integrate his code
into the official project. Let&rsquo;s start by switching back into our
repository and adding John&rsquo;s public repository as a remote.</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">../my-git-repo</span>
<span class="k">git</span> <span class="k">remote</span> <span class="k">add</span> <span class="n">john</span> <span class="n">http://bitbucket.org/&lt;username&gt;/johns-repo.git</span>
</pre></div>


<p>Note that we don&rsquo;t care about anything in John&rsquo;s private
repository&mdash;the only thing that matters are his published changes.
Let&rsquo;s download his branches and take a look at what he&rsquo;s been
working on:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">fetch</span> <span class="n">john</span>
<span class="k">git</span> <span class="k">branch</span> <span class="na">-r</span>
<span class="k">git</span> <span class="k">log</span> <span class="n">master..john/pink-page</span> <span class="na">--stat</span>
</pre></div>


<p>We can visualize this history information as the following.</p>

<figure>
	<img style='max-width: 360px' src='media/9-8.png' />
	<figcaption>Before merging John&rsquo;s <code>pink-page</code>
branch</figcaption>
</figure>

<p>Let&rsquo;s take a look at his actual changes:</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">john/pink-page</span>
</pre></div>


<p>Open up the <code>pink.html</code> file to see if it&rsquo;s ok. Remember
that John isn&rsquo;t a trusted collaborator, and we would normally have no
idea what this file might contain. With that in mind, it&rsquo;s incredibly
important to verify its contents. <strong>Never blindly merge content from
a third-party contributor.</strong></p>


<h2>Integrate John&rsquo;s Contributions (You)</h2>

<p>Assuming we approve John&rsquo;s updates, we&rsquo;re now ready to merge it
into the project.</p>
	
<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">merge</span> <span class="n">john/pink-page</span>
</pre></div>


<p>Notice that is the exact same way we incorporated Mary&rsquo;s changes in
the centralized workflow, except now we&rsquo;re pulling from and pushing to
different locations:</p>

<figure>
	<img style='max-width: 430px' src='media/9-9.png' />
	<figcaption>The integrator workflow with John</figcaption>
</figure>

<p>Furthermore, John&rsquo;s workflow is just like ours: develop in a local,
private repository, then push changes to the public one. The integrator
workflow is merely a standardized way of organizing the collaboration
effort&mdash;nothing has changed about how we develop locally, and we&rsquo;re
using the same Git commands as we have been for the last few modules.</p>


<h2>Publish John&rsquo;s Contributions (You)</h2>

<p>We&rsquo;ve integrated John&rsquo;s contribution into our local
<code>my-git-repo</code> repository, but no one else knows what we&rsquo;ve
done. It&rsquo;s time to publish our <code>master</code> branch again.</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">push</span> <span class="n">origin</span> <span class="n">master</span>
</pre></div>


<p>Since we designated our public Bitbucket repository as the
&ldquo;official&rdquo; source for our project, everyone (i.e., Mary and John)
will now be able to synchronize with it.</p>


<h2>Update Mary&rsquo;s Repository (Mary)</h2>

<p>Mary should now be pulling changes from our Bitbucket repository instead of
the central one from the previous module. This should be fairly easy for her to
configure.</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">../marys-repo</span>
<span class="k">git</span> <span class="k">remote</span> <span class="k">rm</span> <span class="n">origin</span>
<span class="k">git</span> <span class="k">remote</span> <span class="k">add</span> <span class="n">origin</span> <span class="n">http://bitbucket.org/&lt;username&gt;/my-git-repo.git</span>
</pre></div>


<p>Again, remember to change <code>&lt;username&gt;</code> to your Bitbucket
account&rsquo;s username. For the sake of brevity, we&rsquo;ll do a blind merge
to add John&rsquo;s updates to Mary&rsquo;s repository (normally, Mary should
check what she&rsquo;s integrating before doing so).</p>

<div class="highlight"><pre><span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">fetch</span> <span class="n">origin</span>
<span class="k">git</span> <span class="k">rebase</span> <span class="n">origin/master</span>
</pre></div>


<p>For Mary, it doesn&rsquo;t really matter that the updates came from John.
All she has to know is that the &ldquo;official&rdquo; <code>master</code>
branch moved forward, prompting her to synchronize her private repository.</p>


<h2>Update John&rsquo;s Repository (John)</h2>

<p>John still needs to incorporate the pink page into his <code>master</code>
branch. He should <em>not</em> merge directly from his <code>pink-page</code>
topic branch because we could have edited his contribution before publishing it
or included other contributions along with it. Instead, he&rsquo;ll pull from
the &ldquo;official&rdquo; <code>master</code>:</p>

<div class="highlight"><pre><span class="k">cd</span> <span class="n">../johns-repo</span>
<span class="k">git</span> <span class="k">checkout</span> <span class="n">master</span>
<span class="k">git</span> <span class="k">fetch</span> <span class="n">origin</span>
<span class="k">git</span> <span class="k">rebase</span> <span class="n">origin/master</span>
</pre></div>


<p>If John had updated <code>master</code> directly from his local
<code>pink-page</code>, it could have wound up out-of-sync from the main
project. For this reason, the integrator workflow requires that everyone
<em>pull</em> from a single, official repository, while they all <em>push</em>
to their own public repositories:</p>

<figure>
	<img style='max-width: 510px' src='media/9-10.png' />
	<figcaption>The integrator workflow with many developers</figcaption>
</figure>

<p>In this way, additions from one contributor can be approved, integrated, and
made available to everyone without interrupting anyone&rsquo;s independent
developments.</p>


<h2>Conclusion</h2>

<p>Using the integrator workflow, our private development process largely
remains the same (develop a feature branch, merge it into <code>master</code>,
and publish it). But, we&rsquo;ve added an additional task: incorporating
changes from third-party contributors. Luckily, this doesn&rsquo;t require any
new skills&mdash;just access to a few more remote repositories.</p>

<p>While this setup forces us to keep track of more remotes, it also makes it
much, much easier to work with a large number of developers. You&rsquo;ll never
have to worry about security using an integrator workflow because you&rsquo;ll
still be the only one with access to the &ldquo;official&rdquo; repository.</p>

<p>There&rsquo;s also an interesting side-effect to this kind of security. By
giving each developer their own public repository, the integrator workflow
creates a more stable development environment for open-source software
projects. Should the lead developer stop maintaining the &ldquo;official&rdquo;
repository, any of the other participants could take over by simply designating
their public repository as the new &ldquo;official&rdquo; project. This is part
of what makes Git a <em>distributed</em> version control system: there is no
single central repository that Git forces everyone to rely upon.</p>

<figure>
	<img style='max-width: 510px' src='media/9-11.png' />
	<figcaption>John/Mary taking over project maintenance</figcaption>
</figure>

<p>In the next module, we&rsquo;ll take a look at an even more flexible way to share
commits. This low-level approach will also give us a better understanding of how
Git internally manages our content.</p>

<p class='sequential-nav'>
	<a href='patch-workflows.html'>Continue to <em>Patch Workflows</em>&nbsp;&rsaquo;</a>
</p>


</div> <!-- #content -->

<div style='clear: both'></div>

<footer id='footer'>
  <ul id='copyright'>
  	<li>
			&copy; 2012-2013 <a href='../../index.html'>RyPress.com</a>
		</li>
		<li>
			<a href='../../licensing.html'>All Rights Reserved</a>
		</li>
		<li>
			<a href='../../tos.html'>Terms of Service</a>
		</li>
  </ul>
</footer>

</div> <!-- #page -->


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37121774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>